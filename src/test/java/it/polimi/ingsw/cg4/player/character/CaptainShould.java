package it.polimi.ingsw.cg4.player.character;

import static org.junit.Assert.*;
import it.polimi.ingsw.cg4.player.Player;
import it.polimi.ingsw.cg4.rules.AttackRules;
import it.polimi.ingsw.cg4.rules.GameConfig;
import it.polimi.ingsw.cg4.rules.MovementRules;

import org.junit.BeforeClass;
import org.junit.Test;

/*
 * I test di unità per tutte le sottoclassi di HumanCharacter sono identiche, 
 * dato che nell'implementazione corrente non si hanno differenze tra gli umani.
 */
/**
 * It's used to test the Captain class.
 *
 */
public class CaptainShould {

    private static Player p = new Player( "Mario" );
    private static Character captain;

    /**
     * This initializer create a new character creator.
     */
    @BeforeClass
    public static void init() {
        CharacterCreator creator = new CharacterCreator();
        captain = creator.create( "Captain" );
        p.setCharacter( captain );
        p.incMovedSectors( 1 );
    }

    /**
     * This method tests the activation end deactivation of character status.
     */
    @Test
    public void statusTest() {
        assertEquals( MovementRules.getMovementRange( p ), 1 );
        captain.activateStatus( GameConfig.ADRENALINE_STATUS );
        assertEquals( MovementRules.getMovementRange( p ), 2 );
        captain.deactivateStatus( GameConfig.ADRENALINE_STATUS );
        assertEquals( MovementRules.getMovementRange( p ), 1 );
    }

    /**
     * It's used to test the method canAttack after the attack status.
     */
    @Test
    public void attackTest() {
        assertEquals( AttackRules.canAttack( p ), false );
        captain.activateStatus( GameConfig.ATTACK_STATUS );
        assertEquals( AttackRules.canAttack( p ), true );
        captain.deactivateStatus( GameConfig.ATTACK_STATUS );
        assertEquals( AttackRules.canAttack( p ), false );

    }
    
    /**
     * Tests the name getter method.
     */
    @Test
    public void nameTest() {
        assertEquals( captain.getName(), "Captain" );

    }

    /**
     * Tests the race getter method.
     */
    @Test
    public void raceTest() {
        assertEquals( captain.getRace(), GameConfig.HUMAN_RACE );
    }

    /**
     * It's used to test toString method.
     */
    @Test
    public void toStringTest() {
        assertEquals( captain.toString(), captain.getName() + "["
                + captain.getRace().toString() + "]" );
    }
}
