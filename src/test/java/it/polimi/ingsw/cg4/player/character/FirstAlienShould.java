package it.polimi.ingsw.cg4.player.character;

import static org.junit.Assert.*;
import it.polimi.ingsw.cg4.player.Player;
import it.polimi.ingsw.cg4.rules.AttackRules;
import it.polimi.ingsw.cg4.rules.GameConfig;
import it.polimi.ingsw.cg4.rules.MovementRules;

import org.junit.BeforeClass;
import org.junit.Test;

/*
 * I test di unità per tutte le sottoclassi di AlienCharacter sono identiche,
 * dato che nell'implementazione corrente non si hanno differenze tra gli alieni.
 */

/**
 * It's used to test the FirstAlien class.
 *
 */
public class FirstAlienShould {

    private static Player p = new Player( "Mario" );
    private static Character firstAlien;

    /**
     * This initializer create a new character creator.
     */
    @BeforeClass
    public static void init() {
        CharacterCreator creator = new CharacterCreator();
        firstAlien = creator.create( "FirstAlien" );
        p.setCharacter( firstAlien );
        p.incMovedSectors( 1 );
    }

    /**
     * This method tests the activation end deactivation of voracious character status.
     */
    @Test
    public void statusTest() {
        assertEquals( MovementRules.getMovementRange( p ), 2 );
        firstAlien.activateStatus( GameConfig.VORACIOUS_STATUS );
        assertEquals( MovementRules.getMovementRange( p ), 3 );
        firstAlien.deactivateStatus( GameConfig.VORACIOUS_STATUS );
        assertEquals( MovementRules.getMovementRange( p ), 2 );
    }
    
    /**
     * It's used to test the method canAttack..
     */
    @Test
    public void attackTest() {
        assertEquals( AttackRules.canAttack( p ), true );
    }
    
    /**
     * Tests the name getter method.
     */
    @Test
    public void nameTest() {
        assertEquals( firstAlien.getName(), "First Alien" );
    }

    /**
     * Tests the race getter method.
     */
    @Test
    public void raceTest() {
        assertEquals( firstAlien.getRace(), GameConfig.ALIEN_RACE );
    }

    /**
     * It's used to test toString method.
     */
    @Test
    public void toStringTest() {
        assertEquals( firstAlien.toString(), firstAlien.getName() + "["
                + firstAlien.getRace().toString() + "]" );
    }

}
