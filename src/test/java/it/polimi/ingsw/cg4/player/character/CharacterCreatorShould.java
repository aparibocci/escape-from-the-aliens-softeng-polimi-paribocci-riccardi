package it.polimi.ingsw.cg4.player.character;

import static org.junit.Assert.*;

import java.util.List;

import it.polimi.ingsw.cg4.player.character.Captain;
import it.polimi.ingsw.cg4.player.character.Character;
import it.polimi.ingsw.cg4.player.character.CharacterCreator;
import it.polimi.ingsw.cg4.player.character.FirstAlien;
import it.polimi.ingsw.cg4.player.character.FourthAlien;
import it.polimi.ingsw.cg4.player.character.Pilot;
import it.polimi.ingsw.cg4.player.character.Psychologist;
import it.polimi.ingsw.cg4.player.character.SecondAlien;
import it.polimi.ingsw.cg4.player.character.Soldier;
import it.polimi.ingsw.cg4.player.character.ThirdAlien;
import org.junit.BeforeClass;
import org.junit.Test;
/**
 * It's used to test the character creator.
 *
 */
public class CharacterCreatorShould {

    private static CharacterCreator characterCreator;

    /**
     * This initializer create a new character creator.
     */
    @BeforeClass
    public static void init() {
        characterCreator = new CharacterCreator();

    }

    /**
     * This method tests the character creation.
     * In this test all character are created.
     */
    @Test
    public void creationTest() {
        
        assertTrue( characterCreator.create( "Captain" ) instanceof Captain );
        assertTrue( characterCreator.create( "Pilot" ) instanceof Pilot );
        assertTrue( characterCreator.create( "Psychologist" ) instanceof Psychologist );
        assertTrue( characterCreator.create( "Soldier" ) instanceof Soldier );
        assertTrue( characterCreator.create( "FirstAlien" ) instanceof FirstAlien );
        assertTrue( characterCreator.create( "SecondAlien" ) instanceof SecondAlien );
        assertTrue( characterCreator.create( "ThirdAlien" ) instanceof ThirdAlien );
        assertTrue( characterCreator.create( "FourthAlien" ) instanceof FourthAlien );

    }

    /**
     * To be used to test the throw of invalid character name exception.
     * Invalid character name are passed to creator.
     */
    @Test( expected = Exception.class )
    public void invalidNameTest() {
        characterCreator.create( "Pilots" );
    }

    /**
     * This method tests the aliens list getter.
     */
    @Test
    public void getAlienList() {
        List< Character > aliens = characterCreator.getAliensList();
        assertTrue( aliens.get( 0 ) instanceof FirstAlien );
        assertTrue( aliens.get( 1 ) instanceof SecondAlien );
        assertTrue( aliens.get( 2 ) instanceof ThirdAlien );
        assertTrue( aliens.get( 3 ) instanceof FourthAlien );

    }

    /**
     * This method tests the humans list getter.
     */
    @Test
    public void getHumansList() {
        List< Character > humans = characterCreator.getHumansList();
        assertTrue( humans.get( 0 ) instanceof Captain );
        assertTrue( humans.get( 1 ) instanceof Pilot );
        assertTrue( humans.get( 2 ) instanceof Psychologist );
        assertTrue( humans.get( 3 ) instanceof Soldier );

    }

}
