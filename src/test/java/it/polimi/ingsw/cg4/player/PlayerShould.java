package it.polimi.ingsw.cg4.player;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg4.board.Coordinate;
import it.polimi.ingsw.cg4.card.CardCreator;
import it.polimi.ingsw.cg4.card.ItemCard;
import it.polimi.ingsw.cg4.player.Player;

import it.polimi.ingsw.cg4.player.character.CharacterCreator;
import it.polimi.ingsw.cg4.rules.GameConfig;

import org.junit.BeforeClass;
import org.junit.Test;
/**
 * To be used to test player class.
 *
 */
public class PlayerShould {

    private static Player player;
    private static boolean hasAttacked;

    /**
     * This initializer create a new player.
     */
    @BeforeClass
    public static void init() {
        player = new Player( "Mario" );
        hasAttacked = false;
    }

    /**
     * To be used to test the name getter.
     */
    @Test
    public void infoTest() {
        assertEquals( player.getName(), "Mario" );
        CharacterCreator creator = new CharacterCreator();
        player.setCharacter( creator.create( "FirstAlien" ) );
        assertEquals( player.getCharacter().getName(), creator.create( "FirstAlien" ).getName() );
    }

    /**
     * Given an amount this method increases the score.
     */
    @Test
    public void scoreTest() {
        assertEquals( player.getScore(), 0 );
        player.raiseScore( 1000 );
        assertEquals( player.getScore(), 1000 );
        player.raiseScore( 1000 );
        assertEquals( player.getScore(), 2000 );
    }

    /**
     * Used to test the player hand size.
     * Three is the max hand size.
     */
    @Test
    public void itemsTest() {
        assertEquals( player.getHandSize(), 0 );
        CardCreator creator = new CardCreator();
        ItemCard item1 = (ItemCard) creator.create( "Attack" );
        ItemCard item2 = (ItemCard) creator.create( "Adrenaline" );
        ItemCard item3 = (ItemCard) creator.create( "Sedatives" );
        player.addItem( item1 );
        assertEquals( player.getHandSize(), 1 );
        player.addItem( item2 );
        player.addItem( item3 );
        assertEquals( player.getHandSize(), 3 );
        assertEquals( player.getItemIndex( item2 ), 1 );
        player.removeItem( 0 );
        assertEquals( player.getHandSize(), 2 );
        assertEquals( player.getItemIndex( item1 ), -1 );
        assertEquals( player.getItemIndex( item3 ), 1 );
        assertEquals( player.getItem( 1 ), item3 );
    }

    /**
     * This method tests the player state in room and game context.
     */
    @Test
    public void stateTest() {
        assertEquals( player.getState(), GameConfig.NOT_CONNECTED_STATE );
        player.setState( GameConfig.LOGGED_IN_STATE );
        assertEquals( player.getState(), GameConfig.LOGGED_IN_STATE );
        player.setState(GameConfig.CHAR_ASSIGNED_STATE);
        assertEquals(player.getState(), GameConfig.CHAR_ASSIGNED_STATE);
        player.setState( GameConfig.IN_GAME_STATE );
        assertEquals(player.getState(), GameConfig.IN_GAME_STATE);
        player.setState(GameConfig.LOSER_STATE);
        assertEquals(player.getState(), GameConfig.LOSER_STATE);
        player.setState(GameConfig.WINNER_STATE);
        assertEquals(player.getState(), GameConfig.WINNER_STATE);
        
        
    }

    /**
     * To be used to test the throw of invalid hand over flow exception.
     * Four items are added at hand size.
     */
    @Test( expected = Exception.class )
    public void handOverflowTest() {
        CardCreator cardCreator = new CardCreator();
        ItemCard item1 = (ItemCard) cardCreator.create( "Adrenaline" );
        ItemCard item2 = (ItemCard) cardCreator.create( "Attack" );
        ItemCard item3 = (ItemCard) cardCreator.create( "Adrenaline" );
        ItemCard item4 = (ItemCard) cardCreator.create( "Sedatives" );
        player.addItem( item1 );
        player.addItem( item2 );
        player.addItem( item3 );
        player.addItem( item4 );

    }

    /**
     * It's used to test the increment of player moved sector.
     */
    @Test
    public void incMovedSectors() {
        int i = player.getMovedSectors();
        player.incMovedSectors( 5 );
        assertEquals( player.getMovedSectors(), i + 5 );
        player.incMovedSectors(0);
        assertEquals( player.getMovedSectors(), i + 5 );
        player.incMovedSectors( -1 );
        assertEquals( player.getMovedSectors(), i + 4 ); 
    }
    /**
     * It's used to test the method hasAttacked.
     */
    @Test
    public void testHasAttacked(){
        
        player.setHasAttacked( hasAttacked );
        assertEquals( player.hasAttacked(), false );
        hasAttacked = true;
        player.setHasAttacked( hasAttacked );
        assertEquals( player.hasAttacked(), true );
        
    }

    /**
     * This method test the adding of a new coordinate to the list.
     */
    @Test
    public void CoordHistoryTest() {
        List< Coordinate > coordHistory = new ArrayList< Coordinate >();
        coordHistory.add( Coordinate.fromCartesian( 2, 2 ) );
        coordHistory.add( Coordinate.fromCartesian( 3, 2 ) );
        coordHistory.add( Coordinate.fromCube( 0, 0, 0 ) );
        coordHistory.add( Coordinate.fromCartesian( 2, 2 ));
        player.addCoord( Coordinate.fromCartesian( 2, 2 ) );
        player.addCoord( Coordinate.fromCartesian( 3, 2 ) );
        player.addCoord( Coordinate.fromCube( 0, 0, 0 ));
        player.addCoord( Coordinate.fromCartesian( 2, 2 ) );
        assertEquals( player.getCoordHistory(), coordHistory );
        

    }
    
    /**
     * To be used to test the throw of invalid item index exception.
     * Invalid item index are passed to the item getter.
     */
    @Test( expected = Exception.class )
    public void invalidItemIndexTest() {
        player.getItem( 9 );
        player.getItem( player.getHandSize() );
        player.getItem( -2 );
    }

}
