package it.polimi.ingsw.cg4.rules;

import static org.junit.Assert.*;
import it.polimi.ingsw.cg4.player.Player;
import it.polimi.ingsw.cg4.player.character.CharacterCreator;
import it.polimi.ingsw.cg4.player.character.CharacterRace;

import org.junit.BeforeClass;
import org.junit.Test;
/**
 * It's used to test the attack rules.
 *
 */

public class AttackRulesShould {

    public static final CharacterRace ATTACK_RACE = GameConfig.ALIEN_RACE;
    private static Player alien;
    private static Player human;

    /**
     * This initializer creates two different player.
     */
    @BeforeClass
    public static void init() {
        CharacterCreator creator = new CharacterCreator();
    
        human = new Player( "Carlo" );
        alien = new Player( "Giovanni" );
        human.setCharacter( creator.create( "Pilot" ) );
        alien.setCharacter( creator.create( "FirstAlien" ) );
      
    }

    
    /**
     * This method tests the canAttack method and the status necessary 
     * to the attack action.
     */
    @Test
    public void canAttackTest() {
        assertEquals(AttackRules.canAttack( alien ), false);
        alien.incMovedSectors( 1 );
        assertEquals(AttackRules.canAttack( alien ), true);
        alien.setHasAttacked( true );
        assertEquals(AttackRules.canAttack( alien ), false);

        assertEquals(AttackRules.canAttack( human ), false);
        human.incMovedSectors( 1 );
        assertEquals(AttackRules.canAttack( human ), false);
        human.getCharacter().activateStatus( GameConfig.ATTACK_STATUS);
        assertEquals(AttackRules.canAttack( human ), true);
        
    }
    
    /**
     * This method tests the canDefende method and the status necessary 
     * to the defense action.
     */
    @Test
    public void canDefendeTest(){
        assertEquals(  AttackRules.canDefend( alien ), false );
        assertEquals(  AttackRules.canDefend( human ), false );
        human.getCharacter().activateStatus( GameConfig.DEFENSE_STATUS );
        alien.getCharacter().activateStatus( GameConfig.DEFENSE_STATUS );
        assertEquals(  AttackRules.canDefend( alien ), false );
        assertEquals(  AttackRules.canDefend( human ), true );
    }
    
    /**
     * It's used to test the canBecomeVoraciusTest.
     */
    @Test 
    public void canBecomeVoraciusTest(){
        assertEquals(  AttackRules.canBecomeVoracious( alien , human), true );
        assertEquals(  AttackRules.canBecomeVoracious( alien , alien), false );
        assertEquals(  AttackRules.canBecomeVoracious( human , human), false );
        assertEquals(  AttackRules.canBecomeVoracious( human , alien), false );
        
    }
    
    

}
