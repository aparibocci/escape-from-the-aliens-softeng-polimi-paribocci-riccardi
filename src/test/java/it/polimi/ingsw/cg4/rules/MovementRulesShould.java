package it.polimi.ingsw.cg4.rules;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import it.polimi.ingsw.cg4.board.Board;
import it.polimi.ingsw.cg4.board.BoardCreator;
import it.polimi.ingsw.cg4.board.Coordinate;
import it.polimi.ingsw.cg4.board.SectorType;
import it.polimi.ingsw.cg4.player.Player;
import it.polimi.ingsw.cg4.player.character.CharacterCreator;
import it.polimi.ingsw.cg4.player.character.CharacterRace;
import it.polimi.ingsw.cg4.player.character.CharacterStatus;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * It's used to test the movement rules.
 *
 */
public class MovementRulesShould {
    
    private static Player human;
    private static Player alien;
    private static BoardCreator boardCreator = new BoardCreator();
    private static Board board = boardCreator.create( "Galilei" );

    /**
     * This method specifies  the race movement rules.
     */
    public static final Map< CharacterRace, Set< SectorType > > RACE_MOVEMENT_RULES;
    static {
        RACE_MOVEMENT_RULES = new HashMap< CharacterRace, Set< SectorType > >();
        RACE_MOVEMENT_RULES.put( GameConfig.ALIEN_RACE, new HashSet< SectorType >( Arrays.asList(
                GameConfig.SECURE_SECTOR, GameConfig.DANGEROUS_SECTOR
        ) ) );
        RACE_MOVEMENT_RULES.put( GameConfig.HUMAN_RACE, new HashSet< SectorType >( Arrays.asList(
                GameConfig.SECURE_SECTOR, GameConfig.DANGEROUS_SECTOR, GameConfig.OPEN_HATCH_SECTOR
        ) ) );
    }
    
    /**
     * This method specifies the base movement range.
     */
    public static final Map< CharacterRace, Integer > BASE_MOVEMENT_RANGE;
    static {
        BASE_MOVEMENT_RANGE = new HashMap< CharacterRace, Integer >();
        BASE_MOVEMENT_RANGE.put( GameConfig.ALIEN_RACE, 2 );
        BASE_MOVEMENT_RANGE.put( GameConfig.HUMAN_RACE, 1 );
    }
    
    /**
     * This method specifies the movement bonus amount.
     */
    public static final Map< CharacterStatus, Integer > MOVEMENT_BONUS_AMOUNT;
    static {
        MOVEMENT_BONUS_AMOUNT = new HashMap< CharacterStatus, Integer >();
        MOVEMENT_BONUS_AMOUNT.put( GameConfig.ADRENALINE_STATUS, 1 );
        MOVEMENT_BONUS_AMOUNT.put( GameConfig.VORACIOUS_STATUS, 1 );
    }
    
    /**
     * This initializer creates two players, sets their character
     * and their position on the board.
     */
    @BeforeClass
    public static void init() {
        CharacterCreator creator = new CharacterCreator();
        human = new Player( "Carlo" );
        alien = new Player( "Giovanni" );
        human.setCharacter( creator.create( "Pilot" ) );
        alien.setCharacter( creator.create( "FirstAlien" ) );
        board.addPlayer( human , board.getHumanBase().getCoordinate() );
        board.addPlayer( alien, board.getAlienBase().getCoordinate() );
        
    }

    /**
     * It's used to test the canMoveTo method, using refresh reacheable 
     * sector algorithm.
     */
    @Test
    public void canMoveToTest() {
        human.refreshReachableSectors();
        assertEquals( human.canMoveTo( board.getSector( Coordinate.fromCartesian( 8 ,11 )) ), true );
        alien.refreshReachableSectors();
        assertEquals( alien.canMoveTo( board.getSector( Coordinate.fromCartesian( 4, 11 ) ) ), true );
        assertEquals( alien.canMoveTo( board.getSector( Coordinate.fromCartesian( 3, 11 ) ) ), true );
    }
    
    /**
     * This method tests the race valid movements in specific sector. 
     */
    @Test
    public void canAccessTo(){
        alien.refreshReachableSectors();
        assertEquals(MovementRules.canAccessTo( alien.getCharacter().getRace(), GameConfig.HUMAN_SECTOR ), false);
        assertEquals(MovementRules.canAccessTo( alien.getCharacter().getRace(), GameConfig.CLOSED_HATCH_SECTOR), false);
        assertEquals(MovementRules.canAccessTo( alien.getCharacter().getRace(), GameConfig.VOID_SECTOR ), false);
        assertEquals(MovementRules.canAccessTo( alien.getCharacter().getRace(), GameConfig.DANGEROUS_SECTOR ), true);
        assertEquals(MovementRules.canAccessTo( alien.getCharacter().getRace(), GameConfig.OPEN_HATCH_SECTOR ), false);
        assertEquals(MovementRules.canAccessTo( human.getCharacter().getRace(), GameConfig.HUMAN_SECTOR ), false);
        assertEquals(MovementRules.canAccessTo( human.getCharacter().getRace(), GameConfig.CLOSED_HATCH_SECTOR), false);
        assertEquals(MovementRules.canAccessTo( human.getCharacter().getRace(), GameConfig.VOID_SECTOR ), false);
        assertEquals(MovementRules.canAccessTo( human.getCharacter().getRace(), GameConfig.DANGEROUS_SECTOR ), true);
        assertEquals(MovementRules.canAccessTo( human.getCharacter().getRace(), GameConfig.OPEN_HATCH_SECTOR ), true);
        
    }
    
    /**
     * It's used to test the movement range getter.
     */
    @Test 
    public void getMovementRangeTest(){
       assertEquals( MovementRules.getMovementRange( human ), 1 );
       human.getCharacter().activateStatus( GameConfig.ADRENALINE_STATUS );
       assertEquals( MovementRules.getMovementRange( human ), 2 );
       human.getCharacter().deactivateStatus( GameConfig.ADRENALINE_STATUS );
       assertEquals( MovementRules.getMovementRange( human ), 1 );
       assertEquals( MovementRules.getMovementRange( alien ), 2 );
       alien.getCharacter().activateStatus( GameConfig.VORACIOUS_STATUS );
       assertEquals( MovementRules.getMovementRange( alien ), 3 );
       alien.getCharacter().deactivateStatus( GameConfig.VORACIOUS_STATUS );
       assertEquals( MovementRules.getMovementRange( alien), 2 );
       
       
    }

}
