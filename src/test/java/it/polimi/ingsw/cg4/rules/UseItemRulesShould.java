package it.polimi.ingsw.cg4.rules;

import static org.junit.Assert.*;

import it.polimi.ingsw.cg4.card.CardCreator;
import it.polimi.ingsw.cg4.card.ItemCard;
import it.polimi.ingsw.cg4.player.Player;
import it.polimi.ingsw.cg4.player.character.CharacterCreator;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * It's used to test the use item rules.
 *
 */
public class UseItemRulesShould {
    private static Player human;
    private static Player alien;
    
    /**
     * This method creates two players and sets their characters.
     */
    @BeforeClass
    public static void init() {
        CharacterCreator creator = new CharacterCreator();
        human = new Player( "Carlo" );
        alien = new Player( "Giovanni" );
        human.setCharacter( creator.create( "Pilot" ) );
        alien.setCharacter( creator.create( "FirstAlien" ) );
    }

    /**
     * This method tests the races that can use item.
     */
    @Test
    public void canUseItemTest() {
        CardCreator creator = new CardCreator(); 
        assertEquals( UseItemRules.canUseItem( human, (ItemCard) creator.create( "Defense" ) ), false );
        assertEquals( UseItemRules.canUseItem( human, (ItemCard) creator.create( "Attack" ) ), true );
        assertEquals( UseItemRules.canUseItem( human, (ItemCard) creator.create( "Sedatives") ), true );
        assertEquals( UseItemRules.canUseItem( alien, (ItemCard) creator.create( "Sedatives") ), false );
        assertEquals( UseItemRules.canUseItem( alien, (ItemCard) creator.create( "Sedatives") ), false );
        
    }

}
