package it.polimi.ingsw.cg4.rules;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg4.player.Player;
import it.polimi.ingsw.cg4.player.character.CharacterCreator;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * It's used to test the end turn rules.
 *
 */
public class EndTurnRulesShould {

    private static Player human1,human2,alien1, alien2;
    private static List <Player> players;
    private static CharacterCreator creator;
   
    /**
     * This initializer creates four player and sets their state
     * and character.
     */
    @BeforeClass
    public static void init() {
        human1 = new Player( "Carlo" );
        human2 = new Player( "Giovanni" );
        alien1 = new Player( "Alessia" );
        alien2 = new Player( "Marco" );
        players = new ArrayList<Player>();
        players.add( human1 );
        players.add( human2 );
        players.add( alien1 );
        players.add( alien2 );
        creator = new CharacterCreator();
        human1.setCharacter( creator.create( "Pilot" ));
        human1.setState( GameConfig.IN_GAME_STATE );
        human2.setCharacter( creator.create( "Soldier" ));
        human2.setState( GameConfig.IN_GAME_STATE );
        alien1.setCharacter( creator.create( "FirstAlien" ));
        alien1.setState( GameConfig.IN_GAME_STATE );
        alien2.setCharacter( creator.create( "SecondAlien" ));
        alien2.setState( GameConfig.IN_GAME_STATE );
    }

    /**
     * This method tests the possibility to end the turn.
     */
    @Test
    public void canEndTurnTest() {
         assertEquals( EndTurnRules.canEndTurn( human1 ), false );
         human1.incMovedSectors( 1 );
         assertEquals( EndTurnRules.canEndTurn( human1 ), true );
    }
    
    /**
     * This method removes the player to the list in order to test the 
     * no alien or no human left.
     */
    @Test
    public void noAlienHumanLeftTest(){
        assertEquals( EndGameRules.haveHumansWonAfterAttack( players ), false );
        assertEquals( EndGameRules.haveAlienLostAfterEscape( players ), false );
        players.remove( alien1 );
        alien1.setState( GameConfig.LOSER_STATE );
        players.remove( alien2 );
        alien1.setState( GameConfig.LOSER_STATE );
        assertEquals( EndGameRules.haveHumansWonAfterAttack( players ), true );
        players.add( alien1 );
        alien1.setState( GameConfig.IN_GAME_STATE );
        assertEquals( EndGameRules.haveAliensWonAfterAttack( players ), false );
        players.remove( human1 );
        human1.setState( GameConfig.LOSER_STATE );
        players.remove( human2 );
        human2.setState( GameConfig.LOSER_STATE );
        assertEquals( EndGameRules.haveAliensWonAfterAttack( players ), true );
        assertEquals( EndGameRules.haveAlienLostAfterEscape( players ), true );
        
        
        
    }

}
