package it.polimi.ingsw.cg4.networking.server;

import static org.junit.Assert.*;
import it.polimi.ingsw.cg4.networking.server.Room;
import it.polimi.ingsw.cg4.networking.server.User;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * It's used to test the end turn action.
 *
 */
public class EndTurnActionShould {

    private static User user1;
    private static User user2;
    private static Room room;
    
    /**
     * This initializer creates four users and adds them in their rooms.
     */
    @BeforeClass
    public static void init(){
        user1 = new User( "Mario" );
        user1.setFlagTest( true );
        user2 = new User("Federico");
        user2.setFlagTest( true );
        room = new Room("polimi",user1);
        room.addUserTest(user2);
        room.startGame( "Galilei" );
    }

    /**
     * This method tests the end of the game.
     * The game end at fortieth turn.
     */
    @Test
    public void EndGameMaxTurnTest() {
        new GameActionEndTurn(room.getGame().getCurrentPlayer().getUser()).execute();
        for(int i = 0; i < 50; i++ )
            room.getGame().nextTurn();
        assertEquals( room.getGame().hasEnded(), true );
        new GameActionEndTurn(room.getGame().getCurrentPlayer().getUser()).execute();
    }

}
