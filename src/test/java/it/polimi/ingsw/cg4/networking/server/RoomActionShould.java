package it.polimi.ingsw.cg4.networking.server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import it.polimi.ingsw.cg4.networking.server.Room;
import it.polimi.ingsw.cg4.networking.server.User;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * It's used to test the action in the room context.
 *
 */
public class RoomActionShould {

    private static User user1;
    private static User user2;
    private static Room room;
    
    
    private static final Map< String, String > ROOM_COMMANDS = new HashMap< String, String >();
    static {
        ROOM_COMMANDS.put( "chat", "chat <text> - sends a new message to everyone with <text> as content." );
        ROOM_COMMANDS.put( "help", "help - shows the valid commands." );
        ROOM_COMMANDS.put( "logout", "logout - ends the current session and disconnects from server." );
        ROOM_COMMANDS.put( "start", "start <map_name> - [ADMIN ONLY] starts a new game with map <map_name> [ Fermi | Galilei | Galvani ]." );
        ROOM_COMMANDS.put( "users", "users - displays the room user list." );
    }
    
    /**
     * This initializer creates 2 users and adds them in their room. 
     * 
     */
    @BeforeClass
    public static void init() {
        
        user1 = new User("Vincy");
        user1.setFlagTest( true );
        user2 = new User("Amos");
        user2.setFlagTest( true );
        room = new Room("polimi",user1);
        room.addUserTest(user2);
    }

    /**
     * This method tests the room action.
     * First the user starts the game and uses chat and logout command.
     */
    @Test
    public void ActionTest() {
        new RoomActionChat( user1, "Iniziamo la partita" ).execute();
        new RoomActionHelp( user1, new ArrayList< String >( ROOM_COMMANDS.values() )).execute();
        new RoomActionLogout( user1 );
        new RoomActionStart( user1, "Fermi" ).execute();
        new RoomActionUsers( user1 ).execute();
    }

}
