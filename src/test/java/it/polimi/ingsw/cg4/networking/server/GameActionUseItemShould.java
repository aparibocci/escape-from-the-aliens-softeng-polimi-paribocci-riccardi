package it.polimi.ingsw.cg4.networking.server;

import static org.junit.Assert.*;
import it.polimi.ingsw.cg4.board.Sector;
import it.polimi.ingsw.cg4.card.CardCreator;
import it.polimi.ingsw.cg4.card.ItemCard;
import it.polimi.ingsw.cg4.networking.server.Room;
import it.polimi.ingsw.cg4.networking.server.User;
import it.polimi.ingsw.cg4.player.Player;
import it.polimi.ingsw.cg4.rules.AttackRules;
import it.polimi.ingsw.cg4.rules.GameConfig;
import it.polimi.ingsw.cg4.rules.MovementRules;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * It's used to test the use item game action.
 *
 */
public class GameActionUseItemShould {
    private static User user1;
    private static User user2;
    private static Room room;
    private Player human,alien;

    /**
     * This initializer creates two users and adds them in their rooms.
     */
    @BeforeClass
    public static void init(){
        user1 = new User("Vincy");
        user1.setFlagTest( true );
        user2 = new User("Amos");
        user2.setFlagTest( true );
        room = new Room("polimi",user1);
        room.addUserTest(user2);
        room.startGame( "Galilei" );
    }

    /**
     * This method tests the adrenaline item.
     * In this test the player increases his movement range 
     * after the activation of adrenline status.
     * 
     */
    @Test
    public void useItemAdrenalineTest(){
        CardCreator creator = new CardCreator();
        ItemCard adrenaline = (ItemCard) creator.create( "Adrenaline" );
        if ( room.getGame().getCurrentPlayer().getCharacter().getRace().equals( GameConfig.ALIEN_RACE ) ){
            room.getGame().getCurrentPlayer().resetMovedSectors();
            room.getGame().nextPlayer();
            room.getGame().getCurrentPlayer().refreshReachableSectors();
        }
        human = room.getGame().getCurrentPlayer();
        human.addItem( adrenaline );
        assertTrue( MovementRules.getMovementRange( human ) == 1);
        new GameActionUseItem( human.getUser(), human.getItemIndex( adrenaline )).execute();
        assertTrue( MovementRules.getMovementRange( human ) == 2);
    }
    
    /**
     * This method tests the attack item.
     * In this test the player can attack
     * after the activation of attack status.
     * 
     */
    @Test
    public void useItemAttackTest(){
        CardCreator creator = new CardCreator();
        ItemCard attack = (ItemCard) creator.create( "Attack" );
        if ( room.getGame().getCurrentPlayer().getCharacter().getRace().equals( GameConfig.ALIEN_RACE ) ){
            room.getGame().getCurrentPlayer().resetMovedSectors();
            room.getGame().nextPlayer();
            room.getGame().getCurrentPlayer().refreshReachableSectors();
        }
        human = room.getGame().getCurrentPlayer();
        human.addItem( attack );
        assertTrue( !AttackRules.canAttack( human ));
        human.incMovedSectors( 1 );
        new GameActionUseItem( human.getUser(), human.getItemIndex( attack )).execute();
        assertTrue( AttackRules.canAttack( human ));
        
    }
    
    /**
     * This method tests the sedatives item.
     * In this test the player not draw
     * after the activation of sedative status.
     * 
     */
    @Test
    public void useItemSedativeTest(){

        CardCreator creator = new CardCreator();
        ItemCard sedatives = (ItemCard) creator.create( "Sedatives" );
        playerMovement("UR","DR");
        playerMovement("DR","UR");
       
        if ( room.getGame().getCurrentPlayer().getCharacter().getRace().equals( GameConfig.ALIEN_RACE ) ){
            room.getGame().getCurrentPlayer().resetMovedSectors();
            room.getGame().nextPlayer();
            room.getGame().getCurrentPlayer().refreshReachableSectors();
        }
        human.getUser().setDrawTest( false );
        human.addItem( sedatives );
        new GameActionUseItem( human.getUser(), human.getItemIndex( sedatives )).execute();
        playerMovement("DR","UR");
        assertTrue(!human.getUser().isDrawTest());
    }
    
    /**
     * This method tests the teleport item.
     * In this test the player teleport to his base 
     * after the activation of teleport status.
     * 
     */
    @Test
    public void useItemTeleportTest(){
        CardCreator creator = new CardCreator();
        ItemCard teleport = (ItemCard) creator.create( "Teleport" );
        playerMovement("UR","DR");
        if ( room.getGame().getCurrentPlayer().getCharacter().getRace().equals( GameConfig.ALIEN_RACE ) ){
            room.getGame().getCurrentPlayer().resetMovedSectors();
            room.getGame().nextPlayer();
            room.getGame().getCurrentPlayer().refreshReachableSectors();
        }
        assertTrue( !human.getSector().equals( human.getBoard().getHumanBase() ));
        human.addItem( teleport );
        new GameActionUseItem( human.getUser(), human.getItemIndex( teleport )).execute();
        assertTrue( human.getSector().equals( human.getBoard().getHumanBase() ));
        
    }
    
    /**
     * This method tests the spotlight item.
     * In this test the player is notified about the other 
     * player's position.
     * 
     */
    @Test
    public void useItemSpotlightTest(){
        CardCreator creator = new CardCreator();
        ItemCard spotlight = (ItemCard) creator.create( "Spotlight" );
        playerMovement("UR","DR");
        if ( room.getGame().getCurrentPlayer().getCharacter().getRace().equals( GameConfig.ALIEN_RACE ) ){
            room.getGame().getCurrentPlayer().resetMovedSectors();
            room.getGame().nextPlayer();
            room.getGame().getCurrentPlayer().refreshReachableSectors();
        }
        human.addItem( spotlight );
        new GameActionUseItem( human.getUser(), human.getItemIndex( spotlight )).execute();
        
    }
    
    
    private Sector currentPlayerMoveTo( String direction ){
        Player player = room.getGame().getCurrentPlayer();
        Sector sector = room.getGame().getState().getBoard().getSector( player.getSector().getCoordinate().getNeighbor( direction ) );
        return sector;
    }
    
    private void playerMovement( String alienDest, String humanDest){
        
        if(room.getGame().getCurrentPlayer().getCharacter().getRace().equals( GameConfig.HUMAN_RACE )){
            if ( human == null )
                human = room.getGame().getCurrentPlayer();
            new GameActionMove(human.getUser(),currentPlayerMoveTo(humanDest)).execute();
            new GameActionEndTurn(human.getUser()).execute();
            if ( alien == null )
                alien = room.getGame().getCurrentPlayer();
            new GameActionMove(alien.getUser(),currentPlayerMoveTo(alienDest)).execute();
            if ( ! alien.getState().equals( GameConfig.IN_GAME_STATE ) )
                return;
            new GameActionEndTurn(alien.getUser()).execute();
            
        }
        else{
            if ( alien == null )
                alien = room.getGame().getCurrentPlayer();
            new GameActionMove(alien.getUser(),currentPlayerMoveTo(alienDest)).execute();
            if ( ! alien.getState().equals( GameConfig.IN_GAME_STATE ) )
                return;
            new GameActionEndTurn(alien.getUser()).execute();
            if ( human == null )
                human = room.getGame().getCurrentPlayer();
            new GameActionMove(human.getUser(),currentPlayerMoveTo(humanDest)).execute();
            new GameActionEndTurn(human.getUser()).execute();
        }
    }

}
