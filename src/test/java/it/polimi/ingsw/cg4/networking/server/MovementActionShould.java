package it.polimi.ingsw.cg4.networking.server;

import static org.junit.Assert.*;
import it.polimi.ingsw.cg4.board.Sector;
import it.polimi.ingsw.cg4.networking.server.Room;
import it.polimi.ingsw.cg4.networking.server.User;
import it.polimi.ingsw.cg4.player.Player;
import it.polimi.ingsw.cg4.rules.GameConfig;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * It's used to test the movement action in game context.
 *
 */
public class MovementActionShould {
    private static User user1;
    private static User user2;
    private static User user3;
    private static User user4;
    private static Room room;
    private static Room room2;
    private Player human,alien,human2,alien2;
    
    /**
     * This initializer creates four users and adds them in their rooms.
     */
    @BeforeClass
    public static void init() {
        
        user1 = new User("Vincy");
        user1.setFlagTest( true );
        user2 = new User("Amos");
        user2.setFlagTest( true );
        room = new Room("polimi",user1);
        room.addUserTest(user2);
        room.startGame( "Fermi" );
        user3 = new User("Marco");
        user3.setFlagTest( true );
        user4 = new User("Alessia");
        user4.setFlagTest( true );
        room2 = new Room("cg4",user3);
        room2.addUserTest(user4);
        room2.startGame( "Fermi" );
        
    }

    /**
     * This method tests the escape action movement.
     * First the humans move to the escape hatch sector.
     */
    @Test
    public void escapeHatchMovementTest1() {
        playerMovement("U","DR");
        playerMovement("U","UR");
        playerMovement("U","UR");
        playerMovement("D","U");
        playerMovement("U","UL");
        playerMovement("D","U");
        playerMovement("U","UR");
        playerMovement("D","U");
        if ( room.getGame().getCurrentPlayer().getCharacter().getRace().equals( GameConfig.ALIEN_RACE ) ){
            room.getGame().getCurrentPlayer().resetMovedSectors();
            room.getGame().nextPlayer();
            room.getGame().getCurrentPlayer().refreshReachableSectors();
       }
        
        playerMovement("U","UL");
        if(human.getState().equals( GameConfig.IN_GAME_STATE ))
        {
            playerMovement("U","U");
            playerMovement("D","U");
            playerMovement("U","UL");
            playerMovement("D","U");
            if ( room.getGame().getCurrentPlayer().getCharacter().getRace().equals( GameConfig.ALIEN_RACE ) ){
                room.getGame().getCurrentPlayer().resetMovedSectors();
                room.getGame().nextPlayer();
                room.getGame().getCurrentPlayer().refreshReachableSectors();
            }
            playerMovement("U","UR");
        }
        if(alien.getState().equals( GameConfig.LOSER_STATE )){
            assertEquals( human.getState().equals( GameConfig.WINNER_STATE ), true);
        }
        
    }
    
    /**
     * This method tests the escape action movement.
     * First the humans move to the escape hatch sector.
     */
    @Test
    public void escapeHatchMovementTest2() {
        playerMovement2("U","DR");
        playerMovement2("U","UR");
        playerMovement2("U","UR");
        playerMovement2("D","U");
        playerMovement2("U","UL");
        playerMovement2("D","U");
        playerMovement2("U","UR");
        playerMovement2("D","U");
        if ( room2.getGame().getCurrentPlayer().getCharacter().getRace().equals( GameConfig.ALIEN_RACE ) ){
            room2.getGame().getCurrentPlayer().resetMovedSectors();
            room2.getGame().nextPlayer();
            room2.getGame().getCurrentPlayer().refreshReachableSectors();
       }
        playerMovement2("U","UL");
        if(human2.getState().equals( GameConfig.IN_GAME_STATE ))
        {
            playerMovement2("U","U");
            playerMovement2("D","U");
            playerMovement2("U","UL");
            playerMovement2("D","U");
            if ( room2.getGame().getCurrentPlayer().getCharacter().getRace().equals( GameConfig.ALIEN_RACE ) ){
                room2.getGame().getCurrentPlayer().resetMovedSectors();
                room2.getGame().nextPlayer();
                room2.getGame().getCurrentPlayer().refreshReachableSectors();
            }
            playerMovement2("U","UR");
            if(alien2.getState().equals( GameConfig.LOSER_STATE )){
                assertEquals( human2.getState().equals( GameConfig.WINNER_STATE ), true);
            }
        }
        if(alien2.getState().equals( GameConfig.LOSER_STATE )){
            assertEquals( human2.getState().equals( GameConfig.WINNER_STATE ), true);
        }
        
    }
    

    private Sector currentPlayerMoveTo( String direction ){
        Player player = room.getGame().getCurrentPlayer();
        Sector sector = room.getGame().getState().getBoard().getSector( player.getSector().getCoordinate().getNeighbor( direction ) );
        return sector;
    }
    
    private void playerMovement( String alienDest, String humanDest){
        
        if(room.getGame().getCurrentPlayer().getCharacter().getRace().equals( GameConfig.HUMAN_RACE )){
            if ( human == null )
                human = room.getGame().getCurrentPlayer();
            new GameActionMove(human.getUser(),currentPlayerMoveTo(humanDest)).execute();
            if ( ! human.getState().equals( GameConfig.IN_GAME_STATE ) )
                return;
            new GameActionEndTurn(human.getUser()).execute();
            if ( alien == null )
                alien = room.getGame().getCurrentPlayer();
            new GameActionMove(alien.getUser(),currentPlayerMoveTo(alienDest)).execute();
            if ( ! alien.getState().equals( GameConfig.IN_GAME_STATE ) )
                return;
            new GameActionEndTurn(alien.getUser()).execute();
            
        }
        else{
            if ( alien == null )
                alien = room.getGame().getCurrentPlayer();
            new GameActionMove(alien.getUser(),currentPlayerMoveTo(alienDest)).execute();
            if ( ! alien.getState().equals( GameConfig.IN_GAME_STATE ) )
                return;
            new GameActionEndTurn(alien.getUser()).execute();
            if ( human == null )
                human = room.getGame().getCurrentPlayer();
            new GameActionMove(human.getUser(),currentPlayerMoveTo(humanDest)).execute();
            new GameActionEndTurn(human.getUser()).execute();
        }
    }
    

    private Sector currentPlayer2MoveTo( String direction ){
        Player player = room2.getGame().getCurrentPlayer();
        Sector sector = room2.getGame().getState().getBoard().getSector( player.getSector().getCoordinate().getNeighbor( direction ) );
        return sector;
    }
    
    private void playerMovement2( String alienDest, String humanDest){
        
        if(room2.getGame().getCurrentPlayer().getCharacter().getRace().equals( GameConfig.HUMAN_RACE )){
            if ( human2 == null )
                human2 = room2.getGame().getCurrentPlayer();
            new GameActionMove(human2.getUser(),currentPlayer2MoveTo(humanDest)).execute();
            if ( ! human2.getState().equals( GameConfig.IN_GAME_STATE ) )
                return;
            new GameActionEndTurn(human2.getUser()).execute();
            if ( alien2 == null )
                alien2 = room2.getGame().getCurrentPlayer();
            new GameActionMove(alien2.getUser(),currentPlayer2MoveTo(alienDest)).execute();
            if ( ! alien2.getState().equals( GameConfig.IN_GAME_STATE ) )
                return;
            new GameActionEndTurn(alien2.getUser()).execute();
            
        }
        else{
            if ( alien2 == null )
                alien2 = room2.getGame().getCurrentPlayer();
            new GameActionMove(alien2.getUser(),currentPlayer2MoveTo(alienDest)).execute();
            if ( ! alien2.getState().equals( GameConfig.IN_GAME_STATE ) )
                return;
            new GameActionEndTurn(alien2.getUser()).execute();
            if ( human2 == null )
                human2 = room2.getGame().getCurrentPlayer();
            new GameActionMove(human2.getUser(),currentPlayer2MoveTo(humanDest)).execute();
            if ( ! human2.getState().equals( GameConfig.IN_GAME_STATE ) )
                return;
            new GameActionEndTurn(human2.getUser()).execute();
        }
    }

}
