package it.polimi.ingsw.cg4.networking.server;

import static org.junit.Assert.*;
import it.polimi.ingsw.cg4.board.Sector;
import it.polimi.ingsw.cg4.card.CardCreator;
import it.polimi.ingsw.cg4.card.ItemCard;
import it.polimi.ingsw.cg4.networking.server.Room;
import it.polimi.ingsw.cg4.networking.server.User;
import it.polimi.ingsw.cg4.player.Player;
import it.polimi.ingsw.cg4.rules.AttackRules;
import it.polimi.ingsw.cg4.rules.GameConfig;

import org.junit.BeforeClass;
import org.junit.Test;


public class GameAttackShould {
    private static User user1;
    private static User user2;
    private static User user3;
    private static User user4;
    private static Room room;
    private static Room room2;
    private Player human,alien,human2,alien2;
   
    /**
     * This initializer creates 4 users and adds them in their rooms.
     * There are two different rooms, the flag test value must be 
     * true.
     */
    @BeforeClass
    public static void init(){
        user1 = new User("Vincy");
        user1.setFlagTest( true );
        user2 = new User("Amos");
        user2.setFlagTest( true );
        room = new Room("polimi",user1);
        room.addUserTest(user2);
        room.startGame( "Galilei" );
        user3 = new User("Marco");
        user3.setFlagTest( true );
        user4 = new User("Alessia");
        user4.setFlagTest( true );
        room2 = new Room("cg4",user3);
        room2.addUserTest(user4);
        room2.startGame( "Galilei" );        
    }

    /**
     * This method tests the attack action.
     *  First the humans and the aliens
     *  move to the same sector.
     */
    @Test
    public void attackTest() {
       CardCreator creator = new CardCreator();
       ItemCard defense = (ItemCard) creator.create( "Defense" );
       playerMovement("UR","DR");
       playerMovement("DR","UR");
       playerMovement("DR","UR");
       playerMovement("DR","UR");
       
       if ( room.getGame().getCurrentPlayer().getCharacter().getRace().equals( GameConfig.ALIEN_RACE ) ){
           room.getGame().getCurrentPlayer().resetMovedSectors();
           room.getGame().nextPlayer();
           room.getGame().getCurrentPlayer().refreshReachableSectors();
       }
       if( human.getItemIndex( defense ) == -1 ){
           if( human.getHandSize() > 0 )
               human.removeItem( 0 );
           human.addItem( defense );
           human.getCharacter().activateStatus( AttackRules.DEFENSE_STATUS );
       }
       if(room.getGame().getCurrentPlayer().getCharacter().getRace().equals( GameConfig.HUMAN_RACE )){
           new GameActionMove(human.getUser(),currentPlayerMoveTo("D")).execute();
           new GameActionEndTurn(human.getUser()).execute();
           new GameActionMove(alien.getUser(),currentPlayerMoveTo("D")).execute();
           
           new GameActionAttack(alien.getUser()).execute();
           if(human.getCharacter().isAffectedBy( GameConfig.DEFENSE_STATUS )){
               human.getCharacter().deactivateStatus( AttackRules.DEFENSE_STATUS );
               human.removeItem( human.getItemIndex( defense ) );
           }
           if ( human.getState().equals( GameConfig.IN_GAME_STATE ) ) {
               new GameActionEndTurn(alien.getUser()).execute();
               new GameActionMove(human.getUser(),currentPlayerMoveTo("U")).execute();
               new GameActionEndTurn(human.getUser()).execute();
               new GameActionMove(alien.getUser(),currentPlayerMoveTo("U")).execute();
               alien.setHasAttacked( false );
               new GameActionAttack(alien.getUser()).execute();
               
               assertTrue( human.getState().equals( GameConfig.LOSER_STATE ));
           } else {
               assertTrue( human.getState().equals( GameConfig.LOSER_STATE ));
           }
       }
    }
    
    /**
     * This method tests the humans victory after 
     * the human attack. First the human and the alien
     * move to the same sector.
     */
    @Test
    public void humanVictoryTest(){
        
        CardCreator creator = new CardCreator();
        ItemCard attack = (ItemCard) creator.create( "Attack" );
        playerMovement2("UR","DR");
        playerMovement2("DR","UR");
        playerMovement2("DR","UR");
        playerMovement2("DR","UR");
        
        if ( room2.getGame().getCurrentPlayer().getCharacter().getRace().equals( GameConfig.ALIEN_RACE ) ){
            room2.getGame().getCurrentPlayer().resetMovedSectors();
            room2.getGame().nextPlayer();
            room2.getGame().getCurrentPlayer().refreshReachableSectors();
        }
        if( human2.getItemIndex( attack ) == -1 ){
            if( human2.getHandSize() > 0 )
                human2.removeItem( 0 );
            human2.addItem( attack );
            human2.getCharacter().activateStatus( AttackRules.ATTACK_STATUS );
        }
        if(room2.getGame().getCurrentPlayer().getCharacter().getRace().equals( GameConfig.HUMAN_RACE )){
            human2.incMovedSectors( 1 );
            if(!human2.getCharacter().isAffectedBy( GameConfig.ATTACK_STATUS ))
                human2.getCharacter().activateStatus( GameConfig.ATTACK_STATUS );
            new GameActionAttack(human2.getUser()).execute();
            assertTrue( alien2.getState().equals( GameConfig.LOSER_STATE ));
        }
        
    }
    
    private Sector currentPlayerMoveTo( String direction ){
        Player player = room.getGame().getCurrentPlayer();
        Sector sector = room.getGame().getState().getBoard().getSector( player.getSector().getCoordinate().getNeighbor( direction ) );
        return sector;
    }
    
    private void playerMovement( String alienDest, String humanDest){
        
        if(room.getGame().getCurrentPlayer().getCharacter().getRace().equals( GameConfig.HUMAN_RACE )){
            if ( human == null )
                human = room.getGame().getCurrentPlayer();
            new GameActionMove(human.getUser(),currentPlayerMoveTo(humanDest)).execute();
            new GameActionEndTurn(human.getUser()).execute();
            if ( alien == null )
                alien = room.getGame().getCurrentPlayer();
            new GameActionMove(alien.getUser(),currentPlayerMoveTo(alienDest)).execute();
            if ( ! alien.getState().equals( GameConfig.IN_GAME_STATE ) )
                return;
            new GameActionEndTurn(alien.getUser()).execute();
            
        }
        else{
            if ( alien == null )
                alien = room.getGame().getCurrentPlayer();
            new GameActionMove(alien.getUser(),currentPlayerMoveTo(alienDest)).execute();
            if ( ! alien.getState().equals( GameConfig.IN_GAME_STATE ) )
                return;
            new GameActionEndTurn(alien.getUser()).execute();
            if ( human == null )
                human = room.getGame().getCurrentPlayer();
            new GameActionMove(human.getUser(),currentPlayerMoveTo(humanDest)).execute();
            new GameActionEndTurn(human.getUser()).execute();
        }
    }
    
    private Sector currentPlayer2MoveTo( String direction ){
        Player player = room2.getGame().getCurrentPlayer();
        Sector sector = room2.getGame().getState().getBoard().getSector( player.getSector().getCoordinate().getNeighbor( direction ) );
        return sector;
    }
    
    private void playerMovement2( String alienDest, String humanDest){
        
        if(room2.getGame().getCurrentPlayer().getCharacter().getRace().equals( GameConfig.HUMAN_RACE )){
            if ( human2 == null )
                human2 = room2.getGame().getCurrentPlayer();
            new GameActionMove(human2.getUser(),currentPlayer2MoveTo(humanDest)).execute();
            if ( ! human2.getState().equals( GameConfig.IN_GAME_STATE ) )
                return;
            new GameActionEndTurn(human2.getUser()).execute();
            if ( alien2 == null )
                alien2 = room2.getGame().getCurrentPlayer();
            new GameActionMove(alien2.getUser(),currentPlayer2MoveTo(alienDest)).execute();
            if ( ! alien2.getState().equals( GameConfig.IN_GAME_STATE ) )
                return;
            new GameActionEndTurn(alien2.getUser()).execute();
            
        }
        else{
            if ( alien2 == null )
                alien2 = room2.getGame().getCurrentPlayer();
            new GameActionMove(alien2.getUser(),currentPlayer2MoveTo(alienDest)).execute();
            if ( ! alien2.getState().equals( GameConfig.IN_GAME_STATE ) )
                return;
            new GameActionEndTurn(alien2.getUser()).execute();
            if ( human2 == null )
                human2 = room2.getGame().getCurrentPlayer();
            new GameActionMove(human2.getUser(),currentPlayer2MoveTo(humanDest)).execute();
            if ( ! human2.getState().equals( GameConfig.IN_GAME_STATE ) )
                return;
            new GameActionEndTurn(human2.getUser()).execute();
        }
    }

}
