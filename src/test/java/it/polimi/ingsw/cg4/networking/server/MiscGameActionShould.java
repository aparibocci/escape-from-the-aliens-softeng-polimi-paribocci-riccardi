package it.polimi.ingsw.cg4.networking.server;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import it.polimi.ingsw.cg4.networking.server.Room;
import it.polimi.ingsw.cg4.networking.server.User;

import org.junit.BeforeClass;
import org.junit.Test;
/**
 * It's used to test all action in the game context.
 *
 */

public class MiscGameActionShould {
    
    private static User user1;
    private static User user2;
    private static Room room;
    
    /**
     * This method represent the valid room action.
     */
    private static final Map< String, String > ROOM_COMMANDS = new HashMap< String, String >();
    static {
        ROOM_COMMANDS.put( "chat", "chat <text> - sends a new message to everyone with <text> as content." );
        ROOM_COMMANDS.put( "help", "help - shows the valid commands." );
        ROOM_COMMANDS.put( "logout", "logout - ends the current session and disconnects from server." );
        ROOM_COMMANDS.put( "start", "start <map_name> - [ADMIN ONLY] starts a new game with map <map_name> [ Fermi | Galilei | Galvani ]." );
        ROOM_COMMANDS.put( "users", "users - displays the room user list." );
    }

    /**
     * This initializer creates two users and adds them in their room.
     */
    @BeforeClass
    public static void init() {

        user1 = new User("Vincy");
        user1.setFlagTest( true );
        user2 = new User("Amos");
        user2.setFlagTest( true );
        room = new Room("polimi",user1);
        room.addUserTest(user2);
        room.startGame( "Galilei" );
    
    }


    /**
     * This method tests the room action.
     * In the second part the user disconnect from 
     * the room.
     */
    @Test
    public void actionTest() {
        new GameActionLog( user1 ).execute();
        new GameActionHelp( user1, new ArrayList< String >( ROOM_COMMANDS.values() )).execute();
        new GameActionChat( user1, "Bella giocata" );
        assertTrue( room.getUsers().size() == 2 );
        user1.disconnectFromGame();
        assertTrue( room.getUsers().size() == 1 );
    }

}
