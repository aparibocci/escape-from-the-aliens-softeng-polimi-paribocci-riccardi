package it.polimi.ingsw.cg4.board;

import static org.junit.Assert.*;
import it.polimi.ingsw.cg4.board.BoardCreator;
import it.polimi.ingsw.cg4.board.Fermi;
import it.polimi.ingsw.cg4.board.Galilei;
import it.polimi.ingsw.cg4.board.Galvani;

import org.junit.BeforeClass;
import org.junit.Test;
/**
 * To be used to test the class BoardCreator.
 *
 */

public class BoardCreatorShould {

    private static BoardCreator creator;
    
    /**
     * To be used to initialize the board creator.
     */
    @BeforeClass
    public static void init() {
        creator = new BoardCreator();
    }
    
    /**
     * To be used to test the creation of new board using board creator.
     * In this test are created the three main maps. 
     */
    @Test
    public void creationTest() {
        assertTrue( creator.create( "Fermi" )   instanceof Fermi );
        assertTrue( creator.create( "Galilei" ) instanceof Galilei );
        assertTrue( creator.create( "Galvani" ) instanceof Galvani );
        
        
    }
    
    /**
     * To be used to test the throw of invalid name exception.
     * Invalid map name are passed to creator.  
     */
    @Test( expected = Exception.class )
    public void invalidNameTest() {
        creator.create( "MappaIgnota" );
        creator.create( null );
        creator.create( "FERMI" );
    }

    
}
