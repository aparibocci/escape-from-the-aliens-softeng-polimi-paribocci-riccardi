package it.polimi.ingsw.cg4.board;

import static org.junit.Assert.*;
import it.polimi.ingsw.cg4.board.Board;
import it.polimi.ingsw.cg4.board.BoardCreator;
import it.polimi.ingsw.cg4.board.Coordinate;
import it.polimi.ingsw.cg4.player.Player;
import it.polimi.ingsw.cg4.player.character.CharacterCreator;
import it.polimi.ingsw.cg4.rules.AttackRules;
import it.polimi.ingsw.cg4.rules.GameConfig;
import it.polimi.ingsw.cg4.rules.MovementRules;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * To be used to test the class Board.
 *
 */
public class BoardShould {

    private static BoardCreator creator;
    private static Board board;

    /**
     * To be used to initialize a new board.
     */
    @BeforeClass
    public static void init() {
        creator = new BoardCreator();
        board = creator.create( "Fermi" );
    }
    
    /**
     * To be used to test equality between different board.
     * In this test the same boards with different name are tested.
     */
    @Test
    public void equalityTest() {
        Board board2 = creator.create( "Fermi" );
        assertEquals( board, board2 );
        board.hashCode();
        board2.hashCode();
    }

    /**
     * To be used to test same of the methods, like the getters.
     */
    @Test
    public void infoTest() {
        assertEquals( board.getName(), "Fermi" );
        assertEquals( board.toString() != null, true );
        assertEquals( board.getCols(), 23 );
        assertEquals( board.getRows(), 14 );
        assertTrue( board.getSector( Coordinate.fromCartesian( 0, 0 ) ).isEmpty( board ) );
        assertEquals( MovementRules.canAccessTo( GameConfig.HUMAN_RACE, board.getSector( Coordinate.fromCartesian( 4, 11 ) ).getType() ), true );
        assertEquals( board.getSector( Coordinate.fromCartesian( 0, 0 ) ).getType().toChar(), '-' );
        assertEquals( board.getSector( Coordinate.fromCartesian( 4, 9 ) ).getType().toChar(), 'H' );
        assertEquals( board.getSector( Coordinate.fromCartesian( 4, 11 ) ).getType().toChar(), 's' );
        assertEquals( board.getSector( Coordinate.fromCartesian( 9, 11 ) ).getType().toChar(), 'h' );
        assertEquals( board.getSector( Coordinate.fromCartesian( 3, 11 ) ).getType().toChar(), 'd' );
        assertEquals( board.getSector( Coordinate.fromCartesian( 0, 9 ) ).getType().toChar(), 'H' );
        assertEquals( board.getSector( Coordinate.fromCartesian( 8, 11 ) ).getType().toChar(), 'a' );

    }

    /**
     * To be used to test the adding of new players in the board.
     * In this test are created two different players.
     */
    @Test
    public void addPlayerTest() {
        CharacterCreator charCreator = new CharacterCreator();
        Player p1 = new Player( "Mario" );
        p1.setCharacter( charCreator.create( "Captain" ) );
        Player p2 = new Player( "Luca" );
        p2.setCharacter( charCreator.create( "FirstAlien" ) );
        board.addPlayer( p1, Coordinate.fromCartesian( 3, 11 ) );
        board.addPlayer( p2, Coordinate.fromCartesian( 5, 11 ) );
        assertEquals( p1.getSector().contains( board, p1 ), true );
        assertEquals( p1.getSector().contains( board, p2 ), false );
        assertTrue( board.getPlayers().contains( p1 ) );
        assertTrue( board.getPlayers().contains( p2 ) );
        assertEquals( p1.getBoard(), board );
        assertEquals( board.getSector( Coordinate.fromCartesian( 5, 11 ) ),
                p2.getSector() );
    }

    /**
     * To be used to test the aliens movement in the board.
     * In this test the method canMoveTo is used more than once.
     */
    @Test
    public void alienMovementTest() {
        CharacterCreator charCreator = new CharacterCreator();
        Player p = new Player( "Marco" );
        p.setCharacter( charCreator.create( "FourthAlien" ) );
        board.addPlayer( p, Coordinate.fromCartesian( 5, 11 ) );
        p.refreshReachableSectors();
        assertEquals( MovementRules.getMovementRange( p ), 2 );
        assertEquals( MovementRules.canMoveTo( p, p.getSector() ), false );
        assertEquals( MovementRules.canMoveTo( p, board.getSector( Coordinate.fromCartesian(
                6, 11 ) ) ), true );
        assertEquals( MovementRules.canMoveTo( p, board.getSector( Coordinate.fromCartesian(
                7, 12 ) ) ), true );
        assertEquals( MovementRules.canMoveTo( p, board.getSector( Coordinate.fromCartesian(
                7, 13 ) ) ), false );
        p.getCharacter().activateStatus( AttackRules.VORACIOUS_STATUS );
        p.refreshReachableSectors();
        assertEquals( MovementRules.canMoveTo( p, board.getSector( Coordinate.fromCartesian(
                7, 13 ) ) ), true );
        assertEquals( MovementRules.canMoveTo( p, board.getSector( Coordinate.fromCartesian(
                9, 14 ) ) ), false );
    }
    /**
     * To be used to test the humans movement in the board.
     * In this test the method canMoveTo is used more than once.
     */
    @Test
    public void humanMovementTest() {
        CharacterCreator charCreator = new CharacterCreator();
        Player p = new Player( "Alberto" );
        p.setCharacter( charCreator.create( "Soldier" ) );
        board.addPlayer( p, Coordinate.fromCartesian( 5, 11 ) );
        p.refreshReachableSectors();
        assertEquals( MovementRules.getMovementRange( p ), 1 );
        assertEquals( p.canMoveTo( p.getSector() ), false );
        assertEquals( p.canMoveTo( board.getSector( Coordinate.fromCartesian(
                6, 11 ) ) ), true );
        assertEquals( p.canMoveTo( board.getSector( Coordinate.fromCartesian(
                7, 11 ) ) ), false );
        p.getCharacter().activateStatus( GameConfig.ADRENALINE_STATUS );
        p.refreshReachableSectors();
        assertEquals( p.canMoveTo( board.getSector( Coordinate.fromCartesian(
                7, 11 ) ) ), true );
        assertEquals( p.canMoveTo( board.getSector( Coordinate.fromCartesian(
                8, 11 ) ) ), false );
    }

    /**
     * To be used to test the equals method.
     * 
     */
    @Test
    public void equalsTest() {
        assertEquals( board.equals( null ), false );
        assertEquals( board.equals( 5 ), false );
        assertEquals(board.equals( "Fermos" ), false);
        assertEquals(board.equals( "Galilei" ), false);
        
        
        

    }
    
    /**
     * To be used to test the throw of invalid sector coordinate.
     * Invalid sector coordinate are passed to the coordinate creator.
     */
    @Test( expected = Exception.class )
    public void sectorCoordinateNotValidTest() {
        Coordinate coordinate = Coordinate.fromCartesian( 99, 99 );
        board.getSector( coordinate );

    }
    
    /**
     * To be used to test the humans base getter.
     */
    @Test
    public void getHumanBase() {
        assertEquals( board.getHumanBase(),
                board.getSector( Coordinate.fromCartesian( 9, 11 ) ) );
    }
    
    /**
     * To be used to test the aliens base getter.
     */
    @Test
    public void getAlienBase() {
        assertEquals( board.getAlienBase(),
                board.getSector( Coordinate.fromCartesian( 8, 11 ) ) );
    }

}
