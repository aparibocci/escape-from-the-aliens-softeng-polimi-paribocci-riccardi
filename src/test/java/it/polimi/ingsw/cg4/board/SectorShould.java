package it.polimi.ingsw.cg4.board;

import static org.junit.Assert.*;

import java.lang.Character;
import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg4.board.Coordinate;
import it.polimi.ingsw.cg4.board.Sector;
import it.polimi.ingsw.cg4.board.SectorType;
import it.polimi.ingsw.cg4.gamedata.GameUtilities;
import it.polimi.ingsw.cg4.player.Player;
import it.polimi.ingsw.cg4.player.character.CharacterCreator;
import it.polimi.ingsw.cg4.rules.GameConfig;

import org.junit.Test;
/**
 * To be used to test the Sector class.
 *
 */

public class SectorShould {
    
    /**
     * This method tests the distance between two sector, having 
     * different coordinate or the same coordinate.
     */
    @Test
    public void distanceBetweenTest(){
        BoardCreator creator = new BoardCreator();
        Board board = creator.create( "Fermi" );
        CharacterCreator charCreator = new CharacterCreator();
        List<Player> players = new ArrayList<Player>();
        Sector s = new Sector( Coordinate.fromCube( 0, 0, 0 ), GameConfig.SECURE_SECTOR );
        Sector t = new Sector( Coordinate.fromCube(1, 1, 1), GameConfig.ALIEN_SECTOR );
        int distance = Sector.distanceBetween( s, t );
        assertEquals(distance,1);
        assertFalse (distance == 2);
        assertEquals( distance , Sector.distanceBetween( t, s ));
        Sector g = new Sector(Coordinate.fromCartesian( 0, 0 ),GameConfig.DANGEROUS_SECTOR);
        Sector f = new Sector(Coordinate.fromCartesian( 3, 2 ),GameConfig.VOID_SECTOR);
        int distance1 = Sector.distanceBetween( g,f );
        assertEquals(distance1,4 );
        Player player = new Player("GIOCO");
        players.add( player );
        player.setCharacter( charCreator.create( "Pilot" ));
        GameUtilities.setPlayersPosition( board, players );
        assertEquals( f.isEmpty( board ) , true);
        player.setSector( f );
        assertEquals( f.isEmpty( board ) , false);
    }
    
    /**
     * This method tests the closure of the escape hatch sector.
     * 
     */
    @Test 
    public void EscapeHatchCloseTest(){
        Sector g = new Sector( Coordinate.fromCube( 0, 0, 0 ), GameConfig.OPEN_HATCH_SECTOR );
        g.close();
        assertEquals(g.getType(), GameConfig.CLOSED_HATCH_SECTOR);
        g.close();
        assertFalse(g.getType().equals( GameConfig.OPEN_HATCH_SECTOR ));
        assertTrue(g.getType().equals( GameConfig.CLOSED_HATCH_SECTOR ));
    }
    
    /**
     * To be used to test the method equals between two sector
     * using coordinate and type.
     */
    @Test
    public void equalityTest() {
        Sector s = new Sector( Coordinate.fromCube( 0, 0, 0 ), GameConfig.SECURE_SECTOR );
        Sector t = new Sector( Coordinate.fromCartesian( 0, 0 ), GameConfig.SECURE_SECTOR );
        Sector u = new Sector( Coordinate.fromCartesian( 0, 0 ), GameConfig.VOID_SECTOR );
        Sector l = new Sector( Coordinate.fromCartesian( 0, 0 ), GameConfig.VOID_SECTOR );
        assertEquals( s, t );
        assertEquals( s.equals( u ), false );
        assertEquals( s.equals( l ), false );
        assertEquals( l.equals( s ), false );
        s.hashCode();
        t.hashCode();
        Sector g = new Sector( Coordinate.fromCartesian( 1, 0 ), GameConfig.VOID_SECTOR );
        assertEquals(g.equals( u ), false);
        assertEquals(g,g);
    }
   
    /**
     * To be used to test the throw of invalid type convert exception.
     * Invalid sector string "type" are passed to the method toChar.
     */
    @Test( expected = Exception.class )
    public void toCharErrTest() {
        SectorType type = SectorType.fromChar( 'Z' );
        assertTrue( type != GameConfig.ALIEN_SECTOR );
    }
    
    /**
     * To be used to tests the equals method. 
     */
    @Test
    public void equalsTest() {
        Sector f = new Sector( Coordinate.fromCube( 0, 0, 0 ), GameConfig.SECURE_SECTOR );
        assertEquals( f.equals( null ), false );
        assertEquals( f.equals( new Sector(Coordinate.fromCube( 0, 1, 0 ), SectorType.SECURE ) ) , false );
        assertEquals( f.equals( 5 ), false );
        assertEquals( f.equals( new Sector(Coordinate.fromCube( 0, 1, 0 ), SectorType.SECURE ) ),false);
        assertEquals( f.equals( new Sector(Coordinate.fromCube( 0, 0, 0 ), SectorType.DANGEROUS ) ),false);
        assertEquals( f.equals( new Sector(Coordinate.fromCube( 0, 0, 0 ), SectorType.SECURE ) ),true);
        assertEquals( f.equals( new Sector(null, SectorType.DANGEROUS ) ),false);
        assertEquals( f.toString().equals( Character.toString( f.type.toChar() ) ), true );
    }

}
