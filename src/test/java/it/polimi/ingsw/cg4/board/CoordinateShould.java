package it.polimi.ingsw.cg4.board;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg4.board.Coordinate;

import org.junit.BeforeClass;
import org.junit.Test;
/**
 * To be used to test the Coordinate class.
 *
 */
public class CoordinateShould {

    private static Coordinate coordinate;
    private static Coordinate cubeCoordinate;

    /**
     * This initializer sets up a new coordinate.
     */
    @BeforeClass
    public static void init() {
        coordinate = Coordinate.fromCartesian( 2, 2 );
        cubeCoordinate = Coordinate.fromCube( 1, 1, 1 );

    }

    /**
     * This method tests the string representation of different coordinate.
     */
    @Test
    public void toStringTest() {

        String string = (char) ( 'A' + coordinate.getCol() )
                + String.format( "%02d", coordinate.getRow() + 1 );
        assertEquals( string, coordinate.toString() );

    }
    
    /**
     * This method tests the string representation of cube coordinate.
     */
    @Test
    public void toCubeString() {
        String string = "(" + 1 + ", " + 1 + ", " + 1 + ")";
        assertEquals( string, cubeCoordinate.toCubeString() );
    }

    /**
     * To be used to test the equals method.
     */
    @Test
    public void equalsTest() {

        assertTrue( coordinate.equals( coordinate ) );
        assertTrue( coordinate.equals( Coordinate.fromCartesian( 2, 2 ) ) );
        assertEquals( coordinate.equals( null ), false );
        assertEquals( coordinate.equals( 5 ), false );
        assertEquals( coordinate.equals( Coordinate.fromCartesian( 2, 3 ) ),
                false );
        assertEquals( cubeCoordinate.equals( Coordinate.fromCube( 1, 2, 1 ) ),
                false );
        assertEquals( cubeCoordinate.equals( Coordinate.fromCube( 2, 1, 1 ) ),
                false );
        assertEquals( cubeCoordinate.equals( Coordinate.fromCube( 1, 1, 2 ) ),
                false );
        assertTrue( cubeCoordinate.equals( cubeCoordinate ) );

    }

    /**
     * This method tests the coordinate parameters getter.
     * 
     */
    @Test
    public void CoordinateTest() {
        assertEquals( cubeCoordinate.getX(), 1 );
        assertEquals( cubeCoordinate.getY(), 1 );
        assertEquals( cubeCoordinate.getZ(), 1 );
    }
    
    /**
     * This method tests coordinate parsing.
     */
    @Test
    public void parseCoordinateTest(){
        List<String> args = new ArrayList<String>();
        args.add( "N" );
        args.add( "08" );
        assertTrue( Coordinate.fromCartesian( 7, 13 ).equals( Coordinate.parseCoordinate( args )));
        
    }
    
    /**
     * To be used to test the throw of invalid string exception.
     * Invalid string are passed to the parseCoordinate.
     */
    @Test( expected = Exception.class )
    public void parseCoordinateErrorTest() {
        List<String> args = new ArrayList<String>();
        args.add( "AAA" );
        Coordinate.parseCoordinate( args );
        args.add( "06" );
        Coordinate.parseCoordinate( args );
        
    }
}
