package it.polimi.ingsw.cg4.card.deck;

import static org.junit.Assert.*;
import it.polimi.ingsw.cg4.card.ItemCard;
import it.polimi.ingsw.cg4.card.deck.CardDeckCreator;
import it.polimi.ingsw.cg4.card.deck.ItemCardDeck;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * To be used to test item card deck.
 *
 */

public class ItemCardDeckShould {

    private static ItemCardDeck itemCardDeck;
    private static CardDeckCreator itemCardDeckCreator = new CardDeckCreator();

    /**
     * This method create a new istance of item card deck.
     */
    @BeforeClass
    public static void init() {

        itemCardDeck = (ItemCardDeck) itemCardDeckCreator.create( "ItemCardDeck" );
    }

    /**
     * To be used to test the draw and fill algorithms.
     */
    @Test
    public void contentTest() {
        assertTrue( itemCardDeck.isEmpty() );
        itemCardDeck.fillWithDefaultCards();
        assertEquals( itemCardDeck.isEmpty(), false );
        while ( !itemCardDeck.isEmpty() )
            assertTrue( itemCardDeck.draw() instanceof ItemCard );

    }

    /**
     * To be used to test the discard algorithms.
     */
    public void discardTest() {
        ItemCardDeck discardDeck = (ItemCardDeck) itemCardDeckCreator
                .create( "ItemCardDeck" );

        itemCardDeck.fillWithDefaultCards();
        int initialSize = itemCardDeck.getCards().size();
        discardDeck.insertToBottom( itemCardDeck.draw() );
        discardDeck.insertToBottom( itemCardDeck.draw() );
        discardDeck.insertToBottom( itemCardDeck.draw() );
        assertEquals( itemCardDeck.getCards().size(), initialSize - 3 );
        assertEquals( discardDeck.getCards().size(), 3 );
        discardDeck.insertAllToBottom( itemCardDeck.drawAll() );
        assertEquals( itemCardDeck.isEmpty(), true );
        assertEquals( discardDeck.getCards().size(), initialSize );

    }

}
