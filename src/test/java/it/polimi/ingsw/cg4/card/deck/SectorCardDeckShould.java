package it.polimi.ingsw.cg4.card.deck;

import static org.junit.Assert.*;
import it.polimi.ingsw.cg4.card.SectorCard;
import it.polimi.ingsw.cg4.card.deck.CardDeckCreator;
import it.polimi.ingsw.cg4.card.deck.SectorCardDeck;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * To be used to test sector card deck.
 *
 */
public class SectorCardDeckShould {

    private static SectorCardDeck deck;
    private static CardDeckCreator creator = new CardDeckCreator();

    /**
     * This method create a new sector card deck.
     */
    @BeforeClass
    public static void init() {
        deck = (SectorCardDeck) creator.create( "SectorCardDeck" );
    }

    /**
     * To be used to test the draw and fill algorithms.
     */
    @Test
    public void contentTest() {
        assertEquals( deck.isEmpty(), true );
        deck.fillWithDefaultCards();
        assertEquals( deck.isEmpty(), false );
        while ( !deck.isEmpty() ) {
            assertTrue( deck.draw() instanceof SectorCard );
        }
        assertEquals(deck.isEmpty(), true);
        deck.drawAll();
        assertEquals(deck.isEmpty(), true);
    }

    /**
     * To be used to test the discard algorithms.
     */
    @Test
    public void discardTest() {
        SectorCardDeck discardDeck = (SectorCardDeck) creator
                .create( "SectorCardDeck" );
        deck.fillWithDefaultCards();
        int initialSize = deck.getCards().size();
        discardDeck.insertToBottom( deck.draw() );
        discardDeck.insertToBottom( deck.draw() );
        discardDeck.insertToBottom( deck.draw() );
        assertEquals( discardDeck.getCards().size(), 3 );
        assertEquals( deck.getCards().size(), initialSize - 3 );
        discardDeck.insertAllToBottom( deck.drawAll() );
        assertEquals( deck.isEmpty(), true );
        assertEquals( discardDeck.getCards().size(), initialSize );
    }

}
