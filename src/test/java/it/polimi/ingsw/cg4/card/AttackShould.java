package it.polimi.ingsw.cg4.card;

import static org.junit.Assert.*;
import it.polimi.ingsw.cg4.card.CardCreator;
import it.polimi.ingsw.cg4.card.ItemCard;
import it.polimi.ingsw.cg4.rules.UseItemRules;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * To be used to test item card attack.
 *
 */
public class AttackShould {

    private static ItemCard card;

    /**
     * This initializer sets up a new item card attack.
     */
    @BeforeClass
    public static void init() {
        CardCreator creator = new CardCreator();
        card = (ItemCard) creator.create( "Attack" );
    }
    
    /**
     * In this method test the getter of name and description.
     */
    @Test
    public void infoTest() {
        assertEquals( card.getName(), "ATTACK" );
        assertEquals( card.getDescription(),
                "Allows you to attack, using the same rules as the Aliens." );
        assertEquals( UseItemRules.PASSIVE_ITEMS.contains( card ), false );
    }

    /**
     * To be used to tests the toString method.
     */
    @Test
    public void toStringTest() {
        CardCreator cardCreator = new CardCreator();
        card = (ItemCard) cardCreator.create( "Attack" );
        assertEquals( card.getName() + " [" + card.getDescription() + "]",
                card.toString() );

    }

}
