package it.polimi.ingsw.cg4.card.deck;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import it.polimi.ingsw.cg4.card.Card;
import it.polimi.ingsw.cg4.card.EscapeHatchCard;
import it.polimi.ingsw.cg4.card.deck.CardDeckCreator;
import it.polimi.ingsw.cg4.card.deck.EscapeHatchCardDeck;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * To be used to test escape hatch card deck.
 *
 */

public class EscapeHatchCardDeckShould {
	private static EscapeHatchCardDeck escapeHatchCardDeck;
	private static CardDeckCreator creator = new CardDeckCreator();

	/**
	 * This method create a new istance of escape card deck.
	 */
	@BeforeClass
	public static void init() {
		escapeHatchCardDeck = (EscapeHatchCardDeck) creator.create( "EscapeHatchCardDeck" );
	}
	
	/**
	 * To be used to test the draw and fill algorithms.
	 */
	@Test
	public void contentTest() {
	    escapeHatchCardDeck.drawAll();
		assertTrue( escapeHatchCardDeck.isEmpty() );
        escapeHatchCardDeck.fillWithDefaultCards();
        assertEquals( escapeHatchCardDeck.isEmpty(), false );
        while ( !escapeHatchCardDeck.isEmpty() )
            assertTrue( escapeHatchCardDeck.draw() instanceof EscapeHatchCard );
	}
	
	/**
	 * To be used to test the discard algorithms.
	 */
	@Test
	public void discardTest(){
		EscapeHatchCardDeck discardDeck = (EscapeHatchCardDeck) creator
                .create( "EscapeHatchCardDeck" );
        escapeHatchCardDeck.fillWithDefaultCards();
        int initialSize = escapeHatchCardDeck.getCards().size();
        discardDeck.insertToBottom( escapeHatchCardDeck.draw() );
        discardDeck.insertToBottom( escapeHatchCardDeck.draw() );
        discardDeck.insertToBottom( escapeHatchCardDeck.draw() );
        assertEquals( escapeHatchCardDeck.getCards().size(), initialSize - 3 );
        assertEquals( discardDeck.getCards().size(), 3 );
        discardDeck.insertAllToBottom( escapeHatchCardDeck.drawAll() );
        assertEquals( escapeHatchCardDeck.isEmpty(), true );
        assertEquals( discardDeck.getCards().size(), initialSize );
	}
	
	/**
	 * This method tests the representation of deck.
	 * First the deck fill with default card.
	 */
	@Test 
	public void toStringTest(){
	    escapeHatchCardDeck.fillWithDefaultCards();
	    List< String > lines = Arrays.asList( escapeHatchCardDeck.toString().split( "\n" ) );
	    int cardIndex = 0;
	    while ( ! escapeHatchCardDeck.isEmpty() ) {
	        Card c = escapeHatchCardDeck.draw();
	        assertEquals( c.getName(), lines.get( cardIndex ) );
	        cardIndex++;
	    }
	}
	
	/**
	 * To be used to test the shuffle algorithms.
	 */
	@Test
	public void shuffleTest(){
	    List< Card > cards = new ArrayList< Card >();
	    escapeHatchCardDeck.fillWithDefaultCards();
	    cards.addAll( escapeHatchCardDeck.drawAll() );
	    escapeHatchCardDeck.fillWithDefaultCards();
	    escapeHatchCardDeck.shuffle();
	    while ( ! escapeHatchCardDeck.isEmpty() ) {
	        Card c = escapeHatchCardDeck.draw();
	        Card foundCard = null;
	        for ( Card card : cards ) {
	            if ( card.getName() == c.getName() ) {
	                foundCard = card;
	                break;
	            }
	        }
	        assertTrue( foundCard != null );
	        cards.remove( foundCard );
	    }
	    assertTrue( cards.isEmpty() );
	}

}
