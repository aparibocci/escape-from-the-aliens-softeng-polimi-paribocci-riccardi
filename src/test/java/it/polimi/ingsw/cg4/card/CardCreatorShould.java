package it.polimi.ingsw.cg4.card;

import static org.junit.Assert.*;

import it.polimi.ingsw.cg4.card.Card;
import it.polimi.ingsw.cg4.card.CardCreator;
import it.polimi.ingsw.cg4.card.Noise;
import it.polimi.ingsw.cg4.card.NoiseAny;
import it.polimi.ingsw.cg4.rules.GameConfig;
import org.junit.BeforeClass;
import org.junit.Test;
/**
 * To be used to test card creator.
 *
 */
public class CardCreatorShould {

    private static CardCreator creator;

    /**
     * This method initialize a new creator.
     */
    @BeforeClass
    public static void init() {
        creator = new CardCreator();
    }
    
    /**
     * To be used to tests the creation of a new item card.
     * In this test are created all item card.
     */
    @Test
    public void creationTest() {
        assertTrue( GameConfig.ADRENALINE_CARD_CLASS.isInstance( creator.create( "Adrenaline") ));
        assertTrue( GameConfig.ATTACK_CARD_CLASS.isInstance( creator.create( "Attack") ));
        assertTrue( GameConfig.DEFENSE_CARD_CLASS.isInstance( creator.create( "Defense") ));
        assertTrue( GameConfig.GREEN_HATCH_CARD_CLASS.isInstance( creator.create( "GreenEscapeHatch") ));
        assertTrue( GameConfig.RED_HATCH_CARD_CLASS.isInstance( creator.create( "RedEscapeHatch") ));
        assertTrue( GameConfig.SEDATIVES_CARD_CLASS.isInstance( creator.create( "Sedatives") ));
        assertTrue( GameConfig.SILENCE_CARD_CLASS.isInstance( creator.create( "Silence") ));
        assertTrue( GameConfig.SPOTLIGHT_CARD_CLASS.isInstance( creator.create( "Spotlight") ));
        assertTrue( GameConfig.TELEPORT_CARD_CLASS.isInstance( creator.create( "Teleport") ));
        
    }

    /**
     * To be used to tests the creation of a new sector card with object.
     * In this test are created all sector card.
     */
    @Test
    public void itemIconCreationTest() {
        Card noise = creator.create( "Noise", false );
       
        assertTrue(GameConfig.NOISE_CARD_CLASS.isInstance( noise ));
        assertEquals( ( (Noise) noise ).hasItemIcon(), false );
        Card noiseItem = creator.create( "Noise", true );
        assertTrue(GameConfig.NOISE_CARD_CLASS.isInstance( noiseItem ));
        assertEquals( ( (Noise) noiseItem ).hasItemIcon(), true );
        Card noiseAny = creator.create( "NoiseAny", false );
        assertTrue(GameConfig.NOISE_ANY_CARD_CLASS.isInstance( noiseAny ));
        assertEquals( ( (NoiseAny) noiseAny ).hasItemIcon(), false );
        Card noiseAnyItem = creator.create( "NoiseAny", true );
        assertTrue(GameConfig.NOISE_ANY_CARD_CLASS.isInstance( noiseAnyItem ));
        assertEquals( ( (NoiseAny) noiseAnyItem ).hasItemIcon(), true );
    }

    /**
     * To be used to test the throw of invalid card name exception.
     * Invalid name are passed to creator.
     */
    @Test( expected = Exception.class )
    public void invalidNameTest() {
        creator.create( "Sedative" );
        creator.create( null );

    }

    /**
     * To be used to test the throw of invalid card sector name exception.
     * Invalid sector name are passed to creator.
     */
    @Test( expected = Exception.class )
    public void invalidAltNameTest() {
        creator.create( "Nooise", true );
        creator.create( null, true );
    }

    /**
     * To be used to test the equals method.
     * In this test are created two different item card.
     */
    @Test
    public void equalsTest() {
        Card card = creator.create( "Attack" );

        assertEquals( card.equals( null ), false );
        assertEquals( card.equals( "Defense" ), false );
        assertEquals( card.equals( card ), true );
        assertEquals( card.equals( creator.create( "Defense" ) ), false );

    }

}
