package it.polimi.ingsw.cg4.card;

import static org.junit.Assert.*;
import it.polimi.ingsw.cg4.card.CardCreator;
import it.polimi.ingsw.cg4.card.ItemCard;
import it.polimi.ingsw.cg4.rules.UseItemRules;

import org.junit.BeforeClass;
import org.junit.Test;
/**
 * To be used to test the defense item card.
 */

public class DefenseShould {

    private static ItemCard card;
    
    /**
     * This initializer create a new defense item card.
     */
    @BeforeClass
    public static void init() {
        CardCreator creator = new CardCreator();
        card = ( ItemCard )creator.create( "Defense" );
    }
    
    /**
     * This method test the name and description getter.
     */
    @Test
    public void infoTest() {
        assertEquals( card.getName(), "DEFENSE" );
        assertEquals( card.getDescription(), "When an Alien attacks you, this card is activated and you are not affected by the attack." );
        assertEquals( UseItemRules.PASSIVE_ITEMS.contains( card ), false);
    }

}
