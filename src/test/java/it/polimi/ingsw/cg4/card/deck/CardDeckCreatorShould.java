package it.polimi.ingsw.cg4.card.deck;

import static org.junit.Assert.*;
import it.polimi.ingsw.cg4.card.deck.CardDeck;
import it.polimi.ingsw.cg4.card.deck.CardDeckCreator;
import it.polimi.ingsw.cg4.card.deck.EscapeHatchCardDeck;
import it.polimi.ingsw.cg4.card.deck.ItemCardDeck;
import it.polimi.ingsw.cg4.card.deck.SectorCardDeck;


import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
/**
 * To be used to test card deck creator.
 *
 */
public class CardDeckCreatorShould {

    private static CardDeckCreator cardDeckCreator;
    
    /**
     * This initializer sets up the creator.
     */
    @BeforeClass
    public static void init() {
        cardDeckCreator = new CardDeckCreator();
    }

    /**
     * This method used to test the creation of new card deck.
     * In this case are tested three different type of deck.
     */
    @Test
    public void creationTest() {
        
        assertTrue( cardDeckCreator.create( "ItemCardDeck" ) instanceof ItemCardDeck );
        assertTrue( cardDeckCreator.create( "EscapeHatchCardDeck" ) instanceof EscapeHatchCardDeck );
        assertTrue( cardDeckCreator.create( "SectorCardDeck" ) instanceof SectorCardDeck );

    }
    
    /**
     * To be used to test the throw of invalid card deck name exception.
     * Invalid  card deck name are passed to creator.
     */
    @Test( expected = Exception.class )
    public void invalidNameTest() {
        cardDeckCreator.create( "ItemsCardDeck" );
        cardDeckCreator.create( null );
        cardDeckCreator.create( "itemcarddeck" );
    }

    /**
     * To be used to test the deck list getter.
     * In this method are tested all deck.
     */
    @Test
    public void getDeckListTest() {
        List< CardDeck > decks = cardDeckCreator.getDeckList();
        assertEquals( decks.size(), 6 );
        assertTrue( decks.get( 0 ) instanceof SectorCardDeck );
        assertTrue( decks.get( 1 ) instanceof SectorCardDeck );
        assertTrue( decks.get( 2 ) instanceof EscapeHatchCardDeck );
        assertTrue( decks.get( 3 ) instanceof EscapeHatchCardDeck );
        assertTrue( decks.get( 4 ) instanceof ItemCardDeck );
        assertTrue( decks.get( 5 ) instanceof ItemCardDeck );

    }

}
