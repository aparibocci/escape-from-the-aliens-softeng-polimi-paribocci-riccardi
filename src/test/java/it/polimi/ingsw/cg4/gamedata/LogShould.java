package it.polimi.ingsw.cg4.gamedata;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg4.board.Board;
import it.polimi.ingsw.cg4.board.BoardCreator;
import it.polimi.ingsw.cg4.card.deck.CardDeck;
import it.polimi.ingsw.cg4.gamedata.EventLog;
import it.polimi.ingsw.cg4.player.Player;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 *To be used to test Log class.
 *
 */
public class LogShould {

    private static EventLog log;
    private String text = "prova";

    /**
     * This method create a new log.
     */
    @BeforeClass
    public static void init() {
        log = new EventLog();

    }

    /**
     * This method test the new string adding to log.
     */
    @Test
    public void logTest() {
        String text1 = "prova1";
        log.add( text );
        log.add( text1 );
        assertTrue( log.toString().contains(
                text.subSequence( 0, text.length() ) ) );
        assertTrue( log.toString().contains(
                text1.subSequence( 0, text.length() ) ) );
    }
    
    /**
     * To be used to test the throw of invalid game state exception.
     * Invalid sizes are passed to creator.
     */
    @Test( expected = Exception.class )
    public void GameStateErrTest() {
        BoardCreator boardCreator = new BoardCreator();
        Board board = boardCreator.create( "Fermi" );
        List<Player> players = new ArrayList<Player>();
        List<CardDeck> decks = new ArrayList<CardDeck>();
        GameState state = new GameState( board, players, decks );
        assertTrue( state != null );
        
    }

}
