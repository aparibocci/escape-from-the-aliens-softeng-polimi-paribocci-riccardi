package it.polimi.ingsw.cg4.gamedata;

import static org.junit.Assert.*;
import it.polimi.ingsw.cg4.gamedata.EventLogEntry;

import java.util.Calendar;

import org.junit.BeforeClass;
import org.junit.Test;
/**
 * To be used to test LogEntry class.
 *
 */
public class LogEntryShould {

    private static EventLogEntry logEntry;
    private static Calendar time;
    private static String text = "prova";

    /**
     * This initializer create a new log entry.
     */
    @BeforeClass
    public static void init() {
        logEntry = new EventLogEntry( text );
        time = Calendar.getInstance();
    }

    /**
     * To be used to test toString method.
     */
    @Test
    public void toStringTest() {

        String string = String.format( "%02d", time.get( Calendar.HOUR ) )
                + ":" + String.format( "%02d", time.get( Calendar.MINUTE ) )
                + ":" + String.format( "%02d", time.get( Calendar.SECOND ) )
                + " - " + text;
        assertEquals( logEntry.toString(), string );

    }

}
