package it.polimi.ingsw.cg4.card;

/**
 * Defense Card class.
 * 
 * @see ItemCard
 */
public class Defense extends ItemCard {

    /**
     * Creates an instance of the Defense Card class.
     */
    public Defense() {
        super( "DEFENSE", "When an Alien attacks you, this card is activated"
                + " and you are not affected by the attack." );
    }
}
