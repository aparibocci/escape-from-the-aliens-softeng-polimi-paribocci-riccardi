package it.polimi.ingsw.cg4.card;

/**
 * Class used to represent a game Card.
 * It possesses a name and a description.
 * It is abstract (only its subclasses can be instantiated).
 * Creation through CardCreator is recommended.
 * 
 */
public abstract class Card {

    private final String name;
    private final String description;

    protected Card( String name, String description ) {
        this.name = name;
        this.description = description;
    }

    /**
     * 
     * @return the Card name.
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @return the Card description.
     */
    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return this.name + " [" + this.description + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ( ( description == null ) ? 0 : description.hashCode() );
        result = prime * result + ( ( name == null ) ? 0 : name.hashCode() );
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if ( this == obj )
            return true;
        if ( obj == null )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        Card other = (Card) obj;
        if ( !name.equals( other.name ) )
            return false;
        return true;
    }

}
