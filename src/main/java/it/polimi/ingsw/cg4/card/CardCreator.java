package it.polimi.ingsw.cg4.card;

import it.polimi.ingsw.cg4.rules.CreatorConfig;

import java.util.List;

/**
 * Utility class used to create Card instances.
 * Valid Card names are specified inside the static field VALID_NAMES.
 *
 * @see Card
 */
public class CardCreator {

    private static final String CLASS_PATH = "it.polimi.ingsw.cg4.card.";

    private static final List< String > VALID_NAMES = CreatorConfig.VALID_ITEM_CARD_NAMES;

    private static final List< String > VALID_NAMES_ALT = CreatorConfig.VALID_SECTOR_CARD_NAMES;
    
    private static final String NOT_FOUND_ERROR = "Invalid card name";

    /**
     * Used to create Item Cards and Escape Hatch Cards.
     * @param name the Card name.
     * @return the instance of the desired Card (if the name is valid).
     */
    public Card create( String name ) {
        try {
            if ( VALID_NAMES.contains( name ) )
                return (Card) Class.forName( CLASS_PATH + name ).getConstructor().newInstance();
            throw new IllegalArgumentException( NOT_FOUND_ERROR );
        } catch ( Exception e ) {
            throw new RuntimeException( e );
        }
    }

    /**
     * Used to create Sector Cards.
     * @param name the Card name.
     * @param itemIcon specifies the presence (or the absence) of the Item icon on the Card.
     * @return the instance of the desired Card (if the name is valid) with the itemIcon parameter.
     */
    public Card create( String name, boolean itemIcon ) {
        try {
            if ( VALID_NAMES_ALT.contains( name ) )
                return (Card) Class.forName( CLASS_PATH + name ).getConstructor( boolean.class ).newInstance( itemIcon );
            throw new IllegalArgumentException( NOT_FOUND_ERROR );
        } catch ( Exception e ) {
            throw new RuntimeException( e );
        }
    }

}
