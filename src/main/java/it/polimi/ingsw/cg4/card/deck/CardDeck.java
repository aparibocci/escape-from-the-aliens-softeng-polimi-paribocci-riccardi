package it.polimi.ingsw.cg4.card.deck;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import it.polimi.ingsw.cg4.card.Card;
import it.polimi.ingsw.cg4.card.CardCreator;

/**
 * Class used to represent a Deck made of Cards.
 * Cards can be drawn from the top and can be inserted to the bottom.
 * The deck can also be shuffled.
 * A general CardDeck cannot be instantiated directly (it is abstract): creation through
 * CardDeckCreator is recommended.
 * 
 * @see Card
 * @see CardDeckCreator
 */
public abstract class CardDeck {

    private final List< Card > cards;
    private final CardCreator creator;

    protected CardDeck() {
        cards = new ArrayList< Card >();
        this.creator = new CardCreator();
    }

    /**
     * 
     * @return the list of Cards in the Deck (unmodifiable).
     */
    public List< Card > getCards() {
        return Collections.unmodifiableList( cards );
    }

    /**
     * Draws card from the top, removing it from the Deck.
     * 
     * @return the drawn card.
     */
    public Card draw() {
        return cards.remove( 0 );
    }

    /**
     * 
     * @return true if the Deck is empty, else otherwise.
     */
    public boolean isEmpty() {
        return cards.isEmpty();
    }

    /**
     * Draws the entire card in the Deck from the top, removing them from the Deck.
     * 
     * @return the List containing the drawn cards (top card is first in the list).
     */
    public List< Card > drawAll() {
        List< Card > toBeReturned = new ArrayList< Card >();
        toBeReturned.addAll( cards );
        cards.clear();
        return toBeReturned;
    }

    /**
     * Given a Card, insert it to the bottom.
     * 
     * @param card the Card to be inserted to the bottom of the Deck.
     */
    public void insertToBottom( Card card ) {
        cards.add( card );
    }

    /**
     * Given a list of Cards, insert it to the bottom.
     * 
     * @param cards the Cards to be inserted to the bottom of the Deck.
     */
    public void insertAllToBottom( List< Card > cards ) {
        this.cards.addAll( cards );
    }

    /**
     * Shuffles the Deck.
     */
    public void shuffle() {
        Collections.shuffle( cards );
    }

    protected CardCreator getCreator() {
        return creator;
    }

    /**
     * Fill the CardDeck with the default Card list in the game context.
     */
    public abstract void fillWithDefaultCards();

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for ( Card card : this.cards ) {
            builder.append( card.getName() + "\n" );
        }
        return builder.toString();
    }

}
