package it.polimi.ingsw.cg4.card;

/**
 * Noise Card class.
 *
 * @see SectorCard
 */
public class Noise extends SectorCard {

    /**
     * Creates an instance of the Noise card with (or without) the Item icon.
     * @param itemIcon indicates the presence (or the absence) of the Item icon.
     */
    public Noise( boolean itemIcon ) {
        super( "NOISE IN YOUR SECTOR",
                "You must reveal your Sector's coordinate.", itemIcon );
    }

}
