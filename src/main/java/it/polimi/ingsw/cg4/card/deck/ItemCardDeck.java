package it.polimi.ingsw.cg4.card.deck;

import it.polimi.ingsw.cg4.rules.GameConfig;

/**
 * Item Card Deck class.
 * 
 * @see CardDeck
 */
public class ItemCardDeck extends CardDeck {

    /**
     * Creates an instance of the Item CardDeck class.
     */
    public ItemCardDeck() {
        super();
    }

    @Override
    public void fillWithDefaultCards() {
        this.drawAll();
        for ( int i = 0; i < GameConfig.ATTACK_N; i++ )
            this.insertToBottom( this.getCreator().create( "Attack" ) );
        for ( int i = 0; i < GameConfig.DEFENSE_N; i++ )
        this.insertToBottom( this.getCreator().create( "Defense" ) );
        for ( int i = 0; i < GameConfig.ADRENALINE_N; i++ )
            this.insertToBottom( this.getCreator().create( "Adrenaline" ) );
        for ( int i = 0; i < GameConfig.SEDATIVES_N; i++ )
            this.insertToBottom( this.getCreator().create( "Sedatives" ) );
        for ( int i = 0; i < GameConfig.TELEPORT_N; i++ )
            this.insertToBottom( this.getCreator().create( "Teleport" ) );
        for ( int i = 0; i < GameConfig.SPOTLIGHT_N; i++ )
            this.insertToBottom( this.getCreator().create( "Spotlight" ) );

    }

}
