package it.polimi.ingsw.cg4.card;

/**
 * Represents a Sector Card.
 * Cannot be instantiated directly: it is an abstract class.
 * 
 * @see Card
 */
public abstract class SectorCard extends Card {

    private final boolean itemIcon;

    protected SectorCard( String name, String description, boolean itemIcon ) {
        super( name, description );
        this.itemIcon = itemIcon;
    }

    /**
     * 
     * @return true if the SectorCard has the Item icon, false otherwise.
     */
    public boolean hasItemIcon() {
        return itemIcon;
    }

}
