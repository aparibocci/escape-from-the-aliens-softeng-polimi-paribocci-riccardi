package it.polimi.ingsw.cg4.card;

/**
 * Spotlight Card class.
 * 
 * @see ItemCard
 */
public class Spotlight extends ItemCard {

    /**
     * Creates an instance of the Spotlight Card class.
     */
    public Spotlight() {
        super(
                "SPOTLIGHT",
                "Name any Sector. Any player that is in the named Sector or any of the six adjacent Sectors"
                        + " must announce his position." );

    }
}
