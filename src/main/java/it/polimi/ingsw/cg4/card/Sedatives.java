package it.polimi.ingsw.cg4.card;

/**
 * Sedatives Card class.
 * 
 * @see ItemCard
 */
public class Sedatives extends ItemCard {

    /**
     * Creates an instance of the Sedatives Card class.
     */
    public Sedatives() {
        super( "SEDATIVES",
                "You do not draw a Dangerous Sector Card this turn,"
                        + " even if you move into a Dangerous Sector." );

    }

}
