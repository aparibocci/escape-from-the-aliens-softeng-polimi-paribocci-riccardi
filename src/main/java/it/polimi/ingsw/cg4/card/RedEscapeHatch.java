package it.polimi.ingsw.cg4.card;

/**
 * RedEscapeHatch Card class.
 * 
 * @see EscapeHatchCard
 */
public class RedEscapeHatch extends EscapeHatchCard {

    /**
     * Creates an instance of the RedEscapeHatch Card class.
     */
    public RedEscapeHatch() {
        super( "RED ESCAPE HATCH",
                "That's too bad... The escape hatch is broken and it can't be used!" );

    }

}
