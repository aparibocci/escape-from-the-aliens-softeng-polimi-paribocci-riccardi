package it.polimi.ingsw.cg4.card;

/**
 * Teleport Card class.
 *
 * @see ItemCard
 */
public class Teleport extends ItemCard {

    /**
     * Creates an instance of the Teleport Card class.
     */
    public Teleport() {
        super( "TELEPORT", "Allows you to move directly to the Human Sector."
                + "Can be used before or after your movement." );

    }

}
