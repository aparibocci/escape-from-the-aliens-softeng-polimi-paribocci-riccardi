package it.polimi.ingsw.cg4.card.deck;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Utility class used to create CardDeck instances.
 * Valid CardDeck names are specified inside the static field VALID_NAMES.
 *
 * @see CardDeck
 */
public class CardDeckCreator {

    private static final String CLASS_PATH = "it.polimi.ingsw.cg4.card.deck.";

    private static final String ESCAPE_HATCH_DECK_NAME = "EscapeHatchCardDeck";
    private static final String ITEM_DECK_NAME = "ItemCardDeck";
    private static final String SECTOR_DECK_NAME = "SectorCardDeck";
    private static final List< String > VALID_NAMES = Arrays.asList(
            ESCAPE_HATCH_DECK_NAME,
            ITEM_DECK_NAME,
            SECTOR_DECK_NAME
    );
    
    private static final String NOT_FOUND_ERROR = "Invalid card deck name";

    /**
     * Used to create CardDecks instances.
     * @param name the CardDeck name.
     * @return the instance of the desired CardDeck (if the name is valid).
     */
    public CardDeck create( String name ) {
        try {
            if ( VALID_NAMES.contains( name ) )
                return (CardDeck) Class.forName( CLASS_PATH + name ).getConstructor().newInstance();
            throw new IllegalArgumentException( NOT_FOUND_ERROR );
        } catch ( Exception e ) {
            throw new RuntimeException( e );
        }
    }
    
    /**
     * 
     * @return the list of pre-set game Decks, ready for use: [sectorDeck, sectorDiscard, 
     * escapeHatchDeck, escapeHatchDiscard, itemDeck, itemDiscard]
     */
    public List< CardDeck > getDeckList() {
        List< CardDeck > decks = new ArrayList< CardDeck >();
        CardDeck sectorDeck = this.create( SECTOR_DECK_NAME );
        sectorDeck.fillWithDefaultCards();
        sectorDeck.shuffle();
        CardDeck sectorDiscard = this.create( SECTOR_DECK_NAME );
        CardDeck escapeHatchDeck = this.create( ESCAPE_HATCH_DECK_NAME );
        escapeHatchDeck.fillWithDefaultCards();
        escapeHatchDeck.shuffle();
        CardDeck escapeHatchDiscard = this.create( ESCAPE_HATCH_DECK_NAME );
        CardDeck itemDeck = this.create( ITEM_DECK_NAME );
        itemDeck.fillWithDefaultCards();
        itemDeck.shuffle();
        CardDeck itemDiscard = this.create( ITEM_DECK_NAME );
        decks.add( sectorDeck );
        decks.add( sectorDiscard );
        decks.add( escapeHatchDeck );
        decks.add( escapeHatchDiscard );
        decks.add( itemDeck );
        decks.add( itemDiscard );
        return decks;
    }

}
