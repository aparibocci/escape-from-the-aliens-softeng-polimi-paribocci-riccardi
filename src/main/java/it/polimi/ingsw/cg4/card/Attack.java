package it.polimi.ingsw.cg4.card;

/**
 * Class Card Attack.
 * 
 * @see ItemCard
 */
public class Attack extends ItemCard {

    /**
     * Creates an instance of the Attack Card class.
     */
    public Attack() {
        super( "ATTACK",
                "Allows you to attack, using the same rules as the Aliens." );
    }

}
