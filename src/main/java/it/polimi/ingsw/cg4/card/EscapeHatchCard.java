package it.polimi.ingsw.cg4.card;

/**
 * Represents an Escape Hatch Card.
 * Cannot be instantiated directly: it is an abstract class.
 * 
 * @see Card
 */
public abstract class EscapeHatchCard extends Card {

    protected EscapeHatchCard( String name, String description ) {
        super( name, description );

    }

}
