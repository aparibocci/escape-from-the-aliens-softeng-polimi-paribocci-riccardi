package it.polimi.ingsw.cg4.card;

/**
 * NoiseAny Card class.
 *
 * @see SectorCard
 */
public class NoiseAny extends SectorCard {

    /**
     * Creates an instance of the NoiseAny card with (or without) the Item icon.
     * @param itemIcon indicates the presence (or the absence) of the Item icon.
     */
    public NoiseAny( boolean itemIcon ) {
        super( "NOISE IN ANY SECTOR",
                "You must reveal any Sector's coordinate.", itemIcon );
    }

}
