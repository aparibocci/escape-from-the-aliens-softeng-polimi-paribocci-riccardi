package it.polimi.ingsw.cg4.card;

/**
 * GreenEscapeHatch Card class.
 * 
 * @see EscapeHatchCard
 */
public class GreenEscapeHatch extends EscapeHatchCard {

    /**
     * Creates an instance of the GreenEscapeHatch Card class.
     */
    public GreenEscapeHatch() {
        super( "GREEN ESCAPE HATCH",
                "Well done! You can escape from the spaceship!" );

    }

}
