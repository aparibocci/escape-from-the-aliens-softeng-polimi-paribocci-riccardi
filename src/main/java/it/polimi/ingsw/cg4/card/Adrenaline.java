package it.polimi.ingsw.cg4.card;

/**
 * Card Adrenaline Class.
 * 
 * @see ItemCard
 */
public class Adrenaline extends ItemCard {

    /**
     * Creates an instance of the Adrenaline Card class.
     */
    public Adrenaline() {
        super( "ADRENALINE", "Allows you to move two Sectors this turn." );
    }
}
