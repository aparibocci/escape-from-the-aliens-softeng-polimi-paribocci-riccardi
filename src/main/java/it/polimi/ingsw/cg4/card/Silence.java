package it.polimi.ingsw.cg4.card;

/**
 * Silence Card class.
 * 
 * @see SectorCard
 */
public class Silence extends SectorCard {

    /**
     * Creates an instance of the Silence Card class.
     */
    public Silence() {
        super( "SILENCE", "You're lucky... for now.", false );
    }

}
