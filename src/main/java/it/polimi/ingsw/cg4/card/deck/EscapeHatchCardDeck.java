package it.polimi.ingsw.cg4.card.deck;

import it.polimi.ingsw.cg4.rules.GameConfig;

/**
 * Escape Hatch Card Deck class.
 * 
 * @see CardDeck
 */
public class EscapeHatchCardDeck extends CardDeck {

    /**
     * Creates an instance of the EscapeHatch CardDeck class.
     */
    public EscapeHatchCardDeck() {
        super();
    }

    @Override
    public void fillWithDefaultCards() {
        this.drawAll();
        for ( int i = 0; i < GameConfig.GREEN_HATCH_N; i++ )
            this.insertToBottom( this.getCreator().create( "GreenEscapeHatch" ) );
        for ( int i = 0; i < GameConfig.RED_HATCH_N; i++ )
            this.insertToBottom( this.getCreator().create( "RedEscapeHatch" ) );
    }

}
