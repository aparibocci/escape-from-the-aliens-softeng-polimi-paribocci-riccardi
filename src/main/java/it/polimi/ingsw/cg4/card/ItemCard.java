package it.polimi.ingsw.cg4.card;

/**
 * Represents an Item Card.
 * Cannot be instantiated directly: it is an abstract class.
 * 
 * @see Card
 */
public abstract class ItemCard extends Card {

    protected ItemCard( String name, String description ) {
        super( name, description );
    }

}
