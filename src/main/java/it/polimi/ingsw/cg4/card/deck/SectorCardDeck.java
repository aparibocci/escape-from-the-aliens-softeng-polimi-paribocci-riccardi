package it.polimi.ingsw.cg4.card.deck;

import it.polimi.ingsw.cg4.rules.GameConfig;

/**
 * Sector Card Deck class.
 * 
 * @see CardDeck
 */
public class SectorCardDeck extends CardDeck {

    /**
     * Creates an instance of the Sector CardDeck class.
     */
    public SectorCardDeck() {
        super();
    }

    @Override
    public void fillWithDefaultCards() {
        this.drawAll();
        for ( int i = 0; i < GameConfig.NOISE_N; i++ )
            this.insertToBottom( this.getCreator().create( "Noise", false ) );
        for ( int i = 0; i < GameConfig.NOISE_I_N; i++ )
            this.insertToBottom( this.getCreator().create( "Noise", true ) );
        for ( int i = 0; i < GameConfig.NOISE_ANY_N; i++ )
            this.insertToBottom( this.getCreator().create( "NoiseAny", false ) );
        for ( int i = 0; i < GameConfig.NOISE_ANY_I_N; i++ )
            this.insertToBottom( this.getCreator().create( "NoiseAny", true ) );
        for ( int i = 0; i < GameConfig.SILENCE_N; i++ )
            this.insertToBottom( this.getCreator().create( "Silence" ) );
    }

}
