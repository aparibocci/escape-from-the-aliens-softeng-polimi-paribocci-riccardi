package it.polimi.ingsw.cg4.networking.common;

import java.rmi.Remote;
import java.rmi.RemoteException;
/**
 * Class used to represent the RMI connection manager.
 *
 */

public interface RMIRoomConnectionManager extends Remote {

    /**
     * Used to manage the controls of subscriber in RMI connection..
     * @param userName the name of subscriber.
     * @param cmd the command action in room context.
     * @param clientIn 
     * @throws RemoteException
     */
    void rmiSubscribe( String userName, String cmd, RMIClientIn clientIn  ) throws RemoteException;
    
}
