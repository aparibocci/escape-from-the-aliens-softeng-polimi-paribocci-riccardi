package it.polimi.ingsw.cg4.networking.common;

/**
 * Allows the configuration of networking values common to both client
 * and server.
 *
 */
public class CommonConfig {

    public static final int SERVER_SOCKET_PORT = 7779;
    public static final int SERVER_RMI_PORT = 7780;
    
    // Amount of seconds to wait before timeout in user input request.
    public static final int LINE_TIMEOUT_SECONDS = 5;
    
    public static final String MSG_SEPARATOR = "&";
    public static final String DEFAULT_CONTEXT = "MSGBOX";
    public static final String CONNECTION_FAILURE_MESSAGE = "Connection ended: ";
    
    private CommonConfig() {
        
    }
    
}
