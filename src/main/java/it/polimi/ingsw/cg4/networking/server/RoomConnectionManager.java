package it.polimi.ingsw.cg4.networking.server;

import it.polimi.ingsw.cg4.networking.common.CommonConfig;
import it.polimi.ingsw.cg4.networking.common.ContextConfig;
import it.polimi.ingsw.cg4.networking.common.MessageLine;
import it.polimi.ingsw.cg4.networking.common.RMIClientIn;
import it.polimi.ingsw.cg4.networking.common.RMIRoomConnectionManager;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class is used to manage the incoming connection requests
 * sent by the clients. It logically represents the server interface.
 * It is implemented using a thread that repeatedly checks for incoming requests
 * for socket-based connections.
 * RMI-purposed methods are also provided, to be used by clients through
 * a remote interface.
 * The singleton pattern has been used, since it fits the application
 * of the class.
 */
class RoomConnectionManager extends Thread implements RMIRoomConnectionManager {
    
    private static final Logger LOG = Logger.getLogger( Logger.GLOBAL_LOGGER_NAME );
    private static RoomConnectionManager instance = null;
    private final Map< String, Room > rooms = new HashMap< String, Room >();
    private boolean acceptingConnections = true;
    private static final int SOCKET_PORT = CommonConfig.SERVER_SOCKET_PORT;
    private static final int RMI_PORT = CommonConfig.SERVER_RMI_PORT;
    
    private RoomConnectionManager() {
        
    }
    
    /**
     * 
     * @return a new instance of the class if it is not present,
     * the old one otherwise (singleton pattern).
     */
    public static RoomConnectionManager getInstance() {
        if ( instance == null )
            instance = new RoomConnectionManager();
        return instance;
    }
    
    @Override
    public void run() {
        try {
            Registry registry = LocateRegistry.createRegistry( RMI_PORT );
            RMIRoomConnectionManager stub = (RMIRoomConnectionManager)UnicastRemoteObject.exportObject(this, 0 );
            registry.rebind( "RoomConnectionManager", stub );
            LOG.info( "Room manager ready. Listening..." );
            while ( acceptingConnections ) {
                lookForNewUsers();
            }
            registry.unbind( "RoomConnectionManager" );
            UnicastRemoteObject.unexportObject( this, true );
        } catch ( RemoteException | NotBoundException e ) {
            LOG.log( Level.SEVERE, e.getMessage(), e );
            return;
        }
    }
    
    private void lookForNewUsers() {
        try ( ServerSocket managerSocket = new ServerSocket( SOCKET_PORT ) ) {
            Socket socket = managerSocket.accept();
            @SuppressWarnings( "resource" )
            List< String > args = toArgs( new Scanner( socket.getInputStream() ).nextLine() );
            socketSubscribe( args, socket );
        } catch ( IOException e ) {
            LOG.log( Level.SEVERE, e.getMessage(), e );
            return;
        }
    }

    private void socketSubscribe( List< String > args, Socket socket ) throws IOException {
        String userName = args.get( 0 );
        if ( args.size() == 3 && "create".equalsIgnoreCase( args.get( 1 ) ) ) {
            actionSocketCreateRoom( userName, socket, args );
        } else if ( args.size() == 3 && "join".equalsIgnoreCase( args.get( 1 ) ) ) {
            actionSocketJoinRoom( userName, socket, args );
        } else if ( args.size() == 2 && "autojoin".equalsIgnoreCase( args.get( 1 ) ) ) {
            actionSocketAutojoinRoom( userName, socket );
        } else {
            sendSocketError( CommonConfig.CONNECTION_FAILURE_MESSAGE + "command is not valid.", socket );
            socket.close();
        }
    }

    @Override
    public void rmiSubscribe( String userName, String cmd, RMIClientIn clientIn ) {
        List< String > args = RoomConnectionManager.toArgs( cmd );
        if ( args.size() == 2 && "create".equalsIgnoreCase( args.get( 0 ) ) ) {
            actionRMICreateRoom( userName, args.get( 1 ), clientIn );
        } else if ( args.size() == 2 && "join".equalsIgnoreCase( args.get( 0 ) ) ) {
            actionRMIJoinRoom( userName, args.get( 1 ), clientIn );
        } else if ( args.size() == 1 && "autojoin".equalsIgnoreCase( args.get( 0 ) ) ) {
            actionRMIAutojoinRoom( userName, clientIn );
        } else {
            sendRMIError( CommonConfig.CONNECTION_FAILURE_MESSAGE + "command is not valid.", clientIn );
        }
    }

    /**
     * 
     * @return the rooms of the connection manager, mapped with names.
     */
    public Map< String, Room > getRooms() {
        return Collections.unmodifiableMap( rooms );
    }
    
    /**
     * 
     * @param roomName the name of the room to be removed.
     */
    public void removeRoom( String roomName ) {
        rooms.remove( roomName );
    }

    
    private void createRoom( String roomName, User creator ) {
        LOG.info( creator.getName() + " created the room " + roomName );
        creator.getOutThread().dispatchMessage( "Connection enstablished." );
        creator.getOutThread().dispatchMessage( "Room " + roomName + " has been created by user " + creator.getName() );
        rooms.put( roomName, new Room( this, roomName, creator ) );
    }
    
    private void addToRoom( String roomName, User user ) {
        rooms.get( roomName ).addUser( user );
        user.getOutThread().dispatchMessage( "Connection enstablished." );
        user.getOutThread().dispatchMessage( "You have joined the room " + roomName + "." );
        LOG.info( user.getName() + " joined the room " + roomName );
    }
    
    private User createSocketUser( Socket socket, String userName ) {
        return new User( userName, socket );
    }

    private User createRMIUser( String userName, RMIClientIn clientIn  ) {
        return new User( userName, clientIn );
    }
    
    private void sendSocketError( String error, Socket socket ) throws IOException {
        PrintWriter w;
        w = new PrintWriter( socket.getOutputStream() );
        w.println( new MessageLine( ContextConfig.MSGBOX_CONTEXT, error ) );
        w.flush();
    }

    private void sendRMIError( String error, RMIClientIn clientIn ) {
        try {
            clientIn.receive( new MessageLine( ContextConfig.MSGBOX_CONTEXT, error ) );
        } catch ( RemoteException e ) {
            LOG.log( Level.SEVERE, e.getMessage(), e );
            return;
        }
    }

    private void actionSocketCreateRoom( String userName, Socket socket, List< String > args ) throws IOException {
        String roomName = args.get( 2 );
        if ( rooms.containsKey( roomName ) ) {
            sendSocketError( CommonConfig.CONNECTION_FAILURE_MESSAGE + "room " + roomName + " already exists!", socket );
            socket.close();
        } else {
            User brokerThread = createSocketUser( socket, userName );
            this.createRoom( roomName, brokerThread );
        }
    }
    
    private void actionSocketJoinRoom( String userName, Socket socket, List< String > args ) throws IOException {
        String roomName = args.get( 2 );
        if ( ! rooms.containsKey( roomName ) ) {
            sendSocketError( CommonConfig.CONNECTION_FAILURE_MESSAGE + "room " + roomName + " does not exist!", socket );
            socket.close();
        } else if ( rooms.get( roomName ).isPlaying() || rooms.get( roomName ).isGameEnded() ) {
            sendSocketError( CommonConfig.CONNECTION_FAILURE_MESSAGE + "room " + roomName + " is already hosting a game!", socket );
            socket.close();
        } else {
            try {
                User brokerThread = createSocketUser( socket, userName );
                this.addToRoom( roomName, brokerThread );
            } catch ( IllegalStateException e ) {
                LOG.log( Level.INFO, e.getMessage(), e );
                sendSocketError( CommonConfig.CONNECTION_FAILURE_MESSAGE + e.getMessage(), socket );
                socket.close();
            }
        }
    }
    
    private void actionSocketAutojoinRoom( String userName, Socket socket ) throws IOException {
        Room room = findFreeRoom();
        if ( room == null ) {
            sendSocketError( CommonConfig.CONNECTION_FAILURE_MESSAGE + "there is no available room at the moment. Please try later.", socket );
            socket.close();
            return;
        }
        String roomName = room.getName();
        actionSocketJoinRoom( userName, socket, Arrays.asList( userName, "join", roomName ) );
    }

    private Room findFreeRoom() {
        for ( Room room : rooms.values() )
            if ( room.isPreparing() && ! room.isFull() )
                return room;
        return null;
    }

    private void actionRMICreateRoom( String userName, String roomName, RMIClientIn clientIn ) {
        if ( rooms.containsKey( roomName ) ) {
            sendRMIError( CommonConfig.CONNECTION_FAILURE_MESSAGE + "room " + roomName + " already exists!", clientIn );
        } else {
            User user = createRMIUser( userName, clientIn );
            this.createRoom( roomName, user );
        }
    }

    private void actionRMIJoinRoom( String userName, String roomName, RMIClientIn clientIn ) {
        if ( ! rooms.containsKey( roomName ) ) {
            sendRMIError( CommonConfig.CONNECTION_FAILURE_MESSAGE + "room " + roomName + " does not exist!", clientIn );
        } else if ( rooms.get( roomName ).isPlaying() || rooms.get( roomName ).isGameEnded() ) {
            sendRMIError( CommonConfig.CONNECTION_FAILURE_MESSAGE + "room " + roomName + " is already hosting a game!", clientIn );
        } else {
            try {
                User user = createRMIUser( userName, clientIn );
                this.addToRoom( roomName, user );
            } catch ( IllegalStateException e ) {
                LOG.log( Level.INFO, e.getMessage(), e );
                sendRMIError( CommonConfig.CONNECTION_FAILURE_MESSAGE + e.getMessage(), clientIn );
            }
        }
    }

    private void actionRMIAutojoinRoom( String userName, RMIClientIn clientIn ) {
        Room room = findFreeRoom();
        if ( room == null ) {
            sendRMIError( CommonConfig.CONNECTION_FAILURE_MESSAGE + "there is no available room at the moment. Please try later.", clientIn );
            return;
        }
        String roomName = room.getName();
        actionRMIJoinRoom( userName, roomName, clientIn );
    }

    private static List< String > toArgs( String cmd ) {
        return Arrays.asList( cmd.split( " " ) );
    }
}
