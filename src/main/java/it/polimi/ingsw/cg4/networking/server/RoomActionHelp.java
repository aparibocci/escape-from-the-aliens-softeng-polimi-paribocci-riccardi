package it.polimi.ingsw.cg4.networking.server;

import java.util.List;

/**
 * Class used to represent user help action in the room context.
 *
 */
public class RoomActionHelp extends RoomAction {

    private List< String > validNames;

    /**
     * Create a new user help action.
     * @param user the user that requires the command list.
     * @param validNames the list of valid command.
     */
    public RoomActionHelp( User user, List< String > validNames ) {
        super( user );
        this.validNames = validNames;
    }

    @Override
    public void execute() {
        StringBuilder builder = new StringBuilder();
        builder.append( "Valid command list: \n" );
        for ( String s : validNames )
            builder.append( s + "\n" );
        builder.append( "\n" );
        this.sendToUser( this.getUser(), builder.toString() );
    }

}
