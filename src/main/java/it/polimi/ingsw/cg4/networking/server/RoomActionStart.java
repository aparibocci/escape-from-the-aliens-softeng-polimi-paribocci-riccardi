package it.polimi.ingsw.cg4.networking.server;

import it.polimi.ingsw.cg4.networking.common.ContextConfig;
import it.polimi.ingsw.cg4.rules.CreatorConfig;

/**
 * Class used to represent user start game action in the room context.
 *
 */
public class RoomActionStart extends RoomAction {

    private String mapName;

    /**
     * Create a new user start game action.
     * @param user the user that is starting the game.
     * @param mapName the name of the chosen map for the game.
     */
    public RoomActionStart( User user, String mapName ) {
        super( user );
        this.mapName = Character.toUpperCase( mapName.charAt( 0 ) ) + mapName.substring( 1 ).toLowerCase();
    }

    @Override
    public void execute() {
        if ( ! this.getUser().isAdmin() ) {
            this.sendToUser( this.getUser(), "Only the room admin can start the game." );
            return;
        }
        if ( ! this.getUser().getRoom().isReady() ) {
            this.sendToUser( this.getUser(), "The game cannot be started due to insufficient player number." );
            return;
        }
        if ( ! isValidMapName() ) {
            this.sendToUser( this.getUser(), "The map name is not valid." );
            return;
        }
        this.sendToEveryone( this.getUser().getName() + " is starting the game on map " + mapName + "..." );
        this.sendToEveryone( ContextConfig.GAME_STARTING_CONTEXT, "STARTING GAME" );
        this.sendToEveryone( ContextConfig.MAP_NAME_CONTEXT, mapName );
        this.getUser().getRoom().startGame( mapName );
    }

    private boolean isValidMapName() {
        return CreatorConfig.VALID_BOARD_NAMES.contains( mapName );
    }

}
