package it.polimi.ingsw.cg4.networking.server;

import it.polimi.ingsw.cg4.networking.common.ContextConfig;

/**
 * Class used to represent a Room Action.
 * It is abstract ( only its subclasses can be instantiated )
 *
 */
public abstract class RoomAction implements Action {

    private final User user;

    /**
     * Create a new room action.
     * @param user the user that is requiring a new action in the room context.
     */
    protected RoomAction( User user ) {
        this.user = user;
    }

    /**
     * 
     * @return the User who performs the action.
     */
    public User getUser() {
        return user;
    }
    
    @Override
    public void sendToUser( User dest, String message ) {
        this.sendToUser( dest, ContextConfig.ROOM_CONTEXT, message );
    }
    
    @Override
    public void sendToUser( User dest, String context, String message ) {
        user.getRoom().sendToUser( dest, context, message );
    }
    
    @Override
    public void sendToEveryone( String message ) {
        this.sendToEveryone( ContextConfig.ROOM_CONTEXT, message );
    }
    
    @Override
    public void sendToEveryone( String context, String message ) {
        user.getRoom().sendToEveryone( context, message );
    }

}
