package it.polimi.ingsw.cg4.networking.server;

import it.polimi.ingsw.cg4.board.Coordinate;
import it.polimi.ingsw.cg4.board.Sector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


class PlayingActionManager {
    
    private static final Map< String, String > GAME_COMMANDS = new HashMap< String, String >();
    static {
        GAME_COMMANDS.put( "move", "move <x> <y> - moves your character in sector with column <x> and row <y> (only if you can)." );
        GAME_COMMANDS.put( "attack", "attack - starts an attack in your character's sector (only if you can)." );
        GAME_COMMANDS.put( "end", "end - ends the current turn (only if you moved)." );
        GAME_COMMANDS.put( "use", "use <i> - uses the item with index <i>. (only if you can)." );
        GAME_COMMANDS.put( "chat", "chat <text> - sends a new message to everyone with <text> as content." );
        GAME_COMMANDS.put( "help", "help - shows the valid commands." );
        GAME_COMMANDS.put( "log", "log - shows the event log for the current game." );
        GAME_COMMANDS.put( "logout", "logout - ends the current session and disconnects from server, losing the game." );
    }

    private static final Logger LOG = Logger.getLogger( Logger.GLOBAL_LOGGER_NAME );
    private final User user;

    /**
     *
     * @param user the user who performs the actions;
     */
    public PlayingActionManager( User user ) {
        this.user = user;
    }

    /**
     * 
     * @param args the arguments of the command to be parsed.
     * @param cmd the original command (in String format).
     * @return the action corrisponding to the command, or null if no corresponding action
     * is present.
     */
    public Action parsePlayingAction( List< String > args, String cmd ) {
        String cmdType = args.get( 0 );
        if ( GAME_COMMANDS.containsKey( cmdType ) ) {
            if ( args.size() == 3 && "move".equals( cmdType ) && user.getRoom().getGame().getCurrentPlayer().equals( user.getPlayer() ) ) {
                return commandGameMove( args );
            } else if ( args.size() == 1 && "attack".equals( cmdType ) && user.getRoom().getGame().getCurrentPlayer().equals( user.getPlayer() ) ) {
                return commandGameAttack();
            } else if ( args.size() == 1 && "end".equals( cmdType ) && user.getRoom().getGame().getCurrentPlayer().equals( user.getPlayer() ) ) {
                return commandGameEndTurn();
            } else if ( args.size() == 2 && "use".equals( cmdType ) && user.getRoom().getGame().getCurrentPlayer().equals( user.getPlayer() ) ) {
                return commandGameUseItem( args );
            } else if ( args.size() > 1 && "chat".equals( cmdType ) ){
                return commandGameChat( args, cmd );
            } else if ( args.size() == 1 && "help".equals( cmdType ) ) {
                return commandGameHelp();
            } else if ( args.size() == 1 && "log".equals( cmdType ) ) {
                return commandGameLog();
            } else if ( args.size() == 1 && "logout".equals( cmdType ) ) {
                return commandGameLogout();
            }
        }
        return null;
    }
    
    /**
     *
     * @param args the command arguments.
     * @return the GameActionMove action, or null if the arguments are invalid.
     */
    private Action commandGameMove( List< String > args ) {
        Sector dest;
        try {
            dest = user.getRoom().getGame().getState().getBoard().getSector( Coordinate.parseCoordinate( args.subList( 1, args.size() ) ) );
        } catch ( IllegalArgumentException e ) {
            user.getRoom().sendToUser( user, e.getMessage() );
            LOG.log( Level.INFO, e.getMessage(), e );
            return null;
        }
        return new GameActionMove( user, dest );
    }

    /**
     *
     * @param args the command arguments.
     * @return the GameActionAttack action.
     */
    private Action commandGameAttack() {
        return new GameActionAttack( user );
    }

    /**
     *
     * @param args the command arguments.
     * @return the GameActionEndTurn action.
     */
    private Action commandGameEndTurn() {
        return new GameActionEndTurn( user );
    }

    /**
     *
     * @param args the command arguments.
     * @return the GameActionUseItem action, or null if the arguments are invalid.
     */
    private Action commandGameUseItem( List< String > args ) {
        int index;
        try {
            index = Integer.parseInt( args.get( 1 ) );
        } catch ( NumberFormatException e ) {
            LOG.log( Level.INFO, e.getMessage(), e );
            return null;
        }
        try {
            return new GameActionUseItem( user, index );
        } catch ( IllegalArgumentException e ) {
            LOG.log( Level.INFO, e.getMessage(), e );
            return null;
        }
    }

    /**
     *
     * @param args the command arguments.
     * @return the GameActionChat action.
     */
    private Action commandGameChat( List< String > args, String cmd ) {
        String text = cmd.substring( args.get( 0 ).length() + 1 );
        return new GameActionChat( user, text );
    }

    /**
     *
     * @return the GameActionHelp action.
     */
    private Action commandGameHelp() {
        return new GameActionHelp( user, new ArrayList< String >( GAME_COMMANDS.values() ) );
    }

    /**
     *
     * @return the GameActionLog action.
     */
    private Action commandGameLog() {
        return new GameActionLog( user );
    }

    /**
     *
     * @return the GameActionLogout action.
     */
    private Action commandGameLogout() {
        return new RoomActionLogout( user );
    }
    
}
