package it.polimi.ingsw.cg4.networking.common;

import java.rmi.Remote;
import java.rmi.RemoteException;


public interface RMIUserIn extends Remote {

    void receive( String msg ) throws RemoteException;
    
}
