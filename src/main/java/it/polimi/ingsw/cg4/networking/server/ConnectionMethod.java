package it.polimi.ingsw.cg4.networking.server;

/**
 * Enumeration used to describe the connection method used by a client
 * (remote method invocation or socket).
 *
 */
public enum ConnectionMethod {

    RMI, SOCKET
    
}
