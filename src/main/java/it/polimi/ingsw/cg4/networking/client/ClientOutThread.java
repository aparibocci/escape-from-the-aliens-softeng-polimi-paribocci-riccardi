package it.polimi.ingsw.cg4.networking.client;

import it.polimi.ingsw.cg4.networking.common.RMIUserIn;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.rmi.RemoteException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Thread managing messages to be sent to the server from the client.
 */
public class ClientOutThread extends Thread {

    private static final Logger LOG = Logger.getLogger( Logger.GLOBAL_LOGGER_NAME );
    private PrintWriter out;
    private Scanner inputScanner;
    private boolean running;
    private RMIUserIn userIn;
    private final ConnectionMethod method;
    
    /**
     * Constructor used in Socket-based connections.
     * @param socket the connection's socket.
     * @param inputScanner the input scanner used by the client.
     */
    public ClientOutThread( Socket socket, Scanner inputScanner ) {
        this.inputScanner = inputScanner;
        try {
            this.out = new PrintWriter( socket.getOutputStream() );
        } catch ( IOException e ) {
            LOG.log( Level.SEVERE, e.getMessage(), e );
        }
        running = true;
        this.method = ConnectionMethod.SOCKET;
    }
    
    /**
     * Constructor used in RMI-based connections.
     * @param inputScanner the input scanner used by the client.
     * @param userIn the remote interface representing the user's input.
     * thread, available through RMI.
     */
    public ClientOutThread( Scanner inputScanner, RMIUserIn userIn ) {
        this.inputScanner = inputScanner;
        this.userIn = userIn;
        running = true;
        this.method = ConnectionMethod.RMI;
    }

    @Override
    public void run() {
        while ( running ) {
            send();
            try {
                Thread.sleep( 5 );
            } catch ( InterruptedException e ) {
                LOG.log( Level.SEVERE, e.getMessage(), e );
            }

        }
    }
    
    private String send() {
        String msg = null;
        msg = inputScanner.nextLine();
        if ( msg != null && ! msg.replace( " ", "" ).isEmpty() ) {
            if ( method.equals( ConnectionMethod.SOCKET ) ) {
                out.println( msg );
                out.flush();
            } else {
                try {
                    userIn.receive( msg );
                } catch ( RemoteException e ) {
                    LOG.log( Level.SEVERE, e.getMessage(), e );
                }
            }
        }
        return msg;
    }

    /**
     * Closes the thread.
     */
    public void close() {
        running = false;
        out.close();
        out = null;
    }
}
