package it.polimi.ingsw.cg4.networking.common;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Allows the configuration of contexts used in communication between
 * server and clients (specifically for the GUI).
 *
 */
public class ContextConfig {

    // Context values configuration.
    public static final String MSG_SEPARATOR = "&";
    public static final String MSGBOX_CONTEXT = "MSGBOX";
    public static final String ROOM_CONTEXT = "ROOM";
    public static final String EVENT_CONTEXT = "EVENT";
    public static final String GAME_CONTEXT = "GAME";
    public static final String USER_LIST_CONTEXT = "USERS";
    public static final String GAME_STARTING_CONTEXT = "GAME_START";
    public static final String USER_JOIN_CONTEXT = "JOIN";
    public static final String CONNECTION_CONTEXT = "CONNECT";
    public static final String USER_ROOM_LOGOUT_CONTEXT = "ROOM_LOGOUT";
    public static final String SECTOR_REQUIRED_CONTEXT = "SECTOR_REQUIRED";
    public static final String MOVEMENT_REQUIRED_CONTEXT = "MOVE_REQUIRED";
    public static final String DANGEROUS_SECTOR_CONTEXT = "DANGEROUS";
    public static final String INFO_CONTEXT = "INFO";
    public static final String POSITION_CONTEXT = "POSITION";
    public static final String DRAW_ITEM_CONTEXT = "DRAW_ITEM";
    public static final String REMOVE_ITEM_CONTEXT = "REM_ITEM";
    public static final String CLI_CONTEXT = "CLI";
    public static final String NEW_CARD_CONTEXT = "NEW_CARD";
    public static final String MAP_NAME_CONTEXT = "MAP";
    public static final String ITEM_DISCARD_CONTEXT = "DISCARD";
    public static final String ATTACK_CONTEXT = "ATTACK";
    public static final String NOISE_CONTEXT = "NOISE";
    public static final String TELEPORT_CONTEXT = "TELEPORT";
    public static final String DEATH_CONTEXT = "DEATH";
    public static final String STATUS_ON_CONTEXT = "STATUS_ON";
    public static final String STATUS_OFF_CONTEXT = "STATUS_OFF";
    public static final String USER_REPORT_CONTEXT = "REPORT";
    public static final String END_GAME_CONTEXT = "END_GAME";
    public static final String TIMER_CONTEXT = "TIMER";
    public static final String TIMER_RESET_CONTEXT = "RESET_TIMER";
    public static final String DEFAULT_MSG_CONTEXT = MSGBOX_CONTEXT;
    
    // System contexts (will not be shown in CLI mode).
    public static final Set< String > SYS_CONTEXT = new HashSet< String >( 
            Arrays.asList( GAME_STARTING_CONTEXT, USER_JOIN_CONTEXT,
                    USER_ROOM_LOGOUT_CONTEXT, CONNECTION_CONTEXT,
                    SECTOR_REQUIRED_CONTEXT, MOVEMENT_REQUIRED_CONTEXT,
                    DANGEROUS_SECTOR_CONTEXT, INFO_CONTEXT, POSITION_CONTEXT,
                    DRAW_ITEM_CONTEXT, REMOVE_ITEM_CONTEXT, NEW_CARD_CONTEXT,
                    MAP_NAME_CONTEXT, ITEM_DISCARD_CONTEXT, ATTACK_CONTEXT,
                    NOISE_CONTEXT, TELEPORT_CONTEXT, DEATH_CONTEXT,
                    STATUS_ON_CONTEXT, STATUS_OFF_CONTEXT, USER_REPORT_CONTEXT,
                    END_GAME_CONTEXT, TIMER_CONTEXT, TIMER_RESET_CONTEXT )
    );
    
    private ContextConfig() {
        
    }
    
}
