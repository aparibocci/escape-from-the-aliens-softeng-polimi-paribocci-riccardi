package it.polimi.ingsw.cg4.networking.client;

/**
 * Enumeration identifying the game mode (Command line interface or
 * graphical user interface).
 */
public enum GameMode {

    CLI, GUI
    
}
