package it.polimi.ingsw.cg4.networking.server;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Contains the main method that starts the server.
 *
 */
public class Server {
    
    private static final Logger LOG = Logger.getLogger( Logger.GLOBAL_LOGGER_NAME );

    private Server() {
        
    }
    
    /**
     * Server's main method: sets up the logger and starts the connection manager.
     * @param args command-line arguments (not used in this context).
     */
    public static void main( String[] args ) {
        try {
            ServerLogger.setup( "server.log" );
        } catch ( SecurityException | IOException e ) {
            LOG.log( Level.SEVERE, e.getMessage(), e );
        }
        RoomConnectionManager manager = RoomConnectionManager.getInstance();
        manager.start();
    }

}
