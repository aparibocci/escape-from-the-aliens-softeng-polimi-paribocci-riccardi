package it.polimi.ingsw.cg4.networking.server;

import it.polimi.ingsw.cg4.networking.common.ContextConfig;
import it.polimi.ingsw.cg4.networking.common.MessageLine;
import it.polimi.ingsw.cg4.networking.common.RMIClientIn;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Thread managing messages to be sent to the client associated with
 * a server-side user.
 */
public class UserOutThread extends Thread {

    private static final Logger LOG = Logger.getLogger( Logger.GLOBAL_LOGGER_NAME );
    private String id;
    private PrintWriter out;
    private Queue< String > buffer;
    private boolean running;
    private final ConnectionMethod method;
    private RMIClientIn clientIn;
    
    /**
     * Constructor used in Socket-based connections.
     * @param socket the socket relative to the current connection.
     * @param id the user id (its name).
     * @throws IOException
     */
    public UserOutThread( Socket socket, String id ) throws IOException {
        buffer = new ConcurrentLinkedQueue< String >();
        this.id = id;
        this.out = new PrintWriter( socket.getOutputStream() );
        running = true;
        this.method = ConnectionMethod.SOCKET;
    }

    /**
     * 
     * Constructor used in RMI-based connections.
     * @param id the user id (its name).
     * @param clientIn the remote interface representing the client's input.
     * thread, available through RMI.
     */
    public UserOutThread( String id, RMIClientIn clientIn ) {
        this.clientIn = clientIn;
        buffer = new ConcurrentLinkedQueue< String >();
        this.id = id;
        running = true;
        this.method = ConnectionMethod.RMI;
    }
    

    /**
     * 
     * @return the thread's user name.
     */
    public String getUserName() {
        return id;
    }

    @Override
    public void run() {
        while ( running ) {
            String msg = buffer.poll();
            if ( msg != null )
                send( msg );
            else {
                try {
                    synchronized ( buffer ) {
                        buffer.wait();
                    }
                } catch ( InterruptedException e ) {
                    LOG.log( Level.SEVERE, e.getMessage(), e );
                }
            }
        }
    }

    /**
     * Adds a message line to the message buffer, using the default context.
     * @param msg the text of the message.
     */
    public void dispatchMessage( String msg ) {
        dispatchMessage( new MessageLine( ContextConfig.DEFAULT_MSG_CONTEXT, msg ) );
    }


    /**
     * Adds a message line to the message buffer.
     * @param messageLine the message line to be sent.
     */
    public void dispatchMessage( MessageLine messageLine ) {
        List< String > lines = Arrays.asList( messageLine.getMessage().split( "\n" ) );
        for ( String line : lines )
            if ( ! line.isEmpty() )
                buffer.add( new MessageLine( messageLine.getContext(), line ).toString() );
        synchronized ( buffer ) {
            buffer.notify();
        }
    }

    /**
     * Checking the connection method (socket or RMI), behaves appropriately
     * and sends a message to the associated client.
     * @param msg
     */
    public void send( String msg ) {
        if ( method.equals( ConnectionMethod.SOCKET ) ) {
            out.println( msg );
            out.flush();
        } else {
            try {
                clientIn.receive( new MessageLine( msg ) );
            } catch ( RemoteException e ) {
                LOG.log( Level.SEVERE, e.getMessage(), e );
                return;
            }
        }
    }

    /**
     * Closes the thread.
     */
    public void close() {
        running = false;
        if ( out != null ) {
            out.close();
            out = null;
        }
    }

}
