package it.polimi.ingsw.cg4.networking.client;

import it.polimi.ingsw.cg4.gui.template.MessageGetter;
import it.polimi.ingsw.cg4.networking.common.CommonConfig;
import it.polimi.ingsw.cg4.networking.common.ContextConfig;
import it.polimi.ingsw.cg4.networking.common.MessageLine;
import it.polimi.ingsw.cg4.networking.common.RMIClientIn;
import it.polimi.ingsw.cg4.networking.common.RMIRoomConnectionManager;
import it.polimi.ingsw.cg4.networking.common.RMIUserIn;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Thread used to instantiate a client-server connection request.
 * It uses two client threads: an input thread for message receiving
 * and an output thread for message sending.
 */
public class ClientThread extends Thread {

    private static final Logger LOG = Logger.getLogger( Logger.GLOBAL_LOGGER_NAME );
    private static final int MAX_LINES = 100;
    private static final int SOCKET_PORT = CommonConfig.SERVER_SOCKET_PORT;
    private static final int RMI_PORT = CommonConfig.SERVER_RMI_PORT;
    private final String id;
    private Socket socket;
    private ClientInThread in;
    private ClientOutThread out;
    private final String address;
    private final LinkedBlockingQueue< MessageLine > lines;
    private MessageGetter msgGetter;
    private final Scanner inputScanner;
    private final ConnectionMethod connectionMethod;

    /**
     * Checking the connection method, calls the adequate methods for connecting
     * with the server.
     * 
     * @param id the user's name
     * @param serverAddress the server's address.
     * @param topic the connection command ( create room | join room | autojoin )
     * @param inputScanner the input scanner for the client.
     * @param connectionMethod the connection method for the current request.
     * @param mode the game mode (CLI or GUI).
     * @param outputStream the output stream for the client.
     */
    public ClientThread( String id, String serverAddress, String topic, Scanner inputScanner,
            ConnectionMethod connectionMethod , GameMode mode ) {
        this.out = null;
        this.id = id;
        this.address = serverAddress;
        this.lines = new LinkedBlockingQueue< MessageLine >( MAX_LINES );
        this.inputScanner = inputScanner;
        this.connectionMethod = connectionMethod;
        try {
            if ( connectionMethod.equals( ConnectionMethod.SOCKET ) )
                socketSubscribe( topic, inputScanner, ClientConfig.OUTPUT_STREAM, mode );
            else
                rmiSubscribe( topic, ClientConfig.OUTPUT_STREAM, mode );
        } catch ( InterruptedException e ) {
            LOG.log( Level.SEVERE, e.getMessage(), e );
        }
    }

    @Override
    public void start() {
        if ( in != null )
            in.start();
        if ( out != null && ! out.isAlive() )
            out.start();
    }

    /**
     * 
     * @return a new line from the server, or null if the timeout is reached.
     */
    public MessageLine getLine() {
        try {
            return lines.poll( CommonConfig.LINE_TIMEOUT_SECONDS, TimeUnit.SECONDS );
        } catch ( InterruptedException e ) {
            LOG.log( Level.SEVERE, e.getMessage(), e );
            return null;
        }
    }

    private void socketSubscribe( String topic, Scanner inputScanner, PrintStream outputStream, GameMode mode ) throws InterruptedException {
        PrintWriter p = null;
        try {
            socket = new Socket( address, SOCKET_PORT );
            p = new PrintWriter( socket.getOutputStream() );
            p.println( id + " " + topic );
            p.flush();
            this.in = new ClientInThread( socket, outputStream, mode, lines );
            this.out = new ClientOutThread( socket, inputScanner );
        } catch ( UnknownHostException e ) {
            LOG.log( Level.SEVERE, e.getMessage(), e );
            if ( mode.equals( GameMode.GUI ) )
                this.lines.put( new MessageLine( ContextConfig.MSGBOX_CONTEXT, CommonConfig.CONNECTION_FAILURE_MESSAGE + "could not determine the server IP address." ) );
            else
                outputStream.println( CommonConfig.CONNECTION_FAILURE_MESSAGE + "could not determine the server IP address." );
        } catch ( IOException e ) {
            LOG.log( Level.SEVERE, e.getMessage(), e );
            if ( mode.equals( GameMode.GUI ) )
                this.lines.put( new MessageLine( ContextConfig.MSGBOX_CONTEXT, CommonConfig.CONNECTION_FAILURE_MESSAGE + "could not reach the server." ) );
            else
                outputStream.println( CommonConfig.CONNECTION_FAILURE_MESSAGE + "could not reach the server." );
        }
    }
    
    private void rmiSubscribe( String topic, PrintStream outputStream, GameMode mode ) {
        try {
            this.in = new ClientInThread( this, outputStream, mode, lines );
            Registry registry = LocateRegistry.getRegistry( this.address, RMI_PORT );
            RMIRoomConnectionManager mgr = (RMIRoomConnectionManager)registry.lookup( "RoomConnectionManager" );
            mgr.rmiSubscribe( id, topic, (RMIClientIn)UnicastRemoteObject.exportObject( this.in, 0 ) );
        } catch ( NotBoundException | RemoteException e ) {
            LOG.log( Level.SEVERE, e.getMessage(), e );
        }
    }

    /**
     * 
     * @return the MessageGetter associated to the current thread.
     */
    public MessageGetter getMessageGetter() {
        return msgGetter;
    }
    
    /**
     * 
     * @param msgGetter the MessageGetter to associate to the thread.
     */
    public void setMessageGetter( MessageGetter msgGetter ) {
        this.msgGetter = msgGetter;
    }

    /**
     * Method to be used in RMI-based connections.
     * Server-side user input thread is set and started remotely.
     * @param userIn the remote object representing the user's input thread.
     */
    public void setOutThread( RMIUserIn userIn ) {
        this.out = new ClientOutThread( this.inputScanner, userIn );
        out.start();
    }
    
    /**
     * 
     * @return the connection method (Socket or RMI) used in the thread.
     */
    public ConnectionMethod getConnectionMethod() {
        return connectionMethod;
    }
    
}
