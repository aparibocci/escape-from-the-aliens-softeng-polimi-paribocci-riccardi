package it.polimi.ingsw.cg4.networking.server;

import it.polimi.ingsw.cg4.networking.common.ContextConfig;
import it.polimi.ingsw.cg4.networking.common.RMIClientIn;
import it.polimi.ingsw.cg4.networking.common.RMIUserIn;
import it.polimi.ingsw.cg4.player.Player;
import it.polimi.ingsw.cg4.rules.GameConfig;
import it.polimi.ingsw.cg4.rules.EndGameRules;

import java.io.IOException;
import java.net.Socket;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Represents a server-side logical representation of the human players
 * playing the game.
 * Each user has got a name, can access rooms (one room at a time), can be
 * related to a Player of the game and can disconnect from the server.
 * It has got two threads used for client-server comunication.
 *
 */
public class User {

    private static final Logger LOG = Logger.getLogger( Logger.GLOBAL_LOGGER_NAME );
    private Room room;
    private final String name;
    private UserInThread in;
    private UserOutThread out;
    private Player player;
    private boolean flagTest;
    private boolean drawTest;
    private boolean hasDisconnected;
    
    /**
     * Constructor to be used for Socket-based connections.
     * @param name the user's name.
     * @param socket the connection socket.
     */
    public User( String name, Socket socket ) {
        this.name = name;
        this.room = null;
        this.flagTest = false;
        this.drawTest = false;
        this.hasDisconnected = false;
        try {
            this.in = new UserInThread( socket, name, this );
            this.out = new UserOutThread( socket, name );
            this.in.start();
            this.out.start();
        } catch ( IOException e ) {
            LOG.log( Level.SEVERE, e.getMessage(), e );
        }
    }

    /**
     * Constructor to be used for RMI-based connections.
     * @param name the user's name.
     * @param clientIn remote interface corresponding to the client-side
     * input thread object.
     */
    public User( String name, RMIClientIn clientIn ) {
        this.name = name;
        this.room = null;
        this.flagTest = false;
        this.drawTest = false;
        this.hasDisconnected = false;
        this.in = new UserInThread( name, this );
        this.out = new UserOutThread( name, clientIn );
        this.in.start();
        this.out.start();
        try {
            clientIn.setOutThread( (RMIUserIn)UnicastRemoteObject.exportObject( this.in, 0 ) );
        } catch ( RemoteException e ) {
            LOG.log( Level.SEVERE, e.getMessage(), e );
            return;
        }
    }
    
    // Used for test purposes (network is disabled).
    User( String name ) {
        this.name = name;
        this.room = null;
        this.drawTest = false;
    }
    
    /**
     * 
     * @param player the player being associated to the user.
     */
    public void setPlayer( Player player ) {
        this.player = player;
    }
    
    /**
     * Closes the input and output threads.
     */
    public void closeThreads() {
        in.close();
        out.close();
    }
    
    /**
     * 
     * @param room room being associated to the user.
     */
    public void setRoom( Room room ) {
        this.room = room;
    }
    
    /**
     * 
     * @return the user's current room (null if the user does not
     * belong to any room).
     */
    public Room getRoom() {
        return room;
    }
    
    /**
     * 
     * @return the user's name.
     */
    public String getName() {
        return name;
    }
    
    /**
     * 
     * @return true if the user is the administrator of his room,
     * false otherwise.
     */
    public boolean isAdmin() {
        if ( room == null )
            return false;
        return this.room.getAdmin().equals( this );
    }
    
    /**
     * 
     * @return the user's input thread.
     */
    public UserInThread getInThread() {
        if( flagTest )
            return null;
        return in;
    }

    /**
     * 
     * @return the user's output thread.
     */
    public UserOutThread getOutThread() {
        if( flagTest )
            return null;
        return out;
    }

    /**
     * 
     * @return the player associated to the user.
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Kills a player and checks winning conditions.
     * @param p the player to be killed
     */
    private void killPlayer( Player p ) {
        this.getRoom().getGame().addToEventLog( p.getName() + "[" + p.getCharacter().getRace() + "] is dead!" );
        this.getRoom().sendToEveryone( ContextConfig.DEATH_CONTEXT, p.getCharacter().getRace().toString().toLowerCase() );
        p.setState( GameConfig.LOSER_STATE );
        while ( p.getHandSize() > 0 )
            this.getRoom().getGame().getState().getItemDiscard().insertToBottom( p.removeItem( 0 ) );
        if ( EndGameRules.haveAliensWonAfterAttack( this.getRoom().getGame().getState().getPlayers() ) ) {
            for ( Player q : this.getRoom().getGame().getState().getPlayers() ) {
                if ( q.getCharacter().getRace().equals( GameConfig.ALIEN_RACE ) && q.getState().equals( GameConfig.IN_GAME_STATE ) )
                    q.setState( GameConfig.WINNER_STATE );
            }
            if ( this.getRoom().getGame().hasEnded() )
                this.getRoom().endGame();
        } else if ( EndGameRules.haveHumansWonAfterAttack( this.getRoom().getGame().getState().getPlayers() ) ) {
            for ( Player q : this.getRoom().getGame().getState().getPlayers() ) {
                if ( q.getCharacter().getRace().equals( GameConfig.HUMAN_RACE ) && q.getState().equals( GameConfig.IN_GAME_STATE ) )
                    q.setState( GameConfig.WINNER_STATE );
            }
            if ( this.getRoom().getGame().hasEnded() )
                this.getRoom().endGame();
        }
    }

    /**
     * Disconnects the user from the current game.
     * The user is also removed from the room users if a game is not being played.
     * Otherwise, data about the user who left the room is kept until the end of the game.
     */
    public void disconnectFromGame() {
        this.hasDisconnected = true;
        this.getRoom().sendToEveryone( ContextConfig.EVENT_CONTEXT, this.name + " disconnected from the room." );
        if ( this.getRoom().isPlaying() && this.getPlayer().getState().equals( GameConfig.IN_GAME_STATE ) ) {
            this.getRoom().sendToEveryone( ContextConfig.EVENT_CONTEXT, this.name + " is considered to be dead." );
            killPlayer( this.getPlayer() );
        } else {
            this.getRoom().removeUser( this );
        }
    }

    /**
     * 
     * @return true if the user has left the room, false otherwise.
     */
    public boolean hasDisconnected() {
        return hasDisconnected;
    }
    
    // To be used for testing purposes
    boolean isFlagTest() {
        return flagTest;
    }

    // To be used for testing purposes
    void setFlagTest( boolean flagTest ) {
        this.flagTest = flagTest;
    }

    // To be used for testing purposes
    boolean isDrawTest() {
        return drawTest;
    }

    // To be used for testing purposes
    void setDrawTest( boolean drawTest ) {
        this.drawTest = drawTest;
    }
    
}
