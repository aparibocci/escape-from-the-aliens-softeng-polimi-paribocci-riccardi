package it.polimi.ingsw.cg4.networking.client;

import java.io.PrintStream;


/**
 * Utility class used to gather constant values for client-specific configuration.
 *
 */
public class ClientConfig {
    
    // Patterns to be matched for user and connection parameters (regexp notation)
    public static final String USERNAME_PATTERN = "\\w+";
    public static final String ROOM_NAME_PATTERN = "\\w+";
    public static final String IP_PATTERN = "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
    
    // Alien and human race identifiers
    public static final String ALIEN_RACE = "alien";
    public static final String HUMAN_RACE = "human";
    
    // Output and error streams
    public static final PrintStream ERROR_STREAM = System.err;
    public static final PrintStream OUTPUT_STREAM = System.out;
    
    private ClientConfig() {
        
    }
    
}
