package it.polimi.ingsw.cg4.networking.server;

import it.polimi.ingsw.cg4.rules.GameConfig;

/**
 * Utility class used to gather constant values for server-specific configuration.
 *
 */
public class ServerConfig {
    
    // Determines whether to hide, or not, log displaying in the console.
    public static final boolean HIDE_LOG = true;
    
    // Number of lines stored in the user input thread's buffer.
    // (May be kept relatively small using modern technologies).
    public static final int USER_QUEUE_SIZE = 100;
    
    // Room-related values
    public static final int MIN_ROOM_READY = GameConfig.MIN_GAME_PLAYERS;
    public static final int MAX_ROOM_CAPACITY = GameConfig.MAX_GAME_PLAYERS;
    
    private ServerConfig() {
        
    }
    
}
