package it.polimi.ingsw.cg4.networking.common;

import java.io.Serializable;

/**
 * Interface of MessageLine class (to be used in RMI connections).
 *
 */
public interface MessageLineInterface extends Serializable {

    /**
     * 
     * @return the MessageLine's context.
     */
    String getContext();
    
    /**
     * 
     * @return the MessageLine's message.
     */
    String getMessage();
    
}
