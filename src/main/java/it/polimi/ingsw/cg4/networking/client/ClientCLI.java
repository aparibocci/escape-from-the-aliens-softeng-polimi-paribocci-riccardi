package it.polimi.ingsw.cg4.networking.client;


import java.io.IOException;
import java.util.Scanner;

/**
 * Contains the main method that starts the client in CLI mode.
 *
 */
public class ClientCLI {
    
    private ClientCLI() {
        
    }

    /**
     * Client's main method for CLI mode: sets up the logger and asks for
     * user information, connection parameters. Then executes the given
     * connection command.
     * @param args command-line arguments (not used in this context).
     */
    public static void main( String[] args ) {
        try {
            ClientLogger.setup( "game.log" );
        } catch ( SecurityException | IOException e ) {
            ClientConfig.ERROR_STREAM.println( e.getMessage() );
        }
        Scanner input = new Scanner( System.in );
        ClientConfig.OUTPUT_STREAM.print( "Name: " );
        String id = input.nextLine();
        ClientConfig.OUTPUT_STREAM.println( "Server IP address: " );
        String address = input.nextLine();
        ClientConfig.OUTPUT_STREAM.println( "Connection method [ SOCKET / RMI ]:" );
        String method;
        do {
            method = input.nextLine();
            if ( ! method.equalsIgnoreCase( ConnectionMethod.SOCKET.toString() ) && ! method.equalsIgnoreCase( ConnectionMethod.RMI.toString() ) )
                ClientConfig.OUTPUT_STREAM.println( "Please insert a valid method." );
        } while ( ! method.equalsIgnoreCase( ConnectionMethod.SOCKET.toString() ) && ! method.equalsIgnoreCase( ConnectionMethod.RMI.toString() ) );
        ClientConfig.OUTPUT_STREAM.print( "Connection command [ create ROOM_NAME / join ROOM_NAME / autojoin ]: " );
        String topic = input.nextLine();
        ClientThread thread = new ClientThread( id, address, topic, input, ConnectionMethod.valueOf( method.toUpperCase() ), GameMode.CLI );
        thread.start();
    }

}
