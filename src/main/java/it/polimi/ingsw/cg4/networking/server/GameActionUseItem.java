package it.polimi.ingsw.cg4.networking.server;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg4.board.Board;
import it.polimi.ingsw.cg4.board.Coordinate;
import it.polimi.ingsw.cg4.card.ItemCard;
import it.polimi.ingsw.cg4.networking.common.ContextConfig;
import it.polimi.ingsw.cg4.player.Player;
import it.polimi.ingsw.cg4.rules.GameConfig;
import it.polimi.ingsw.cg4.rules.ScoreRules;
import it.polimi.ingsw.cg4.rules.UseItemRules;

/**
 * Class used to represent user use item action.
 *
 */
public class GameActionUseItem extends GameAction {

    private static final Logger LOG = Logger.getLogger( GameActionUseItem.class.getName() );
    private final Player user;
    private final int itemIndex;

    /**
     * Create a new user use item action.
     * @param roomUser the user that is using the item.
     * @param itemIndex the index referring to the user hand.
     */
    public GameActionUseItem( User roomUser, int itemIndex ) {
        super( roomUser );
        this.user = this.getGame().getCurrentPlayer();
        this.itemIndex = itemIndex;
        try {
            user.getItem( itemIndex );
        } catch ( IllegalArgumentException e ) {
            throw e;
        }
    }

    @Override
    public void execute() {
        if ( UseItemRules.canUseItem( user, user.getItem( itemIndex ) ) ) {
            this.getGame().addToEventLog( user.getName() + " uses " + user.getItem( itemIndex ).getName() + "." );
            activateItem();
            user.raiseScore( ScoreRules.USE_ITEM_SCORE );
            this.getGame().getState().getItemDiscard().insertToBottom( user.removeItem( itemIndex ) );
            this.sendToUser( this.getUser(), ContextConfig.REMOVE_ITEM_CONTEXT, Integer.toString( itemIndex ) );
        } else {
            this.sendToUser( this.getUser(),  "You cannot use that item." );
        }
    }
    
    private void activateItem() {
        ItemCard item = user.getItem( itemIndex );
        if ( GameConfig.ADRENALINE_CARD_CLASS.isInstance( item ) ) {
            activateAdrenalineEffect();
        } else if ( GameConfig.ATTACK_CARD_CLASS.isInstance( item ) ) {
            activateAttackEffect();
        } else if ( GameConfig.SEDATIVES_CARD_CLASS.isInstance( item ) ) {
            activateSedativesEffect();
        } else if ( GameConfig.SPOTLIGHT_CARD_CLASS.isInstance( item ) ) {
            activateSpotlightEffect();
        } else if ( GameConfig.TELEPORT_CARD_CLASS.isInstance( item ) ) {
            activateTeleportEffect();
        }
    }

    private void activateAdrenalineEffect() {
        user.getCharacter().activateStatus( GameConfig.ADRENALINE_STATUS );
        this.sendToUser( this.getUser(), ContextConfig.STATUS_ON_CONTEXT, GameConfig.ADRENALINE_STATUS.toString() );
        this.getGame().addToEventLog( user.getName() + " can move faster!" );
        user.refreshReachableSectors();
    }

    private void activateAttackEffect() {
        user.getCharacter().activateStatus( GameConfig.ATTACK_STATUS );
        this.sendToUser( this.getUser(), ContextConfig.STATUS_ON_CONTEXT, GameConfig.ATTACK_STATUS.toString() );
        this.getGame().addToEventLog( user.getName() + " can attack!" );
    }

    private void activateSedativesEffect() {
        user.getCharacter().activateStatus( GameConfig.SEDATIVES_STATUS );
        this.sendToUser( this.getUser(), ContextConfig.STATUS_ON_CONTEXT, GameConfig.SEDATIVES_STATUS.toString() );
        this.getGame().addToEventLog( user.getName() + " is moving silently!" );
    }

    private void activateSpotlightEffect() {
        Coordinate coord = getTargetCoordinate();
        this.getGame().addToEventLog( "Lights near Sector " + coord + "." );
        boolean playerFound = false;
        Set< Coordinate > coordSet = new HashSet< Coordinate >();
        coordSet.add( coord );
        for ( String dir : Coordinate.DIR.keySet() ) {
            Coordinate target = coord.getNeighbor( dir );
            if ( target.getCol() >= 0 && target.getCol() < this.getGame().getState().getBoard().getCols() &&
                    target.getRow() >= 0 && target.getRow() < this.getGame().getState().getBoard().getRows() ) {
                coordSet.add( target );
            }
        }
        for ( Coordinate target : coordSet ) {
            for ( Player p : this.getGame().getState().getPlayers() ) {
                if ( ! p.equals( user ) && p.getSector().getCoordinate().equals( target ) && p.getState().equals( GameConfig.IN_GAME_STATE ) ) {
                    this.getGame().addToEventLog( p.getName() + " is in Sector " + target + "!" );
                    playerFound = true;
                    user.raiseScore( ScoreRules.SPOTLIGHT_SCORE );
                }
            }
        }
        if ( ! playerFound )
            this.getGame().addToEventLog( "But nobody seems to be there..." );
    }

    private Coordinate getTargetCoordinate() {     
        Coordinate coord = null;
        this.sendToUser( user.getUser(), "Choose a sector of the board." );
        this.sendToUser( this.getUser(), ContextConfig.SECTOR_REQUIRED_CONTEXT, "SECTOR_REQUIRED" );
        do {
            try {
                String line;
                if( this.getUser().isFlagTest() ){
                    line = Character.toString( this.getUser().getPlayer().getSector().getCoordinate().toString().charAt( 0 ));
                    line+= " " + this.getUser().getPlayer().getSector().getCoordinate().toString().substring( 1 );
                } else
                    line = this.getUser().getInThread().getLine();
                if ( line != null )
                    coord = readCoordinate( line );
                if ( coord != null && invalidCoords( coord.getRow(), coord.getCol(), this.getGame().getState().getBoard() ) )
                        this.sendToUser( user.getUser(), "Please choose a sector of the board." );
            } catch ( IllegalArgumentException e ) {
                LOG.log( Level.WARNING, e.getMessage(), e );
                this.sendToUser( this.getUser(), e.getMessage() );
            }
        } while ( coord == null || ! this.getGame().getState().getBoard().containsCoordinates( coord ) );
        this.sendToUser( this.getUser(), ContextConfig.MOVEMENT_REQUIRED_CONTEXT, "MOVEMENT_REQUIRED" );
        return coord;
    }

    private Coordinate readCoordinate( String line ) {
        Coordinate coord;
        try {
            coord = Coordinate.parseCoordinate( Arrays.asList( line.split( " " ) ) );
        } catch ( IllegalArgumentException e ) {
            LOG.log( Level.WARNING, e.getMessage(), e );
            coord = null;
        }
        return coord;
    }

    private void activateTeleportEffect() {
        user.setSector( this.getGame().getState().getBoard().getHumanBase() );
        this.sendToUser( user.getUser(), ContextConfig.POSITION_CONTEXT, user.getSector().getCoordinate().toString() );
        user.refreshReachableSectors();
        this.sendToEveryone( ContextConfig.TELEPORT_CONTEXT, "teleport" );
        this.getGame().addToEventLog( user.getName() + " teleports to his base!" );
        this.sendToUser( this.getUser(), ContextConfig.CLI_CONTEXT, this.getGame().playerCurrentCoord() );
    }

    private boolean invalidCoords( int row, int col, Board board ) {
        return row < 0 || row > board.getRows() || col < 0 || col > board.getCols();
    }

}
