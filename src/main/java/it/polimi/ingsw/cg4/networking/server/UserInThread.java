package it.polimi.ingsw.cg4.networking.server;

import it.polimi.ingsw.cg4.networking.common.CommonConfig;
import it.polimi.ingsw.cg4.networking.common.RMIUserIn;

import java.io.IOException;
import java.io.Serializable;
import java.net.Socket;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Thread managing incoming message from the client associated with
 * a server-side user. It is serializable and it implements a remote interface
 * used in RMI connection contexts.
 */
public class UserInThread extends Thread implements RMIUserIn, Serializable {

    private static final long serialVersionUID = 5882441380844838796L;
    private static final Logger LOG = Logger.getLogger( Logger.GLOBAL_LOGGER_NAME );
    private String id;
    private transient Scanner in;
    private final transient BlockingQueue< String > lines;
    private boolean running;
    private final transient User user;

    /**
     * Constructor used in Socket-based connections.
     * @param socket the socket relative to the current connection.
     * @param id the user id (its name).
     * @param user the user linked to the thread.
     * @throws IOException
     */
    public UserInThread( Socket socket, String id, User user ) throws IOException {
        this.id = id;
        this.in = new Scanner( socket.getInputStream() );
        this.lines = new LinkedBlockingQueue< String >( ServerConfig.USER_QUEUE_SIZE );
        this.running = true;
        this.user = user;
    }
    
    /**
     * Constructor used in RMI-based connections.
     * @param id the user id (its name).
     * @param user the user linked to the thread.
     */
    public UserInThread( String id, User user ) {
        this.id = id;
        this.lines = new LinkedBlockingQueue< String >( ServerConfig.USER_QUEUE_SIZE );
        this.running = false;
        this.user = user;
    }
    
    @Override
    public void run() {
        while ( running ) {
            receive();
            try {
                Thread.sleep( 5 );
            } catch ( InterruptedException e ) {
                LOG.log( Level.SEVERE, e.getMessage(), e );
            }
        }
    }
    
    /**
     * 
     * @return the thread's user name.
     */
    public String getUserName() {
        return id;
    }
    
    /**
     * 
     * @return a new line from the client, or null if the timeout is reached.
     */
    public String getLine() {
        try {
            return lines.poll( CommonConfig.LINE_TIMEOUT_SECONDS, TimeUnit.SECONDS );
        } catch ( InterruptedException e ) {
            LOG.log( Level.SEVERE, e.getMessage(), e );
            return null;
        }
    }
    
    // to be used in socket connections
    private String receive() {
        String msg = null;
        try {
            msg = in.nextLine();
        } catch ( NoSuchElementException e ) {
            LOG.log( Level.SEVERE, e.getMessage(), e );
            this.close();
        }
        if ( msg == null )
            this.user.disconnectFromGame();
        if ( msg != null ) {
            try {
                lines.put( msg );
            } catch ( InterruptedException e ) {
                LOG.log( Level.SEVERE, e.getMessage(), e );
            }
        }
        return msg;
    }
    
    // to be used in RMI connections
    @Override
    public void receive( String message ) {
        try {
            lines.put( message );
        } catch ( InterruptedException e ) {
            LOG.log( Level.SEVERE, e.getMessage(), e );
        }
    }

    /**
     * Closes the thread.
     */
    public void close() {
        running = false;
        if ( in != null ) {
            in.close();
            in = null;
        }
    }

}
