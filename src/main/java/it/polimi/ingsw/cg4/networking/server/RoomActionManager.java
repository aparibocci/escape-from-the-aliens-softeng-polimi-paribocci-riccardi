package it.polimi.ingsw.cg4.networking.server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


class RoomActionManager {

    private static final Map< String, String > ROOM_COMMANDS = new HashMap< String, String >();
    static {
        ROOM_COMMANDS.put( "chat", "chat <text> - sends a new message to everyone with <text> as content." );
        ROOM_COMMANDS.put( "help", "help - shows the valid commands." );
        ROOM_COMMANDS.put( "logout", "logout - ends the current session and disconnects from server." );
        ROOM_COMMANDS.put( "start", "start <map_name> - [ADMIN ONLY] starts a new game with map <map_name> [ Fermi | Galilei | Galvani ]." );
        ROOM_COMMANDS.put( "users", "users - displays the room user list." );
    }

    private final User user;
    private final UserActionThread thread;

    /**
     * 
     * @param user the user who performs the actions;
     * @param thread the UserActionThread related to the user.
     */
    public RoomActionManager( User user, UserActionThread thread  ) {
        this.user = user;
        this.thread = thread;
    }

    /**
     * 
     * @param args the arguments of the command to be parsed.
     * @param cmd the original command (in String format).
     * @return the action corrisponding to the command, or null if no corresponding action
     * is present.
     */
    public Action parseRoomAction( List< String > args, String cmd ) {
        String cmdType = args.get( 0 );
        if ( ROOM_COMMANDS.containsKey( cmdType ) ) {
            if ( args.size() > 1 && "chat".equals( cmdType ) ) {
                return commandRoomChat( args, cmd );
            } else if ( args.size() == 1 && "help".equals( cmdType ) ) {
                return commandRoomHelp();
            } else if ( args.size() == 1 && "logout".equals( cmdType ) ) {
                return commandRoomLogout();
            } else if ( args.size() == 2 && "start".equals( cmdType ) ){
                return commandRoomStart( args );
            } else if ( args.size() == 1 && "users".equals( cmdType ) ) {
                return commandRoomUsers();
            }
        }
        return null;
    }

    /**
     *
     * @param args the command arguments.
     * @param cmd the original command (in String format).
     * @return the RoomActionChat action.
     */
    private Action commandRoomChat( List< String > args, String cmd ) {
        String text = cmd.substring( args.get( 0 ).length() + 1 );
        return new RoomActionChat( user, text );
    }

    /**
     *
     * @return the RoomActionHelp action.
     */
    private Action commandRoomHelp() {
        return new RoomActionHelp( user, new ArrayList< String >( ROOM_COMMANDS.values() ) );
    }

    /**
     * Disconnects the user and closes its action thread.
     * @return the RoomActionLogout action.
     * @see UserActionThread
     */
    private Action commandRoomLogout() {
        RoomAction logoutAction = new RoomActionLogout( user );
        if ( logoutAction != null )
            thread.close();
        return logoutAction;
    }

    /**
     * Starts the game with the given parameters.
     * @param args the command arguments.
     * @return the RoomActionStart action.
     */
    private Action commandRoomStart( List< String > args ) {
        return new RoomActionStart( user, args.get( 1 ) );
    }


    /**
     * Lists all the users in the room.
     * @return the RoomActionUsers action.
     */
    private Action commandRoomUsers() {
        return new RoomActionUsers( user );
    }
    
}
