package it.polimi.ingsw.cg4.networking.server;

/**
 * Class used to represent user chat action in the game context.    
 *
 */
public class GameActionChat extends GameAction {

    private final String text;

    /**
     * Create a new user chat action.
     * @param user the user that is sending a chat message.
     * @param text the text being sent by the user.
     */
    public GameActionChat( User user, String text ) {
        super( user );
        this.text = text;
    }

    @Override
    public void execute() {
        String line = this.getUser().getName() + " > " + text;
        this.getGame().getChat().add( line );
        this.sendToEveryone( line );
    }

}
