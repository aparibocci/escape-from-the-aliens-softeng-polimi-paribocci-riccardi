package it.polimi.ingsw.cg4.networking.server;

import it.polimi.ingsw.cg4.networking.common.ContextConfig;
import it.polimi.ingsw.cg4.player.character.CharacterStatus;
import it.polimi.ingsw.cg4.rules.GameConfig;
import it.polimi.ingsw.cg4.rules.EndTurnRules;
import it.polimi.ingsw.cg4.rules.ScoreRules;

/**
 * Class used to represent user end turn action.
 *
 */
public class GameActionEndTurn extends GameAction {

    /**
     * Create a new user end turn action.
     * @param user the user that ended his turn.
     */
    public GameActionEndTurn( User user ) {
        super( user );
    }

    @Override
    public void execute() {
        if ( EndTurnRules.canEndTurn( this.getGame().getCurrentPlayer() ) ) {
            processPlayerEndTurn();
            if(this.getGame().getCurrentPlayer().getCharacter().getRace().equals( GameConfig.ALIEN_RACE ))
                this.getGame().getCurrentPlayer().raiseScore( ScoreRules.ALIEN_TURN_SCORE );
            else
                this.getGame().getCurrentPlayer().raiseScore( ScoreRules.HUMAN_TURN_SCORE );
            this.getGame().nextPlayer();
            this.getGame().getCurrentPlayer().refreshReachableSectors();
            if ( this.getUser().getRoom().getGame().hasEnded() )
                this.getUser().getRoom().endGame();
        } else {
            this.sendToUser( this.getUser(),  "You must move before ending your turn." );
        }
    }
    
    private void processPlayerEndTurn() {
        this.getGame().getCurrentPlayer().setHasAttacked( false );
        for ( CharacterStatus status : EndTurnRules.ET_RESET_STATUSES ) {
            this.getGame().getCurrentPlayer().getCharacter().deactivateStatus( status );
            this.sendToUser( this.getUser(), ContextConfig.STATUS_OFF_CONTEXT, status.toString() );
        }
        this.getGame().getCurrentPlayer().resetMovedSectors();
        this.getGame().getCurrentPlayer().addCoord( this.getGame().getCurrentPlayer().getSector().getCoordinate() );
    }

}
