package it.polimi.ingsw.cg4.networking.server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


class EndingActionManager {
    
    private static final Map< String, String > ENDING_COMMANDS = new HashMap< String, String >();
    static {
        ENDING_COMMANDS.put( "chat", "chat <text> - sends a new message to everyone with <text> as content." );
        ENDING_COMMANDS.put( "help", "help - shows the valid commands." );
        ENDING_COMMANDS.put( "log", "log - shows the event log for the finished game." );
        ENDING_COMMANDS.put( "logout", "logout - ends the current session and disconnects from server." );
    }

    private final User user;
    private final UserActionThread thread;

    public EndingActionManager( User user, UserActionThread thread  ) {
        this.user = user;
        this.thread = thread;
    }
    
    public Action parseEndingAction( List< String > args, String cmd ) {
        String cmdType = args.get( 0 );
        if ( ENDING_COMMANDS.containsKey( cmdType ) ) {
            if ( args.size() > 1 && "chat".equals( cmdType ) ){
                return commandEndingChat( args, cmd );
            } else if ( args.size() == 1 && "help".equals( cmdType ) ) {
                return commandEndingHelp();
            } else if ( args.size() == 1 && "log".equals( cmdType ) ) {
                return commandEndingLog();
            } else if ( args.size() == 1 && "logout".equals( cmdType ) ) {
                return commandEndingLogout();
            }
        }
        return null;
    }
    
    private Action commandEndingChat( List< String > args, String cmd ) {
        String text = cmd.substring( args.get( 0 ).length() + 1 );
        return new RoomActionChat( user, text );
    }

    private Action commandEndingHelp() {
        return new RoomActionHelp( user, new ArrayList< String >( ENDING_COMMANDS.values() ) );
    }

    private Action commandEndingLog() {
        return new GameActionLog( user );
    }

    private Action commandEndingLogout() {
        RoomAction logoutAction = new RoomActionLogout( user );
        if ( logoutAction != null )
            thread.close();
        return logoutAction;
    }
    
}
