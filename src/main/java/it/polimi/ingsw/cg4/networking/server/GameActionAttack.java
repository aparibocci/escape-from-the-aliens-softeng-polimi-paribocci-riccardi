package it.polimi.ingsw.cg4.networking.server;

import it.polimi.ingsw.cg4.card.CardCreator;
import it.polimi.ingsw.cg4.card.ItemCard;
import it.polimi.ingsw.cg4.networking.common.ContextConfig;
import it.polimi.ingsw.cg4.player.Player;
import it.polimi.ingsw.cg4.rules.AttackRules;
import it.polimi.ingsw.cg4.rules.GameConfig;
import it.polimi.ingsw.cg4.rules.EndGameRules;
import it.polimi.ingsw.cg4.rules.MovementRules;
import it.polimi.ingsw.cg4.rules.ScoreRules;

/**
 * Class used to represent user attack action.
 * 
 *@see GameAction
 */
public class GameActionAttack extends GameAction {

    private final Player attacker;

    /**
     * Create a new user attack action.
     * 
     * @param user the user who performs the action.
     */
    public GameActionAttack( User user ) {
        super( user );
        this.attacker = this.getGame().getCurrentPlayer();
    }    

    @Override
    public void execute() {
        if ( AttackRules.canAttack( attacker ) ) {
            attacker.incMovedSectors( MovementRules.getMovementRange( attacker ) - attacker.getMovedSectors() );
            attacker.refreshReachableSectors();
            this.getGame().addToEventLog( attacker.getName() + " attacks in " + attacker.getSector().getCoordinate() + "." );
            this.sendToEveryone( ContextConfig.ATTACK_CONTEXT, attacker.getCharacter().getRace().toString().toLowerCase() );
            boolean attackFailed = true;
            for ( Player p : this.getGame().getState().getPlayers() ) {
                if ( !p.equals( attacker ) && attacker.getSector().contains( this.getGame().getState().getBoard(), p )
                        && p.getState().equals( GameConfig.IN_GAME_STATE ) ) {
                    if ( AttackRules.canDefend( p ) ) {
                        activateDefense( p );
                    } else {
                        killPlayer( p );
                        attackFailed = false;
                        if ( attacker.getCharacter().getRace() .equals( GameConfig.HUMAN_RACE ) && p.getCharacter().getRace().equals( GameConfig.ALIEN_RACE ) ) {
                            attacker.raiseScore( ScoreRules.HUMAN_KILL_SCORE );
                            p.raiseScore( ScoreRules.DEFEAT_SCORE );
                        }
                        if ( attacker.getCharacter().getRace().equals( GameConfig.ALIEN_RACE ) ) {
                            if ( p.getCharacter().getRace().equals( GameConfig.ALIEN_RACE ) ){
                                p.raiseScore( ScoreRules.DEFEAT_SCORE );
                                attacker.raiseScore( ScoreRules.ALIEN_KILL_ALIEN_SCORE );
                            }
                            else {
                                attacker.raiseScore( ScoreRules.ALIEN_KILL_SCORE );
                                p.raiseScore( ScoreRules.DEFEAT_SCORE );
                            }
                        }
                    }
                }
            }
            attacker.setHasAttacked( true );
            if(attackFailed)
                attacker.raiseScore( ScoreRules.ALIEN_FAIL_ATTACK_SCORE );
        } else {
            this.sendToUser( this.getUser(), "You cannot attack now." );
        }
    }

    /**
     * Used to activate the card defense.
     * 
     * @param p the player who activates the card.
     */
    private void activateDefense( Player p ) {
        CardCreator creator = new CardCreator();
        this.getGame().addToEventLog( p.getName() + " defends himself!" );
        int defenseIndex = p.getItemIndex( (ItemCard)creator.create( "Defense" ) );
        this.getGame().getState().getItemDiscard().insertToBottom( p.removeItem( defenseIndex ) );
        this.sendToUser( p.getUser(), ContextConfig.REMOVE_ITEM_CONTEXT, Integer.toString( defenseIndex ) );
        if ( p.getItemIndex( (ItemCard)creator.create( "Defense" ) ) == -1 ) {
            p.getCharacter().deactivateStatus( AttackRules.DEFENSE_STATUS );
            this.sendToUser( p.getUser(), ContextConfig.STATUS_OFF_CONTEXT, AttackRules.DEFENSE_STATUS.toString() );
        }
    }

    /**
     * Used to manage the events that occur when the attacker kills a player.
     * 
     * @param p the player being killed.
     */
    private void killPlayer( Player p ) {
        this.getGame().addToEventLog( p.getName() + "[" + p.getCharacter().getRace() + "] is dead!" );
        this.sendToEveryone( ContextConfig.DEATH_CONTEXT, p.getCharacter().getRace().toString().toLowerCase() );
        p.setState( GameConfig.LOSER_STATE );
        discardHand( p );
        if ( AttackRules.canBecomeVoracious( attacker, p ) ) {
            attacker.getCharacter().activateStatus( AttackRules.VORACIOUS_STATUS );
            this.sendToUser( attacker.getUser(), ContextConfig.STATUS_ON_CONTEXT, AttackRules.VORACIOUS_STATUS.toString() );
            this.getGame().addToEventLog( attacker.getName() + " is now voracious!" );
            checkAlienVictory();
        } else {
            checkHumanVictory();
        }
    }

    private void discardHand( Player p ) {
        while ( p.getHandSize() > 0 ) {
            this.getGame().getState().getItemDiscard()
                    .insertToBottom( p.removeItem( 0 ) );
        }
    }

    private void checkAlienVictory() {
        if ( EndGameRules.haveAliensWonAfterAttack( this.getGame().getState()
                .getPlayers() ) ) {
            for ( Player q : this.getGame().getState().getPlayers() ) {
                if ( q.getCharacter().getRace().equals( GameConfig.ALIEN_RACE )
                        && q.getState().equals( GameConfig.IN_GAME_STATE ) ){
                    q.setState( GameConfig.WINNER_STATE );
                    q.raiseScore( ScoreRules.ALIEN_VICTORY_SCORE );
                }    
            }
            if ( attacker.getUser().getRoom().getGame().hasEnded() )
                attacker.getUser().getRoom().endGame();
        }
    }

    private void checkHumanVictory() {
        if ( EndGameRules.haveHumansWonAfterAttack( this.getGame().getState()
                .getPlayers() ) ) {
            for ( Player q : this.getGame().getState().getPlayers() ) {
                if ( q.getCharacter().getRace().equals( GameConfig.HUMAN_RACE )
                        && q.getState().equals( GameConfig.IN_GAME_STATE ) ){
                    q.setState( GameConfig.WINNER_STATE );
                    q.raiseScore( ScoreRules.HUMAN_VICTORY_SCORE );
                }    
            }
            if ( attacker.getUser().getRoom().getGame().hasEnded() )
                attacker.getUser().getRoom().endGame();
        }
    }

}
