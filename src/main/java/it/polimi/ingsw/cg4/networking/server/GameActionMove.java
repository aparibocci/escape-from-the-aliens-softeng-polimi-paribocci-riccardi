package it.polimi.ingsw.cg4.networking.server;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg4.board.Board;
import it.polimi.ingsw.cg4.board.Coordinate;
import it.polimi.ingsw.cg4.board.Sector;
import it.polimi.ingsw.cg4.card.EscapeHatchCard;
import it.polimi.ingsw.cg4.card.ItemCard;
import it.polimi.ingsw.cg4.card.SectorCard;
import it.polimi.ingsw.cg4.card.deck.CardDeck;
import it.polimi.ingsw.cg4.networking.common.ContextConfig;
import it.polimi.ingsw.cg4.player.Player;
import it.polimi.ingsw.cg4.rules.AttackRules;
import it.polimi.ingsw.cg4.rules.GameConfig;
import it.polimi.ingsw.cg4.rules.EndGameRules;
import it.polimi.ingsw.cg4.rules.MovementRules;
import it.polimi.ingsw.cg4.rules.ScoreRules;
import it.polimi.ingsw.cg4.rules.UseItemRules;

/**
 * Class used to represent user move action.
 *
 */
public class GameActionMove extends GameAction {

    private static final Logger LOG = Logger.getLogger( GameActionMove.class.getName() );
    private final Player player;
    private final Sector dest;

    /**
     * Create a new user move action.
     * @param user the user that is performing the moviment.
     * @param dest the movement destination.
     */
    public GameActionMove( User user, Sector dest ) {
        super( user );
        this.player = this.getGame().getCurrentPlayer();
        this.dest = dest;
    }

    @Override
    public void execute() {
        if ( MovementRules.canMoveTo( player, dest ) ) {
            movePlayer();
            this.sendToUser( player.getUser(), ContextConfig.POSITION_CONTEXT, dest.getCoordinate().toString() );
            if ( dest.getType().equals( GameConfig.DANGEROUS_SECTOR ) && ! player.getCharacter().isAffectedBy( GameConfig.SEDATIVES_STATUS ) )
                dangerousSectorMovement();
            else if ( dest.getType().equals( GameConfig.OPEN_HATCH_SECTOR ) ){
                escapeHatchSectorMovement();
                player.raiseScore( ScoreRules.ESCAPE_HATCH_OPEN_SCORE );
            }
            player.refreshReachableSectors();
            this.sendToUser( player.getUser(), ContextConfig.CLI_CONTEXT, player.getUser().getRoom().getGame().toString() );
        } else {
            this.sendToUser( this.getUser(), "You cannot move in that Sector." );
        }
    }
    
    private void movePlayer() {
        int movedSectors = Sector.distanceBetween( player.getSector(), dest );
        if ( MovementRules.UNIQUE_MOVEMENT_PER_TURN )
            player.incMovedSectors( MovementRules.getMovementRange( player ) - player.getMovedSectors() );
        else
            player.incMovedSectors( movedSectors );
        player.setSector( dest );
    }
    
    private void dangerousSectorMovement() {
        boolean decidedToAttack = false;
        if ( AttackRules.canAttack( player ) )
            decidedToAttack = chooseWhetherAttack();
        if ( decidedToAttack ) {
            new GameActionAttack( this.getUser() ).execute();
        } else {
            this.getUser().getPlayer().setHasAttacked( true );
            SectorCard card = (SectorCard)this.getGame().getState().getSectorDeck().draw();
            this.getGame().addToEventLog( this.getUser().getName() + " draws a Sector Card..." );
            String name = "sector_" + card.getName();
            if ( card.hasItemIcon() )
                name += "_item";
            this.sendToUser( this.getUser(), ContextConfig.NEW_CARD_CONTEXT, name );
            if ( card.hasItemIcon() ) {
                if ( player.getHandSize() == GameConfig.MAX_HAND_SIZE ) {
                    int i = getItemToDiscard();
                    this.getGame().getState().getItemDiscard().insertToBottom( player.removeItem( i ) );
                    this.getGame().addToEventLog( this.getUser().getName() + " discards an Item Card..." );
                    this.sendToUser( this.getUser(), ContextConfig.REMOVE_ITEM_CONTEXT, Integer.toString( i ) );
                }
                ItemCard item = (ItemCard)this.getGame().getState().getItemDeck().draw();
                this.sendToUser( this.getUser(), ContextConfig.NEW_CARD_CONTEXT, "item_" + item.getName() );
                this.getGame().addToEventLog( this.getUser().getName() + " draws an Item Card..." );
                this.sendToUser( this.getUser(),  "You drew the Item Card " + item.getName() + "." );
                player.addItem( item );
                this.sendToUser( this.getUser(), ContextConfig.DRAW_ITEM_CONTEXT, this.getUser().getPlayer().getHandSize()-1 + " " + item.getName().replace( " ", ContextConfig.MSG_SEPARATOR  ) + " " + item.getDescription().replace( " ", ContextConfig.MSG_SEPARATOR ) );
                if ( player.getCharacter().getRace().equals( UseItemRules.USE_ITEM_RACE ) && GameConfig.DEFENSE_CARD_CLASS.isInstance( item ) ) {
                    player.getCharacter().activateStatus( AttackRules.DEFENSE_STATUS );
                    this.sendToUser( this.getUser(), ContextConfig.STATUS_ON_CONTEXT, AttackRules.DEFENSE_STATUS.toString() );
                }
                checkEmptyDeck( this.getGame().getState().getItemDeck(), this.getGame().getState().getItemDiscard() );
            }
            activateSectorEffect( card );
            this.getGame().getState().getSectorDiscard().insertToBottom( card );
            checkEmptyDeck( this.getGame().getState().getSectorDeck(), this.getGame().getState().getSectorDiscard() );
        }
    }
    
    private boolean chooseWhetherAttack() {
        if ( this.getUser().isFlagTest() )
            return false;
        this.sendToUser( this.getUser(), ContextConfig.CLI_CONTEXT, "You are in a Dangerous Sector." );
        this.sendToUser( this.getUser(), ContextConfig.CLI_CONTEXT, "Do you want to draw a Dangerous Sector card? You can choose to attack, instead." );
        this.sendToUser( this.getUser(), ContextConfig.CLI_CONTEXT, "[Y: draw, N: attack]" );
        this.sendToUser( this.getUser(), ContextConfig.DANGEROUS_SECTOR_CONTEXT, "DANGEROUS" );
        String decidedToAttack = "N";
        do {
            try {
                decidedToAttack = this.getUser().getInThread().getLine().substring( 0, 1 );
            } catch ( IllegalArgumentException e ) {
                LOG.log( Level.WARNING, e.getMessage(), e );
                this.sendToUser( this.getUser(), e.getMessage() );
            } catch ( NullPointerException e ) {
                LOG.log( Level.INFO, e.getMessage(), e );
                decidedToAttack = null;
            }
            if ( decidedToAttack != null && ! "Y".equalsIgnoreCase( decidedToAttack ) && ! "N".equalsIgnoreCase( decidedToAttack ) ) {
                this.sendToUser( this.getUser(), ContextConfig.CLI_CONTEXT, "Please insert a valid choice. [Y: draw, N: attack]" );
            }
        } while ( ! "Y".equalsIgnoreCase( decidedToAttack ) && ! "N".equalsIgnoreCase( decidedToAttack ) );
        if ( "Y".equalsIgnoreCase( decidedToAttack ) )
            return false;
        return true;
    }

    private int getItemToDiscard() {
        if ( this.getUser().isFlagTest() )
            return 0;
        int i = -1;
        this.sendToUser( this.getUser(), ContextConfig.ITEM_DISCARD_CONTEXT, "DISCARD" );
        this.sendToUser( player.getUser(), "Your hand is full. Choose card to discard [0.." + (player.getHandSize()-1) + "]" );
        do {
            try {
                i = Integer.parseInt( player.getUser().getInThread().getLine() );
            } catch ( NumberFormatException e ) {
                LOG.log( Level.WARNING, e.getMessage(), e );
            }
            if ( i < 0 || i >= player.getHandSize() )
                this.sendToUser( this.getUser(), ContextConfig.CLI_CONTEXT, "Please insert a valid index." );
        } while ( i < 0 || i >= player.getHandSize() );
        return i;
    }

    private void escapeHatchSectorMovement() {
        EscapeHatchCard card = (EscapeHatchCard)this.getGame().getState().getEscapeHatchDeck().draw();
        this.getGame().addToEventLog( this.getUser().getName() + " draws an Escape Hatch Card..." );
        this.sendToUser( this.getUser(), ContextConfig.NEW_CARD_CONTEXT, "hatch_" +card.getName() );
        activateEscapeHatchEffect( card );
        this.getGame().getState().getEscapeHatchDiscard().insertToBottom( card );
        checkEmptyDeck( this.getGame().getState().getEscapeHatchDeck(), this.getGame().getState().getEscapeHatchDiscard() );
    }
    
    private void activateSectorEffect( SectorCard card ) {
        if ( GameConfig.NOISE_CARD_CLASS.isInstance( card ) ) {
            this.sendToEveryone( ContextConfig.NOISE_CONTEXT, "noise" );
            this.getGame().addToEventLog( player.getName() + ": noise in Sector " + dest.getCoordinate() + "." );
        } else if ( GameConfig.NOISE_ANY_CARD_CLASS.isInstance( card ) ) {
            Sector noiseSector = getNoiseSectorChoice();
            this.sendToEveryone( ContextConfig.NOISE_CONTEXT, "noise" );
            this.getGame().addToEventLog( player.getName() + ": noise in Sector " + noiseSector.getCoordinate() + "." );
        } else if ( GameConfig.SILENCE_CARD_CLASS.isInstance( card ) ) {
            this.sendToEveryone( ContextConfig.NOISE_CONTEXT, "silence" );
            this.getGame().addToEventLog( player.getName() + ": silence..." );
        }
    }
    
    private Sector getNoiseSectorChoice() {
        Coordinate coord = null;
        this.sendToUser( this.getUser(), ContextConfig.EVENT_CONTEXT, "You drew the Noise in Any Sector card." );
        this.sendToUser( this.getUser(), ContextConfig.EVENT_CONTEXT, "Choose a sector of the board." );
        this.sendToUser( this.getUser(), ContextConfig.SECTOR_REQUIRED_CONTEXT, "SECTOR_REQUIRED" );
        do {
            try {
                String line;
                if( this.getUser().isFlagTest() ){
                    line = Character.toString( this.getUser().getPlayer().getSector().getCoordinate().toString().charAt( 0 ));
                    line += " " + this.getUser().getPlayer().getSector().getCoordinate().toString().substring( 1 );
                }
                else
                    line = this.getUser().getInThread().getLine();
                if ( line != null )
                    coord = readCoordinate( line );
                if ( coord != null && invalidCoords( coord.getRow(), coord.getCol(), this.getGame().getState().getBoard() ) )
                        this.sendToUser( player.getUser(), "Please choose a sector of the board." );
            } catch ( IllegalArgumentException e ) {
                LOG.log( Level.WARNING, e.getMessage(), e );
                this.sendToUser( this.getUser(), e.getMessage() );
            }
        } while ( coord == null || ! this.getGame().getState().getBoard().containsCoordinates( coord ) );
        this.sendToUser( this.getUser(), ContextConfig.MOVEMENT_REQUIRED_CONTEXT, "MOVEMENT_REQUIRED" );
        return this.getUser().getRoom().getGame().getState().getBoard().getSector( coord );
    }

    private void activateEscapeHatchEffect( EscapeHatchCard card ) {
        dest.close();
        if ( GameConfig.GREEN_HATCH_CARD_CLASS.isInstance( card ) ) {
            this.getGame().addToEventLog( player.getName() + " escapes using the hatch in " + dest.getCoordinate() + "!" );
            this.player.setState( GameConfig.WINNER_STATE );
            checkHumanVictory();
        } else {
            this.getGame().addToEventLog( player.getName() + " tries to escape using the hatch in " + dest.getCoordinate() + ", but it is damaged!" );
        }
        this.getGame().addToEventLog( "The hatch in " + dest.getCoordinate() + " is now closed and cannot be used." );
    }
    
    private void checkEmptyDeck( CardDeck main, CardDeck discard ) {
        if ( main.isEmpty() ) {
            main.insertAllToBottom( discard.drawAll() );
            main.shuffle();
        }
    }
    
    private void checkHumanVictory() {
        if ( EndGameRules.haveAlienLostAfterEscape( this.getGame().getState().getPlayers() ) ) {
            for ( Player p : this.getGame().getState().getPlayers() ) {
                if ( p.getState().equals( GameConfig.IN_GAME_STATE ) ) {
                    p.setState( GameConfig.LOSER_STATE );
                }
            }
            if ( player.getUser().getRoom().getGame().hasEnded() )
                player.getUser().getRoom().endGame();
        }
    }

    private boolean invalidCoords( int row, int col, Board board ) {
        return row < 0 || row > board.getRows() || col < 0 || col > board.getCols();
    }

    private Coordinate readCoordinate( String line ) {
        Coordinate coord;
        try {
            coord = Coordinate.parseCoordinate( Arrays.asList( line.split( " " ) ) );
        } catch ( IllegalArgumentException e ) {
            LOG.log( Level.WARNING, e.getMessage(), e );
            coord = null;
        }
        return coord;
    }

}
