package it.polimi.ingsw.cg4.networking.server;

import it.polimi.ingsw.cg4.networking.common.ContextConfig;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Thread used to get user action requests, parse them using the apposite
 * parsers and finally execute them.
 *
 */
class UserActionThread extends Thread {
    
    private final User user;
    private final RoomActionManager roomManager;
    private final PlayingActionManager playingManager;
    private final EndingActionManager endingManager;
    private boolean running;

    /**
     * Initializes the room action manager, game action manager and
     * end game action manager.
     * @param user the user to be associated to the thread.
     */
    public UserActionThread( User user ) {
        this.user = user;
        this.roomManager = new RoomActionManager( user, this );
        this.playingManager = new PlayingActionManager( user );
        this.endingManager = new EndingActionManager( user, this );
        running = true;
    }
    
    @Override
    public void run() {
        while ( running ) {
            Action action = null;
            String cmd = user.getInThread().getLine();
            if ( cmd != null && ! cmd.isEmpty() ) {
                action = parseAction( cmd );
                if ( action != null ) {
                    action.execute();
                } else {
                    user.getRoom().sendToUser( user, ContextConfig.CLI_CONTEXT, "Command not valid (use \"help\" to see the valid commands)." );
                    user.getRoom().sendToUser( user, ContextConfig.MSGBOX_CONTEXT, "Command not valid." );
                }
            }
        }
    }
    
    private Action parseAction( String cmd ) {
        List< String > args = new ArrayList< String >();
        args = Arrays.asList( cmd.split( " " ) );
        if ( user.getRoom().isPreparing() )
            return roomManager.parseRoomAction( args, cmd );
        else if ( user.getRoom().isPlaying() ) {
            return playingManager.parsePlayingAction( args, cmd );
        } else if ( user.getRoom().isGameEnded() ) {
            return endingManager.parseEndingAction( args, cmd );
        }
        return null;
    }
    
    /**
     * Stops and closes the thread.
     */
    public void close() {
        running = false;
    }
    
}
