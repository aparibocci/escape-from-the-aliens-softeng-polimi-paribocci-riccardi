package it.polimi.ingsw.cg4.networking.server;

import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class used to setup the server logger, allowing file-logging of
 * events and errors).
 */
public class ServerLogger {
    
    private static FileHandler fileTxt;

    private ServerLogger() {
        
    }
    
    /**
     * Creates the logger with the given file name.
     * If the server configuration is set to hide console errors,
     * removes the console handler from the logger handlers.
     * @param fileName the name of the log file to be memorized.
     * @throws IOException
     */
    public static void setup( String fileName ) throws IOException {
        Logger logger = Logger.getLogger( Logger.GLOBAL_LOGGER_NAME );
        Logger rootLogger = Logger.getLogger( "" );
        Handler[] handlers = rootLogger.getHandlers();
        if ( ServerConfig.HIDE_LOG && handlers[0] instanceof ConsoleHandler )
                rootLogger.removeHandler( handlers[0] );
        logger.setLevel( Level.INFO );
        fileTxt = new FileHandler( fileName );
        logger.addHandler( fileTxt );
    }
    
}