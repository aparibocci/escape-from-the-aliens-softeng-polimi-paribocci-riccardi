package it.polimi.ingsw.cg4.networking.server;


enum RoomState {

    NOT_READY, READY, PLAYING, GAME_ENDED
    
}
