package it.polimi.ingsw.cg4.networking.server;

import it.polimi.ingsw.cg4.networking.common.ContextConfig;

import java.util.List;

/** 
 * Class used to represent user users game action in the room context. 
 *
 */
public class RoomActionUsers extends RoomAction {

    /**
     * Create a new users action.
     * @param user the user that is asking for the user list.
     */
    public RoomActionUsers( User user ) {
        super( user );
    }

    @Override
    public void execute() {
        List< User > roomUsers = this.getUser().getRoom().getUsers();
        for ( User user : roomUsers ) {
            String msg = user.getName();
            if ( this.getUser().getRoom().getAdmin().equals( user ) )
                msg += " [ADMIN]";
            this.sendToUser( this.getUser(), ContextConfig.USER_LIST_CONTEXT, msg );
        }
    }

}
