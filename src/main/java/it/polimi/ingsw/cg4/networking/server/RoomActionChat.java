package it.polimi.ingsw.cg4.networking.server;

/**
 * Class used to represent user chat action in the room context.
 *
 */
public class RoomActionChat extends RoomAction {

    private String text;

    /**
     * Create a new user chat action.
     * @param user the user that is sending a chat message.
     * @param text the text being sent by the user.
     */
    public RoomActionChat( User user, String text ) {
        super( user );
        this.text = text;
    }

    @Override
    public void execute() {
        this.sendToEveryone( this.getUser().getName() + " > " + text );
    }

}
