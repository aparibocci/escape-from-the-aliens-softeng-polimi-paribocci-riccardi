package it.polimi.ingsw.cg4.networking.server;

import it.polimi.ingsw.cg4.networking.common.CommonConfig;

/**
 * Class used to represent user logout action in the room context. 
 *
 */
public class RoomActionLogout extends RoomAction {

    /**
     * Create a new user logout action.
     * @param user the user that is disconnecting.
     */
    public RoomActionLogout( User user ) {
        super( user );
    }

    @Override
    public void execute() {
        this.getUser().getOutThread().send( CommonConfig.CONNECTION_FAILURE_MESSAGE + "disconnected from server." );
        if ( ! this.getUser().getRoom().isPlaying() )
            this.getUser().getRoom().removeUser( this.getUser() );
        else
            this.getUser().disconnectFromGame();
        this.getUser().closeThreads();
    }

}
