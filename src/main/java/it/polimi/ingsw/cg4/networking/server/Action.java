package it.polimi.ingsw.cg4.networking.server;


/**
 * Interface used to represent a User's action.
 *
 */
public interface Action {

    /**
     * executes the Action
     */
    void execute();
    
    /**
     * Sends a message to a user.
     * @param dest the user who receives the message.
     * @param message the text message to be sent.
     */
    void sendToUser( User dest, String message );

    /**
     * Sends a MessageLine to a user.
     * @param context the message's context.
     * @param message the message's text.
     */
    void sendToUser( User dest, String context, String message );

    /**
     * Sends a message to every user in the room.
     * @param message the text message to be sent.
     */
    void sendToEveryone( String message );

    /**
     * Sends a MessageLine to every user in the room.
     * @param context the message's context.
     * @param message the message's text.
     */
    void sendToEveryone( String context, String message );
    
}
