package it.polimi.ingsw.cg4.networking.common;

import it.polimi.ingsw.cg4.networking.common.MessageLineInterface;

import java.util.ArrayList;
import java.util.List;

/**
 * Class representing a message line (containing a context and
 * the message text) to be used in comunication between client and server.
 *
 */
public class MessageLine implements MessageLineInterface {
    
    private static final long serialVersionUID = -7792332223819332238L;
    private static final String SEPARATOR = CommonConfig.MSG_SEPARATOR;
    private static final String DEFAULT_CONTEXT = CommonConfig.DEFAULT_CONTEXT;
    
    private final String context;
    private final String message;
    
    /**
     * Given a context and the message text, builds the message line.
     * @param context the message context.
     * @param text the message text.
     */
    public MessageLine( String context, String text ) {
        this.context = context;
        this.message = text;
    }

    /**
     * Given some text, builds the message line using the default context.
     * @param text the message text.
     */
    public MessageLine( String text ) {
        int breakIndex = text.indexOf( SEPARATOR.charAt( 0 ) );
        List< String > list = new ArrayList< String >();
        if ( breakIndex == -1 ) {
            this.context = DEFAULT_CONTEXT;
            this.message = text;
            return;
        }
        list.add( text.substring( 0, breakIndex ) );
        list.add( text.substring( breakIndex+1 ) );
        if ( list.size() > 1 ) {
            this.context = list.get( 0 );
            this.message = list.get( 1 );
        } else {
            this.context = DEFAULT_CONTEXT;
            this.message = list.get( 0 );
        }
    }
    
    @Override
    public String getContext() {
        return context;
    }

    @Override
    public String getMessage() {
        return message;
    }
    
    @Override
    public String toString() {
        return this.context + SEPARATOR + this.message;
    }
    
}
