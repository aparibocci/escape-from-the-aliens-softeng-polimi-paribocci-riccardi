package it.polimi.ingsw.cg4.networking.server;

import it.polimi.ingsw.cg4.networking.common.ContextConfig;

/**
 * Class used to represent user log action.
 *
 */
public class GameActionLog extends GameAction {
    
    /**
     * Create a new user log action.
     * @param user the user that is requiring the game log.
     */
    public GameActionLog( User user ) {
        super( user );
    }

    @Override
    public void execute() {
        this.sendToUser( this.getUser(), ContextConfig.EVENT_CONTEXT, this.getGame().getEventLog().toString() );
    }

}
