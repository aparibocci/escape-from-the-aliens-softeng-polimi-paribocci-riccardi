package it.polimi.ingsw.cg4.networking.server;

import it.polimi.ingsw.cg4.board.Board;
import it.polimi.ingsw.cg4.card.deck.CardDeck;
import it.polimi.ingsw.cg4.gamedata.GameUtilities;
import it.polimi.ingsw.cg4.gamedata.Game;
import it.polimi.ingsw.cg4.networking.common.ContextConfig;
import it.polimi.ingsw.cg4.networking.common.MessageLine;
import it.polimi.ingsw.cg4.player.Player;







import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * In the game context, a Room is composed by Users and is capable
 * of hosting a game, from the preparation state (when the users
 * join the room) to the game state, ending with the game ending state.
 * Users can freely join the room (if it is not full) and leave it.
 * 
 * Every non-empty room has got an administrator (which
 * is a User), who is given the task to start the game.
 * If the administrator leaves the room, another User is given the title.
 * 
 * This class also features methods for message sending (both to every user
 * or to a specific one).
 */
public class Room {

    private static final int MIN_READY = ServerConfig.MIN_ROOM_READY;
    private static final int MAX_SIZE = ServerConfig.MAX_ROOM_CAPACITY;
    private final String name;
    private final List< User > users;
    private User admin;
    private RoomState state;
    private Game game;
    private boolean flagTest ;
    private final RoomConnectionManager manager;

    /**
     * Room 
     * @param manager the RoomConnectionManager related to the room.
     * @param name the room name.
     * @param creator the user who is creating the room.
     */
    public Room( RoomConnectionManager manager, String name, User creator ) {
        this.name = name;
        this.users = new ArrayList< User >();
        this.state = RoomState.NOT_READY;
        this.admin = creator;
        this.manager = manager;
        this.addUser( creator );
        flagTest = false;
    }
    
    // Package-visible constructor, to be used in test
    Room( String name, User creator ) {
        this.name = name;
        this.users = new ArrayList< User >();
        this.state = RoomState.NOT_READY;
        this.admin = creator;
        this.manager = null;
        this.addUserTest( creator );
        flagTest = true;
    }

    /**
     * 
     * @return the room administrator.
     */
    public User getAdmin() {
        return admin;
    }
    
    /**
     * 
     * @return the room's name.
     */
    public String getName() {
        return name;
    }
    
    /**
     * 
     * @return the list of Users in the room.
     */
    public List< User > getUsers() {
        return Collections.unmodifiableList( users );
    }

    /**
     * Adds a new user to the room (only if it is not full or if it does not
     * already contain a user with the same name as the user wanting to join).
     * @param user the User being added.
     */
    public void addUser( User user ) {
        if ( this.isFull() )
            throw new IllegalStateException( "Room " + name + " is full." );
        for ( User u : users )
            if ( u.getName().equals( user.getName() ) )
                throw new IllegalStateException( "Room " + name + " already contains user " + user.getName() );
        users.add( user );
        user.setRoom( this );
        new UserActionThread( user ).start();
        sendToEveryone( ContextConfig.ROOM_CONTEXT, "Player " + user.getName() + " has joined the room. There are now " + users.size() + " players in the room." );
        sendToUser( user, ContextConfig.USER_JOIN_CONTEXT, "USER JOINED" );
        sendToEveryone( ContextConfig.USER_JOIN_CONTEXT, user.getName() );
        if ( this.state.equals( RoomState.NOT_READY ) && users.size() == MIN_READY ) {
            this.state = RoomState.READY;
            sendToEveryone( ContextConfig.ROOM_CONTEXT, "Game at room " + name + " can be started." );
        }
    }
    
    /**
     * Removes a user from the room.
     * If he is an administrator, gives administrator capabilities
     * to the next oldest user (by joining time).
     * @param user the User being removed.
     */
    public void removeUser( User user ) {
        if ( ! this.contains( user ) )
            throw new IllegalArgumentException( "Room " + name + " does not contain " + user.getName() );
        users.remove( user );
        if ( users.isEmpty() ) {
            this.manager.removeRoom( this.name );
        } else {
            if ( this.isPreparing() ) {
                this.sendToEveryone( ContextConfig.ROOM_CONTEXT, user.getName() + " has left the room. There are now " + users.size() + " players in the room." );
                this.sendToEveryone( ContextConfig.USER_ROOM_LOGOUT_CONTEXT, "USER LOGOUT" );
            }
            if ( this.admin.equals( user ) ) {
                this.admin = users.get( 0 );
                if ( this.isPreparing() )
                    this.sendToEveryone( ContextConfig.ROOM_CONTEXT, this.admin.getName() + " has became the new room admin." );
            }
        }
    }
    
    /**
     * 
     * @param user the user to check.
     * @return true if the room contains the user, false otherwise.
     */
    public boolean contains( User user ) {
        return users.contains( user );
    }

    /**
     * 
     * @return true if the room does not contain any user, false otherwise.
     */
    public boolean isEmpty() {
        return users.isEmpty();
    }
    
    /**
     * 
     * @return true if the room has reached its maximum size, false otherwise.
     */
    public boolean isFull() {
        return users.size() >= MAX_SIZE;
    }

    /**
     * Sends a MessageLine to a user of the room, using a given context.
     * @param user the message destination.
     * @param context the context of the MessageLine to be sent.
     * @param msg the text of the MessageLine to be sent.
     */
    public void sendToUser( User user, String context, String msg ) {
        if ( this.contains( user ) && !flagTest )
            user.getOutThread().dispatchMessage( new MessageLine( context, msg ) );
    }

    /**
     * Sends a MessageLine to a user of the room, using the default context.
     * @param user the message destination.
     * @param msg the text of the MessageLine to be sent.
     */
    public void sendToUser( User user, String msg ) {
        sendToUser( user, ContextConfig.DEFAULT_MSG_CONTEXT, msg );
    }

    /**
     * Sends a MessageLine to every user of the room, using a given context.
     * @param context the context of the MessageLine to be sent.
     * @param msg the text of the MessageLine to be sent.
     */
    public void sendToEveryone( String context, String message ) {
        if(!flagTest){
            for ( User user : users )
                user.getOutThread().dispatchMessage( new MessageLine( context, message ) );
            
        }
    }

    /**
     * Sends a MessageLine to every user of the room, using the default context.
     * @param user the message destination.
     * @param msg the text of the MessageLine to be sent.
     */
    public void sendToEveryone( String msg ) {
        sendToEveryone( ContextConfig.DEFAULT_MSG_CONTEXT, msg );
    }

    /**
     * 
     * @return true if the room is in the preparation phase (not in game), false otherwise.
     */
    public boolean isPreparing() {
        return this.state.equals( RoomState.READY ) || this.state.equals( RoomState.NOT_READY );
    }

    /**
     * 
     * @return true if the room is in the preparation phase (not in game)
     * and it contains a sufficient number of players for starting a new game,
     * false otherwise.
     */
    public boolean isReady() {
        return this.state.equals( RoomState.READY );
    }
    
    /**
     * 
     * @return true if the room is currently hosting a game, false otherwise.
     */
    public boolean isPlaying() {
        return this.state.equals( RoomState.PLAYING );
    }
    
    /**
     * 
     * @return true if the room's game has ended, false otherwise.
     */
    public boolean isGameEnded() {
        return this.state.equals( RoomState.GAME_ENDED );
    }

    /**
     * Starts a new game in the room, setting up players and races,
     * creating the board, the decks and setting players positions.
     * The first turn starts.
     * @param mapName the name of the chosen map for the new game.
     */
    public void startGame( String mapName ) {
        List< Player > players = createPlayers();
        Board board = GameUtilities.createBoard( mapName );
        List< CardDeck > decks = GameUtilities.createDecks();
        GameUtilities.setPlayersRaces( players );
        GameUtilities.setPlayersPosition( board, players );
        for ( Player p : players ) {
            this.sendToUser( p.getUser(), ContextConfig.INFO_CONTEXT, this.getName() + " " + p.getName() + " " + p.getCharacter().getRace() );
            this.sendToUser( p.getUser(), ContextConfig.POSITION_CONTEXT, p.getSector().getCoordinate().toString() );
        }
        this.game = new Game( board, players, decks );
        this.state = RoomState.PLAYING;
        this.game.addToEventLog( "The game has started." );
        this.game.addToEventLog( game.getCurrentPlayer().getName() + " will start first!" );
        this.game.nextTurn();
        this.game.addToEventLog( game.getCurrentPlayer().getName() + "'s turn starts." );
        this.game.startTurnTimer();
        this.sendToUser( this.game.getCurrentPlayer().getUser(), ContextConfig.MSGBOX_CONTEXT, "Your turn has started." );
        this.sendToUser( this.game.getCurrentPlayer().getUser(), ContextConfig.CLI_CONTEXT, this.game.toString() );
    }
    
    /**
     * Ends the current game (to be called when ending conditions are met).
     */
    public void endGame() {
        this.game.endGameReport();
        this.state = RoomState.GAME_ENDED;
    }
    
    /**
     * Assigns a player to every user of the room.
     * @return the list of players.
     */
    private List< Player > createPlayers() {
        List< Player > players = new ArrayList< Player >();
        for ( User user : users ) {
            user.setPlayer( new Player( user ) );
            players.add( user.getPlayer() );
        }
        return players;
    }
    
    /**
     * 
     * @return the Game associated with the room.
     */
    public Game getGame() {
        return game;
    }
    
    // method used for test purpose.
    void addUserTest( User user ) {
        if ( users.size() > MAX_SIZE )
            throw new IllegalStateException( "Room " + name + " is full." );
        for ( User u : users )
            if ( u.getName().equals( user.getName() ) )
                throw new IllegalStateException( "Room " + name + " already contains user " + user.getName() );
        users.add( user );
        user.setRoom( this );
    }
    
}
