package it.polimi.ingsw.cg4.networking.client;

import it.polimi.ingsw.cg4.networking.common.CommonConfig;
import it.polimi.ingsw.cg4.networking.common.ContextConfig;
import it.polimi.ingsw.cg4.networking.common.MessageLine;
import it.polimi.ingsw.cg4.networking.common.MessageLineInterface;
import it.polimi.ingsw.cg4.networking.common.RMIClientIn;
import it.polimi.ingsw.cg4.networking.common.RMIUserIn;

import java.io.IOException;
import java.io.PrintStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Thread managing incoming message from the server.
 * It is serializable and it implements a remote interface used
 * in RMI connection contexts.
 */
public class ClientInThread extends Thread implements RMIClientIn, Serializable {
    
    private static final long serialVersionUID = 3016023907788113189L;
    private static final Logger LOG = Logger.getLogger( Logger.GLOBAL_LOGGER_NAME );
    private transient Scanner in;
    private boolean running;
    private final transient PrintStream outputStream;
    private final GameMode mode;
    private final transient BlockingQueue< MessageLine > lines;
    private transient ClientThread thread;
    
    /**
     * Constructor used in Socket-based connections.
     * @param socket the connection's socket.
     * @param outputStream the client's output stream.
     * @param mode the client's game mode (CLI or GUI).
     * @param lines the lines buffer where incoming messages will be put.
     */
    public ClientInThread( Socket socket, PrintStream outputStream, GameMode mode, BlockingQueue< MessageLine > lines ) {
        try {
            this.in = new Scanner( socket.getInputStream() );
        } catch ( IOException e ) {
            LOG.log( Level.SEVERE, e.getMessage(), e );
        }
        this.mode = mode;
        this.outputStream = outputStream;
        this.lines = lines;
        this.running = true;
    }
    
    /**
     * Constructor used in Socket-based connections.
     * @param thread the main client thread.
     * @param outputStream the client's output stream.
     * @param mode the client's game mode (CLI or GUI).
     * @param lines the lines buffer where incoming messages will be put.
     */
    public ClientInThread( ClientThread thread, PrintStream outputStream, GameMode mode, BlockingQueue< MessageLine > lines ) {
        this.outputStream = outputStream;
        this.mode = mode;
        this.lines = lines;
        this.running = false;
        this.thread = thread;
    }

    @Override
    public void run() {
        while ( running ) {
            receive();
            try {
                Thread.sleep( 5 );
            } catch ( InterruptedException e ) {
                LOG.log( Level.SEVERE, e.getMessage(), e );
            }

        }
    }

    // to be used in Socket connections
    private MessageLine receive() {
        MessageLine line = null;
        if ( running ) {
            try {
                line = new MessageLine( in.nextLine() );
            } catch ( NoSuchElementException e ) {
                LOG.log( Level.SEVERE, e.getMessage(), e );
                this.close();
                return null;
            } catch ( IllegalArgumentException e ) {
                LOG.log( Level.WARNING, e.getMessage(), e );
                return null;
            }
            if ( mode.equals( GameMode.CLI ) && ! ContextConfig.SYS_CONTEXT.contains( line.getContext() ) ) {
                outputStream.println( line.getMessage() );
            } else {
                if ( line.getMessage().isEmpty() && line.getContext().equals( ContextConfig.DEFAULT_MSG_CONTEXT ) )
                    return null;
                try {
                    lines.put( line );
                } catch ( InterruptedException e ) {
                    LOG.log( Level.SEVERE, e.getMessage(), e );
                }
                if ( line.getMessage().startsWith( CommonConfig.CONNECTION_FAILURE_MESSAGE ) )
                    this.close();
            }
        }
        return line;
    }

    // to be used in RMI connections
    @Override
    public void receive( MessageLineInterface line ) {
        MessageLine l = (MessageLine) line;
        if ( mode.equals( GameMode.CLI ) && ! ContextConfig.SYS_CONTEXT.contains( line.getContext() ) ) {
            outputStream.println( l.getMessage() );
        } else {
            try {
                lines.put( l );
            } catch ( InterruptedException e ) {
                LOG.log( Level.SEVERE, e.getMessage(), e );
            }
        }
    }

    @Override
    public void setOutThread( RMIUserIn userIn ) {
        thread.setOutThread( userIn );
    }

    /**
     * Closes the thread.
     */
    public void close() {
        running = false;
        in.close();
        in = null;
    }
}
