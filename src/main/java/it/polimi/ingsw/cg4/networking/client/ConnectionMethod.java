package it.polimi.ingsw.cg4.networking.client;

/**
 * Enumeration identifying the connection method (Socket or RMI)
 */
public enum ConnectionMethod {

    RMI, SOCKET
    
}
