package it.polimi.ingsw.cg4.networking.server;

import it.polimi.ingsw.cg4.gamedata.Game;
import it.polimi.ingsw.cg4.networking.common.ContextConfig;

/**
 * Class used to represent a game Action.
 * It is abstract ( only its subclasses can be instantiated ).
 *
 */

public abstract class GameAction implements Action {

    private final User user;

    /**
     * Used to create a GameAction.
     * 
     * @param user the user who performs the action in the game context.
     */
    protected GameAction( User user ) {
        this.user = user;
    }

    
    /**
     * 
     * @return the User who performs the action.
     */
    public User getUser() {
        return user;
    }
    
    /**
     * 
     * @return the Game related to the room.
     */
    public Game getGame() {
        return user.getRoom().getGame();
    }
    
    @Override
    public void sendToUser( User dest, String message ) {
        this.sendToUser( dest, ContextConfig.GAME_CONTEXT, message );
    }
    
    @Override
    public void sendToUser( User dest, String context, String message ) {
        this.getUser().getRoom().sendToUser( dest, context, message );
    }
    
    @Override
    public void sendToEveryone( String message ) {
        this.sendToEveryone( ContextConfig.GAME_CONTEXT, message );
    }
    
    @Override
    public void sendToEveryone( String context, String message ) {
        this.getUser().getRoom().sendToEveryone( context, message );
    }

}
