package it.polimi.ingsw.cg4.networking.common;

import java.rmi.Remote;

import java.rmi.RemoteException;
/**
 * Inteface of ClientIn class (to be used in RMI connections).
 *
 */
public interface RMIClientIn extends Remote {
    /**
     * To be used by the server to remotely execute a message reception.
     * @param line the message.
     * @throws RemoteException
     */
    void receive( MessageLineInterface line ) throws RemoteException;
    
    /**
     * To be used by the server to remotely set the destination of 
     * output messages sent by the client (the thread OutThread).
     * @param userIn user that represent destination of message.
     * @throws RemoteException
     */
    void setOutThread( RMIUserIn userIn ) throws RemoteException;
    
}
