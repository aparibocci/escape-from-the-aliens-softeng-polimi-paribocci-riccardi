package it.polimi.ingsw.cg4.gui.application.scene.endgame;

import java.util.Arrays;

import javax.swing.BoxLayout;

import it.polimi.ingsw.cg4.gui.template.BaseCommandPanel;


class EndGameCommandPanel extends BaseCommandPanel {

    private static final long serialVersionUID = 1883863673276866429L;
    
    private final EndGameLogButton logButton;
    private final EndGameTitleButton titleButton;
    
    /**
     * Creates the end game command panel for the end game scene.
     * Buttons are created and displayed.
     */
    public EndGameCommandPanel() {
        super( BoxLayout.X_AXIS );
        this.logButton = new EndGameLogButton();
        this.titleButton = new EndGameTitleButton();
        this.setComponents( Arrays.asList( logButton, titleButton ) );
    }
    
    /**
     * 
     * @return the event log button.
     */
    public EndGameLogButton getLogButton() {
        return logButton;
    }
    
    /**
     * 
     * @return the "to title" button.
     */
    public EndGameTitleButton getTitleButton() {
        return titleButton;
    }

}
