package it.polimi.ingsw.cg4.gui.application.scene.endgame;

import java.util.EventObject;

/**
 * Defines an event that signals the display of the end game scene panel.
 */
public class EndGameSceneShownEvent extends EventObject {

    private static final long serialVersionUID = -8169321537709473090L;

    /**
     * 
     * @param source the object that sent the event.
     */
    public EndGameSceneShownEvent( Object source ) {
        super( source );
    }

}
