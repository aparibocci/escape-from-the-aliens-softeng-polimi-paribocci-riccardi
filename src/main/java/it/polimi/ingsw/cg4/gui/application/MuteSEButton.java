package it.polimi.ingsw.cg4.gui.application;

import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseButton;

class MuteSEButton extends BaseButton {
    
    private static final long serialVersionUID = -1813049681362991978L;

    /**
     * Creates a new sound effects mute button.
     * Some of the properties are specified in the configuration classes.
     */
    public MuteSEButton() {
        super( SceneConfig.SETTINGS_SE_ON_TEXT );
        this.setToolTipText( "Enables/disables sound effects" );
    }
}