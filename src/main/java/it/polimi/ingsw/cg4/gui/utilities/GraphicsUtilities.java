package it.polimi.ingsw.cg4.gui.utilities;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg4.gui.config.GraphicsConfig;
import it.polimi.ingsw.cg4.networking.common.MessageLine;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 * Utility class containing static methods to be called in order to
 * display message boxes or read fonts and images from files.
 */
public class GraphicsUtilities {

    private static final Logger LOG = Logger.getLogger( Logger.GLOBAL_LOGGER_NAME );

    private GraphicsUtilities() {
        
    }
    
    /**
     * Creates a new font, given the font name and size.
     * @param name the font's file name.
     * @param size the font size (pixels).
     * @return the font if it could be loaded, null otherwise.
     */
    public static Font createFont( String name, float size ) {
        File file = new File( GraphicsConfig.FONTS_DIR_NAME + File.separatorChar + name + GraphicsConfig.FONTS_EXTENSION );
        try {
            return Font.createFont( Font.TRUETYPE_FONT, file ).deriveFont( size );
        } catch ( FontFormatException | IOException e ) {
            LOG.log( Level.SEVERE, e.getMessage(), e );
            return null;
        }
    }
    
    /**
     * Creates a new image icon, given the file name.
     * @param fileName the image's file name.
     * @return the image icon associated with the image, or null if it
     * could not be loaded.
     */
    public static ImageIcon createImageIcon( String fileName ) {
        try {
            return new ImageIcon( ImageIO.read( new File( GraphicsConfig.GRAPHICS_DIR_NAME + File.separatorChar + fileName ) ) );
        } catch ( IOException e ) {
            LOG.log( Level.SEVERE, e.getMessage(), e );
            return null;
        }
    }

    /**
     * Shows a new informative message box.
     * @param line the line being shown.
     */
    public static void showMessage( MessageLine line ) {
        String s = line.getMessage();
        JOptionPane.showMessageDialog( null, s, "Message", JOptionPane.INFORMATION_MESSAGE );
    }

    /**
     * Shows a new success message box.
     * @param line the line being shown.
     */
    public static void showSuccessMessage( MessageLine line ) {
        String s = line.getMessage();
        JOptionPane.showMessageDialog( null, s, "Success", JOptionPane.INFORMATION_MESSAGE );
    }

    /**
     * Shows a new error message box.
     * @param line the line being shown.
     */
    public static void showError( MessageLine line ) {
        String s = line.getMessage();
        JOptionPane.showMessageDialog( null, s, "Error", JOptionPane.ERROR_MESSAGE );
    }

    /**
     * Shows a new confirm message dialog.
     * @param line the line being shown.
     * @return the clicked option.
     */
    public static int showConfirmDialog( String s ) {
        return JOptionPane.showConfirmDialog( null, s, "Message", JOptionPane.YES_NO_OPTION );
    }

    /**
     * Shows a message box with a given card image.
     * @param line the message line, containing information about the card to show.
     */
    public static void showCardDrawMessage( MessageLine line ) {
        int index = line.getMessage().indexOf( "_" );
        String s = "You have drawn the card " + line.getMessage().substring( index+1 ).split( "_" )[0] + ".";
        ImageIcon icon = GraphicsUtilities.createImageIcon( line.getMessage().replace( " ", "_" ).toLowerCase() + GraphicsConfig.CARD_EXTENSION );
        JOptionPane.showMessageDialog( null, s, "Card", JOptionPane.INFORMATION_MESSAGE, icon );
    }
    
}
