package it.polimi.ingsw.cg4.gui.application.scene.connection;

import java.util.Arrays;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;

import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseCommandPanel;
import it.polimi.ingsw.cg4.gui.template.BaseRadioButton;


class ConnectionMethodPanel extends BaseCommandPanel {

    private static final long serialVersionUID = 5154445013361383962L;
    private final BaseRadioButton socketRadio;
    private final BaseRadioButton rmiRadio;

    protected ConnectionMethodPanel() {
        super( BoxLayout.X_AXIS );
        this.socketRadio = new BaseRadioButton( SceneConfig.CONNECTION_SOCKET_TEXT );
        socketRadio.setActionCommand( socketRadio.getText() );
        this.rmiRadio = new BaseRadioButton( SceneConfig.CONNECTION_RMI_TEXT );
        rmiRadio.setActionCommand( rmiRadio.getText() );
        ButtonGroup group = new ButtonGroup();
        group.add( socketRadio );
        group.add( rmiRadio );
        socketRadio.setSelected( true );
        this.setComponents( Arrays.asList( socketRadio, rmiRadio ) );
    }

    /**
     * 
     * @return the socket radio button.
     */
    public BaseRadioButton getSocketRadio() {
        return socketRadio;
    }
    
    /**
     * 
     * @return the RMI radio button.
     */
    public BaseRadioButton getRMIRadio() {
        return rmiRadio;
    }

}
