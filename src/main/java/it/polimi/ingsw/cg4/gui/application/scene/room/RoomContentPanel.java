package it.polimi.ingsw.cg4.gui.application.scene.room;

import java.awt.BorderLayout;
import it.polimi.ingsw.cg4.gui.template.BaseListPanel;
import it.polimi.ingsw.cg4.gui.template.BasePanel;

class RoomContentPanel extends BasePanel {

    private static final long serialVersionUID = -7573319375969489072L;
    private final RoomEventPanel eventPanel;
    private final BaseListPanel usersPanel;
    private final RoomCommandPanel commandPanel;

    /**
     * Creates the room content panel
     * and sets the graphical components.
     */
    public RoomContentPanel() {
        super();
        this.eventPanel = new RoomEventPanel();
        this.usersPanel = new BaseListPanel();
        this.commandPanel = new RoomCommandPanel();
        this.addComponents();
    }
    
    /**
     * 
     * @return the event panel.
     */
    public RoomEventPanel getEventPanel() {
        return eventPanel;
    }
    
    /**
     * 
     * @return the command panel.
     */
    public RoomCommandPanel getCommandPanel() {
        return commandPanel;
    }
    
    /**
     * 
     * @return the users panel.
     */
    public BaseListPanel getUsersPanel() {
        return usersPanel;
    }
    
    private void addComponents() {
        this.add( eventPanel, BorderLayout.CENTER );
        this.add( usersPanel, BorderLayout.EAST );
        this.add( commandPanel, BorderLayout.SOUTH );
    }

}
