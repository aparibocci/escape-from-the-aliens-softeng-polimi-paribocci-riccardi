package it.polimi.ingsw.cg4.gui.application.scene.title;

import java.util.EventObject;

/**
 * Defines an event that signals the display of the title scene panel.
 */
public class TitleSceneShownEvent extends EventObject {

    private static final long serialVersionUID = -8169321537709473090L;

    /**
     * 
     * @param source the object that sent the event.
     */
    public TitleSceneShownEvent( Object source ) {
        super( source );
    }

}
