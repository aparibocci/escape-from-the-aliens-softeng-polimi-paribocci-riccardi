package it.polimi.ingsw.cg4.gui.application.scene.title;

import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseButton;

class TitleExitButton extends BaseButton {

    private static final long serialVersionUID = -1227428266373807503L;

    /**
     * Creates a new exit button for the title scene.
     * Some of the properties are specified in the configuration classes.
     */
    public TitleExitButton() {
        super( SceneConfig.TITLE_EXIT_TEXT );
    }

}
