package it.polimi.ingsw.cg4.gui.template;

import it.polimi.ingsw.cg4.gui.config.TemplateConfig;

import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 * Represents an array of graphical components horizontally or vertically.
 */
public class BaseCommandPanel extends JPanel {

    private static final long serialVersionUID = -3060259650511406026L;
    private static final int DEFAULT_PADDING = TemplateConfig.COMMAND_PANELS_DEFAULT_PADDING;
    
    /**
     * Creates a command panel.
     * Specific properties are set using configuration classes.
     * @param axis the axis (horizontal or vertical), from BoxLayout.
     */
    public BaseCommandPanel( int axis ) {
        this.setLayout( new BoxLayout( this, axis ) );
        this.setOpaque( false );
    }
    
    /**
     * Adds the components to the list of elements displayed, with some strut
     * between them.
     * @param components the list of components to be added.
     */
    public void setComponents( List< ? extends JComponent > components ) {
        this.addStrut();
        for ( JComponent component : components ) {
            this.add( component );
            this.addStrut();
        }
    }
    
    private void addStrut() {
        this.add( Box.createVerticalStrut( DEFAULT_PADDING ) );
        this.add( Box.createHorizontalStrut( DEFAULT_PADDING ) );
    }
    
}
