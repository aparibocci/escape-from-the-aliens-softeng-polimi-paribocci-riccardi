package it.polimi.ingsw.cg4.gui.application.scene.game;

import java.awt.Dimension;

import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseEventPanel;


class GameEventPanel extends BaseEventPanel {

    private static final long serialVersionUID = -2183198340844374192L;

    /**
     * Creates a new instance of the game event panel, which works both as
     * event display panel and chat.
     */
    public GameEventPanel() {
        super();
        this.setPreferredSize( new Dimension( SceneConfig.GAME_EVENT_PANEL_WIDTH, SceneConfig.GAME_EVENT_PANEL_HEIGHT ) );
    }
    
}
