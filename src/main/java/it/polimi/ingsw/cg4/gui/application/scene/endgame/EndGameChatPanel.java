package it.polimi.ingsw.cg4.gui.application.scene.endgame;

import java.awt.Dimension;

import it.polimi.ingsw.cg4.gui.template.BaseEventPanel;


class EndGameChatPanel extends BaseEventPanel {

    private static final long serialVersionUID = -2183198340844374192L;

    /**
     * Defines the chat panel for the end game screen.
     */
    public EndGameChatPanel() {
        super();
        this.setPreferredSize( new Dimension( 350, 300 ) );
    }
    
}
