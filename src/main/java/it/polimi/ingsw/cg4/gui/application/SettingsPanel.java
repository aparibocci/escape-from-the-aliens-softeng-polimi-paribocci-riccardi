package it.polimi.ingsw.cg4.gui.application;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.BoxLayout;

import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseButton;
import it.polimi.ingsw.cg4.gui.template.BaseCommandPanel;
import it.polimi.ingsw.cg4.gui.template.TwoStateLabel;

/**
 * Class defining a settings panel: it contains a network status label
 * and audio volume controls.
 */
public class SettingsPanel extends BaseCommandPanel {

    private static final long serialVersionUID = 1133715476600703663L;
    private final TwoStateLabel onlineLabel;

    protected SettingsPanel( MainFrame frame ) {
        super( BoxLayout.X_AXIS );
        MuteBGMButton muteBGMButton = new MuteBGMButton();
        MuteSEButton muteSEButton = new MuteSEButton();
        this.onlineLabel = new TwoStateLabel( SceneConfig.OFFLINE_TEXT, SceneConfig.OFFLINE_COLOR, SceneConfig.ONLINE_TEXT, SceneConfig.ONLINE_COLOR );
        onlineLabel.setFont( muteBGMButton.getFont() );
        muteBGMButton.addActionListener( new MuteBGMButtonListener( muteBGMButton, frame ) );
        muteSEButton.addActionListener( new MuteSEButtonListener( muteSEButton, frame ) );
        this.setComponents( Arrays.asList( onlineLabel, muteBGMButton, muteSEButton ) );
    }
    
    /**
     * 
     * @return the label relative to the online/offline information.
     */
    public TwoStateLabel getOnlineLabel() {
        return onlineLabel;
    }
    
    private class MuteSEButtonListener implements ActionListener {
        
        private final MainFrame frame;
        private final BaseButton button;

        /**
         * 
         * @param button the button relative to the listener.
         * @param frame the frame containing the button.
         */
        public MuteSEButtonListener( BaseButton button, MainFrame frame ) {
            this.button = button;
            this.frame = frame;
        }

        @Override
        public void actionPerformed( ActionEvent e ) {
            frame.getAudioManager().changeMuteSEControl();
            if ( button.getText().equals( SceneConfig.SETTINGS_SE_ON_TEXT ) )
                button.setText( SceneConfig.SETTINGS_SE_OFF_TEXT );
            else
                button.setText( SceneConfig.SETTINGS_SE_ON_TEXT );
        }
    }

    private class MuteBGMButtonListener implements ActionListener {
    
        private final MainFrame frame;
        private final BaseButton button;

        /**
         * 
         * @param button the button relative to the listener.
         * @param frame the frame containing the button.
         */
        public MuteBGMButtonListener( BaseButton button, MainFrame frame ) {
            this.button = button;
            this.frame = frame;
        }
    
        @Override
        public void actionPerformed( ActionEvent e ) {
            frame.getAudioManager().changeMuteBGMControl();
            if ( button.getText().equals( SceneConfig.SETTINGS_BGM_ON_TEXT ) )
                button.setText( SceneConfig.SETTINGS_BGM_OFF_TEXT );
            else
                button.setText( SceneConfig.SETTINGS_BGM_ON_TEXT );
        }
    }

}
