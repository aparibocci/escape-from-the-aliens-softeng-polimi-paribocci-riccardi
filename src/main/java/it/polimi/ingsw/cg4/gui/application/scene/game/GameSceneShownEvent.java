package it.polimi.ingsw.cg4.gui.application.scene.game;

import java.util.EventObject;

/**
 * Defines an event that signals the display of the game scene panel.
 */
public class GameSceneShownEvent extends EventObject {

    private static final long serialVersionUID = -4419024803811849829L;

    /**
     * 
     * @param source the object that sent the event.
     */
    public GameSceneShownEvent( Object source ) {
        super( source );
    }

}
