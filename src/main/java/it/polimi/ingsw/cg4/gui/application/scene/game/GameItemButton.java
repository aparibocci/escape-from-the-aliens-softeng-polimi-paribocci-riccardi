package it.polimi.ingsw.cg4.gui.application.scene.game;

import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseButton;


class GameItemButton extends BaseButton {

    private static final long serialVersionUID = -3304311499587231486L;
    private static final String EMPTY_TEXT = SceneConfig.GAME_NO_ITEM_TEXT;
    private static final String EMPTY_DESCRIPTION = null;
    
    /**
     * Button capable of containing informations about owned item cards.
     */
    public GameItemButton() {
        super( EMPTY_TEXT );
        this.removeInfos();
    }
    
    private void setItemName( String name ) {
        this.setText( name );
    }
    
    private void setItemDescription( String description ) {
        this.setToolTipText( description );
    }

    /**
     * 
     * @param name the item card name.
     * @param description the item card description.
     */
    public void setInfos( String name, String description ) {
        this.setItemName( name );
        this.setItemDescription( description );
        this.setEnabled( true );
    }

    /**
     * Clears the current informations.
     */
    public void removeInfos() {
        this.setItemName( EMPTY_TEXT );
        this.setItemDescription( EMPTY_DESCRIPTION );
        this.setEnabled( false );
    }

    /**
     * 
     * @return true if no item is associated with the button, false
     * otherwise.
     */
    public boolean isEmpty() {
        return ! this.isEnabled();
    }

}
