package it.polimi.ingsw.cg4.gui.template;

import java.awt.BorderLayout;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import it.polimi.ingsw.cg4.gui.config.GraphicsConfig;
import it.polimi.ingsw.cg4.gui.config.TemplateConfig;
import it.polimi.ingsw.cg4.gui.template.BasePanel;

/**
 * Implements a base list panel extending the BasePanel class.
 * It is useful to build lists of lines representing events.
 */
public class BaseListPanel extends BasePanel {

    private static final long serialVersionUID = -2183198340844324192L;
    private final JScrollPane eventPane;
    private final JList< String > list;
    private final DefaultListModel< String > listModel;

    /**
     * List panel constructor:
     * defines a new empty listmodel and set some properties using
     * configuration classes values.
     */
    public BaseListPanel() {
        super();
        listModel = new DefaultListModel< String >();
        this.list = new JList< String >( listModel );
        this.list.setFont( GraphicsConfig.LIST_FONT );
        this.list.setForeground( TemplateConfig.LIST_FONT_COLOR );
        this.list.setBackground( TemplateConfig.LIST_BACKGROUND_COLOR );
        this.eventPane = new JScrollPane( list );
        this.eventPane.setVerticalScrollBarPolicy( ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED );
        this.setComponents();
    }

    /**
     * Adds a new event to the event list.
     * @param text text of the line being added to the event list.
     */
    public void addEvent( String text ) {
        listModel.addElement( text );
        list.ensureIndexIsVisible( listModel.getSize()-1 );
    }

    /**
     * Looks for the first occurrence of a line having a given content
     * and removes it.
     * @param message content of the line to remove from the event list.
     */
    public void removeLine( String message ) {
        int i = 0;
        while ( i < listModel.getSize() ) {
            if ( listModel.getElementAt( i ).equals( message ) )
                listModel.remove( i );
            else
                i++;
        }
    }

    /**
     * Clears the event list content.
     */
    public void clear() {
        listModel.clear();
    }

    private void setComponents() {
        this.add( eventPane, BorderLayout.CENTER );
    }
    
}
