package it.polimi.ingsw.cg4.gui.application.scene.endgame;

import java.util.Arrays;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JLabel;

import it.polimi.ingsw.cg4.gui.config.GraphicsConfig;
import it.polimi.ingsw.cg4.gui.template.BaseCommandPanel;
import it.polimi.ingsw.cg4.gui.template.TextLabel;
import it.polimi.ingsw.cg4.gui.utilities.GraphicsUtilities;


class EndGamePlayerScoreReport extends BaseCommandPanel {

    private static final long serialVersionUID = 1883062673276866429L;
    
    /**
     * 
     * @param isWinner information about the winning status.
     * @param name information about the player name.
     * @param race information about the player race.
     * @param score information about the player score.
     */
    private EndGamePlayerScoreReport( String isWinner, String name, String race, String score ) {
        super( BoxLayout.X_AXIS );
        this.setComponents( Arrays.asList( new JLabel( GraphicsUtilities.createImageIcon( race.toLowerCase() + GraphicsConfig.RACE_SYMBOL_EXTENSION ) ), new TextLabel( isWinner ), new TextLabel( name ), new TextLabel( score ) ) );
    }

    /**
     * 
     * @param args the list of strings representing user's end game informations.
     */
    public EndGamePlayerScoreReport( List< String > args ) {
        this( args.get( 0 ), args.get( 1 ), args.get( 2 ), args.get( 3 ) );
    }

}
