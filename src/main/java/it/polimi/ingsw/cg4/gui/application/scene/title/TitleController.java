package it.polimi.ingsw.cg4.gui.application.scene.title;

import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.util.EventObject;

import it.polimi.ingsw.cg4.gui.application.scene.connection.ConnectionSceneShownEvent;
import it.polimi.ingsw.cg4.gui.config.AudioConfig;
import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseActionListener;
import it.polimi.ingsw.cg4.gui.template.BasePanel;
import it.polimi.ingsw.cg4.gui.template.SceneController;


class TitleController extends SceneController {

    /**
     * 
     * @param contentPanel the content panel related to the title scene.
     */
    public TitleController( TitlePanel contentPanel ) {
        super( contentPanel );
        this.setListeners();
    }

    @Override
    public void setListeners() {
        TitlePanel panel = (TitlePanel) this.getPanel();
        panel.getContentPanel().getCommandPanel().getStartButton().addActionListener( new StartButtonListener( panel ) );
        panel.getContentPanel().getCommandPanel().getExitButton().addActionListener( new ExitButtonListener( panel ) );
    }

    private class StartButtonListener extends BaseActionListener {    

        /**
         * 
         * @param panel the panel containing the button.
         */
        public StartButtonListener( BasePanel panel ) {
            super( panel );
        }

        @Override
        public void actionPerformed( ActionEvent e ) {
            TitlePanel panel = ( TitlePanel )this.getPanel();
            panel.getFrame().getAudioManager().playSE( AudioConfig.BUTTON_SE );
            panel.getContentPanel().getLogoPanel().stop();
            panel.getFrame().transitionTo( SceneConfig.CONNECTION_PANEL_ID );
        }
    }

    private class ExitButtonListener extends BaseActionListener {

        /**
         * 
         * @param panel the panel containing the button.
         */
        public ExitButtonListener( BasePanel panel ) {
            super( panel );
        }

        @Override
        public void actionPerformed( ActionEvent e ) {
            TitlePanel panel = (TitlePanel) this.getPanel();
            panel.getFrame().getAudioManager().playSE( AudioConfig.BUTTON_SE );
            panel.getFrame().dispatchEvent( new WindowEvent( panel.getFrame(), WindowEvent.WINDOW_CLOSING ));
        }
    }

    @Override
    public void eventObjectReceived( EventObject evt ) {
        TitlePanel panel = (TitlePanel) this.getPanel();
        if ( evt instanceof TitleSceneShownEvent ) {
            panel.getContentPanel().getLogoPanel().start();
        } else if ( evt instanceof ConnectionSceneShownEvent ) {
            panel.getContentPanel().getLogoPanel().stop();
        }
    }
    
}
