package it.polimi.ingsw.cg4.gui.application.scene.connection;

import java.util.Arrays;

import javax.swing.BoxLayout;

import it.polimi.ingsw.cg4.gui.template.BaseCommandPanel;

/**
 * Class defining the connection command panel, which contains
 * the buttons that provide connection features access.
 */
public class ConnectionCommandPanel extends BaseCommandPanel {

    private static final long serialVersionUID = 1883863673276866429L;

    private final ConnectionCreateRoomButton createButton;
    private final ConnectionJoinRoomButton joinButton;
    private final ConnectionAutojoinRoomButton autojoinButton;
    private final ConnectionBackButton backButton;
    
    /**
     * Buttons are set and shown.
     * Some of the properties are specified in the configuration classes.
     */
    public ConnectionCommandPanel() {
        super( BoxLayout.X_AXIS );
        this.createButton = new ConnectionCreateRoomButton();
        this.joinButton = new ConnectionJoinRoomButton();
        this.autojoinButton = new ConnectionAutojoinRoomButton();
        this.backButton = new ConnectionBackButton();
        this.setComponents( Arrays.asList( createButton, joinButton, autojoinButton, backButton ) );
    }
    
    /**
     * 
     * @return the room creation button.
     */
    public ConnectionCreateRoomButton getCreateButton() {
        return createButton;
    }

    /**
     * 
     * @return the join room button.
     */
    public ConnectionJoinRoomButton getJoinButton() {
        return joinButton;
    }

    /**
     * 
     * @return the room autojoin button.
     */
    public ConnectionAutojoinRoomButton getAutojoinButton() {
        return autojoinButton;
    }
    
    /**
     * 
     * @return the back button.
     */
    public ConnectionBackButton getBackButton() {
        return backButton;
    }

}
