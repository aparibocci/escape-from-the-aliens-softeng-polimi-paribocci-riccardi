package it.polimi.ingsw.cg4.gui.application;

import java.io.IOException;

import it.polimi.ingsw.cg4.networking.client.ClientConfig;
import it.polimi.ingsw.cg4.networking.client.ClientLogger;

import javax.swing.SwingUtilities;

/**
 * Contains the main method that starts the client in GUI mode.
 *
 */
public class ClientGUI {
    
    private ClientGUI() {
        
    }

    /**
     * Client's main method for GUI mode: sets up the logger and starts
     * the main frame.
     * @param args command-line arguments (not used in this context).
     */
    public static void main( String[] args ) {
        try {
            ClientLogger.setup( "game.log" );
        } catch ( SecurityException | IOException e ) {
            ClientConfig.ERROR_STREAM.println( e.getMessage() );
        }
        SwingUtilities.invokeLater( new Runnable() {
            @Override
            public void run() {
                MainFrame mainFrame = new MainFrame();
                mainFrame.setVisible( true );
            }
        } );
    }
}
