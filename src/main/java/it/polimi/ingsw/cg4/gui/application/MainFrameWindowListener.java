package it.polimi.ingsw.cg4.gui.application;

import it.polimi.ingsw.cg4.networking.client.ConnectionMethod;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

class MainFrameWindowListener implements WindowListener {

    private final MainFrame frame;
    
    /**
     * 
     * @param mainFrame the main frame containing the game's graphical interface.
     */
    public MainFrameWindowListener( MainFrame mainFrame ) {
        this.frame = mainFrame;
    }

    @Override
    public void windowOpened( WindowEvent e ) {
        // Empty method (no operation needed, yet necessary for interface implementation)
    }

    @Override
    public void windowClosing( WindowEvent e ) {
        if ( frame.getClientThread() != null && frame.getClientThread().getConnectionMethod().equals( ConnectionMethod.RMI ) )
            frame.writeAsUser( "logout" );
    }

    @Override
    public void windowClosed( WindowEvent e ) {
        // Empty method (no operation needed, yet necessary for interface implementation)
    }

    @Override
    public void windowIconified( WindowEvent e ) {
        // Empty method (no operation needed, yet necessary for interface implementation)
    }

    @Override
    public void windowDeiconified( WindowEvent e ) {
        // Empty method (no operation needed, yet necessary for interface implementation)
    }

    @Override
    public void windowActivated( WindowEvent e ) {
        // Empty method (no operation needed, yet necessary for interface implementation)
    }

    @Override
    public void windowDeactivated( WindowEvent e ) {
        // Empty method (no operation needed, yet necessary for interface implementation)
    }

}
