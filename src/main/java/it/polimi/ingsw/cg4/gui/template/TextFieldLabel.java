package it.polimi.ingsw.cg4.gui.template;

import javax.swing.BorderFactory;

import it.polimi.ingsw.cg4.gui.config.TemplateConfig;

/**
 * Represents a label used in a TextField.
 * Custom properties are set using configuration classes values.
 */
public class TextFieldLabel extends TextLabel {
    
    private static final long serialVersionUID = 5746150329494149754L;
    private static final int DEFAULT_PADDING = TemplateConfig.DEFAULT_TEXT_FIELD_PADDING;

    /**
     * 
     * @param text the content of the label.
     */
    public TextFieldLabel( String text ) {
        super( text );
        this.setAlignmentX( RIGHT_ALIGNMENT );
        this.setBorder( BorderFactory.createCompoundBorder( this.getBorder(), BorderFactory.createEmptyBorder( DEFAULT_PADDING, DEFAULT_PADDING, DEFAULT_PADDING, DEFAULT_PADDING * 5 ) ) );
    }

}
