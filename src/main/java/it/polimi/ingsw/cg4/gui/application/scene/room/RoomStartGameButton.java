package it.polimi.ingsw.cg4.gui.application.scene.room;

import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseButton;


class RoomStartGameButton extends BaseButton {

    private static final long serialVersionUID = -992188077240130152L;

    /**
     * Creates a new start game button for the room scene.
     * Some of the properties are specified in the configuration classes.
     */
    public RoomStartGameButton() {
        super( SceneConfig.ROOM_START_GAME_TEXT );
        this.setToolTipText( "Select a map and starts a new game (ADMIN only)" );
    }

}
