package it.polimi.ingsw.cg4.gui.config;

import java.awt.Color;
import java.awt.Font;

public class GraphicsConfig {
    
    public static final String GRAPHICS_DIR_NAME = "graphics";
    public static final String FONTS_DIR_NAME = "fonts";

    private static final String GIF = ".gif";
    private static final String JPG = ".jpg";
    private static final String PNG = ".png";
    private static final String TTF = ".ttf";

    public static final String FRAME_ICON_NAME = "icon";
    public static final String FRAME_ICON_EXTENSION = PNG;
    public static final String CURSOR_NAME = "cursor";
    public static final String CURSOR_EXTENSION = GIF;
    public static final int CURSOR_WIDTH = 16;
    public static final int CURSOR_HEIGHT = 16;
    public static final String FRAME_TITLE = "Escape from the Aliens in Outer Space";
    public static final boolean MAXIMIZED_SCREEN = true;
    public static final String BACKGROUND_NAME = "background";
    public static final String BACKGROUND_EXTENSION = JPG;
    public static final Color BUTTON_BACKGROUND_COLOR = Color.BLACK;
    
    public static final String BUTTON_FONT = "button_font";
    public static final float BUTTON_FONT_SIZE = 24f;
    public static final String LABEL_FONT = "label_font";
    public static final float LABEL_FONT_SIZE = 18f;
    public static final float TEXT_FIELD_FONT_SIZE = 16f;
    public static final String SPECIAL_FONT = "special_font";
    public static final Font LIST_FONT = new Font( "Courier New", Font.PLAIN, 14 );
    public static final Font MSG_BOX_FONT = new Font( "Verdana", Font.PLAIN, 18 );
    public static final String FONTS_EXTENSION = TTF;
    
    public static final String TITLE_LOGO_NAME = "title_logo";
    public static final String TITLE_LOGO_EXTENSION = PNG;
    
    public static final String RADIO_OFF_NAME = "radio_icon";
    public static final String RADIO_ON_NAME = "radio_selected_icon";
    public static final String RADIO_EXTENSION = PNG;
    
    public static final String MAP_EXTENSION = PNG;
    public static final String CARD_EXTENSION = PNG;
    public static final String STATUS_EXTENSION = PNG;

    public static final String RACE_SYMBOL_EXTENSION = PNG;
    
    public static final double HIGH_RES_SCALE_FACTOR = 1.0;
    public static final double MED_RES_SCALE_FACTOR = 0.80;
    public static final double LOW_RES_SCALE_FACTOR = 0.66;
    public static final int HIGH_RES_SCREEN_WIDTH = 1920;
    public static final int LOW_RES_SCREEN_WIDTH = 1280;
    
    private GraphicsConfig() {
        
    }
    
}
