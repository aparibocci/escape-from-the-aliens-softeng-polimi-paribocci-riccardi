package it.polimi.ingsw.cg4.gui.application.scene.connection;

import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseButton;


class ConnectionAutojoinRoomButton extends BaseButton {

    private static final long serialVersionUID = -5124195531863640170L;

    /**
     * Creates a new autojoin button for the connection scene.
     * Some of the properties are specified in the configuration classes.
     */
    public ConnectionAutojoinRoomButton() {
        super( SceneConfig.CONNECTION_AUTOJOIN_ROOM_TEXT );
        this.setToolTipText( "Looks for a free room in the server and joins it." );
    }

}
