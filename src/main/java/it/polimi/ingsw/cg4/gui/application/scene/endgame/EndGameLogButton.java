package it.polimi.ingsw.cg4.gui.application.scene.endgame;

import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseButton;


class EndGameLogButton extends BaseButton {

    private static final long serialVersionUID = -992188077240130152L;

    /**
     * Creates a new game log button for the end game scene.
     * Some of the properties are specified in the configuration classes.
     */
    public EndGameLogButton() {
        super( SceneConfig.END_GAME_EVENT_LOG_TEXT );
        this.setToolTipText( "Shows the event log for the finished game." );
    }

}
