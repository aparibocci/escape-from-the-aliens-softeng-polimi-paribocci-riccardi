package it.polimi.ingsw.cg4.gui.application.scene.title;

import java.awt.BorderLayout;

import it.polimi.ingsw.cg4.gui.audio.AudioManager;
import it.polimi.ingsw.cg4.gui.template.BasePanel;


class TitleContentPanel extends BasePanel {

    private static final long serialVersionUID = -5969824526304317504L;
    private final TitleLogoPanel logoPanel;
    private final TitleCommandPanel commandPanel;

    /**
     * Creates the title content panel
     * and sets the graphical components.
     */
    public TitleContentPanel( AudioManager manager ) {
        super();
        this.logoPanel = new TitleLogoPanel( manager );
        this.commandPanel = new TitleCommandPanel();
        this.addComponents();
    }
    
    /**
     * 
     * @return the logo panel.
     */
    public TitleLogoPanel getLogoPanel() {
        return logoPanel;
    }
    
    /**
     * 
     * @return the title command panel.
     */
    public TitleCommandPanel getCommandPanel() {
        return commandPanel;
    }
    
    private void addComponents() {
        this.add( logoPanel, BorderLayout.NORTH );
        this.add( commandPanel, BorderLayout.CENTER );
    }

}
