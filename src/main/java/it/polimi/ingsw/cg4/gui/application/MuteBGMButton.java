package it.polimi.ingsw.cg4.gui.application;

import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseButton;


class MuteBGMButton extends BaseButton {
    
    private static final long serialVersionUID = -1813049681362991978L;

    /**
     * Creates a new mute button for background music.
     * Some of the properties are specified in the configuration classes.
     */
    public MuteBGMButton() {
        super( SceneConfig.SETTINGS_BGM_ON_TEXT );
        this.setToolTipText( "Enables/disables background music" );
    }
}