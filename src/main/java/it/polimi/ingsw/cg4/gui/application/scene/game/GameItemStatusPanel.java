package it.polimi.ingsw.cg4.gui.application.scene.game;

import java.util.Arrays;

import javax.swing.BoxLayout;

import it.polimi.ingsw.cg4.gui.template.BaseCommandPanel;


class GameItemStatusPanel extends BaseCommandPanel {

    private static final long serialVersionUID = -7123549470309653330L;
    
    protected GameItemStatusPanel( GameItemsPanel itemsPanel, GameStatusPanel statusPanel ) {
        super( BoxLayout.Y_AXIS );
        this.setComponents( Arrays.asList( itemsPanel, statusPanel ) );
    }

    
    
}
