package it.polimi.ingsw.cg4.gui.application.scene.title;

import it.polimi.ingsw.cg4.gui.audio.AudioManager;
import it.polimi.ingsw.cg4.gui.config.AudioConfig;
import it.polimi.ingsw.cg4.gui.config.GraphicsConfig;
import it.polimi.ingsw.cg4.gui.template.BaseFlickeringImagePanel;

class TitleLogoPanel extends BaseFlickeringImagePanel {

    private static final long serialVersionUID = 5918127842147875121L;

    /**
     * Creates the title logo panel using configuration values.
     * @param manager the audio manager (required for the flickering image panel).
     */
    public TitleLogoPanel( AudioManager manager ) {
        super( GraphicsConfig.TITLE_LOGO_NAME, GraphicsConfig.TITLE_LOGO_EXTENSION, 5, manager, AudioConfig.TITLE_FLICKERING_SE );
    }

}
