package it.polimi.ingsw.cg4.gui.application.scene.connection;

import java.util.Arrays;

import javax.swing.BoxLayout;

import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseCommandPanel;
import it.polimi.ingsw.cg4.gui.template.TextFieldLabel;


class ConnectionLabelPanel extends BaseCommandPanel {

    private static final long serialVersionUID = 1883863673276866429L;
    
    /**
     * Creates a new instance of the connection label panel, which contains labels
     * for the connection input form.
     */
    public ConnectionLabelPanel() {
        super( BoxLayout.Y_AXIS );
        this.setComponents( Arrays.asList( new TextFieldLabel( SceneConfig.CONNECTION_USERNAME_TEXT ), new TextFieldLabel( SceneConfig.CONNECTION_ROOM_TEXT ), new TextFieldLabel( SceneConfig.CONNECTION_SERVER_TEXT ) ) );
    }

}
