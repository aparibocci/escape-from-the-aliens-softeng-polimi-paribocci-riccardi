package it.polimi.ingsw.cg4.gui.application.scene.connection;

import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.TextLabel;


class ConnectionInfoLabel extends TextLabel {

    private static final long serialVersionUID = 7411998634944144885L;

    /**
     * Text label representing the connection help message.
     */
    public ConnectionInfoLabel() {
        super( SceneConfig.CONNECTION_INFO_TEXT );
    }
    
}
