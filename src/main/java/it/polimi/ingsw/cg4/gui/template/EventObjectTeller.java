package it.polimi.ingsw.cg4.gui.template;

import java.util.EventObject;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Class to be used by subclasses to implement EventObjectTeller methods.
 * It allows its subclasses to add, remove listeners and to fire change events.
 */
public abstract class EventObjectTeller {

    private final List< EventObjectListener > listeners;
 
    protected EventObjectTeller() {
        this.listeners = new CopyOnWriteArrayList< EventObjectListener >();
    }
    
    /**
     * Adds a new event object listener to the listener list.
     * @param listener the listener being added.
     */
    public void addEventObjectListener( EventObjectListener listener ) {
        listeners.add( listener );
    }

    /**
     * Removes a given event object listener from the listener list.
     * @param listener the listener being removed.
     */
    public void removeEventObjectListener( EventObjectListener listener ) {
        listeners.remove( listener );
    }
    
    /**
     * 
     * @return true if the listener list is empty, false otherwise.
     */
    public boolean hasListeners() {
        return ! listeners.isEmpty();
    }
    
    protected void fireChangeEvent( EventObject evt ) {
        for ( EventObjectListener listener : listeners ) {
            listener.eventObjectReceived( evt );
        }
    }
    
}
