package it.polimi.ingsw.cg4.gui.template;

import java.awt.BorderLayout;
import java.awt.LayoutManager;

import it.polimi.ingsw.cg4.gui.config.TemplateConfig;

import javax.swing.JPanel;

/**
 * Class representing a base game panel, extending the JPanel class.
 */
public class BasePanel extends JPanel {

    private static final long serialVersionUID = -6927676407518063685L;
    
    /**
     * Creates a new panel instance using the BorderLayout LayoutManager.
     * Custom properties are set with values from configuration files.
     */
    public BasePanel() {
        this.setBackground( TemplateConfig.BACKGROUND_COLOR );
        this.setLayout( new BorderLayout( TemplateConfig.BORDER_LAYOUT_DEFAULT_PADDING, TemplateConfig.BORDER_LAYOUT_DEFAULT_PADDING ) );
        this.setOpaque( false );
    }

    /**
     * Creates a new panel instance using a custom LayoutManager.
     * Custom properties are set with values from configuration files.
     * @param layoutManager the LayoutManager to use.
     */
    public BasePanel( LayoutManager layoutManager ) {
        this.setBackground( TemplateConfig.BACKGROUND_COLOR );
        this.setLayout( layoutManager );
        this.setOpaque( false );
    }
    
}
