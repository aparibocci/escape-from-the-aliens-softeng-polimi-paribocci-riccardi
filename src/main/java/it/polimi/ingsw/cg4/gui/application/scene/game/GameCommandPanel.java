package it.polimi.ingsw.cg4.gui.application.scene.game;

import java.util.Arrays;

import javax.swing.BoxLayout;

import it.polimi.ingsw.cg4.gui.template.BaseCommandPanel;


class GameCommandPanel extends BaseCommandPanel {

    private static final long serialVersionUID = 1883863673276866429L;

    private final GameAttackButton attackButton;
    private final GameHistoryVisibleButton historyVisibleButton;
    private final GameEventLogButton eventLogButton;
    private final GameEndTurnButton endTurnButton;
    private final GameLogoutButton logoutButton;
    
    /**
     * Creates the game command panel, adds the buttons and shows them.
     */
    public GameCommandPanel() {
        super( BoxLayout.X_AXIS );
        this.attackButton = new GameAttackButton();
        this.historyVisibleButton = new GameHistoryVisibleButton();
        this.eventLogButton = new GameEventLogButton();
        this.endTurnButton = new GameEndTurnButton();
        this.logoutButton = new GameLogoutButton();
        this.setComponents( Arrays.asList( attackButton, historyVisibleButton, eventLogButton, endTurnButton, logoutButton ) );
    }
    
    /**
     * 
     * @return the game attack button.
     */
    public GameAttackButton getAttackButton() {
        return attackButton;
    }

    /**
     * 
     * @return the game history visible button.
     */
    public GameHistoryVisibleButton getHistoryVisibleButton() {
        return historyVisibleButton;
    }

    /**
     * 
     * @return the game event log button.
     */
    public GameEventLogButton getEventLogButton() {
        return eventLogButton;
    }

    /**
     * 
     * @return the game end turn button.
     */
    public GameEndTurnButton getEndTurnButton() {
        return endTurnButton;
    }

    /**
     * 
     * @return the game logout button.
     */
    public GameLogoutButton getLogoutButton() {
        return logoutButton;
    }

}
