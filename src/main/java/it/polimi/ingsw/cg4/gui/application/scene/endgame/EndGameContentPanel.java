package it.polimi.ingsw.cg4.gui.application.scene.endgame;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import it.polimi.ingsw.cg4.gui.template.BasePanel;

class EndGameContentPanel extends BasePanel {

    private static final long serialVersionUID = -7572319375969489072L;
    private final EndGameChatPanel chatPanel;
    private final EndGamePlayersPanel playersPanel;
    private final EndGameCommandPanel commandPanel;

    /**
     * The content panel related to the end game panel.
     * The component panels are set and shown.
     */
    public EndGameContentPanel() {
        super();
        this.chatPanel = new EndGameChatPanel();
        this.playersPanel = new EndGamePlayersPanel();
        this.commandPanel = new EndGameCommandPanel();
        this.addComponents();
    }
    
    /**
     * 
     * @return the end game chat panel.
     */
    public EndGameChatPanel getChatPanel() {
        return chatPanel;
    }
    
    /**
     * 
     * @return the end game players panel, which provides informations
     * about the winner/losers statuses and the players' scores.
     */
    public EndGamePlayersPanel getPlayersPanel() {
        return playersPanel;
    }
    
    /**
     * 
     * @return the command panel.
     */
    public EndGameCommandPanel getCommandPanel() {
        return commandPanel;
    }
    
    private void addComponents() {
        BasePanel panel = new BasePanel( new GridLayout( 1, 2 ) );
        panel.add( playersPanel );
        panel.add( chatPanel );
        this.add( panel, BorderLayout.CENTER );
        this.add( commandPanel, BorderLayout.SOUTH );
    }

}
