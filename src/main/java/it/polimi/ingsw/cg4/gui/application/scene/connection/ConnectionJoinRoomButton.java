package it.polimi.ingsw.cg4.gui.application.scene.connection;

import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseButton;


class ConnectionJoinRoomButton extends BaseButton {

    private static final long serialVersionUID = -5124195531863640170L;

    /**
     * Creates a new join room button for the connection scene.
     * Some of the properties are specified in the configuration classes.
     */
    public ConnectionJoinRoomButton() {
        super( SceneConfig.CONNECTION_JOIN_ROOM_TEXT );
        this.setToolTipText( "Connects to the server and joins a room" );
    }

}
