package it.polimi.ingsw.cg4.gui.application.scene.game;

import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseButton;


class GameLogoutButton extends BaseButton {

    private static final long serialVersionUID = -992188077240130152L;

    /**
     * Creates a new logout button for the game scene.
     * Some of the properties are specified in the configuration classes.
     */
    public GameLogoutButton() {
        super( SceneConfig.GAME_LOGOUT_TEXT );
        this.setToolTipText( "Ends the current session and disconnects from server, losing the game" );
    }

}
