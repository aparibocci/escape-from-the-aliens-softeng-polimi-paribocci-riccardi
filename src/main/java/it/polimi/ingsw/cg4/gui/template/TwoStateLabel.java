package it.polimi.ingsw.cg4.gui.template;

import java.awt.Color;

/**
 * Defines a particular kind of TextLabel that has two states (that is, two
 * different text values) and two colors. The state may be changed with
 * method invocation.
 */
public class TwoStateLabel extends TextLabel {

    private static final long serialVersionUID = 8231963958395766409L;
    private final String defaultText;
    private final Color defaultColor;
    private final String altText;
    private final Color altColor;
    private boolean alternative;

    /**
     * 
     * @param defaultText the initial label text.
     * @param defaultColor the initial label color.
     * @param altText the alternative label text.
     * @param altColor the alternative label color.
     */
    public TwoStateLabel( String defaultText, Color defaultColor, String altText, Color altColor ) {
        super( defaultText );
        this.setForeground( defaultColor );
        this.defaultText = defaultText;
        this.defaultColor = defaultColor;
        this.altText = altText;
        this.altColor = altColor;
        this.alternative = false;
    }
    
    /**
     * Changes the state of the label: from the default text/color
     * to the alternative ones, and vice versa.
     */
    public void changeState() {
        alternative = ! alternative;
        if ( alternative ) {
            this.setText( altText );
            this.setForeground( altColor );
        } else {
            this.setText( defaultText );
            this.setForeground( defaultColor );
        }
    }

}
