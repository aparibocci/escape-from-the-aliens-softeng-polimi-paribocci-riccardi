package it.polimi.ingsw.cg4.gui.application.scene.game;

import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseButton;


class GameHistoryVisibleButton extends BaseButton {

    private static final long serialVersionUID = -5109686123387917116L;

    /**
     * Creates a new history on/off button for the game scene.
     * Some of the properties are specified in the configuration classes.
     */
    public GameHistoryVisibleButton() {
        super( SceneConfig.GAME_HISTORY_ON_TEXT );
        this.setToolTipText( "Enables/disables the movement history line for your character" );
    }

}
