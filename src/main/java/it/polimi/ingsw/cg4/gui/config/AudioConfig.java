package it.polimi.ingsw.cg4.gui.config;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class AudioConfig {

    private static final String AUDIO_FOLDER_NAME = "audio";
    private static final String BGM_FOLDER_NAME = "bgm";
    private static final String SE_FOLDER_NAME = "se";
    
    private static final String MID = ".mid";
    private static final String WAV = ".wav";
    
    public static final String BGM_PATH = AUDIO_FOLDER_NAME + File.separatorChar + BGM_FOLDER_NAME;
    public static final String BGM_EXTENSION = MID;

    public static final String TITLE_BGM = "title";
    public static final String GAME_BGM = "game";
    
    public static final Map< String, String > SCENE_BGM = new HashMap< String, String >();
    static {
        SCENE_BGM.put( SceneConfig.CONNECTION_PANEL_ID, TITLE_BGM );
        SCENE_BGM.put( SceneConfig.TITLE_PANEL_ID, TITLE_BGM );
        SCENE_BGM.put( SceneConfig.ROOM_PANEL_ID, TITLE_BGM );
        SCENE_BGM.put( SceneConfig.GAME_PANEL_ID, GAME_BGM );
        SCENE_BGM.put( SceneConfig.END_GAME_PANEL_ID, GAME_BGM );
    }

    public static final String SE_PATH = AUDIO_FOLDER_NAME + File.separatorChar + SE_FOLDER_NAME;
    public static final String SE_EXTENSION = WAV;

    public static final String ALIEN_SUFFIX = "alien";
    public static final String HUMAN_SUFFIX = "human";
    public static final String BUZZER_SE = "buzzer";
    public static final String BUTTON_SE = "confirm";
    public static final String TIMER_SE = "timer";
    public static final String ATTACK_SE_PREFIX = "attack_";
    public static final String DEATH_SE_PREFIX = "death_";
    public static final String MOVEMENT_SE = "movement";
    public static final String NOISE_SE = "noise";
    public static final String SILENCE_SE = "silence";
    public static final String TELEPORT_SE = "teleport";
    public static final String SUCCESS_SE = "success";
    public static final String INFO_SE = SUCCESS_SE;
    public static final String TITLE_FLICKERING_SE = "title_noise";
    
    private AudioConfig() {
        
    }
    
}
