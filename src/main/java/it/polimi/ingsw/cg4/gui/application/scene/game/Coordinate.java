package it.polimi.ingsw.cg4.gui.application.scene.game;

import java.util.List;

/**
 * Class representing a Coordinate to be used in 2D maps.
 * The data is stored and managed using a cube coordinate policy (to make later calculations easier).
 * The instances, though, can also be done passing cartesian (row, column) informations.
 */
class Coordinate {
    
    private final int x;
    private final int y;
    private final int z;
    
    private Coordinate( int x, int y, int z ) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    /**
     * Creates a Coordinate instance using cartesian notation.
     * 
     * @param row row value of the Coordinate.
     * @param col col value of the Coordinate.
     * @return the instance of the desired Coordinate.
     */
    public static Coordinate fromCartesian( int row, int col ) {
        int x = col;
        int z = row - ( col-(col&1) ) / 2;
        int y = -x-z;
        return new Coordinate( x, y, z );
    }
    
    
    /**
     * 
     * @return the row of the Coordinate.
     */
    public int getRow() {
        return z + (x - (x&1)) / 2;
    }
    
    /**
     * 
     * @return the column of the Coordinate.
     */
    public int getCol() {
        return x;
    }
    
    /**
     * 
     * @param args list of Strings representing the Coordinate to be parsed.
     * @return the Coordinate value represented by args.
     */
    public static Coordinate parseCoordinate( List< String > args ) {
        if ( args.size() < 2 || args.get( 0 ).length() != 1 )
            throw new IllegalArgumentException( "Invalid argument size when parsing Coordinate string." );
        int col = (int)Character.toUpperCase( args.get( 0 ).charAt( 0 ) ) - (int)'A';
        int row = Integer.parseInt( args.get( 1 ) ) - 1;
        return Coordinate.fromCartesian( row, col );
    }

    @Override
    public String toString() {
        return (char)( 'A'+this.getCol() ) + String.format( "%02d", this.getRow()+1 );
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + x;
        result = prime * result + y;
        result = prime * result + z;
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if ( this == obj )
            return true;
        if ( obj == null )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        Coordinate other = (Coordinate) obj;
        if ( x != other.x || y != other.y || z != other.z )
            return false;
        return true;
    }
}
