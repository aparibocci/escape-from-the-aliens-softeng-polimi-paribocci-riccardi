package it.polimi.ingsw.cg4.gui.audio;

import it.polimi.ingsw.cg4.gui.config.AudioConfig;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Sequencer;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * This class is used to manage audio reproduction features.
 * Both MIDI background music files and sound effects reproduction support is provided.
 */
public class AudioManager {

    private static final Logger LOG = Logger.getLogger( Logger.GLOBAL_LOGGER_NAME );
    private Sequencer playingBGM;
    private String playingBGMName;
    private boolean muteBGM;
    private boolean muteSE;

    /**
     * Initializes the audio manager.
     */
    public AudioManager() {
        this.playingBGM = null;
        this.muteBGM = false;
        this.muteSE = false;
    }
    
    /**
     * Changes the BGM control value, muting/unmuting it.
     */
    public void changeMuteBGMControl() {
        muteBGM = ! muteBGM;
        muteBGM();
    }
    
    /**
     * Given the muteBGM local property, changes the MIDI tracks to
     * mute/unmute them.
     */
    private void muteBGM() {
        if ( playingBGM != null )
            for ( int i = 0; i < playingBGM.getSequence().getTracks().length; i++ )
                playingBGM.setTrackMute( i, muteBGM );
    }
    
    /**
     * Mutes/unmutes the sound effects volume.
     */
    public void changeMuteSEControl() {
        muteSE = ! muteSE;
    }

    /**
     * Plays a new background music track.
     * Looping is enabled: if a MIDI track reaches the end of the file,
     * it is reproduced again (using the Sequencer class capabilities).
     * @param fileName the name of the midi file to play.
     */
    public void playBGM( String fileName ) {
        if ( playingBGM != null && playingBGM.isRunning() && fileName == playingBGMName )
            return;
        if ( playingBGM == null || ! playingBGM.isOpen() ) {
            InputStream is;
            try {
                playingBGM = MidiSystem.getSequencer();
                playingBGM.open();
            } catch ( MidiUnavailableException e ) {
                LOG.log( Level.WARNING, e.getMessage(), e );
                return;
            }
            try {
                is = new BufferedInputStream( new FileInputStream( new File( AudioConfig.BGM_PATH + File.separatorChar + fileName + AudioConfig.BGM_EXTENSION ) ) );
            } catch ( FileNotFoundException e ) {
                LOG.log( Level.WARNING, e.getMessage(), e );
                return;
            }
            try {
                playingBGM.setSequence( is );
            } catch ( IOException | InvalidMidiDataException e ) {
                LOG.log( Level.WARNING, e.getMessage(), e );
                return;
            }
            muteBGM();
            playingBGM.setLoopCount( Sequencer.LOOP_CONTINUOUSLY );
            playingBGMName = fileName;
            playingBGM.start();
        } else {
            if ( playingBGM.isOpen() ) {
                playingBGM.stop();
                playingBGM.close();
                this.playBGM( fileName );
            }
        }
    }
    
    /**
     * Reproduce a sound effect.
     * @param fileName the name of the sound effect.
     */
    public void playSE( final String fileName ) {
        if ( ! muteSE ) {
            try {
                File file = new File( AudioConfig.SE_PATH + File.separatorChar + fileName + AudioConfig.SE_EXTENSION );
                AudioInputStream ais = AudioSystem.getAudioInputStream( file );
                Clip clip = AudioSystem.getClip();
                clip.open( ais );
                clip.start();
            } catch ( IOException | LineUnavailableException | UnsupportedAudioFileException e ) {
                LOG.log( Level.WARNING, e.getMessage(), e );
            }
        }
    }
    
}
