package it.polimi.ingsw.cg4.gui.application.scene.connection;

import it.polimi.ingsw.cg4.gui.application.MainFrame;
import it.polimi.ingsw.cg4.gui.config.GraphicsConfig;
import it.polimi.ingsw.cg4.gui.template.ScenePanel;


public class ConnectionPanel extends ScenePanel {

    private static final long serialVersionUID = -1016092098570542012L;
    private ConnectionContentPanel contentPanel;

    /**
     * Creates the connection panel and adds the graphical components.
     * @param frame the frame which contains the connection panel.
     */
    public ConnectionPanel( MainFrame frame ) {
        super( frame, GraphicsConfig.BACKGROUND_NAME + GraphicsConfig.BACKGROUND_EXTENSION );
        this.addComponents();
    }

    @Override
    protected void addComponents() {
        this.contentPanel = new ConnectionContentPanel();
        this.add( contentPanel );
        this.setController( new ConnectionController( this ) );
    }
    
    /**
     * 
     * @return the connection content panel.
     */
    public ConnectionContentPanel getContentPanel() {
        return contentPanel;
    }

}
