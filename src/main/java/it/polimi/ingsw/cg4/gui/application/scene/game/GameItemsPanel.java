package it.polimi.ingsw.cg4.gui.application.scene.game;

import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;

import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseCommandPanel;


class GameItemsPanel extends BaseCommandPanel {

    private static final long serialVersionUID = 1883863673276866429L;

    private final transient List< GameItemButton > items;
    
    /**
     * Builds a new instance of the game items panel, which
     * contains item buttons.
     */
    public GameItemsPanel() {
        super( BoxLayout.Y_AXIS );
        this.items = new ArrayList< GameItemButton >();
        for ( int i = 0; i < SceneConfig.GAME_MAX_ITEMS; i++ )
            items.add( new GameItemButton() );
        this.setComponents( items );
    }
    
    /**
     * Clears the items button list.
     */
    public void clearData() {
        while ( ! items.get( 0 ).isEmpty() )
            removeItem( 0 );
    }
    
    /**
     * 
     * @return the item button list.
     */
    public List< GameItemButton > getItemButtons() {
        return items;
    }

    /**
     *
     * @param index the index of the button (0-based).
     * @param cardName the name of the item card.
     * @param cardDescription the description of the item card.
     */
    public void setItemInfos( int index , String cardName, String cardDescription ) {
        this.getItemButtons().get( index ).setInfos( cardName, cardDescription );
    }

    /**
     *
     * @param index the index (0-based) of the item to be removed.
     */
    public void removeItem( int index ) {
        int i;
        int lastIndex = items.size()-1;
        items.get( index ).removeInfos();
        for ( i = index+1; i < items.size(); i++ )
            if ( ! items.get( i ).isEmpty() ) {
                items.get( i-1 ).setInfos( items.get( i ).getText(), items.get( i ).getToolTipText() );
                lastIndex = i;
            }
        items.get( lastIndex ).removeInfos();
    }

}
