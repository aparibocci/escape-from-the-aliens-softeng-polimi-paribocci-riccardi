package it.polimi.ingsw.cg4.gui.application.scene.endgame;

import java.awt.GridLayout;
import it.polimi.ingsw.cg4.gui.template.BasePanel;


class EndGamePlayersPanel extends BasePanel {

    private static final long serialVersionUID = 1883861673276866429L;
    
    /**
     * Using the GridLayout manager, this panel is capable of representing
     * end game player reports.
     */
    public EndGamePlayersPanel() {
        super( new GridLayout( 0, 1 ) );
    }
    
    /**
     * Adds a new element to the list.
     * @param item the score report to add.
     */
    public void addItem( EndGamePlayerScoreReport item ) {
        this.add( item );
        this.repaint();
    }
    
    /**
     * Clears the panel's content.
     */
    public void clearItems() {
        this.removeAll();
    }

}
