package it.polimi.ingsw.cg4.gui.application.scene.game;

import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseButton;


class GameEventLogButton extends BaseButton {

    private static final long serialVersionUID = -992188077240130152L;

    /**
     * Creates a new event log button for the game scene.
     * Some of the properties are specified in the configuration classes.
     */
    public GameEventLogButton() {
        super( SceneConfig.GAME_EVENT_LOG_TEXT );
        this.setToolTipText( "Shows the event log for the current game." );
    }

}
