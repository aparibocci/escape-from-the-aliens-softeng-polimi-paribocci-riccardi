package it.polimi.ingsw.cg4.gui.application.scene.connection;

import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseButton;


class ConnectionCreateRoomButton extends BaseButton {

    private static final long serialVersionUID = -5124195531863640170L;

    /**
     * Creates a new create room button for the connection scene.
     * Some of the properties are specified in the configuration classes.
     */
    public ConnectionCreateRoomButton() {
        super( SceneConfig.CONNECTION_CREATE_ROOM_TEXT );
        this.setToolTipText( "Connects to the server and creates a new room" );
    }

}
