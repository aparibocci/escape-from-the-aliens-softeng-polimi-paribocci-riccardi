package it.polimi.ingsw.cg4.gui.template;

import it.polimi.ingsw.cg4.gui.config.TemplateConfig;
import it.polimi.ingsw.cg4.gui.utilities.GraphicsUtilities;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 * Allows the representation of a custom image panel, extending the JPanel class.
 * Custom properties are set with configuration parameters.
 */
public abstract class BaseImagePanel extends JPanel {

    private static final long serialVersionUID = -308458114240656851L;
    
    private JLabel imageLabel;
    
    protected BaseImagePanel( String fileName, String extension ) {
        this.setBackground( TemplateConfig.BACKGROUND_COLOR );
        this.setBorder( new EmptyBorder( -5, -5, -5, -5 ) );
        this.setOpaque( false );
        imageLabel = new JLabel( GraphicsUtilities.createImageIcon( fileName + extension ) );
        this.add( imageLabel );
    }
    
    /**
     * Changes the current displayed image with a new one.
     * @param fileName the image file name.
     * @param extension the image file extension.
     */
    public void changeImage( String fileName, String extension ) {
        imageLabel.setIcon( GraphicsUtilities.createImageIcon( fileName + extension ) );
    }
}
