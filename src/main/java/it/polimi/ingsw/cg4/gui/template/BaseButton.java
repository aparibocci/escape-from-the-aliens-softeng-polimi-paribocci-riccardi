package it.polimi.ingsw.cg4.gui.template;

import it.polimi.ingsw.cg4.gui.config.GraphicsConfig;
import it.polimi.ingsw.cg4.gui.config.TemplateConfig;
import it.polimi.ingsw.cg4.gui.utilities.GraphicsUtilities;

import javax.swing.JButton;
import javax.swing.border.EmptyBorder;

/**
 * Defines the game BaseButton class, extending JButton.
 */
public abstract class BaseButton extends JButton {

    private static final long serialVersionUID = -2857658485197980975L;

    /**
     * Creates an instance of the button.
     * Specific properties are set using configuration classes.
     * @param caption the button text.
     */
    public BaseButton( String caption ) {
        super( caption );
        this.setOpaque( false );
        this.setBackground( GraphicsConfig.BUTTON_BACKGROUND_COLOR );
        this.setForeground( TemplateConfig.BUTTON_CAPTION_COLOR );
        this.setAlignmentX( CENTER_ALIGNMENT );
        this.setAlignmentY( CENTER_ALIGNMENT );
        this.setFocusPainted( false );
        this.setBorder( new EmptyBorder( 0, 0, 0, 0 ) );
        this.setFont( GraphicsUtilities.createFont( GraphicsConfig.BUTTON_FONT, GraphicsConfig.BUTTON_FONT_SIZE ) );
    }

}
