package it.polimi.ingsw.cg4.gui.application.scene.title;

import java.util.Arrays;

import javax.swing.BoxLayout;

import it.polimi.ingsw.cg4.gui.template.BaseCommandPanel;


class TitleCommandPanel extends BaseCommandPanel {
    
    private static final long serialVersionUID = 4684126131630049638L;
    private final TitleStartButton startButton;
    private final TitleExitButton exitButton;


    /**
     * Creates the title command panel and sets the components.
     */
    public TitleCommandPanel() {
        super( BoxLayout.Y_AXIS );
        this.startButton = new TitleStartButton();
        this.exitButton = new TitleExitButton();
        this.setComponents( Arrays.asList( startButton, exitButton ) );
    }
    
    /**
     * 
     * @return the start button.
     */
    public TitleStartButton getStartButton() {
        return startButton;
    }
    
    /**
     * 
     * @return the exit button.
     */
    public TitleExitButton getExitButton() {
        return exitButton;
    }

}
