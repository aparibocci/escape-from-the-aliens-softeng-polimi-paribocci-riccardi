package it.polimi.ingsw.cg4.gui.application.scene.room;

import java.awt.Dimension;

import it.polimi.ingsw.cg4.gui.template.BaseEventPanel;


class RoomEventPanel extends BaseEventPanel {

    private static final long serialVersionUID = -2183198340844374192L;

    /**
     * Creates the room event panel and sets its dimensions.
     */
    public RoomEventPanel() {
        super();
        this.setPreferredSize( new Dimension( 800, 480 ) );
    }
    
}
