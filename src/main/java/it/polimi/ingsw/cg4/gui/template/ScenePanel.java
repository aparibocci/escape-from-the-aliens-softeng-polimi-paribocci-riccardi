package it.polimi.ingsw.cg4.gui.template;

import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import it.polimi.ingsw.cg4.gui.application.MainFrame;
import it.polimi.ingsw.cg4.gui.config.GraphicsConfig;

/**
 * Used in a panel representing a game screen.
 */
public abstract class ScenePanel extends BasePanel {

    private static final Logger LOG = Logger.getLogger( Logger.GLOBAL_LOGGER_NAME );
    private static final long serialVersionUID = -5344533330938343148L;

    private final MainFrame frame;
    private transient SceneController controller;
    private boolean shown;

    private String backgroundImgFileName;
    
    protected ScenePanel( MainFrame frame, String backgroundImgFileName ) {
        super( new GridBagLayout() );
        this.frame = frame;
        this.backgroundImgFileName = backgroundImgFileName;
        this.shown = false;
    }
    
    /**
     * 
     * @return true if the panel is shown, false otherwise.
     */
    public boolean isShown() {
        return shown;
    }
    
    /**
     * 
     * @param shown the value to be set.
     */
    public void setShown( boolean shown ) {
        this.shown = shown;
    }

    /**
     * 
     * @return the MainFrame containing the ScenePanel.
     */
    public MainFrame getFrame() {
        return frame;
    }
    
    protected void setController( SceneController controller ) {
        this.controller = controller;
        this.frame.addEventObjectListener( this.controller );
    }
    
    /**
     * 
     * @return the controller associated with the ScenePanel.
     */
    public SceneController getController() {
        return controller;
    }
    
    /**
     * Adds the scene components to the panel.
     */
    protected abstract void addComponents();
    
    @Override
    public void paintComponent( Graphics g ) {
        super.paintComponent( g );
        if ( backgroundImgFileName != null )
            try {
                g.drawImage( ImageIO.read( new File( GraphicsConfig.GRAPHICS_DIR_NAME + File.separatorChar + backgroundImgFileName ) ), 0, 0, null );
            } catch ( IOException e ) {
                LOG.log( Level.SEVERE, e.getMessage(), e );
            }
        
    }
    
}
