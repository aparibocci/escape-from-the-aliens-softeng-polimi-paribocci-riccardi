package it.polimi.ingsw.cg4.gui.application.scene.game;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EventObject;
import java.util.List;

import javax.swing.JOptionPane;

import it.polimi.ingsw.cg4.gui.application.scene.connection.ConnectionSceneShownEvent;
import it.polimi.ingsw.cg4.gui.application.scene.endgame.EndGameSceneShownEvent;
import it.polimi.ingsw.cg4.gui.application.scene.room.RoomSceneShownEvent;
import it.polimi.ingsw.cg4.gui.application.scene.title.TitleSceneShownEvent;
import it.polimi.ingsw.cg4.gui.config.AudioConfig;
import it.polimi.ingsw.cg4.gui.config.GraphicsConfig;
import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.config.TemplateConfig;
import it.polimi.ingsw.cg4.gui.template.BaseActionListener;
import it.polimi.ingsw.cg4.gui.template.BaseButton;
import it.polimi.ingsw.cg4.gui.template.BasePanel;
import it.polimi.ingsw.cg4.gui.template.MessageReceivedEvent;
import it.polimi.ingsw.cg4.gui.template.SceneController;
import it.polimi.ingsw.cg4.gui.utilities.GraphicsUtilities;
import it.polimi.ingsw.cg4.networking.common.ContextConfig;
import it.polimi.ingsw.cg4.networking.common.MessageLine;

class GameController extends SceneController {

    private final SectorClickListener sectorListener;

    /**
     * 
     * @param contentPanel the content panel related to the game scene.
     */
    public GameController( GamePanel contentPanel ) {
        super( contentPanel );
        sectorListener = new SectorClickListener( contentPanel );
    }
    
    @Override
    public void setListeners() {
        GamePanel panel = (GamePanel) this.getPanel();
        panel.getContentPanel().getEventPanel().getTextField().addActionListener( new CommandListener( panel ) );
        panel.getContentPanel().getMapPanel().addMouseListener( sectorListener );
        panel.getContentPanel().getCommandPanel().getEventLogButton().addActionListener( new EventLogButtonListener( panel ) );
        panel.getContentPanel().getCommandPanel().getEndTurnButton().addActionListener( new EndTurnButtonListener( panel ) );
        panel.getContentPanel().getCommandPanel().getAttackButton().addActionListener( new AttackButtonListener( panel ) );
        panel.getContentPanel().getCommandPanel().getHistoryVisibleButton().addActionListener( new HistoryButtonListener( panel ) );
        panel.getContentPanel().getCommandPanel().getLogoutButton().addActionListener( new LogoutButtonListener( panel ) );
        for ( int i = 0; i < panel.getContentPanel().getItemsPanel().getItemButtons().size(); i++ )
            panel.getContentPanel().getItemsPanel().getItemButtons().get( i ).addActionListener( new ItemButtonListener( panel, Integer.toString( i ) ) );
    }
    
    private class ItemButtonListener extends BaseActionListener {

        private final String id;

        /**
         * 
         * @param panel the panel containing the button.
         */
        public ItemButtonListener( BasePanel panel, String id ) {
            super( panel );
            this.id = id;
        }

        @Override
        public void actionPerformed( ActionEvent e ) {
            GamePanel panel = ( GamePanel )this.getPanel();
            panel.getFrame().writeAsUser( "use " + id );
        }
    }
    
    private class EventLogButtonListener extends BaseActionListener {

        /**
         * 
         * @param panel the panel containing the button.
         */
        public EventLogButtonListener( BasePanel panel ) {
            super( panel );
        }

        @Override
        public void actionPerformed( ActionEvent e ) {
            GamePanel panel = ( GamePanel )this.getPanel();
            panel.getFrame().writeAsUser( "log" );
        }
    }
    
    private class AttackButtonListener extends BaseActionListener {

        /**
         * 
         * @param panel the panel containing the button.
         */
        public AttackButtonListener( BasePanel panel ) {
            super( panel );
        }

        @Override
        public void actionPerformed( ActionEvent e ) {
            GamePanel panel = ( GamePanel )this.getPanel();
            panel.getFrame().writeAsUser( "attack" );
        }
    }
    
    private class HistoryButtonListener extends BaseActionListener {

        /**
         * 
         * @param panel the panel containing the button.
         */
        public HistoryButtonListener( BasePanel panel ) {
            super( panel );
        }

        @Override
        public void actionPerformed( ActionEvent e ) {
            GamePanel panel = ( GamePanel )this.getPanel();
            BaseButton button = panel.getContentPanel().getCommandPanel().getHistoryVisibleButton();
            panel.getContentPanel().getMapPanel().changeHistoryVisible();
            if ( button.getText().equals( SceneConfig.GAME_HISTORY_ON_TEXT ) )
                button.setText( SceneConfig.GAME_HISTORY_OFF_TEXT );
            else
                button.setText( SceneConfig.GAME_HISTORY_ON_TEXT );
        }
    }
    
    private class EndTurnButtonListener extends BaseActionListener {

        /**
         * 
         * @param panel the panel containing the button.
         */
        public EndTurnButtonListener( BasePanel panel ) {
            super( panel );
        }

        @Override
        public void actionPerformed( ActionEvent e ) {
            GamePanel panel = ( GamePanel )this.getPanel();
            panel.getFrame().writeAsUser( "end" );
        }
    }
    
    private class LogoutButtonListener extends BaseActionListener {

        /**
         * 
         * @param panel the panel containing the button.
         */
        public LogoutButtonListener( BasePanel panel ) {
            super( panel );
        }

        @Override
        public void actionPerformed( ActionEvent e ) {
            GamePanel panel = ( GamePanel )this.getPanel();
            panel.getFrame().getAudioManager().playSE( AudioConfig.BUTTON_SE );
            panel.getFrame().writeAsUser( "logout" );
            panel.getFrame().getSettingsPanel().getOnlineLabel().changeState();
            panel.clearData();
            panel.getFrame().transitionTo( SceneConfig.TITLE_PANEL_ID );
        }
    }

    private class CommandListener extends BaseActionListener {

        /**
         * 
         * @param panel the panel containing the text field.
         */
        public CommandListener( BasePanel panel ) {
            super( panel );
        }
        
        @Override
        public void actionPerformed( ActionEvent e ) {
            GamePanel panel = ( GamePanel )this.getPanel();
            String cmd = panel.getContentPanel().getEventPanel().getTextField().getText();
            panel.getContentPanel().getEventPanel().getTextField().clear();
            panel.getFrame().writeAsUser( "chat " + cmd );
        }
    
    }

    @Override
    public void eventObjectReceived( EventObject evt ) {
        GamePanel panel = ( GamePanel )this.getPanel();
        if ( evt instanceof GameSceneShownEvent ) {
            if (  panel.getContentPanel().getEventPanel().getTextField().getListeners( ActionListener.class ).length == 0 )
                this.setListeners();
            panel.setShown( true );
        } else if ( evt instanceof RoomSceneShownEvent || evt instanceof TitleSceneShownEvent || evt instanceof ConnectionSceneShownEvent || evt instanceof EndGameSceneShownEvent ) {
            panel.setShown( false );
            panel.getContentPanel().getMapPanel().removeMouseListener( sectorListener );
        } else if ( evt instanceof MessageReceivedEvent && panel.isShown() ) {
            MessageLine line = (( MessageReceivedEvent )evt ).getLine();
            if ( line.getContext().equals( ContextConfig.EVENT_CONTEXT ) || line.getContext().equals( ContextConfig.GAME_CONTEXT ) )
                panel.getContentPanel().getEventPanel().addEvent( line.getMessage() );
            else if ( line.getContext().equals( ContextConfig.SECTOR_REQUIRED_CONTEXT ) )
                sectorListener.setWaitingMovement( false );
            else if ( line.getContext().equals( ContextConfig.MOVEMENT_REQUIRED_CONTEXT ) )
                sectorListener.setWaitingMovement( true );
            else if ( line.getContext().equals( ContextConfig.DANGEROUS_SECTOR_CONTEXT ) )
                decideDrawOrAttack( panel );
            else if ( line.getContext().equals( ContextConfig.MAP_NAME_CONTEXT ) )
                panel.getContentPanel().getMapPanel().changeImage( line.getMessage().toLowerCase() + "_" + panel.getFrame().getScaleFactor(), GraphicsConfig.MAP_EXTENSION );
            else if ( line.getContext().equals( ContextConfig.DRAW_ITEM_CONTEXT ) ) {
                List< String > itemInfos = Arrays.asList( line.getMessage().split( " " ) );
                int number = Integer.parseInt( itemInfos.get( 0 ) );
                String cardName = itemInfos.get( 1 ).replace( ContextConfig.MSG_SEPARATOR, " " );
                String cardDescription = itemInfos.get( 2 ).replace( ContextConfig.MSG_SEPARATOR, " " );
                panel.getContentPanel().getItemsPanel().setItemInfos( number, cardName, cardDescription );
            } else if ( line.getContext().equals( ContextConfig.REMOVE_ITEM_CONTEXT ) ) {
                panel.getContentPanel().getItemsPanel().removeItem( Integer.parseInt( line.getMessage() ) );
            } else if ( line.getContext().equals( ContextConfig.ITEM_DISCARD_CONTEXT ) ) {
                panel.getFrame().writeAsUser( Integer.toString( chosenItemToDiscard( panel ) ) );
            } else if ( line.getContext().equals( ContextConfig.INFO_CONTEXT ) ) {
                List< String > infos = Arrays.asList( line.getMessage().split( " " ) );
                if ( infos.size() > 2 )
                    panel.getContentPanel().getPlayerPanel().setInfos( SceneConfig.GAME_ROOM_PREFIX + infos.get( 0 ), infos.get( 1 ), infos.get( 2 ) );
            } else if ( line.getContext().equals( ContextConfig.POSITION_CONTEXT ) ) {
                panel.getFrame().getAudioManager().playSE( AudioConfig.MOVEMENT_SE );
                panel.getContentPanel().getPlayerPanel().setSector( line.getMessage() );
                panel.getContentPanel().getMapPanel().drawPlayerPosition( line.getMessage() );
            } else if ( line.getContext().equals( ContextConfig.NEW_CARD_CONTEXT ) ) {
                GraphicsUtilities.showCardDrawMessage( line );
            } else if ( line.getContext().equals( ContextConfig.NOISE_CONTEXT ) ) {
                if ( line.getMessage().equals( AudioConfig.NOISE_SE ) || line.getMessage().equals( AudioConfig.SILENCE_SE ) )
                panel.getFrame().getAudioManager().playSE( line.getMessage() );
            } else if ( line.getContext().equals( ContextConfig.TELEPORT_CONTEXT ) ) {
                panel.getFrame().getAudioManager().playSE( AudioConfig.TELEPORT_SE );
            } else if ( line.getContext().equals( ContextConfig.DEATH_CONTEXT ) ) {
                if ( line.getMessage().equals( AudioConfig.ALIEN_SUFFIX ) || line.getMessage().equals( AudioConfig.HUMAN_SUFFIX ) )
                panel.getFrame().getAudioManager().playSE( AudioConfig.DEATH_SE_PREFIX + line.getMessage() );
            } else if ( line.getContext().equals( ContextConfig.ATTACK_CONTEXT ) ) {
                if ( line.getMessage().equals( AudioConfig.ALIEN_SUFFIX ) || line.getMessage().equals( AudioConfig.HUMAN_SUFFIX ) )
                    panel.getFrame().getAudioManager().playSE( AudioConfig.ATTACK_SE_PREFIX + line.getMessage() );
            } else if ( line.getContext().equals( ContextConfig.TIMER_CONTEXT ) ) {
                String seconds = line.getMessage().split( " " )[1];
                if ( Integer.parseInt( seconds ) <= SceneConfig.CRITICAL_TIME ) {
                    panel.getContentPanel().getPlayerPanel().setTimerColor( SceneConfig.CRITICAL_TIME_COLOR );
                    panel.getFrame().getAudioManager().playSE( AudioConfig.TIMER_SE );
                } else
                    panel.getContentPanel().getPlayerPanel().setTimerColor( TemplateConfig.LABEL_COLOR );
                panel.getContentPanel().getPlayerPanel().setTime( line.getMessage() );
            } else if ( line.getContext().equals( ContextConfig.TIMER_RESET_CONTEXT ) ) {
                panel.getContentPanel().getPlayerPanel().setTimerColor( TemplateConfig.LABEL_COLOR );
                panel.getContentPanel().getPlayerPanel().resetTime();
            } else if ( line.getContext().equals( ContextConfig.END_GAME_CONTEXT ) ) {
                panel.clearData();
                panel.getFrame().transitionTo( SceneConfig.END_GAME_PANEL_ID );
            } else if ( line.getContext().equals( ContextConfig.STATUS_ON_CONTEXT ) ) {
                panel.getContentPanel().getStatusPanel().setStatusEnabled( line.getMessage(), true );
            } else if ( line.getContext().equals( ContextConfig.STATUS_OFF_CONTEXT ) ) {
                panel.getContentPanel().getStatusPanel().setStatusEnabled( line.getMessage(), false );
            } else if ( ! ContextConfig.SYS_CONTEXT.contains( line.getContext() ) && ! line.getContext().equals( ContextConfig.CLI_CONTEXT ) )
                GraphicsUtilities.showMessage( line );
        }
    }
    
    private int chosenItemToDiscard( GamePanel panel ) {
        List< Object > options = new ArrayList< Object >();
        for ( GameItemButton b : panel.getContentPanel().getItemsPanel().getItemButtons() ) {
            if ( b.isEnabled() )
                options.add( b.getText() );
        }
        return JOptionPane.showOptionDialog( panel, "Please select the game board.", "Board choice",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options.toArray(), options.toArray()[0] );
    }

    private void decideDrawOrAttack( GamePanel panel ) {
        Object[] options = SceneConfig.GAME_DANGEROUS_SECTOR_OPTIONS.toArray();
        int wantToDraw = JOptionPane.showOptionDialog( panel, "You are moving to a Dangerous Sector. Choose what to do.", "Decision",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0] );
        if ( wantToDraw == JOptionPane.YES_OPTION || wantToDraw == JOptionPane.CLOSED_OPTION )
            panel.getFrame().writeAsUser( "y" );
        else
            panel.getFrame().writeAsUser( "n" );
    }

}
