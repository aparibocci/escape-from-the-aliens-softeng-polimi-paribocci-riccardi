package it.polimi.ingsw.cg4.gui.application.scene.connection;

import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseButton;


class ConnectionBackButton extends BaseButton {

    private static final long serialVersionUID = -5124195531863640170L;

    /**
     * Creates a new back button for the connection scene.
     * Some of the properties are specified in the configuration classes.
     */
    public ConnectionBackButton() {
        super( SceneConfig.CONNECTION_BACK_TEXT );
        this.setToolTipText( "Returns to the title screen" );
    }    

}
