package it.polimi.ingsw.cg4.gui.application.scene.game;

import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseButton;


class GameAttackButton extends BaseButton {

    private static final long serialVersionUID = -992188077240130152L;

    /**
     * Creates a new attack button for the game scene.
     * Some of the properties are specified in the configuration classes.
     */
    public GameAttackButton() {
        super( SceneConfig.GAME_ATTACK_TEXT );
        this.setToolTipText( "Attempts an attack in your character's sector (only if you can)" );
    }

}
