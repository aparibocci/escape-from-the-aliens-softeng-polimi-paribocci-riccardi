package it.polimi.ingsw.cg4.gui.template;

import java.awt.BorderLayout;

/**
 * Defines an EventPanel, that is, a panel composed by an event list
 * and a text field (useful for chat implementations).
 */
public class BaseEventPanel extends BaseListPanel {

    private static final long serialVersionUID = -2183198340844324192L;
    private final BaseTextField textField;

    /**
     * Creates a new BaseEventPanel instance, positioning
     * the text field below the event panel.
     */
    public BaseEventPanel() {
        super();
        this.textField = new BaseTextField();
        this.add( textField, BorderLayout.SOUTH );
    }
    
    /**
     * 
     * @return the event panel's text field.
     */
    public BaseTextField getTextField() {
        return textField;
    }
    
}
