package it.polimi.ingsw.cg4.gui.template;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.SwingWorker;

import it.polimi.ingsw.cg4.networking.client.ClientThread;
import it.polimi.ingsw.cg4.networking.common.MessageLine;

/**
 * Class representing a swing worker utility used to get message lines from
 * the client thread and process them, making an event teller fire them.
 */
public class MessageGetter extends SwingWorker< Void, MessageLine > {
    
    private static final Logger LOG = Logger.getLogger( Logger.GLOBAL_LOGGER_NAME );
    private final ClientThread thread;
    private final EventObjectTeller eventTeller;
    private boolean running;

    /**
     * 
     * @param thread the client thread to work on.
     * @param eventTeller the event teller to set.
     */
    public MessageGetter( ClientThread thread, EventObjectTeller eventTeller ) {
        this.thread = thread;
        this.eventTeller = eventTeller;
        running = false;
    }
    
    @Override
    public Void doInBackground() {
        while ( true ) {
            if ( running && thread != null ) {
                MessageLine line = thread.getLine();
                if ( line != null ) {
                    publish( line );
                }
            }
            try {
                Thread.sleep( 5 );
            } catch ( InterruptedException e ) {
                LOG.log( Level.SEVERE, e.getMessage(), e );
            }
        }
    }
    
    @Override
    public void process( List< MessageLine > messageLines ) {
        for ( MessageLine line : messageLines )
            eventTeller.fireChangeEvent( new MessageReceivedEvent( eventTeller, line ) );
    }
    
    /**
     * Activates the getting process.
     */
    public void activate() {
        this.running = true;
    }
    
    /**
     * Disable the getting process.
     */
    public void deactivate() {
        this.running = false;
    }
    
}
