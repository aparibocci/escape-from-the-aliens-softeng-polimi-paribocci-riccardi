package it.polimi.ingsw.cg4.gui.application.scene.room;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventObject;

import javax.swing.JOptionPane;

import it.polimi.ingsw.cg4.gui.application.scene.connection.ConnectionSceneShownEvent;
import it.polimi.ingsw.cg4.gui.application.scene.game.GameSceneShownEvent;
import it.polimi.ingsw.cg4.gui.application.scene.title.TitleSceneShownEvent;
import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseActionListener;
import it.polimi.ingsw.cg4.gui.template.BasePanel;
import it.polimi.ingsw.cg4.gui.template.MessageReceivedEvent;
import it.polimi.ingsw.cg4.gui.template.SceneController;
import it.polimi.ingsw.cg4.gui.utilities.GraphicsUtilities;
import it.polimi.ingsw.cg4.networking.common.CommonConfig;
import it.polimi.ingsw.cg4.networking.common.ContextConfig;
import it.polimi.ingsw.cg4.networking.common.MessageLine;


class RoomController extends SceneController {

    /**
     * 
     * @param contentPanel the content panel related to the room scene.
     */
    public RoomController( RoomPanel contentPanel ) {
        super( contentPanel );
    }
    
    @Override
    public void setListeners() {
        RoomPanel panel = (RoomPanel) this.getPanel();
        panel.getContentPanel().getEventPanel().getTextField().addActionListener( new CommandListener( panel ) );
        panel.getContentPanel().getCommandPanel().getStartGameButton().addActionListener( new StartGameButtonListener( panel ) );
        panel.getContentPanel().getCommandPanel().getLogoutButton().addActionListener( new LogoutButtonListener( panel ) );
    }
    
    private class StartGameButtonListener extends BaseActionListener {

        /**
         * 
         * @param panel the panel containing the button.
         */
        public StartGameButtonListener( BasePanel panel ) {
            super( panel );
        }

        @Override
        public void actionPerformed( ActionEvent e ) {
            RoomPanel panel = ( RoomPanel )this.getPanel();
            String roomName = chosenRoomName( panel );
            if ( roomName != null )
                panel.getFrame().writeAsUser( "start " + roomName );
        }

        private String chosenRoomName( RoomPanel panel ) {
            Object[] options = SceneConfig.ROOM_MAP_CHOICE_OPTIONS.toArray();
            int wantToDraw = JOptionPane.showOptionDialog( panel, "Please select the game board.", "Board choice",
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0] );
            if ( wantToDraw == JOptionPane.CLOSED_OPTION )
                return null;
            else
                return ( String )options[wantToDraw];
        }
    }
    
    private class LogoutButtonListener extends BaseActionListener {

        /**
         * 
         * @param panel the panel containing the button.
         */
        public LogoutButtonListener( BasePanel panel ) {
            super( panel );
        }

        @Override
        public void actionPerformed( ActionEvent e ) {
            RoomPanel panel = ( RoomPanel )this.getPanel();
            panel.getFrame().writeAsUser( "logout" );
            panel.getFrame().getSettingsPanel().getOnlineLabel().changeState();
            panel.setShown( false );
            panel.getContentPanel().getEventPanel().clear();
            panel.getContentPanel().getUsersPanel().clear();
            panel.getFrame().transitionTo( SceneConfig.CONNECTION_PANEL_ID );
        }
    }

    private class CommandListener extends BaseActionListener {

        /**
         * 
         * @param panel the panel containing the button.
         */
        public CommandListener( BasePanel panel ) {
            super( panel );
        }
        
        @Override
        public void actionPerformed( ActionEvent e ) {
            RoomPanel panel = ( RoomPanel )this.getPanel();
            String cmd = panel.getContentPanel().getEventPanel().getTextField().getText();
            panel.getContentPanel().getEventPanel().getTextField().clear();
            panel.getFrame().writeAsUser( "chat " + cmd );
        }
    
    }

    @Override
    public void eventObjectReceived( EventObject evt ) {
        RoomPanel panel = ( RoomPanel )this.getPanel();
        if ( panel.isShown() ) {
            if ( evt instanceof GameSceneShownEvent || evt instanceof TitleSceneShownEvent || evt instanceof ConnectionSceneShownEvent ) {
                panel.setShown( false );
                panel.getContentPanel().getUsersPanel().clear();
            } else if ( evt instanceof MessageReceivedEvent ) {
                MessageLine line = ( (MessageReceivedEvent) evt ).getLine();
                if ( line.getMessage().startsWith( CommonConfig.CONNECTION_FAILURE_MESSAGE ) ) {
                    GraphicsUtilities.showError( line );
                    return;
                } else if ( line.getContext().equals( ContextConfig.ROOM_CONTEXT ) || line.getContext().equals( ContextConfig.EVENT_CONTEXT ) ) {
                    panel.getContentPanel().getEventPanel().addEvent( line.getMessage() );
                } else if ( line.getContext().equals( ContextConfig.GAME_STARTING_CONTEXT ) ) {
                    panel.setShown( false );
                    panel.getContentPanel().getEventPanel().clear();
                    panel.getContentPanel().getUsersPanel().clear();
                    panel.getFrame().transitionTo( SceneConfig.GAME_PANEL_ID );
                } else if ( line.getContext().equals( ContextConfig.USER_JOIN_CONTEXT ) || ( line.getContext().equals( ContextConfig.USER_ROOM_LOGOUT_CONTEXT ) ) ) {
                    panel.getContentPanel().getUsersPanel().clear();
                    panel.getFrame().writeAsUser( "users" );
                } else if ( line.getContext().equals( ContextConfig.USER_LIST_CONTEXT ) ) {
                    panel.getContentPanel().getUsersPanel().addEvent( line.getMessage() );
                } else if ( ! ContextConfig.SYS_CONTEXT.contains( line.getContext() ) ) {
                    GraphicsUtilities.showMessage( line );
                }
            }
        } else {
            if ( evt instanceof RoomSceneShownEvent ) {
                panel.setShown( true );
                if ( panel.getContentPanel().getEventPanel().getTextField().getListeners( ActionListener.class ).length == 0 )
                    this.setListeners();
                panel.getContentPanel().getUsersPanel().clear();
                panel.getFrame().writeAsUser( "users" );
            }
        }
    }

}
