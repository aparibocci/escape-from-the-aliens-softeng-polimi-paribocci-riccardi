package it.polimi.ingsw.cg4.gui.application.scene.game;

import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JLabel;

import it.polimi.ingsw.cg4.gui.config.GraphicsConfig;
import it.polimi.ingsw.cg4.gui.template.BaseCommandPanel;
import it.polimi.ingsw.cg4.gui.template.BasePanel;
import it.polimi.ingsw.cg4.gui.utilities.GraphicsUtilities;


class GameStatusPanel extends BasePanel {

    private static final long serialVersionUID = 1883863673276866429L;
    
    private static final List< String > STATUS_NAMES = Arrays.asList(
        "adrenaline", "attack", "defense", "sedatives", "voracious"
    );
    private final transient List< JLabel > statusIcons;
    
    public GameStatusPanel( double scaleFactor ) {
        super( new GridLayout( 3, 1 ) );
        this.statusIcons = new ArrayList< JLabel >();
        for ( String statusName : STATUS_NAMES )
            statusIcons.add( createStatusIcon( statusName, scaleFactor ) );
        BaseCommandPanel firstPanel = new BaseCommandPanel( BoxLayout.X_AXIS );
        firstPanel.setComponents( statusIcons.subList( 0, 2 ) );
        BaseCommandPanel secondPanel = new BaseCommandPanel( BoxLayout.X_AXIS );
        secondPanel.setComponents( statusIcons.subList( 2, 4 ) );
        BaseCommandPanel thirdPanel = new BaseCommandPanel( BoxLayout.X_AXIS );
        thirdPanel.setComponents( statusIcons.subList( 4, 5 ) );
        this.add( firstPanel );
        this.add( secondPanel );
        this.add( thirdPanel );
    }

    private JLabel createStatusIcon( String statusName, double scaleFactor  ) {
        JLabel statusLabel = new JLabel( GraphicsUtilities.createImageIcon( "status_" + statusName.toLowerCase() + "_" + scaleFactor + GraphicsConfig.STATUS_EXTENSION ) );
        statusLabel.setToolTipText( Character.toString( statusName.charAt( 0 ) ).toUpperCase() + statusName.substring( 1 ).toLowerCase() + " status" );
        statusLabel.setEnabled( false );
        return statusLabel;
    }
    
    public void setStatusEnabled( String status, boolean enabled ) {
        String statusName = status.toLowerCase();
        if ( ! STATUS_NAMES.contains( statusName ) || STATUS_NAMES.indexOf( statusName ) >= statusIcons.size() )
            throw new IllegalArgumentException( "Invalid status name" );
        statusIcons.get( STATUS_NAMES.indexOf( statusName ) ).setEnabled( enabled );
    }

    public void disableAll() {
        for ( JLabel statusIcon : statusIcons )
            statusIcon.setEnabled( false );
    }

}
