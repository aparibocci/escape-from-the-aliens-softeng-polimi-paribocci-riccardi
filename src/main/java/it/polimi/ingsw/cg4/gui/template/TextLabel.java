package it.polimi.ingsw.cg4.gui.template;

import it.polimi.ingsw.cg4.gui.config.GraphicsConfig;
import it.polimi.ingsw.cg4.gui.config.TemplateConfig;
import it.polimi.ingsw.cg4.gui.utilities.GraphicsUtilities;

import javax.swing.JLabel;

/**
 * Extending the JLabel class, defines a new standard TextLabel class
 * for the game, using custom parameters from configuration files values.
 */
public class TextLabel extends JLabel {
    
    private static final long serialVersionUID = -5120934732194065517L;

    /**
     * 
     * @param text the label's text.
     */
    public TextLabel( String text ) {
        super( text );
        this.setOpaque( false );
        this.setForeground( TemplateConfig.LABEL_COLOR );
        this.setFont( GraphicsUtilities.createFont( GraphicsConfig.LABEL_FONT, GraphicsConfig.LABEL_FONT_SIZE ) );
    }
    
}
