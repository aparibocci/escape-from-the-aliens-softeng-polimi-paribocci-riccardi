package it.polimi.ingsw.cg4.gui.template;

import it.polimi.ingsw.cg4.networking.common.MessageLine;

import java.util.EventObject;

/**
 * This class represents the reception of an event object related to
 * the reception of a MessageLine.
 */
public class MessageReceivedEvent extends EventObject {

    private static final long serialVersionUID = -8085194937529242327L;
    private final MessageLine line;

    /**
     * 
     * @param source the object that fired the message event.
     * @param line the line sent.
     */
    public MessageReceivedEvent( Object source, MessageLine line  ) {
        super( source );
        this.line = line;
    }
    
    /**
     * 
     * @return the received line.
     */
    public MessageLine getLine() {
        return line;
    }

}
