
package it.polimi.ingsw.cg4.gui.application.scene.room;

import it.polimi.ingsw.cg4.gui.application.MainFrame;
import it.polimi.ingsw.cg4.gui.config.GraphicsConfig;
import it.polimi.ingsw.cg4.gui.template.ScenePanel;


public class RoomPanel extends ScenePanel {

    private static final long serialVersionUID = -894166461105496047L;
    
    private RoomContentPanel contentPanel;

    /**
     * Creates the room panel
     * and sets the graphical components.
     * @parameter frame the main frame containing the panel.
     */
    public RoomPanel( MainFrame frame ) {
        super( frame, GraphicsConfig.BACKGROUND_NAME + GraphicsConfig.BACKGROUND_EXTENSION );
        this.addComponents();
        this.setController( new RoomController( this ) );
    }

    @Override
    protected void addComponents() {
        this.contentPanel = new RoomContentPanel();
        this.add( contentPanel );
    }

    /**
     * 
     * @return the content panel related to this scene panel.
     */
    public RoomContentPanel getContentPanel() {
        return contentPanel;
    }

}
