package it.polimi.ingsw.cg4.gui.application.scene.room;

import java.util.EventObject;

/**
 * Defines an event that signals the display of the room scene panel.
 */
public class RoomSceneShownEvent extends EventObject {

    private static final long serialVersionUID = 2541916632871714444L;

    /**
     * 
     * @param source the object that sent the event.
     */
    public RoomSceneShownEvent( Object source ) {
        super( source );
    }

}
