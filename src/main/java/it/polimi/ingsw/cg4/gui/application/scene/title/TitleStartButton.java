package it.polimi.ingsw.cg4.gui.application.scene.title;

import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseButton;

class TitleStartButton extends BaseButton {

    private static final long serialVersionUID = 8614094508802663395L;

    /**
     * Creates a new start button for the title scene.
     * Some of the properties are specified in the configuration classes.
     */
    public TitleStartButton() {
        super( SceneConfig.TITLE_START_TEXT );
    }

}
