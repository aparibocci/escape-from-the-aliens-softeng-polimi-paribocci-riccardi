package it.polimi.ingsw.cg4.gui.template;

import java.util.EventObject;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import it.polimi.ingsw.cg4.gui.config.GraphicsConfig;
import it.polimi.ingsw.cg4.gui.config.TemplateConfig;
import it.polimi.ingsw.cg4.gui.utilities.GraphicsUtilities;

import javax.swing.JFrame;

/**
 * Represents a base frame for the game, extending the JFrame class.
 * Specific properties are set using configuration classes.
 */
public abstract class BaseFrame extends JFrame {

    private static final long serialVersionUID = 425179652351956157L;
    
    private final transient List< EventObjectListener > listeners;
    
    protected BaseFrame( String title ) {
        this.setTitle( title );
        this.setIconImage( GraphicsUtilities.createImageIcon( GraphicsConfig.FRAME_ICON_NAME + GraphicsConfig.FRAME_ICON_EXTENSION ).getImage() );
        this.setExtendedState( JFrame.MAXIMIZED_BOTH );
        this.getContentPane().setBackground( TemplateConfig.BACKGROUND_COLOR );
        this.setDefaultCloseOperation( EXIT_ON_CLOSE );
        this.listeners = new CopyOnWriteArrayList< EventObjectListener >();
    }
    
    /**
     * Adds an event object listener to the listener list.
     * @param listener the listener to add.
     */
    public void addEventObjectListener( EventObjectListener listener ) {
        listeners.add( listener );
    }

    /**
     * Removes an event object listener to the listener list.
     * @param listener the listener to remove.
     */
    public void removeEventObjectListener( EventObjectListener listener ) {
        listeners.remove( listener );
    }
    
    protected void fireChangeEvent( EventObject evt ) {
        for ( EventObjectListener listener : listeners ) {
            listener.eventObjectReceived( evt );
        }
    }

}
