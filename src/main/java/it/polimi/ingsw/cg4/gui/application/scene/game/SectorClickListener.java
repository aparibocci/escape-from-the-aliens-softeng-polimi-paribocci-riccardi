package it.polimi.ingsw.cg4.gui.application.scene.game;

import it.polimi.ingsw.cg4.gui.utilities.GraphicsUtilities;
import it.polimi.ingsw.cg4.networking.common.ContextConfig;
import it.polimi.ingsw.cg4.networking.common.MessageLine;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;


class SectorClickListener implements MouseListener {

    private static final int CENTERS_COLS = 23;
    private static final int CENTERS_ROWS = 14;
    private static final int W = 44;
    private static final int H = 39;
    private double scaleFactor;
    private boolean waitingMovement;
    private final GamePanel panel;
    private final Point[][] centers;
    private List< Point > nastyCenters;
    
    /**
     * Configures the click listener for the map game,
     * sets the hexagon centers using the scale factor.
     * @param panel the game scene panel.
     */
    public SectorClickListener( GamePanel panel ) {
        this.panel = panel;
        this.scaleFactor = panel.getFrame().getScaleFactor();
        waitingMovement = true;
        this.centers = new Point[2*CENTERS_ROWS][CENTERS_COLS];
        
        for ( int i = 0; i < 2*CENTERS_ROWS; i++ ) {
            for ( int j = 0; j < CENTERS_COLS; j++ ) {
                if ( i % 2 == j % 2 ) {
                    centers[i][j] = new Point( W/2 + j*W*3/4, (i+1)*H/2 );
                }
            }
        }
        this.nastyCenters = new ArrayList< Point >();
        for ( int j = 0; j < CENTERS_COLS; j++ ) {
            if ( j % 2 == 1 ) {
                nastyCenters.add( new Point( W/2 + j*W*3/4 , 0 ) );
            } else {
                nastyCenters.add( new Point( W/2 + j*W*3/4 , (2*CENTERS_ROWS+1)*H/2 ) );
            }
        }
    }
    
    /**
     * Sets whether the map image is waiting for a movement click
     * or not.
     * @param waitingMovement the value to set.
     */
    public void setWaitingMovement( boolean waitingMovement ) {
        this.waitingMovement = waitingMovement;
    }
    
    @Override
    public void mouseClicked( MouseEvent e ) {
        Coordinate c = pixelToCoordinate( (int)( e.getX() / scaleFactor ), (int)( e.getY() / scaleFactor ) );
        if ( SwingUtilities.isRightMouseButton( e ) && c != null ) {
            panel.getContentPanel().getMapPanel().changeMarker( c );
        } else {
            if ( c == null ) {
                GraphicsUtilities.showError( new MessageLine( ContextConfig.MSGBOX_CONTEXT, "Please choose a valid sector." ) );
                return;
            }
            String dest = c.toString();
            int wantToMove;
            if ( waitingMovement )
                wantToMove = GraphicsUtilities.showConfirmDialog( "Moving to Sector " + dest + ". Are you sure?" );
            else
                wantToMove = GraphicsUtilities.showConfirmDialog( "Selecting Sector " + dest + ". Are you sure?" );
            if ( wantToMove == JOptionPane.YES_OPTION ) {
                String text = "";
                if ( waitingMovement )
                    text += "move ";
                text += dest.substring( 0, 1 ) + " " + dest.substring( 1 );
                panel.getFrame().writeAsUser( text );
            }
        }
    }

    private Coordinate pixelToCoordinate( int x, int y ) {
        // TODO optimize
        double d = Double.MAX_VALUE;
        int r = 0, c = 0;
        for ( int i = 0; i < 2*CENTERS_ROWS; i++ )
            for ( int j = 0; j < CENTERS_COLS; j++ ) {
                Point p = centers[i][j];
                if ( p != null ) {
                    double tmp = Point.distance( x, y, p.x, p.y );
                    if ( tmp < d ) {
                        d = tmp;
                        r = i;
                        c = j;
                    }
                }
            }
        for ( Point p : nastyCenters )
            if ( Point.distance( x, y, p.x, p.y ) < d )
                return null;
        return Coordinate.fromCartesian( r / 2, c );
    }

    @Override
    public void mousePressed( MouseEvent e ) {
        // Empty method (no operation needed, yet necessary for interface implementation)
    }

    @Override
    public void mouseReleased( MouseEvent e ) {
        // Empty method (no operation needed, yet necessary for interface implementation)
    }

    @Override
    public void mouseEntered( MouseEvent e ) {
        // Empty method (no operation needed, yet necessary for interface implementation)
    }

    @Override
    public void mouseExited( MouseEvent e ) {
        // Empty method (no operation needed, yet necessary for interface implementation)
    }

}
