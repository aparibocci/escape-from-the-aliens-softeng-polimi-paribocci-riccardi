package it.polimi.ingsw.cg4.gui.application.scene.game;

import java.awt.Color;
import java.util.Arrays;

import javax.swing.BoxLayout;

import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseCommandPanel;
import it.polimi.ingsw.cg4.gui.template.TextLabel;
import it.polimi.ingsw.cg4.networking.client.ClientConfig;


class GamePlayerPanel extends BaseCommandPanel {

    private static final long serialVersionUID = -1867145399325006276L;

    private final TextLabel roomNameLabel;
    private final TextLabel playerNameLabel;
    private final TextLabel playerRaceLabel;
    private final TextLabel sectorLabel;
    private final TextLabel timerLabel;
    
    protected GamePlayerPanel() {
        super( BoxLayout.X_AXIS );
        this.roomNameLabel = new TextLabel( "" );
        this.playerNameLabel = new TextLabel( "" );
        this.playerRaceLabel = new TextLabel( "" );
        this.sectorLabel = new TextLabel( "" );
        this.timerLabel = new TextLabel( SceneConfig.GAME_TIMER_WAIT_TEXT );
        this.setComponents( Arrays.asList( roomNameLabel, playerNameLabel, playerRaceLabel, sectorLabel, timerLabel ) );
    }
    
    /**
     * Sets informations about the player.
     * @param roomName the name of the current room.
     * @param playerName the name of the player.
     * @param playerRace the race of the player.
     */
    public void setInfos( String roomName, String playerName, String playerRace ) {
        roomNameLabel.setText( roomName );
        playerNameLabel.setText( playerName );
        playerRaceLabel.setText( playerRace );
        if ( playerRace.equalsIgnoreCase( ClientConfig.ALIEN_RACE ) )
            playerRaceLabel.setForeground( SceneConfig.ALIEN_COLOR );
        else if ( playerRace.equalsIgnoreCase( ClientConfig.HUMAN_RACE ) )
            playerRaceLabel.setForeground( SceneConfig.HUMAN_COLOR );
    }
    
    /**
     * 
     * @param color the new timer label's color.
     */
    public void setTimerColor( Color color ) {
        timerLabel.setForeground( color );
    }
    
    /**
     * 
     * @param time the time (in seconds) to be displayed in the timer.
     */
    public void setTime( String time ) {
        String name = time.split( " " )[0];
        String seconds = time.split( " " )[1];
        if ( name.equalsIgnoreCase( playerNameLabel.getText() ) )
            timerLabel.setText( "Your turn (" + seconds + " seconds left)" );
        else
            timerLabel.setText( name + "'s turn (" + seconds + " seconds left)" );
    }
    
    public void resetTime() {
        timerLabel.setText( SceneConfig.GAME_TIMER_WAIT_TEXT );
    }
    
    public void setSector( String sectorCoord ) {
        sectorLabel.setText( sectorCoord );
    }

}
