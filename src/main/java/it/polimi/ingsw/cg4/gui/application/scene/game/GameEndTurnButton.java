package it.polimi.ingsw.cg4.gui.application.scene.game;

import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseButton;


class GameEndTurnButton extends BaseButton {

    private static final long serialVersionUID = -992188077240130152L;

    /**
     * Creates a new end turn button for the game scene.
     * Some of the properties are specified in the configuration classes.
     */
    public GameEndTurnButton() {
        super( SceneConfig.GAME_END_TURN_TEXT );
        this.setToolTipText( "Ends the current turn (only if you moved)" );
    }

}
