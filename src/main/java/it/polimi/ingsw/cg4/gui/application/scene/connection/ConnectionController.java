package it.polimi.ingsw.cg4.gui.application.scene.connection;

import java.awt.event.ActionEvent;
import java.util.EventObject;
import java.util.Scanner;

import it.polimi.ingsw.cg4.gui.application.scene.endgame.EndGameSceneShownEvent;
import it.polimi.ingsw.cg4.gui.application.scene.game.GameSceneShownEvent;
import it.polimi.ingsw.cg4.gui.application.scene.room.RoomSceneShownEvent;
import it.polimi.ingsw.cg4.gui.application.scene.title.TitleSceneShownEvent;
import it.polimi.ingsw.cg4.gui.config.AudioConfig;
import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseActionListener;
import it.polimi.ingsw.cg4.gui.template.BasePanel;
import it.polimi.ingsw.cg4.gui.template.MessageReceivedEvent;
import it.polimi.ingsw.cg4.gui.template.SceneController;
import it.polimi.ingsw.cg4.gui.utilities.GraphicsUtilities;
import it.polimi.ingsw.cg4.networking.client.ClientConfig;
import it.polimi.ingsw.cg4.networking.client.ClientThread;
import it.polimi.ingsw.cg4.networking.client.ConnectionMethod;
import it.polimi.ingsw.cg4.networking.client.GameMode;
import it.polimi.ingsw.cg4.networking.common.CommonConfig;
import it.polimi.ingsw.cg4.networking.common.MessageLine;
import it.polimi.ingsw.cg4.networking.common.ContextConfig;


class ConnectionController extends SceneController {

    /**
     * 
     * @param contentPanel the content panel related to the controller scene.
     */
    public ConnectionController( ConnectionPanel contentPanel ) {
        super( contentPanel );
        this.setListeners();
    }

    @Override
    public void setListeners() {
        ConnectionPanel panel = (ConnectionPanel) this.getPanel();
        panel.getContentPanel().getCommandPanel().getCreateButton().addActionListener( new CreateButtonListener( panel ) );
        panel.getContentPanel().getCommandPanel().getJoinButton().addActionListener( new JoinButtonListener( panel ) );
        panel.getContentPanel().getCommandPanel().getAutojoinButton().addActionListener( new AutojoinButtonListener( panel ) );
        panel.getContentPanel().getCommandPanel().getBackButton().addActionListener( new BackButtonListener( panel ) );
    }
    
    private class CreateButtonListener extends BaseActionListener {

        /**
         * 
         * @param panel the panel containing the button.
         */
        public CreateButtonListener( BasePanel panel ) {
            super( panel );
        }

        @Override
        public void actionPerformed( ActionEvent e ) {
            ConnectionPanel panel = ( ConnectionPanel )this.getPanel();
            String username = panel.getContentPanel().getInputPanel().getNamePanel().getText();
            String roomName = panel.getContentPanel().getInputPanel().getRoomPanel().getText();
            String serverAddress = panel.getContentPanel().getInputPanel().getAddressPanel().getText();
            panel.getFrame().getAudioManager().playSE( AudioConfig.BUTTON_SE );
            if ( ! validParameters( username, roomName, serverAddress ) ) {
                panel.getFrame().getAudioManager().playSE( AudioConfig.BUZZER_SE );
                GraphicsUtilities.showError( new MessageLine( "Please make sure the parameters are valid." ) );
                return;
            }
            panel.getFrame().setStreams();
            if ( panel.getContentPanel().getMethodPanel().getSocketRadio().isSelected() )
                panel.getFrame().setClientThread( new ClientThread( username, serverAddress, "create " + roomName, new Scanner( panel.getFrame().getInputStream() ), ConnectionMethod.SOCKET, GameMode.GUI ) );
            else
                panel.getFrame().setClientThread( new ClientThread( username, serverAddress, "create " + roomName, new Scanner( panel.getFrame().getInputStream() ), ConnectionMethod.RMI, GameMode.GUI ) );
            ClientThread thread = panel.getFrame().getClientThread();
            MessageLine line = thread.getLine();
            if ( line == null )
                return;
            if ( line.getMessage().startsWith( CommonConfig.CONNECTION_FAILURE_MESSAGE ) ) {
                panel.getFrame().getAudioManager().playSE( AudioConfig.BUZZER_SE );
                GraphicsUtilities.showError( line );
                return;
            }
            panel.getFrame().getSettingsPanel().getOnlineLabel().changeState();
            panel.getFrame().getAudioManager().playSE( AudioConfig.SUCCESS_SE );
            GraphicsUtilities.showSuccessMessage( line );
            panel.getContentPanel().getInputPanel().getNamePanel().clear();
            panel.getContentPanel().getInputPanel().getRoomPanel().clear();
            panel.getContentPanel().getInputPanel().getAddressPanel().clear();
            panel.getFrame().transitionTo( SceneConfig.ROOM_PANEL_ID );
        }

        private boolean validParameters( String username, String roomName, String serverAddress ) {
            if ( username == null || roomName == null || serverAddress == null )
                return false;
            if ( username.isEmpty() || roomName.isEmpty() || serverAddress.isEmpty() )
                return false;
            return username.matches( ClientConfig.USERNAME_PATTERN ) && roomName.matches( ClientConfig.ROOM_NAME_PATTERN ) && serverAddress.matches( ClientConfig.IP_PATTERN );
        }
    }
    
    private class JoinButtonListener extends BaseActionListener {

        /**
         * 
         * @param panel the panel containing the button.
         */
        public JoinButtonListener( BasePanel panel ) {
            super( panel );
        }

        @Override
        public void actionPerformed( ActionEvent e ) {
            ConnectionPanel panel = ( ConnectionPanel )this.getPanel();
            String username = panel.getContentPanel().getInputPanel().getNamePanel().getText();
            String roomName = panel.getContentPanel().getInputPanel().getRoomPanel().getText();
            String serverAddress = panel.getContentPanel().getInputPanel().getAddressPanel().getText();
            panel.getFrame().getAudioManager().playSE( AudioConfig.BUTTON_SE );
            if ( ! validParameters( username, roomName, serverAddress ) ) {
                panel.getFrame().getAudioManager().playSE( AudioConfig.BUZZER_SE );
                GraphicsUtilities.showError( new MessageLine( "Please make sure the parameters are valid." ) );
                return;
            }
            if ( panel.getContentPanel().getMethodPanel().getSocketRadio().isSelected() )
                panel.getFrame().setClientThread( new ClientThread( username, serverAddress, "join " + roomName, new Scanner( panel.getFrame().getInputStream() ), ConnectionMethod.SOCKET, GameMode.GUI ) );
            else
                panel.getFrame().setClientThread( new ClientThread( username, serverAddress, "join " + roomName, new Scanner( panel.getFrame().getInputStream() ), ConnectionMethod.RMI, GameMode.GUI ) );
            ClientThread thread = panel.getFrame().getClientThread();
            MessageLine line = thread.getLine();
            if ( line.getMessage().startsWith( CommonConfig.CONNECTION_FAILURE_MESSAGE ) ) {
                panel.getFrame().getAudioManager().playSE( AudioConfig.BUZZER_SE );
                GraphicsUtilities.showError( line );
                return;
            }
            panel.getFrame().getAudioManager().playSE( AudioConfig.SUCCESS_SE );
            panel.getFrame().getSettingsPanel().getOnlineLabel().changeState();
            GraphicsUtilities.showSuccessMessage( line );
            panel.getContentPanel().getInputPanel().getNamePanel().clear();
            panel.getContentPanel().getInputPanel().getRoomPanel().clear();
            panel.getContentPanel().getInputPanel().getAddressPanel().clear();
            panel.getFrame().transitionTo( SceneConfig.ROOM_PANEL_ID );
        }

        private boolean validParameters( String username, String roomName, String serverAddress ) {
            if ( username == null || roomName == null || serverAddress == null )
                return false;
            if ( username.isEmpty() || roomName.isEmpty() || serverAddress.isEmpty() )
                return false;
            return username.matches( ClientConfig.USERNAME_PATTERN ) && roomName.matches( ClientConfig.ROOM_NAME_PATTERN ) && serverAddress.matches( ClientConfig.IP_PATTERN );
        }
    }
    
    private class AutojoinButtonListener extends BaseActionListener {

        /**
         * 
         * @param panel the panel containing the button.
         */
        public AutojoinButtonListener( BasePanel panel ) {
            super( panel );
        }

        @Override
        public void actionPerformed( ActionEvent e ) {
            ConnectionPanel panel = ( ConnectionPanel )this.getPanel();
            String username = panel.getContentPanel().getInputPanel().getNamePanel().getText();
            String serverAddress = panel.getContentPanel().getInputPanel().getAddressPanel().getText();
            panel.getFrame().getAudioManager().playSE( AudioConfig.BUTTON_SE );
            if ( ! validParameters( username, serverAddress ) ) {
                panel.getFrame().getAudioManager().playSE( AudioConfig.BUZZER_SE );
                GraphicsUtilities.showError( new MessageLine( "Please make sure the parameters are valid." ) );
                return;
            }
            if ( panel.getContentPanel().getMethodPanel().getSocketRadio().isSelected() )
                panel.getFrame().setClientThread( new ClientThread( username, serverAddress, "autojoin", new Scanner( panel.getFrame().getInputStream() ), ConnectionMethod.SOCKET, GameMode.GUI ) );
            else
                panel.getFrame().setClientThread( new ClientThread( username, serverAddress, "autojoin", new Scanner( panel.getFrame().getInputStream() ), ConnectionMethod.RMI, GameMode.GUI ) );
            ClientThread thread = panel.getFrame().getClientThread();
            MessageLine line = thread.getLine();
            if ( line.getMessage().startsWith( CommonConfig.CONNECTION_FAILURE_MESSAGE ) ) {
                panel.getFrame().getAudioManager().playSE( AudioConfig.BUZZER_SE );
                GraphicsUtilities.showError( line );
                return;
            }
            panel.getFrame().getAudioManager().playSE( AudioConfig.SUCCESS_SE );
            panel.getFrame().getSettingsPanel().getOnlineLabel().changeState();
            GraphicsUtilities.showSuccessMessage( line );
            panel.getContentPanel().getInputPanel().getNamePanel().clear();
            panel.getContentPanel().getInputPanel().getRoomPanel().clear();
            panel.getContentPanel().getInputPanel().getAddressPanel().clear();
            panel.getFrame().transitionTo( SceneConfig.ROOM_PANEL_ID );
        }

        private boolean validParameters( String username, String serverAddress ) {
            if ( username == null || serverAddress == null )
                return false;
            if ( username.isEmpty() || serverAddress.isEmpty() )
                return false;
            return username.matches( ClientConfig.USERNAME_PATTERN ) && serverAddress.matches( ClientConfig.IP_PATTERN );

        }
    }
    
    private class BackButtonListener extends BaseActionListener {

        /**
         * 
         * @param panel the panel containing the button.
         */
        public BackButtonListener( BasePanel panel ) {
            super( panel );
        }

        @Override
        public void actionPerformed( ActionEvent e ) {
            ConnectionPanel panel = ( ConnectionPanel )this.getPanel();
            panel.getFrame().getAudioManager().playSE( AudioConfig.BUTTON_SE );
            panel.getContentPanel().getInputPanel().getNamePanel().clear();
            panel.getContentPanel().getInputPanel().getRoomPanel().clear();
            panel.getContentPanel().getInputPanel().getAddressPanel().clear();
            panel.getFrame().transitionTo( SceneConfig.TITLE_PANEL_ID );
        }
    }

    @Override
    public void eventObjectReceived( EventObject evt ) {
        ConnectionPanel panel = ( ConnectionPanel )this.getPanel();
        if ( panel.isShown() ) {
            if ( evt instanceof MessageReceivedEvent && ! ContextConfig.SYS_CONTEXT.contains( ( (MessageReceivedEvent) evt ).getLine().getContext() ) ) {
                MessageLine line = ( (MessageReceivedEvent) evt ).getLine();
                if ( ! line.getContext().equals( ContextConfig.ROOM_CONTEXT ) )
                    GraphicsUtilities.showMessage( line );
            } else if ( evt instanceof TitleSceneShownEvent || evt instanceof RoomSceneShownEvent || evt instanceof GameSceneShownEvent || evt instanceof EndGameSceneShownEvent )
                panel.setShown( false );
        } else {
            if ( evt instanceof ConnectionSceneShownEvent ) {
                panel.setShown( true );
            }
        }
    }

}
