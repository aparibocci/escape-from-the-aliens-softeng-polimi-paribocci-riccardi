package it.polimi.ingsw.cg4.gui.config;

import java.awt.Color;

public class TemplateConfig {
    
    public static final int COMMAND_PANELS_DEFAULT_PADDING = 16;
    public static final int BORDER_LAYOUT_DEFAULT_PADDING = 32;
    public static final int DEFAULT_TEXT_FIELD_SIZE = 20;
    public static final int DEFAULT_TEXT_FIELD_PADDING = 8;
    
    public static final Color TEXT_FIELD_BACKGROUND_COLOR = Color.BLACK;
    public static final Color TEXT_FIELD_FONT_COLOR = new Color( 255, 120, 120 );
    public static final Color LIST_BACKGROUND_COLOR = Color.BLACK;
    public static final Color LIST_FONT_COLOR = TEXT_FIELD_FONT_COLOR;
    public static final Color BUTTON_CAPTION_COLOR = Color.WHITE;
    public static final Color BACKGROUND_COLOR = Color.BLACK;
    public static final Color LABEL_COLOR = Color.WHITE;

    private TemplateConfig() {
        
    }
    
}
