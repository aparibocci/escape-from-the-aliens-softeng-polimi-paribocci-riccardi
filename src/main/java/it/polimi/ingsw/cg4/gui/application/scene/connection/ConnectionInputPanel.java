package it.polimi.ingsw.cg4.gui.application.scene.connection;

import java.util.Arrays;

import javax.swing.BoxLayout;

import it.polimi.ingsw.cg4.gui.template.BaseCommandPanel;
import it.polimi.ingsw.cg4.gui.template.BaseTextField;


class ConnectionInputPanel extends BaseCommandPanel {

    private static final long serialVersionUID = -815018953491249326L;
    private final BaseTextField namePanel;
    private final BaseTextField roomPanel;
    private final BaseTextField addressPanel;

    protected ConnectionInputPanel() {
        super( BoxLayout.Y_AXIS );
        this.namePanel = new BaseTextField();
        this.roomPanel = new BaseTextField();
        this.addressPanel = new BaseTextField();
        this.setComponents( Arrays.asList( namePanel, roomPanel, addressPanel ) );
    }
    
    /**
     * 
     * @return the connection user name panel.
     */
    public BaseTextField getNamePanel() {
        return namePanel;
    }
    
    /**
     * 
     * @return the connection room name panel.
     */
    public BaseTextField getRoomPanel() {
        return roomPanel;
    }
    
    /**
     * 
     * @return the connection ip address panel.
     */
    public BaseTextField getAddressPanel() {
        return addressPanel;
    }

}
