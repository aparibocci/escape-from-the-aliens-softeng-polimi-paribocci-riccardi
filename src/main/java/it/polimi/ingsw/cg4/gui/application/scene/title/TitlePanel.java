package it.polimi.ingsw.cg4.gui.application.scene.title;

import it.polimi.ingsw.cg4.gui.application.MainFrame;
import it.polimi.ingsw.cg4.gui.config.GraphicsConfig;
import it.polimi.ingsw.cg4.gui.template.ScenePanel;


public class TitlePanel extends ScenePanel {

    private static final long serialVersionUID = -3357847409742926550L;
    private TitleContentPanel contentPanel;

    /**
     * Creates the title panel
     * and sets the graphical components.
     * @parameter frame the main frame containing the panel.
     */
    public TitlePanel( MainFrame frame ) {
        super( frame, GraphicsConfig.BACKGROUND_NAME + GraphicsConfig.BACKGROUND_EXTENSION );
        this.addComponents();
    }

    @Override
    public void addComponents() {
        this.contentPanel = new TitleContentPanel( this.getFrame().getAudioManager() );
        this.add( contentPanel );
        this.setController( new TitleController( this ) );
    }

    /**
     * 
     * @return the content panel related to this scene panel.
     */
    public TitleContentPanel getContentPanel() {
        return contentPanel;
    }

}
