package it.polimi.ingsw.cg4.gui.application.scene.game;

import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg4.gui.utilities.GraphicsUtilities;
import it.polimi.ingsw.cg4.networking.common.CommonConfig;
import it.polimi.ingsw.cg4.networking.common.ContextConfig;
import it.polimi.ingsw.cg4.networking.client.ClientThread;
import it.polimi.ingsw.cg4.networking.common.MessageLine;


class GameMsgGetter implements Runnable {

    private static final Logger LOG = Logger.getLogger( Logger.GLOBAL_LOGGER_NAME );
    private final ClientThread thread;
    private GameEventPanel eventPanel;
    private boolean running;

    /**
     * Create an instance of the game message getter.
     * @param thread the client thread.
     * @param eventPanel the game screen's event panel.
     */
    public GameMsgGetter( ClientThread thread, GameEventPanel eventPanel ) {
        this.thread = thread;
        this.eventPanel = eventPanel;
    }

    @Override
    public void run() {
        while ( true ) {
            if ( running ) {
                MessageLine line = thread.getLine();
                if ( line != null ) {
                    if ( line.getMessage().startsWith( CommonConfig.CONNECTION_FAILURE_MESSAGE ) ) {
                        GraphicsUtilities.showError( line );
                        return;
                    } else if ( line.getContext().equals( ContextConfig.GAME_CONTEXT )
                            || line.getContext().equals( ContextConfig.EVENT_CONTEXT )) {
                        eventPanel.addEvent( line.getMessage() );
                    } else {
                        GraphicsUtilities.showMessage( line );
                    }
                }
            }
            try {
                Thread.sleep( 5 );
            } catch ( InterruptedException e ) {
                LOG.log( Level.SEVERE, e.getMessage(), e );
            }
        }
    }
    
    /**
     * activates the message getter.
     */
    public void activate() {
        this.running = true;
    }
    
    /**
     * deactivates the message getter.
     */
    public void deactivate() {
        this.running = false;
    }

}
