package it.polimi.ingsw.cg4.gui.template;

import it.polimi.ingsw.cg4.gui.config.GraphicsConfig;
import it.polimi.ingsw.cg4.gui.config.TemplateConfig;
import it.polimi.ingsw.cg4.gui.utilities.GraphicsUtilities;

import javax.swing.JRadioButton;

/**
 * Represents a RadioButton with custom properties, extending the JRadioButton class.
 */
public class BaseRadioButton extends JRadioButton {

    private static final long serialVersionUID = -1077932917626517293L;

    /**
     * Radio button constructor.
     * @param text the radio button label.
     */
    public BaseRadioButton( String text ) {
        super( text );
        this.setIcon( GraphicsUtilities.createImageIcon( GraphicsConfig.RADIO_OFF_NAME + GraphicsConfig.RADIO_EXTENSION ) );
        this.setSelectedIcon( GraphicsUtilities.createImageIcon( GraphicsConfig.RADIO_ON_NAME + GraphicsConfig.RADIO_EXTENSION ) );
        this.setForeground( TemplateConfig.LABEL_COLOR );
        this.setOpaque( false );
        this.setFont( GraphicsUtilities.createFont( GraphicsConfig.LABEL_FONT, GraphicsConfig.LABEL_FONT_SIZE ) );
    }
    
}
