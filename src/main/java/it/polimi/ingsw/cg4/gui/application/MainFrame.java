package it.polimi.ingsw.cg4.gui.application;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintWriter;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JPanel;

import it.polimi.ingsw.cg4.gui.application.scene.connection.ConnectionPanel;
import it.polimi.ingsw.cg4.gui.application.scene.connection.ConnectionSceneShownEvent;
import it.polimi.ingsw.cg4.gui.application.scene.endgame.EndGamePanel;
import it.polimi.ingsw.cg4.gui.application.scene.endgame.EndGameSceneShownEvent;
import it.polimi.ingsw.cg4.gui.application.scene.game.GamePanel;
import it.polimi.ingsw.cg4.gui.application.scene.game.GameSceneShownEvent;
import it.polimi.ingsw.cg4.gui.application.scene.room.RoomPanel;
import it.polimi.ingsw.cg4.gui.application.scene.room.RoomSceneShownEvent;
import it.polimi.ingsw.cg4.gui.application.scene.title.TitlePanel;
import it.polimi.ingsw.cg4.gui.application.scene.title.TitleSceneShownEvent;
import it.polimi.ingsw.cg4.gui.audio.AudioManager;
import it.polimi.ingsw.cg4.gui.config.AudioConfig;
import it.polimi.ingsw.cg4.gui.config.GraphicsConfig;
import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseFrame;
import it.polimi.ingsw.cg4.gui.template.BasePanel;
import it.polimi.ingsw.cg4.gui.template.EventObjectTeller;
import it.polimi.ingsw.cg4.gui.template.MessageGetter;
import it.polimi.ingsw.cg4.gui.template.ScenePanel;
import it.polimi.ingsw.cg4.networking.client.ClientThread;

/**
 * Class representing the game main frame, which contains several panels using
 * the CardLayout LayoutManager (only one is shown).
 * This class also links the user interface with the client thread.
 */
public class MainFrame extends BaseFrame {

    private static final Logger LOG = Logger.getLogger( Logger.GLOBAL_LOGGER_NAME );
    private static final long serialVersionUID = 538963019124778364L;
    private static final String FRAME_TITLE = GraphicsConfig.FRAME_TITLE;
    private double scaleFactor;
    private final JPanel cardPanel;
    private final transient AudioManager audioManager;
    private transient PrintWriter writer;
    private transient PipedInputStream inputStream;
    private transient ClientThread clientThread;
    private final SettingsPanel settingsPanel;
    private final transient EventObjectTeller messageTeller;
    private final transient Map< String, EventObject > transitionEvents;
    
    /**
     * Sets up the frame, initializing the components and graphics.
     */
    public MainFrame() {
        super( FRAME_TITLE );
        setScaleFactor();
        this.transitionEvents = new HashMap< String, EventObject >();
        this.audioManager = new AudioManager();
        this.cardPanel = new JPanel( new CardLayout() );
        this.settingsPanel = new SettingsPanel( this );
        this.messageTeller = new MessageTeller();
        this.initializeTransitionEvents();
        this.addComponents();
        this.pack();
        if ( GraphicsConfig.MAXIMIZED_SCREEN )
            this.setExtendedState( JFrame.MAXIMIZED_BOTH );
        this.setLocationRelativeTo( null );
        this.setCursor( Toolkit.getDefaultToolkit().createCustomCursor( Toolkit.getDefaultToolkit().getImage( GraphicsConfig.GRAPHICS_DIR_NAME + File.separatorChar + GraphicsConfig.CURSOR_NAME + GraphicsConfig.CURSOR_EXTENSION ), new Point( GraphicsConfig.CURSOR_WIDTH, GraphicsConfig.CURSOR_HEIGHT ), "Cursor" ) );
        this.addWindowListener( new MainFrameWindowListener( this ) );
    }
    
    private void setScaleFactor() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double width = screenSize.getWidth();
        if ( width < GraphicsConfig.LOW_RES_SCREEN_WIDTH )
            scaleFactor = GraphicsConfig.LOW_RES_SCALE_FACTOR;
        else if ( width < GraphicsConfig.HIGH_RES_SCREEN_WIDTH )
            scaleFactor = GraphicsConfig.MED_RES_SCALE_FACTOR;
        else
            scaleFactor = GraphicsConfig.HIGH_RES_SCALE_FACTOR;
    }
    
    /**
     * Gets the scale factor for the current execution, which expresses
     * the need for some images to be scaled in order to fit into the screen.
     * @return the scale factor.
     */
    public double getScaleFactor() {
        return scaleFactor;
    }
    
    private void initializeTransitionEvents() {
        transitionEvents.put( SceneConfig.TITLE_PANEL_ID, new TitleSceneShownEvent( this ) );
        transitionEvents.put( SceneConfig.CONNECTION_PANEL_ID, new ConnectionSceneShownEvent( this ) );
        transitionEvents.put( SceneConfig.ROOM_PANEL_ID, new RoomSceneShownEvent( this ) );
        transitionEvents.put( SceneConfig.GAME_PANEL_ID, new GameSceneShownEvent( this ) );
        transitionEvents.put( SceneConfig.END_GAME_PANEL_ID, new EndGameSceneShownEvent( this ) );
    }

    /**
     * 
     * @return the card panel containing the scene panels.
     */
    public JPanel getCardPanel() {
        return cardPanel;
    }
    
    /**
     * 
     * @param targetPanelName the name of the target panel to show.
     * Valid names are contained in the apposite configuration class.
     */
    public void transitionTo( String targetPanelName ) {
        CardLayout layout = ( CardLayout )( cardPanel.getLayout() );
        layout.show( cardPanel, targetPanelName );
        audioManager.playBGM( AudioConfig.SCENE_BGM.get( targetPanelName ) );
        this.fireChangeEvent( transitionEvents.get( targetPanelName ) );
    }
    
    /**
     * 
     * @return the settings panel.
     */
    public SettingsPanel getSettingsPanel() {
        return settingsPanel;
    }

    private void addComponents() {
        audioManager.playBGM( AudioConfig.SCENE_BGM.get( SceneConfig.TITLE_PANEL_ID ) );
        cardPanel.add( new TitlePanel( this ), SceneConfig.TITLE_PANEL_ID );
        cardPanel.add( new ConnectionPanel( this ), SceneConfig.CONNECTION_PANEL_ID );
        cardPanel.add( new RoomPanel( this ), SceneConfig.ROOM_PANEL_ID );
        cardPanel.add( new GamePanel( this ), SceneConfig.GAME_PANEL_ID );
        cardPanel.add( new EndGamePanel( this ), SceneConfig.END_GAME_PANEL_ID );
        this.getContentPane().setLayout( new BorderLayout() );
        cardPanel.setPreferredSize( new Dimension( 1024, 768 ) );
        this.getContentPane().add( cardPanel, BorderLayout.CENTER );
        BasePanel bottomPanel = new BasePanel( new GridLayout( 2, 1 ) );
        bottomPanel.add( settingsPanel );
        this.getContentPane().add( settingsPanel, BorderLayout.PAGE_END );
        this.fireChangeEvent( new TitleSceneShownEvent( this ) );
    }
    
    /**
     * 
     * @return the output stream.
     */
    public PipedInputStream getInputStream() {
        PipedOutputStream outputStream = new PipedOutputStream();
        try {
            this.inputStream = new PipedInputStream( outputStream );
        } catch ( IOException e ) {
            LOG.log( Level.SEVERE, e.getMessage(), e );
            return null;
        }
        this.writer = new PrintWriter( outputStream );
        return inputStream;
    }
    
    /**
     * Sends a message to the output stream as if the user in CLI mode were
     * writing it manually.
     * @param text the text to be sent.
     */
    public void writeAsUser( String text ) {
        writer.println( text );
        writer.flush();
    }

    /**
     * Sets the thread and starts it, assigning it a message getter.
     * It also adds object listeners, it they were not added previously.
     * @param clientThread the thread to set.
     */
    public void setClientThread( ClientThread clientThread ) {
        this.clientThread = clientThread;
        clientThread.setMessageGetter( new MessageGetter( clientThread, messageTeller ) );
        clientThread.start();
        if ( ! messageTeller.hasListeners() )
            for ( Component c : cardPanel.getComponents() )
                messageTeller.addEventObjectListener( ( ( ScenePanel )c ).getController() );
        clientThread.getMessageGetter().activate();
        clientThread.getMessageGetter().execute();
    }

    /**
     * @return the client thread.
     */
    public ClientThread getClientThread() {
        return clientThread;
    }
    
    /**
     * 
     * @return the audio manager.
     */
    public AudioManager getAudioManager() {
        return audioManager;
    }

    /**
     * Sets the input and output streams for the current client.
     */
    public void setStreams() {
        PipedOutputStream outputStream = new PipedOutputStream();
        try {
            this.inputStream = new PipedInputStream( outputStream );
        } catch ( IOException e ) {
            LOG.log( Level.SEVERE, e.getMessage(), e );
            return;
        }
        this.writer = new PrintWriter( outputStream );
        
    }
    
}
