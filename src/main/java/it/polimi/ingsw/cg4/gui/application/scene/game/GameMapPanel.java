package it.polimi.ingsw.cg4.gui.application.scene.game;

import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import it.polimi.ingsw.cg4.gui.config.GraphicsConfig;
import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseImagePanel;


class GameMapPanel extends BaseImagePanel {

    private static final long serialVersionUID = -3617383355041884566L;
    private static final int W = 44;
    private static final int H = 39;
    private final transient List< Point > sectorHistory;
    private final transient Set< Point > humanMarkers;
    private final transient Set< Point > alienMarkers;
    private boolean historyVisible;
    private final double scaleFactor;

    protected GameMapPanel( double scaleFactor ) {
        super( "galilei_1.0", GraphicsConfig.MAP_EXTENSION );
        this.sectorHistory = new ArrayList< Point >();
        this.humanMarkers = new HashSet< Point >();
        this.alienMarkers = new HashSet< Point >();
        this.scaleFactor = scaleFactor;
        this.historyVisible = true;
        this.setToolTipText( "Left mouse click: select sector. Right mouse click: add race marker." );
    }
    
    /**
     * Clears the data related to the map panel, including the sector history lines
     * and the human/alien markers.
     */
    public void clearData() {
        this.sectorHistory.clear();
        this.humanMarkers.clear();
        this.alienMarkers.clear();
        this.historyVisible = true;
    }

    /**
     *
     * @param message a message representing the target coordinate.
     */
    public void drawPlayerPosition( String message ) {
        Coordinate playerCoordinates = Coordinate.parseCoordinate( Arrays.asList( Character.toString( message.charAt( 0 ) ), message.substring( 1 ) ) );
        sectorHistory.add( new Point( (int)(( W/2 + playerCoordinates.getCol()*W*3/4 )*scaleFactor), (int)(((playerCoordinates.getRow()+1-0.5*((playerCoordinates.getCol()+1)%2))*H)*scaleFactor)) );
        this.repaint();
    }
    
    /**
     * The history visible setting changes.
     */
    public void changeHistoryVisible() {
        this.historyVisible = ! historyVisible;
        this.repaint();
    }
    
    /**
     * Given a coordinate, changes the race marker status in that sector,
     * cycling between null, human marker and alien marker.
     * @param c the coordinate of the sector.
     */
    public void changeMarker( Coordinate c ) {
        Point p = new Point( (int)(( W/2 + c.getCol()*W*3/4 )*scaleFactor), (int)(((c.getRow()+1-0.5*((c.getCol()+1)%2))*H)*scaleFactor));
        if ( humanMarkers.contains( p ) ) {
            humanMarkers.remove( p );
            alienMarkers.add( p );
        } else if ( alienMarkers.contains( p ) ) {
            alienMarkers.remove( p );
        } else {
            humanMarkers.add( p );
        }
        this.repaint();
    }
    
    @Override
    public void paint( Graphics g ) {
        super.paint( g );
        if ( ! sectorHistory.isEmpty() ) {
            Point lastSectorCenter = sectorHistory.get( sectorHistory.size()-1 );
            g.setColor( SceneConfig.POSITION_COLOR );
            g.fillOval( lastSectorCenter.x - 24/2, lastSectorCenter.y - 24/2, 24, 24 );
        }
        
        g.setColor( SceneConfig.HUMAN_COLOR );
        for ( Point p : humanMarkers )
            g.fillOval( p.x - 16/2, p.y - 16/2, 16, 16 );

        g.setColor( SceneConfig.ALIEN_COLOR );
        for ( Point p : alienMarkers )
            g.fillOval( p.x - 16/2, p.y - 16/2, 16, 16 );
        
        g.setColor( SceneConfig.MOVEMENT_HISTORY_COLOR );
        if ( historyVisible ) {
            for ( int i = 0; i < sectorHistory.size()-1; i++ ) {
                g.fillOval( sectorHistory.get( i ).x - 8/2, sectorHistory.get( i ).y - 8/2, 8, 8 );
                g.drawLine( sectorHistory.get( i ).x, sectorHistory.get( i ).y, sectorHistory.get( i+1 ).x, sectorHistory.get( i+1 ).y );
            }
            if ( ! sectorHistory.isEmpty() )
                g.fillOval( sectorHistory.get( sectorHistory.size()-1 ).x - 8/2, sectorHistory.get( sectorHistory.size()-1 ).y - 8/2, 8, 8 );
        }
    }
    
}
