package it.polimi.ingsw.cg4.gui.application.scene.endgame;

import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseButton;


class EndGameTitleButton extends BaseButton {

    private static final long serialVersionUID = -992188077240130152L;

    /**
     * Creates a new logout button for the end game scene.
     * Some of the properties are specified in the configuration classes.
     */
    public EndGameTitleButton() {
        super( SceneConfig.END_GAME_LOGOUT_TEXT );
        this.setToolTipText( "Leaves the room and returns to the title screen" );
    }

}
