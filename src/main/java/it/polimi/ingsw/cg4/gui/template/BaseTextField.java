package it.polimi.ingsw.cg4.gui.template;

import it.polimi.ingsw.cg4.gui.config.GraphicsConfig;
import it.polimi.ingsw.cg4.gui.config.TemplateConfig;
import it.polimi.ingsw.cg4.gui.utilities.GraphicsUtilities;

import javax.swing.BorderFactory;
import javax.swing.JTextField;

/**
 * Class used to represent a text field, extending the JTextField class.
 */
public class BaseTextField extends JTextField {

    private static final long serialVersionUID = -6906800714017164999L;
    
    private static final int DEFAULT_FIELD_SIZE = TemplateConfig.DEFAULT_TEXT_FIELD_SIZE;
    private static final int DEFAULT_PADDING = TemplateConfig.DEFAULT_TEXT_FIELD_PADDING;
    
    /**
     * Creates a new text field with custom properties via configuration files.
     */
    public BaseTextField() {
        super( DEFAULT_FIELD_SIZE );
        this.setBackground( TemplateConfig.TEXT_FIELD_BACKGROUND_COLOR );
        this.setForeground( TemplateConfig.TEXT_FIELD_FONT_COLOR );
        this.setBorder( BorderFactory.createCompoundBorder( this.getBorder(), BorderFactory.createEmptyBorder( DEFAULT_PADDING, DEFAULT_PADDING, DEFAULT_PADDING, DEFAULT_PADDING ) ) );
        this.setFont( GraphicsUtilities.createFont( GraphicsConfig.LABEL_FONT, GraphicsConfig.TEXT_FIELD_FONT_SIZE ) );
    }
    
    /**
     * Clears the text field's content.
     */
    public void clear() {
        this.setText( "" );
    }
    
}
