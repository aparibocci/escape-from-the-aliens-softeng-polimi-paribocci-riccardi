package it.polimi.ingsw.cg4.gui.application.scene.endgame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.EventObject;

import it.polimi.ingsw.cg4.gui.application.scene.connection.ConnectionSceneShownEvent;
import it.polimi.ingsw.cg4.gui.application.scene.room.RoomSceneShownEvent;
import it.polimi.ingsw.cg4.gui.application.scene.title.TitleSceneShownEvent;
import it.polimi.ingsw.cg4.gui.application.scene.game.GameSceneShownEvent;
import it.polimi.ingsw.cg4.gui.config.AudioConfig;
import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseActionListener;
import it.polimi.ingsw.cg4.gui.template.BasePanel;
import it.polimi.ingsw.cg4.gui.template.MessageReceivedEvent;
import it.polimi.ingsw.cg4.gui.template.SceneController;
import it.polimi.ingsw.cg4.networking.common.ContextConfig;
import it.polimi.ingsw.cg4.networking.common.MessageLine;

class EndGameController extends SceneController {

    /**
     * 
     * @param contentPanel the content panel related to the end game scene.
     */
    public EndGameController( EndGamePanel contentPanel ) {
        super( contentPanel );
    }
    
    @Override
    public void setListeners() {
        EndGamePanel panel = (EndGamePanel) this.getPanel();
        panel.getContentPanel().getCommandPanel().getTitleButton().addActionListener( new TitleButtonListener( panel ) );
        panel.getContentPanel().getCommandPanel().getLogButton().addActionListener( new LogButtonListener( panel ) );
        panel.getContentPanel().getChatPanel().getTextField().addActionListener( new ChatListener( panel ) );
    }
    
    private class TitleButtonListener extends BaseActionListener {

        /**
         * 
         * @param panel the panel containing the button.
         */
        public TitleButtonListener( BasePanel panel ) {
            super( panel );
        }

        @Override
        public void actionPerformed( ActionEvent e ) {
            EndGamePanel panel = ( EndGamePanel )this.getPanel();
            panel.getFrame().getAudioManager().playSE( AudioConfig.BUTTON_SE );
            panel.getFrame().writeAsUser( "logout" );
            panel.getFrame().getSettingsPanel().getOnlineLabel().changeState();
            panel.getContentPanel().getChatPanel().clear();
            panel.getContentPanel().getPlayersPanel().clearItems();
            panel.getFrame().transitionTo( SceneConfig.TITLE_PANEL_ID );
        }
    }
    
    private class LogButtonListener extends BaseActionListener {

        /**
         * 
         * @param panel the panel containing the button.
         */
        public LogButtonListener( BasePanel panel ) {
            super( panel );
        }

        @Override
        public void actionPerformed( ActionEvent e ) {
            EndGamePanel panel = ( EndGamePanel )this.getPanel();
            panel.getFrame().getAudioManager().playSE( AudioConfig.BUTTON_SE );
            panel.getFrame().writeAsUser( "log" );
        }
    }
    
    private class ChatListener extends BaseActionListener {

        /**
         * 
         * @param panel the panel containing the button.
         */
        public ChatListener( BasePanel panel ) {
            super( panel );
        }
        
        @Override
        public void actionPerformed( ActionEvent e ) {
            EndGamePanel panel = ( EndGamePanel )this.getPanel();
            String cmd = panel.getContentPanel().getChatPanel().getTextField().getText();
            panel.getContentPanel().getChatPanel().getTextField().clear();
            panel.getFrame().writeAsUser( "chat " + cmd );
        }
    
    }

    @Override
    public void eventObjectReceived( EventObject evt ) {
        EndGamePanel panel = ( EndGamePanel )this.getPanel();
        if ( evt instanceof EndGameSceneShownEvent ) {
            if ( panel.getContentPanel().getChatPanel().getTextField().getListeners( ActionListener.class ).length == 0 )
                this.setListeners();
            panel.setShown( true );
        } else if ( evt instanceof RoomSceneShownEvent || evt instanceof TitleSceneShownEvent || evt instanceof ConnectionSceneShownEvent || evt instanceof GameSceneShownEvent ) {
            panel.setShown( false );
        } else if ( evt instanceof MessageReceivedEvent ) {     
            MessageLine line = (( MessageReceivedEvent )evt ).getLine();
            if ( line.getContext().equals( ContextConfig.ROOM_CONTEXT ) || line.getContext().equals( ContextConfig.EVENT_CONTEXT ) )
                panel.getContentPanel().getChatPanel().addEvent( line.getMessage() );
            else if ( line.getContext().equals( ContextConfig.USER_REPORT_CONTEXT ) )
                panel.getContentPanel().getPlayersPanel().addItem( new EndGamePlayerScoreReport( Arrays.asList( line.getMessage().split( " " ) ) ) );
        }
    }

}
