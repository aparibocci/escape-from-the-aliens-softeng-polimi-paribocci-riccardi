package it.polimi.ingsw.cg4.gui.template;

/**
 * Represents a controller related to a ScenePanel.
 * It is capable of sending and receiving EventObjects.
 */
public abstract class SceneController extends EventObjectTeller implements EventObjectListener {
    
    private final ScenePanel panel;
    
    protected SceneController( ScenePanel panel ) {
        this.panel = panel;
    }
    
    /**
     * @return the ScenePanel related to the controller.
     */
    public ScenePanel getPanel() {
        return panel;
    }
    
    protected abstract void setListeners();
    
}
