package it.polimi.ingsw.cg4.gui.config;

import java.awt.Color;
import java.util.Arrays;
import java.util.List;

public class SceneConfig {
    
    public static final String TITLE_PANEL_ID = "TITLE";
    public static final String CONNECTION_PANEL_ID = "CONNECTION";
    public static final String ROOM_PANEL_ID = "ROOM";
    public static final String GAME_PANEL_ID = "GAME";
    public static final String END_GAME_PANEL_ID = "END_GAME";

    public static final Color POSITION_COLOR = Color.GREEN;
    public static final Color ALIEN_COLOR = Color.RED;
    public static final Color HUMAN_COLOR = Color.BLUE;
    public static final Color MOVEMENT_HISTORY_COLOR = Color.GREEN;

    public static final String ONLINE_TEXT = "ONLINE ";
    public static final Color ONLINE_COLOR = Color.GREEN;
    public static final String OFFLINE_TEXT = "OFFLINE";
    public static final Color OFFLINE_COLOR = Color.RED;
    public static final String SETTINGS_BGM_ON_TEXT = "MUSIC ON ";
    public static final String SETTINGS_BGM_OFF_TEXT = "MUSIC OFF";
    public static final String SETTINGS_SE_ON_TEXT = "SOUND EFFECTS ON ";
    public static final String SETTINGS_SE_OFF_TEXT = "SOUND EFFECTS OFF";

    public static final String TITLE_START_TEXT = "Start Game";
    public static final String TITLE_EXIT_TEXT = "Exit";
    
    public static final String CONNECTION_METHOD_TEXT = "Connection method";
    public static final String CONNECTION_INFO_TEXT = "Enter your name and choose the connection parameters.";
    public static final String CONNECTION_USERNAME_TEXT = "Username";
    public static final String CONNECTION_ROOM_TEXT = "Room name";
    public static final String CONNECTION_SERVER_TEXT = "Server IP address";
    public static final String CONNECTION_SOCKET_TEXT = "Socket";
    public static final String CONNECTION_RMI_TEXT = "RMI";
    public static final String CONNECTION_CREATE_ROOM_TEXT = "Create room ";
    public static final String CONNECTION_JOIN_ROOM_TEXT = " Join room ";
    public static final String CONNECTION_AUTOJOIN_ROOM_TEXT = " Find room ";
    public static final String CONNECTION_BACK_TEXT = " Back";
    
    public static final String ROOM_START_GAME_TEXT = "Start game";
    public static final String ROOM_LOGOUT_TEXT = "Logout";
    public static final List< String > ROOM_MAP_CHOICE_OPTIONS = Arrays.asList( "Galilei", "Galvani", "Fermi" );

    public static final String GAME_ROOM_PREFIX = "Room: ";
    public static final String GAME_ATTACK_TEXT = "Attack";
    public static final String GAME_HISTORY_ON_TEXT = "History ON ";
    public static final String GAME_HISTORY_OFF_TEXT = "History OFF";
    public static final String GAME_EVENT_LOG_TEXT = "Event log";
    public static final String GAME_END_TURN_TEXT = "End turn";
    public static final String GAME_LOGOUT_TEXT = "Surrender";
    public static final String GAME_NO_ITEM_TEXT = "[No item]";
    public static final String GAME_TIMER_WAIT_TEXT = "Waiting...";
    public static final int GAME_MAX_ITEMS = 3;
    public static final List< String > GAME_DANGEROUS_SECTOR_OPTIONS = Arrays.asList( "Draw", "Attack" );
    public static final int GAME_EVENT_PANEL_WIDTH = 350;
    public static final int GAME_EVENT_PANEL_HEIGHT = 300;

    public static final String END_GAME_EVENT_LOG_TEXT = GAME_EVENT_LOG_TEXT;
    public static final String END_GAME_LOGOUT_TEXT = "To title";
    public static final Color CRITICAL_TIME_COLOR = Color.RED;
    public static final int CRITICAL_TIME = 10;
    
    private SceneConfig() {
        
    }
    
}
