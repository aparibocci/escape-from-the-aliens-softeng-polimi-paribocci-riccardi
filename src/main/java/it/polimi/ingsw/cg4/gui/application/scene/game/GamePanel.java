package it.polimi.ingsw.cg4.gui.application.scene.game;

import it.polimi.ingsw.cg4.gui.application.MainFrame;
import it.polimi.ingsw.cg4.gui.config.GraphicsConfig;
import it.polimi.ingsw.cg4.gui.template.ScenePanel;


public class GamePanel extends ScenePanel {

    private static final long serialVersionUID = -894166461105496047L;
    
    private GameContentPanel contentPanel;

    /**
     * Creates the game panel and adds the graphical components.
     * @param frame the frame which contains the connection panel.
     */
    public GamePanel( MainFrame frame ) {
        super( frame, GraphicsConfig.BACKGROUND_NAME + GraphicsConfig.BACKGROUND_EXTENSION );
        this.addComponents();
    }

    @Override
    protected void addComponents() {
        this.contentPanel = new GameContentPanel( this.getFrame().getScaleFactor() );
        this.add( contentPanel );
        this.setController( new GameController( this ) );
    }

    /**
     * 
     * @return the content panel related to this scene panel.
     */
    public GameContentPanel getContentPanel() {
        return contentPanel;
    }

    /**
     * clears the data contained in the panel's components.
     */
    public void clearData() {
        this.getContentPanel().getItemsPanel().clearData();
        this.getContentPanel().getMapPanel().clearData();
        this.getContentPanel().getStatusPanel().disableAll();
        this.getContentPanel().getEventPanel().getTextField().clear();
        this.getContentPanel().getEventPanel().clear();
        this.getContentPanel().getCommandPanel().getHistoryVisibleButton().setText( "History ON " );
    }

}
