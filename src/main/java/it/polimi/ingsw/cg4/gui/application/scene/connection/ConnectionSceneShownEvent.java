package it.polimi.ingsw.cg4.gui.application.scene.connection;

import java.util.EventObject;

/**
 * Defines an event that signals the display of the connection scene panel.
 */
public class ConnectionSceneShownEvent extends EventObject {

    private static final long serialVersionUID = -327423553243567422L;

    /**
     * 
     * @param source the object that sent the event.
     */
    public ConnectionSceneShownEvent( Object source ) {
        super( source );
    }

}
