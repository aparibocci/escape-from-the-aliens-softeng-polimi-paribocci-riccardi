package it.polimi.ingsw.cg4.gui.application.scene.game;

import java.awt.BorderLayout;
import java.util.Arrays;

import javax.swing.BoxLayout;

import it.polimi.ingsw.cg4.gui.template.BaseCommandPanel;
import it.polimi.ingsw.cg4.gui.template.BasePanel;

class GameContentPanel extends BasePanel {

    private static final long serialVersionUID = -7573319375969489072L;
    private final GamePlayerPanel playerPanel;
    private final GameMapPanel mapPanel;
    private final GameEventPanel eventPanel;
    private final GameCommandPanel commandPanel;
    private final GameItemsPanel itemsPanel;
    private final GameStatusPanel statusPanel;

    /**
     * Creates the game content panel and initializes its components.
     * @param scaleFactor the scale factor (the map panel and status panel's
     * graphics will be scaled).
     */
    public GameContentPanel( double scaleFactor ) {
        super();
        this.playerPanel = new GamePlayerPanel();
        this.mapPanel = new GameMapPanel( scaleFactor );
        this.eventPanel = new GameEventPanel();
        this.commandPanel = new GameCommandPanel();
        this.itemsPanel = new GameItemsPanel();
        this.statusPanel = new GameStatusPanel( scaleFactor );
        this.addComponents();
    }
    
    /**
     * 
     * @return the player panel, which provides informations
     * about the player.
     */
    public GamePlayerPanel getPlayerPanel() {
        return playerPanel;
    }
    
    /**
     * 
     * @return the map panel.
     */
    public GameMapPanel getMapPanel() {
        return mapPanel;
    }
    
    /**
     * 
     * @return the event panel, which also works as a chat.
     */
    public GameEventPanel getEventPanel() {
        return eventPanel;
    }
    
    /**
     * 
     * @return the command panel, containing several action buttons.
     */
    public GameCommandPanel getCommandPanel() {
        return commandPanel;
    }
    
    /**
     * 
     * @return the items panel, which provides informations about the
     * owned item cards.
     */
    public GameItemsPanel getItemsPanel() {
        return itemsPanel;
    }
    
    /**
     * 
     * @return the player's character status panel.
     */
    public GameStatusPanel getStatusPanel() {
        return statusPanel;
    }
    
    private void addComponents() {
        this.add( playerPanel, BorderLayout.NORTH );
        this.add( new GameCenterPanel( new GameItemStatusPanel( itemsPanel, statusPanel ), mapPanel, eventPanel ), BorderLayout.CENTER );
        this.add( commandPanel, BorderLayout.SOUTH );
    }
    
    private class GameCenterPanel extends BaseCommandPanel {

        private static final long serialVersionUID = -6962286926207129397L;

        /**
         * Utility panel used to contain the three main game panel components
         * and gather them together.
         * @param itemStatusPanel the item status panel.
         * @param mapPanel the map panel.
         * @param eventPanel the event panel.
         */
        public GameCenterPanel( GameItemStatusPanel itemStatusPanel, GameMapPanel mapPanel, GameEventPanel eventPanel ) {
            super( BoxLayout.X_AXIS );
            this.setComponents( Arrays.asList( itemStatusPanel, mapPanel, eventPanel ) );
        }
        
    }

}
