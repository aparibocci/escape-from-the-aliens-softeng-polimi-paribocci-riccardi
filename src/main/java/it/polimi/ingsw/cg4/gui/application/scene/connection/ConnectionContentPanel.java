package it.polimi.ingsw.cg4.gui.application.scene.connection;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BasePanel;
import it.polimi.ingsw.cg4.gui.template.TextLabel;


class ConnectionContentPanel extends BasePanel {

    private static final long serialVersionUID = -9093135614963610322L;
    private final ConnectionInputPanel inputPanel;
    private final ConnectionCommandPanel commandPanel;
    private final ConnectionMethodPanel methodPanel;

    /**
     * The content panel related to the connection panel.
     * The component panels are set and shown.
     */
    public ConnectionContentPanel() {
        super();
        this.inputPanel = new ConnectionInputPanel();
        this.commandPanel = new ConnectionCommandPanel();
        this.methodPanel = new ConnectionMethodPanel();
        this.addComponents();
    }
    
    /**
     * 
     * @return the input form panel.
     */
    public ConnectionInputPanel getInputPanel() {
        return inputPanel;
    }
    
    /**
     * 
     * @return the command panel.
     */
    public ConnectionCommandPanel getCommandPanel() {
        return commandPanel;
    }
    
    /**
     * 
     * @return the connection method panel.
     */
    public ConnectionMethodPanel getMethodPanel() {
        return methodPanel;
    }
    
    private void addComponents() {
        this.add( new ConnectionInfoLabel(), BorderLayout.NORTH );
        BasePanel gridPanel = new BasePanel( new GridLayout( 2, 2 ) );
        gridPanel.add( new ConnectionLabelPanel() );
        gridPanel.add( inputPanel );
        gridPanel.add( new TextLabel( SceneConfig.CONNECTION_METHOD_TEXT ) );
        gridPanel.add( methodPanel );
        this.add( gridPanel, BorderLayout.CENTER );
        this.add( commandPanel, BorderLayout.SOUTH );
    }

}
