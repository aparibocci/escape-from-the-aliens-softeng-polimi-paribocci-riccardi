package it.polimi.ingsw.cg4.gui.template;

import java.awt.event.ActionListener;

/**
 * Class used to apply an ActionListener to a BasePanel.
 * The ActionListener interface is implemented.
 * 
 * @see BasePanel
 */
public abstract class BaseActionListener implements ActionListener {
    
    private final BasePanel panel;
    
    /**
     * 
     * @param panel the BasePanel that contains the element listened.
     */
    public BaseActionListener( BasePanel panel ) {
        this.panel = panel;
    }
    
    /**
     * 
     * @return the BasePanel that contains the element listened.
     */
    protected BasePanel getPanel() {
        return panel;
    }

}
