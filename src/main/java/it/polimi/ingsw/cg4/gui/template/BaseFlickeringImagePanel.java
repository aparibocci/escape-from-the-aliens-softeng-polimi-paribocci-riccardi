package it.polimi.ingsw.cg4.gui.template;

import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg4.gui.audio.AudioManager;

/**
 * Allows the creation of an ImagePanel with an image variable with time.
 * A sound effect may also be played when the images change.
 */
public abstract class BaseFlickeringImagePanel extends BaseImagePanel implements Runnable {

    private static final Logger LOG = Logger.getLogger( Logger.GLOBAL_LOGGER_NAME );
    private static final long serialVersionUID = 3724700726636996835L;
    private final transient Thread thread;
    private String fileName;
    private final String extension;
    private int secs;
    private boolean running;
    private final transient AudioManager manager;
    private final String seFileName;

    protected BaseFlickeringImagePanel( String baseFileName, String extension, int secs, AudioManager manager, String seFileName ) {
        super( baseFileName, extension );
        this.fileName = baseFileName;
        this.extension = extension;
        this.secs = secs;
        this.thread = new Thread( this );
        this.manager = manager;
        this.seFileName = seFileName;
        this.start();
        thread.start();
    }
    
    /**
     * Stops the flickering effect.
     */
    public void stop() {
        running = false;
        this.changeImage( fileName, extension );
    }
    
    /**
     * Starts the flickering effect.
     */
    public void start() {
        running = true;
    }

    @Override
    public void run() {
        while ( true ) {
            try {
                Thread.sleep( 5 );
                if ( running ) {
                    Thread.sleep( (long)secs*1000 - 5 );
                    if ( running ) {
                        this.changeImage( fileName + "_flickering", extension );
                        if ( seFileName != null && manager != null )
                            manager.playSE( seFileName );
                    }
                    Thread.sleep( 100 );
                    this.changeImage( fileName, extension );
                }
            } catch ( InterruptedException e ) {
                LOG.log( Level.SEVERE, e.getMessage(), e );
            }
        }
    }

}
