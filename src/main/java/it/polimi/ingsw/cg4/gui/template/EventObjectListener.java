package it.polimi.ingsw.cg4.gui.template;

import java.util.EventObject;

/**
 * Interface representing an EventObject listener.
 */
public interface EventObjectListener {

    /**
     * Signals the reception of a new event.
     * @param evt the event being received.
     */
    public void eventObjectReceived( EventObject evt );
    
}
