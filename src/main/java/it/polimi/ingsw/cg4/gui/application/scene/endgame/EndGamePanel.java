package it.polimi.ingsw.cg4.gui.application.scene.endgame;

import it.polimi.ingsw.cg4.gui.application.MainFrame;
import it.polimi.ingsw.cg4.gui.config.GraphicsConfig;
import it.polimi.ingsw.cg4.gui.template.ScenePanel;

/**
 * Class defining the end game panel.
 */
public class EndGamePanel extends ScenePanel {

    private static final long serialVersionUID = -894166461105496047L;
    
    private EndGameContentPanel contentPanel;

    /**
     * Creates the end game panel and adds the graphical components.
     * @param frame the frame which contains the connection panel.
     */
    public EndGamePanel( MainFrame frame ) {
        super( frame, GraphicsConfig.BACKGROUND_NAME + GraphicsConfig.BACKGROUND_EXTENSION );
        this.addComponents();
    }

    @Override
    protected void addComponents() {
        this.contentPanel = new EndGameContentPanel();
        this.add( contentPanel );
        this.setController( new EndGameController( this ) );
    }
    
    /**
     * 
     * @return the content panel related to this scene panel.
     */
    public EndGameContentPanel getContentPanel() {
        return contentPanel;
    }

}
