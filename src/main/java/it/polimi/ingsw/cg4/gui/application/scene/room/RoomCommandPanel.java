package it.polimi.ingsw.cg4.gui.application.scene.room;

import java.util.Arrays;

import javax.swing.BoxLayout;

import it.polimi.ingsw.cg4.gui.template.BaseCommandPanel;


class RoomCommandPanel extends BaseCommandPanel {

    private static final long serialVersionUID = 1883863673276866429L;

    private final RoomStartGameButton startGameButton;
    private final RoomLogoutButton logoutButton;
    
    /**
     * Creates the room command panel and sets the components.
     */
    public RoomCommandPanel() {
        super( BoxLayout.X_AXIS );
        this.startGameButton = new RoomStartGameButton();
        this.logoutButton = new RoomLogoutButton();
        this.setComponents( Arrays.asList( startGameButton, logoutButton ) );
    }
    
    /**
     * 
     * @return the start game button.
     */
    public RoomStartGameButton getStartGameButton() {
        return startGameButton;
    }

    /**
     * 
     * @return the logout button.
     */
    public RoomLogoutButton getLogoutButton() {
        return logoutButton;
    }

}
