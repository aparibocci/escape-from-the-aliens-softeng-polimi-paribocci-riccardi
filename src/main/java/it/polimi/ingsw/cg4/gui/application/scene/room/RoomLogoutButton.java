package it.polimi.ingsw.cg4.gui.application.scene.room;

import it.polimi.ingsw.cg4.gui.config.SceneConfig;
import it.polimi.ingsw.cg4.gui.template.BaseButton;


class RoomLogoutButton extends BaseButton {

    private static final long serialVersionUID = 6945960027727738199L;

    /**
     * Creates a new logout button for the room scene.
     * Some of the properties are specified in the configuration classes.
     */
    public RoomLogoutButton() {
        super( SceneConfig.ROOM_LOGOUT_TEXT );
        this.setToolTipText( "Ends the current session and disconnects from server" );
    }

}
