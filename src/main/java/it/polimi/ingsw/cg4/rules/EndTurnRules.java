package it.polimi.ingsw.cg4.rules;

import java.util.Arrays;
import java.util.List;

import it.polimi.ingsw.cg4.player.Player;
import it.polimi.ingsw.cg4.player.character.CharacterStatus;

/**
 * Class used to represent end turn rules.
 *
 */
public class EndTurnRules {
    
    public static final int MAX_TURN_SECONDS = 60;
    
    public static final List< CharacterStatus > ET_RESET_STATUSES = Arrays.asList(
            GameConfig.ADRENALINE_STATUS,
            GameConfig.ATTACK_STATUS,
            GameConfig.SEDATIVES_STATUS
    );
    
    private EndTurnRules() {
        
    }
    
    /**
     * 
     * @param player the current player.
     * @return true, if the player can end his turn, false otherwise.
     */
    public static boolean canEndTurn( Player player ) {
        return player.getMovedSectors() > 0;
    }

}
