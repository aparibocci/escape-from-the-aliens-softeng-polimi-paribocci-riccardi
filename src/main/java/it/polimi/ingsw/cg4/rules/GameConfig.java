package it.polimi.ingsw.cg4.rules;

import it.polimi.ingsw.cg4.board.SectorType;
import it.polimi.ingsw.cg4.card.Adrenaline;
import it.polimi.ingsw.cg4.card.Attack;
import it.polimi.ingsw.cg4.card.Defense;
import it.polimi.ingsw.cg4.card.GreenEscapeHatch;
import it.polimi.ingsw.cg4.card.Noise;
import it.polimi.ingsw.cg4.card.NoiseAny;
import it.polimi.ingsw.cg4.card.RedEscapeHatch;
import it.polimi.ingsw.cg4.card.Sedatives;
import it.polimi.ingsw.cg4.card.Silence;
import it.polimi.ingsw.cg4.card.Spotlight;
import it.polimi.ingsw.cg4.card.Teleport;
import it.polimi.ingsw.cg4.player.PlayerState;
import it.polimi.ingsw.cg4.player.character.CharacterRace;
import it.polimi.ingsw.cg4.player.character.CharacterStatus;

/**
 * Allows to configure the game's features and rules.
 *
 */
public class GameConfig {

    // Character races configuration
    public static final CharacterRace ALIEN_RACE = CharacterRace.ALIEN;
    public static final CharacterRace HUMAN_RACE = CharacterRace.HUMAN;

    // Player states configuration
    public static final PlayerState CHAR_ASSIGNED_STATE = PlayerState.CHAR_ASSIGNED;
    public static final PlayerState IN_GAME_STATE = PlayerState.IN_GAME;
    public static final PlayerState LOGGED_IN_STATE = PlayerState.LOGGED_IN;
    public static final PlayerState LOSER_STATE = PlayerState.LOSER;
    public static final PlayerState NOT_CONNECTED_STATE = PlayerState.NOT_CONNECTED;
    public static final PlayerState WINNER_STATE = PlayerState.WINNER;

    // Sector types configuration
    public static final SectorType ALIEN_SECTOR = SectorType.ALIEN;
    public static final SectorType CLOSED_HATCH_SECTOR = SectorType.ESCAPE_HATCH_CLOSED;
    public static final SectorType DANGEROUS_SECTOR = SectorType.DANGEROUS;
    public static final SectorType HUMAN_SECTOR = SectorType.HUMAN;
    public static final SectorType OPEN_HATCH_SECTOR = SectorType.ESCAPE_HATCH;
    public static final SectorType SECURE_SECTOR = SectorType.SECURE;
    public static final SectorType VOID_SECTOR = SectorType.VOID;

    // Character status configuration
    public static final CharacterStatus ADRENALINE_STATUS = CharacterStatus.ADRENALINE;
    public static final CharacterStatus ATTACK_STATUS = CharacterStatus.ATTACK;
    public static final CharacterStatus DEFENSE_STATUS = CharacterStatus.DEFENSE;
    public static final CharacterStatus SEDATIVES_STATUS = CharacterStatus.SEDATIVES;
    public static final CharacterStatus VORACIOUS_STATUS = CharacterStatus.VORACIOUS;

    // Escape hatch cards configuration
    public static final Class< GreenEscapeHatch > GREEN_HATCH_CARD_CLASS = GreenEscapeHatch.class;
    public static final Class< RedEscapeHatch > RED_HATCH_CARD_CLASS = RedEscapeHatch.class;

    // Sector cards configuration
    public static final Class< Noise > NOISE_CARD_CLASS = Noise.class;
    public static final Class< NoiseAny > NOISE_ANY_CARD_CLASS = NoiseAny.class;
    public static final Class< Silence > SILENCE_CARD_CLASS = Silence.class;

    // Item cards configuration
    public static final Class< Adrenaline > ADRENALINE_CARD_CLASS = Adrenaline.class;
    public static final Class< Attack > ATTACK_CARD_CLASS = Attack.class;
    public static final Class< Defense > DEFENSE_CARD_CLASS = Defense.class;
    public static final Class< Sedatives > SEDATIVES_CARD_CLASS = Sedatives.class;
    public static final Class< Spotlight > SPOTLIGHT_CARD_CLASS = Spotlight.class;
    public static final Class< Teleport > TELEPORT_CARD_CLASS = Teleport.class;

    // Misc values configuration
    public static final int MAX_HAND_SIZE = 3;
    public static final int MAX_TURNS = 39;
    public static final int MIN_GAME_PLAYERS = 2;
    public static final int MAX_GAME_PLAYERS = 8;

    // Decks content configuration
    public static final int GREEN_HATCH_N = 3;
    public static final int RED_HATCH_N = 3;
    public static final int NOISE_N = 6;
    public static final int NOISE_I_N = 4;
    public static final int NOISE_ANY_N = 6;
    public static final int NOISE_ANY_I_N = 4;
    public static final int SILENCE_N = 5;
    public static final int ADRENALINE_N = 2;
    public static final int ATTACK_N = 2;
    public static final int DEFENSE_N = 1;
    public static final int SEDATIVES_N = 3;
    public static final int SPOTLIGHT_N = 2;
    public static final int TELEPORT_N = 2;

    private GameConfig() {
        
    }
    
}
