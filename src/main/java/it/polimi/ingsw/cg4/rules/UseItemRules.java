package it.polimi.ingsw.cg4.rules;

import java.util.HashSet;
import java.util.Set;

import it.polimi.ingsw.cg4.card.ItemCard;
import it.polimi.ingsw.cg4.player.Player;
import it.polimi.ingsw.cg4.player.character.CharacterRace;

/**
 * Class used to represent use item rules.
 *
 */
public class UseItemRules {

    public static final CharacterRace USE_ITEM_RACE = GameConfig.HUMAN_RACE;
    public static final Set< Class< ? extends ItemCard > > PASSIVE_ITEMS;
    static {
        PASSIVE_ITEMS = new HashSet< Class< ? extends ItemCard > >();
        PASSIVE_ITEMS.add( GameConfig.DEFENSE_CARD_CLASS );
    }
    
    private UseItemRules() {
        
    }
    
    /**
     * 
     * @param player the player willing to use a specific item.
     * @param itemCard the item card.
     * @return true if the player can use item, false otherwise.
     */
    public static boolean canUseItem( Player player, ItemCard itemCard ) {
        return player.getCharacter().getRace().equals( USE_ITEM_RACE ) && ! isPassive( itemCard );
    }
    
    /**
     * 
     * @param itemCard the item card to check
     * @return true if the item card is passive, false otherwise.
     */
    private static boolean isPassive( ItemCard itemCard ) {
        return PASSIVE_ITEMS.contains( itemCard.getClass() );
    }
    
}
