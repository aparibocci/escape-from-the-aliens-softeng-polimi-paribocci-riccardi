package it.polimi.ingsw.cg4.rules;

import java.util.Arrays;
import java.util.List;

/**
 * Allows the configuration of game component creators.
 *
 */
public class CreatorConfig {
    
    // Board creator configuration
    public static final List< String > VALID_BOARD_NAMES = Arrays.asList(
            "Fermi",
            "Galilei",
            "Galvani"
    );

    // Item card creator configuration
    public static final List< String > VALID_ITEM_CARD_NAMES = Arrays.asList(
            "Adrenaline",
            "Attack",
            "Defense",
            "GreenEscapeHatch",
            "RedEscapeHatch",
            "Sedatives",
            "Silence",
            "Spotlight",
            "Teleport"
    );

    // Sector card creator configuration
    public static final List< String > VALID_SECTOR_CARD_NAMES = Arrays.asList(
            "Noise",
            "NoiseAny"
    );
    
    private CreatorConfig() {
        
    }
    
}
