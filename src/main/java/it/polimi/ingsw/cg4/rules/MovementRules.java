package it.polimi.ingsw.cg4.rules;

import it.polimi.ingsw.cg4.board.Sector;
import it.polimi.ingsw.cg4.board.SectorType;
import it.polimi.ingsw.cg4.player.Player;
import it.polimi.ingsw.cg4.player.character.CharacterRace;
import it.polimi.ingsw.cg4.player.character.CharacterStatus;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Class used to represent game movement rules.
 *
 */
public class MovementRules {
    
    // determines whether only one movement per turn is allowed
    public static final boolean UNIQUE_MOVEMENT_PER_TURN = true;

    public static final Map< CharacterRace, Set< SectorType > > RACE_MOVEMENT_RULES;
    static {
        RACE_MOVEMENT_RULES = new HashMap< CharacterRace, Set< SectorType > >();
        RACE_MOVEMENT_RULES.put( GameConfig.ALIEN_RACE, new HashSet< SectorType >( Arrays.asList(
                GameConfig.SECURE_SECTOR, GameConfig.DANGEROUS_SECTOR
        ) ) );
        RACE_MOVEMENT_RULES.put( GameConfig.HUMAN_RACE, new HashSet< SectorType >( Arrays.asList(
                GameConfig.SECURE_SECTOR, GameConfig.DANGEROUS_SECTOR, GameConfig.OPEN_HATCH_SECTOR
        ) ) );
    }
    
    public static final Map< CharacterRace, Integer > BASE_MOVEMENT_RANGE;
    static {
        BASE_MOVEMENT_RANGE = new HashMap< CharacterRace, Integer >();
        BASE_MOVEMENT_RANGE.put( GameConfig.ALIEN_RACE, 2 );
        BASE_MOVEMENT_RANGE.put( GameConfig.HUMAN_RACE, 1 );
    }
    
    public static final Map< CharacterStatus, Integer > MOVEMENT_BONUS_AMOUNT;
    static {
        MOVEMENT_BONUS_AMOUNT = new HashMap< CharacterStatus, Integer >();
        MOVEMENT_BONUS_AMOUNT.put( GameConfig.ADRENALINE_STATUS, 1 );
        MOVEMENT_BONUS_AMOUNT.put( GameConfig.VORACIOUS_STATUS, 1 );
    }
    
    private MovementRules() {
        
    }
    
    /**
     * 
     * @param player the current player.
     * @param sector the destination sector.
     * @return true, if player can move to destination, false otherwise.
     */
    public static boolean canMoveTo( Player player, Sector sector ) {
        return player.getReachableSectors().contains( sector );
    }
    
    /**
     * 
     * @param race the character race.
     * @param sectorType the type of given sector.
     * @return true if the character can access to given sector, false otherwise.
     */
    public static boolean canAccessTo( CharacterRace race, SectorType sectorType ) {
        return RACE_MOVEMENT_RULES.get( race ).contains( sectorType );
    }
    
    /**
     * Returns the movement range relative to the player's character race.
     * @param player the player.
     * @return the movement range relative to the player's character race.
     */
    public static int getMovementRange( Player player ) {
        int range = BASE_MOVEMENT_RANGE.get( player.getCharacter().getRace() );
        for ( CharacterStatus status : MOVEMENT_BONUS_AMOUNT.keySet() )
            if ( player.getCharacter().isAffectedBy( status ) )
                range += MOVEMENT_BONUS_AMOUNT.get( status );
        return range;
    }

    
}
