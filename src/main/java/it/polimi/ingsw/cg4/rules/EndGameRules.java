package it.polimi.ingsw.cg4.rules;

import java.util.List;

import it.polimi.ingsw.cg4.player.Player;

/**
 * Class used to represent end game rules.
 *
 */
public class EndGameRules {
    
    private EndGameRules() {
        
    }
    
    /**
     * 
     * @param players the list of players.
     * @return true, if the aliens won after attack, false otherwise.
     */
    public static boolean haveAliensWonAfterAttack( List< Player > players ) {
        return noHumanLeft( players );
    }
    
    /**
     * 
     * @param players the list of players.
     * @return true, if the aliens lost after human escape, false otherwise.
     */
    public static boolean haveAlienLostAfterEscape( List< Player > players ) {
        return noHumanLeft( players );
    }
    
    /**
     * 
     * @param players the list of players.
     * @return true if the humans won after attack, false otherwise.
     */
    public static boolean haveHumansWonAfterAttack( List< Player > players ) {
        return noAlienLeft( players );
    }
    
    /**
     * 
     * @param players the list of players.
     * @return true if no human is still playing, false otherwise.
     */
    private static boolean noHumanLeft( List< Player > players ){
        for ( Player p : players )
            if ( p.getCharacter().getRace().equals( GameConfig.HUMAN_RACE )
                    && p.getState().equals( GameConfig.IN_GAME_STATE ) )
                return false;
        return true;
    }
    
    /**
     * 
     * @param players the list of players.
     * @return true if no alien is still playing, false otherwise.
     */

    private static boolean noAlienLeft( List< Player > players ) {
        for ( Player p : players )
            if ( p.getCharacter().getRace().equals( GameConfig.ALIEN_RACE )
                    && p.getState().equals( GameConfig.IN_GAME_STATE ) )
                return false;
        return true;
    }
}
