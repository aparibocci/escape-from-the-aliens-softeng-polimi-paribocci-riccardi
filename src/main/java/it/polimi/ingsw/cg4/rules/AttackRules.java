package it.polimi.ingsw.cg4.rules;

import it.polimi.ingsw.cg4.player.Player;
import it.polimi.ingsw.cg4.player.character.CharacterRace;
import it.polimi.ingsw.cg4.player.character.CharacterStatus;

/**
 * Class used to represent the attack rules.
 *
 */
public class AttackRules {
    
    public static final CharacterRace ATTACK_RACE = GameConfig.ALIEN_RACE;
    public static final CharacterStatus ATTACK_STATUS = GameConfig.ATTACK_STATUS;
    public static final CharacterRace DEFENSE_RACE = GameConfig.HUMAN_RACE;
    public static final CharacterStatus DEFENSE_STATUS = GameConfig.DEFENSE_STATUS;
    public static final CharacterRace VORACIOUS_ATTACKER_RACE = GameConfig.ALIEN_RACE;
    public static final CharacterRace VORACIOUS_KILLED_RACE = GameConfig.HUMAN_RACE;
    public static final CharacterStatus VORACIOUS_STATUS = GameConfig.VORACIOUS_STATUS;
    
    private AttackRules() {
        
    }
    
    /**
     * 
     * @param player the player willing to attack.
     * @return true, if the player can attack, false otherwise.
     */
    public static boolean canAttack( Player player ) {
        return ! player.hasAttacked() && player.getMovedSectors() > 0 && ( player.getCharacter().getRace().equals( ATTACK_RACE ) ||
                player.getCharacter().isAffectedBy( ATTACK_STATUS ) );
    }
    
    /**
     * 
     * @param player the player willing to defend.
     * @return true, if the player can defend, false otherwise.
     */
    public static boolean canDefend( Player player ) {
        return player.getCharacter().getRace().equals( DEFENSE_RACE ) &&
                player.getCharacter().isAffectedBy( DEFENSE_STATUS );
    }
    
    /**
     * 
     * @param attacker the player willing to become voracius.
     * @param killed the player that has been killed.
     * @return true if the player can become voracius, false otherwise.
     */
    public static boolean canBecomeVoracious( Player attacker, Player killed  ) {
        return attacker.getCharacter().getRace().equals( VORACIOUS_ATTACKER_RACE ) &&
                killed.getCharacter().getRace().equals( VORACIOUS_KILLED_RACE );
    }

}
