package it.polimi.ingsw.cg4.rules;

/**
 * Defines score constants for player actions.
 *
 */
public class ScoreRules {
    
    // Alien-race specific scores
    public static final int ALIEN_TURN_SCORE = 1;
    public static final int ALIEN_KILL_SCORE = 30;
    public static final int ALIEN_KILL_ALIEN_SCORE = -20;
    public static final int ALIEN_FAIL_ATTACK_SCORE = -1;
    public static final int ALIEN_VICTORY_SCORE = 25;
    
    // Human race specific scores
    public static final int HUMAN_TURN_SCORE = 2;
    public static final int HUMAN_KILL_SCORE = 40;
    public static final int HUMAN_VICTORY_SCORE = 30;
    public static final int ESCAPE_HATCH_OPEN_SCORE = 5;
    public static final int SPOTLIGHT_SCORE = 2;
    public static final int USE_ITEM_SCORE = 1;
    
    // General scores
    public static final int DEFEAT_SCORE = -20;

    private ScoreRules() {
        
    }

}
