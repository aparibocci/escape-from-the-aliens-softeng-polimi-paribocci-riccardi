package it.polimi.ingsw.cg4.board;

import it.polimi.ingsw.cg4.player.Player;
import it.polimi.ingsw.cg4.player.character.CharacterRace;
import it.polimi.ingsw.cg4.rules.GameConfig;
import it.polimi.ingsw.cg4.rules.MovementRules;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Class representing a Board Sector: it has a Coordinate and a SectorType.
 * In the context of a Board, it can contain Players.
 * This class also provides methods for calculating distances and movement capabilities.
 * 
 * @see Coordinate
 * @see SectorType
 */
public class Sector {

    private final Coordinate coordinate;
    SectorType type;

    /**
     * Instantiates a new Sector.
     * 
     * @param coordinate the Sector coordinate.
     * @param type the Sector type.
     */
    public Sector( Coordinate coordinate, SectorType type ) {
        this.coordinate = coordinate;
        this.type = type;
    }
    
    /**
     * 
     * @return the Sector coordinate.
     */
    public Coordinate getCoordinate() {
        return coordinate;
    }
    
    /**
     * 
     * @return the Sector type.
     */
    public SectorType getType() {
        return type;
    }
    
    /**
     * if the Sector has an appropriate SectorType, it changes to the closed version.
     * (in the case of this game, open hatch sectors are closed).
     */
    public void close() {
        if ( this.type.equals( GameConfig.OPEN_HATCH_SECTOR ) )
            this.type = GameConfig.CLOSED_HATCH_SECTOR;
    }
    
    /**
     * 
     * @param board the Board to refer to.
     * @param player the Player to check.
     * @return true if the Sector contains the given Player in the given Board, false otherwise.
     */
    public boolean contains( Board board, Player player ) {
        for ( Player p : board.getPlayers() ) {
            if ( p.equals( player ) && p.getSector().equals( this ) )
                return true;
        }
        return false;
    }
    
    /**
     * 
     * @param board the Board to refer to.
     * @return true if the Sector does not contain any Player in the given Board, false otherwise.
     */
    public boolean isEmpty( Board board ) {
        for ( Player p : board.getPlayers() )
            if ( p.getSector().equals( this ) )
                return false;
        return true;
    }
    
    /**
     * 
     * @param s the source Sector.
     * @param t the destination Sector.
     * @return the distance between s and t (the number of unitary moves needed to reach t from s).
     */
    public static int distanceBetween( Sector s, Sector t ) {
        return ( Math.abs( s.coordinate.getX() - t.coordinate.getX() ) +
                Math.abs( s.coordinate.getY() - t.coordinate.getY() ) +
                Math.abs( s.coordinate.getZ() - t.coordinate.getZ() ) ) / 2;
    }
    
    /**
     * 
     * @param board the Board to refer to.
     * @param start the starting Sector.
     * @param race the CharacterRace of the moving Player.
     * @param movementRange the movement capability of the moving Player.
     * @return a Set containing the Board Sectors which are reachable from starting Sector with
     * given CharacterRace and movement range (does not contain the starting Sector).
     */
    public static Set< Sector > reachableSectors( Board board, Sector start, CharacterRace race, int movementRange ) {
        Set< Sector > visited = new HashSet< Sector >();
        visited.add( start );
        List< List< Sector > > fringes = new ArrayList< List< Sector > >();
        fringes.add( new ArrayList< Sector >() );
        fringes.get( 0 ).add( start );
        for ( int k = 1; k <= movementRange; k++ ) {
            fringes.add( new ArrayList< Sector >() );
            for ( Sector sector : fringes.get( k-1 ) ) {
                for ( String dir : Coordinate.DIR.keySet() ) {
                    Coordinate c = sector.getCoordinate().getNeighbor( dir );
                    if ( c.getCol() >= 0 && c.getCol() < board.getCols() &&
                            c.getRow() >= 0 && c.getRow() < board.getRows() ) {
                        Sector neighbor = board.getSector( c );
                        if ( ! visited.contains( neighbor ) && MovementRules.canAccessTo( race, neighbor.type ) ) {
                            visited.add( neighbor );
                            fringes.get( k ).add( neighbor );
                        }
                    }
                }
            }
        }
        visited.remove( start );
        return visited;
    }

    @Override
    public String toString() {
        return Character.toString( this.type.toChar() );
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ( ( coordinate == null ) ? 0 : coordinate.hashCode() );
        result = prime * result + ( ( type == null ) ? 0 : type.hashCode() );
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if ( this == obj )
            return true;
        if ( obj == null || getClass() != obj.getClass() )
            return false;
        Sector other = (Sector) obj;
        if ( coordinate == null ) {
            if ( other.coordinate != null )
                return false;
        } else if ( !coordinate.equals( other.coordinate ) )
            return false;
        if ( type != other.type )
            return false;
        return true;
    }

}
