package it.polimi.ingsw.cg4.board;

import it.polimi.ingsw.cg4.player.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Game board, made by Sectors structured in a matrix.
 * It contains Players, each positioned in a Sector.
 * The Sector matrix is accessed with a 0-based index policy.
 * Not to be instantiated through constructor: using the BoardCreator
 * class is recommended.
 * 
 * @see BoardCreator
 * @see Coordinate
 * @see Player
 * @see Sector
 */
public abstract class Board {

    private final int nRows;
    private final int nCols;
    private final String name;
    private List< Player > players;
    private Sector[][] sectors;
    private Sector humanBase;
    private Sector alienBase;

    protected Board( String name, int nRows, int nCols ) {
        this.name = name;
        this.players = new ArrayList< Player >();
        this.nRows = nRows;
        this.nCols = nCols;
        this.sectors = new Sector[nRows][nCols];
    }
    
    /**
     * 
     * @return the human base Sector (supposed unique in the Board context)
     */
    public Sector getHumanBase() {
        return humanBase;
    }
    
    /**
     * 
     * @return the alien base Sector (supposed unique in the Board context)
     */
    public Sector getAlienBase() {
        return alienBase;
    }
    
    /**
     * 
     * @param humanBase the human base Sector.
     */
    public void setHumanBase( Sector humanBase ) {
        this.humanBase = humanBase;
    }
    
    /**
     * 
     * @param alienBase the alien base Sector.
     */
    public void setAlienBase( Sector alienBase ) {
        this.alienBase = alienBase;
    }

    /**
     * 
     * @return the Board name.
     */
    public String getName() {
        return name;
    }
    
    /**
     * 
     * @return the number of rows of the Sector matrix.
     */
    public int getRows() {
        return this.nRows;
    }
    
    /**
     * 
     * @return the number of columns of the Sector matrix.
     */
    public int getCols() {
        return this.nCols;
    }
    
    /**
     * 
     * @return the Sector matrix.
     */
    public Sector[][] getSectors() {
        return this.sectors;
    }

    /**
     * 
     * @param coordinate the Coordinate relative to the Sector to get.
     * @return the Sector with given coordinates.
     */
    public Sector getSector( Coordinate coordinate ) {
        if ( coordinate.getRow() < 0 || coordinate.getRow() >= this.getRows() || coordinate.getCol() < 0 || coordinate.getCol() >= this.getCols() )
            throw new IllegalArgumentException( "Sector coordinate not valid. Please insert a valid coordinate." );
        return this.sectors[coordinate.getRow()][coordinate.getCol()];
    }
    
    /**
     * 
     * @return the list of Players (unmodifiable).
     */
    public List< Player > getPlayers() {
        return Collections.unmodifiableList( players );
    }
    
    /**
     * Adds a Player in the Board, positioning it in Sector with specified Coordinates.
     * 
     * @param player the player to add.
     * @param coordinate the coordinate of the player position.
     */
    public void addPlayer( Player player, Coordinate coordinate ) {
        this.players.add( player );
        Player p = this.players.get( this.players.size() - 1 );
        p.setBoard( this );
        p.setSector( this.getSector( coordinate ) );
    }
    
    /**
     * Given a coordinate, tells whether it is consistent with the Board dimensions.
     * 
     * @param c the coordinate to check.
     * @return true if the coordinate refers to an element of the Board, false otherwise.
     */
    public boolean containsCoordinates( Coordinate c ) {
        return c.getRow() >= 0 && c.getRow() < this.nRows && c.getCol() >= 0 && c.getCol() < this.nCols;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + nCols;
        result = prime * result + nRows;
        result = prime * result + ( ( name == null ) ? 0 : name.hashCode() );
        result = prime * result + Arrays.hashCode( sectors );
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if ( this == obj )
            return true;
        if ( obj == null )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        Board other = (Board) obj;
        if ( nCols != other.nCols || nRows != other.nRows )
            return false;
        if ( !name.equals( other.name ) )
            return false;
        if ( !Arrays.deepEquals( sectors, other.sectors ) )
            return false;
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append( this.getName() + "\n" );
        builder.append( "   " );
        for ( int i = 0; i < this.getCols(); i++ )
            builder.append( (char)( (int)'A' + i ) + " " );
        builder.append( "\n" );
        for ( int i = 0; i < this.getRows(); i++ ) {
            builder.append( String.format( "%02d", i + 1 ) + " " );
            for ( int j = 0; j < this.getCols(); j++ )
                builder.append( this.sectors[i][j].getType().toChar() + " " );
            builder.append( "\n" );
        }
        builder.append( "\n" );
        return builder.toString();
    }

}
