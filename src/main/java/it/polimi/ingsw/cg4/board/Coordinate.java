package it.polimi.ingsw.cg4.board;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class representing a Coordinate to be used in 2D maps.
 * The data is stored and managed using a cube coordinate policy (to make later calculations easier).
 * The instances, though, can also be done passing cartesian (row, column) informations.
 */
public class Coordinate {
    
    /**
     * Maps String directions with Coordinates representing the offset from the origin in given directions.
     * Direction strings are "DR" (down-right), "UR" (up-right), "U" (up), "UL" (up-left), "DL" (down-left), "D" (down).
     */
    public static final Map< String, Coordinate > DIR;
    static {
        Map< String, Coordinate > tmpMap = new HashMap< String, Coordinate >();
        tmpMap.put( "DR", Coordinate.fromCube( 1, -1, 0 ) );
        tmpMap.put( "UR", Coordinate.fromCube( 1, 0, -1 ) );
        tmpMap.put( "U", Coordinate.fromCube( 0, 1, -1 ) );
        tmpMap.put( "UL", Coordinate.fromCube( -1, 1, 0 ) );
        tmpMap.put( "DL", Coordinate.fromCube( -1, 0, 1 ) );
        tmpMap.put( "D", Coordinate.fromCube( 0, -1, 1 ) );
        DIR = Collections.unmodifiableMap( tmpMap );
    }
    
    private final int x;
    private final int y;
    private final int z;
    
    private Coordinate( int x, int y, int z ) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    /**
     * Creates a Coordinate instance using cubic notation.
     * 
     * @param x x value of the Coordinate.
     * @param y y value of the Coordinate.
     * @param z z value of the Coordinate.
     * @return the instance of the desired Coordinate.
     */
    public static Coordinate fromCube( int x, int y, int z ) {
        return new Coordinate( x, y, z );
    }
    
    /**
     * Creates a Coordinate instance using cartesian notation.
     * 
     * @param row row value of the Coordinate.
     * @param col col value of the Coordinate.
     * @return the instance of the desired Coordinate.
     */
    public static Coordinate fromCartesian( int row, int col ) {
        int x = col;
        int z = row - ( col-(col&1) ) / 2;
        int y = -x-z;
        return new Coordinate( x, y, z );
    }
    
    private Coordinate add( Coordinate c ) {
        return new Coordinate( this.x + c.x, this.y + c.y, this.z + c.z );
    }
    
    /**
     * @param direction direction of the desired neighbor (as String).
     * @return the coordinate in the given direction.
     */
    public Coordinate getNeighbor( String direction ) {
        return this.add( DIR.get( direction ) );
    }

    /**
     * @return a String representation of the Coordinate, using cubic notation.
     */
    public String toCubeString() {
        return "(" + x + ", " + y + ", " + z + ")";
    }
    
    /**
     * 
     * @return the row of the Coordinate.
     */
    public int getRow() {
        return z + (x - (x&1)) / 2;
    }
    
    /**
     * 
     * @return the column of the Coordinate.
     */
    public int getCol() {
        return x;
    }
    
    /**
     * 
     * @return the x value of the Coordinate.
     */
    public int getX() {
        return x;
    }
    
    /**
     * 
     * @return the y value of the Coordinate.
     */
    public int getY() {
        return y;
    }
    
    /**
     * 
     * @return the z value of the Coordinate.
     */
    public int getZ() {
        return z;
    }
    
    /**
     * 
     * @param args list of Strings representing the Coordinate to be parsed.
     * @return the Coordinate value represented by args.
     */
    public static Coordinate parseCoordinate( List< String > args ) {
        if ( args.size() < 2 || args.get( 0 ).length() != 1 )
            throw new IllegalArgumentException( "Invalid argument size when parsing Coordinate string." );
        int col = (int)Character.toUpperCase( args.get( 0 ).charAt( 0 ) ) - (int)'A';
        int row = Integer.parseInt( args.get( 1 ) ) - 1;
        return Coordinate.fromCartesian( row, col );
    }

    @Override
    public String toString() {
        return (char)( 'A'+this.getCol() ) + String.format( "%02d", this.getRow()+1 );
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + x;
        result = prime * result + y;
        result = prime * result + z;
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if ( this == obj )
            return true;
        if ( obj == null )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        Coordinate other = (Coordinate) obj;
        if ( x != other.x || y != other.y || z != other.z )
            return false;
        return true;
    }
}
