package it.polimi.ingsw.cg4.board;

import it.polimi.ingsw.cg4.rules.GameConfig;
import it.polimi.ingsw.cg4.rules.CreatorConfig;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Utility class used to create Board instances.
 * Valid Board names are specified inside the static field VALID_NAMES.
 *
 * @see Board
 */
public class BoardCreator {

    
    private static final Logger LOG = Logger.getLogger( Logger.GLOBAL_LOGGER_NAME );
    
    private static final String CLASS_PATH = "it.polimi.ingsw.cg4.board.";
    private static final String MAP_DATA_FOLDER = "maps";

    private static final List< String > VALID_NAMES = CreatorConfig.VALID_BOARD_NAMES;
    
    private static final String NOT_FOUND_ERROR = "Invalid board name";

    /**
     * Given a valid Board name, instantiates a Board having such name.
     * 
     * @param name the Board name.
     * @return an instance of the Board with given name.
     */
    public Board create( String name ) {
        Board board = null;
        if ( ! VALID_NAMES.contains( name ) )
            throw new IllegalArgumentException( NOT_FOUND_ERROR );
        try {
            board = (Board) Class.forName( CLASS_PATH + name ).getConstructor().newInstance();   
        } catch( Exception e ) {
            throw new RuntimeException( e );
        }
        initSectors( board, MAP_DATA_FOLDER + File.separatorChar + name.toLowerCase() + ".txt" );
        return board;
    }
    
    private void initSectors( Board board, String fileName ) {
        try {
            BufferedReader in = new BufferedReader( new FileReader( fileName ) );
            int row = 0;
            while ( in.ready() ) {
                List< String > elems = Arrays.asList( in.readLine().split( " " ) );
                for ( int col = 0; col < 23; col++ ) {
                    board.getSectors()[row][col] = new Sector( Coordinate.fromCartesian( row, col ),
                            SectorType.fromChar( elems.get( col ).charAt( 0 ) ) );
                    if ( board.getSectors()[row][col].getType().equals( GameConfig.ALIEN_SECTOR ) )
                        board.setAlienBase( board.getSectors()[row][col] );
                    else if ( board.getSectors()[row][col].getType().equals( GameConfig.HUMAN_SECTOR ) )
                            board.setHumanBase( board.getSectors()[row][col] );
                }
                row++;
            }
            in.close();
        } catch ( IOException e ) {
            LOG.log( Level.SEVERE, e.getMessage(), e );
            for ( int row = 0; row < 14; row++ )
                for ( int col = 0; col < 23; col++ )
                    board.getSectors()[row][col] = new Sector( Coordinate.fromCartesian( row, col ), GameConfig.SECURE_SECTOR );
        }
    }

}
