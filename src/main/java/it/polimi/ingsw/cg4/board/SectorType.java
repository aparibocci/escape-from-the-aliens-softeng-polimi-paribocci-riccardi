package it.polimi.ingsw.cg4.board;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents different types of Sector.
 * @see Sector
 */
public enum SectorType {
    DANGEROUS, SECURE, HUMAN, ALIEN, ESCAPE_HATCH, ESCAPE_HATCH_CLOSED, VOID;
    
    private static final Map< Character, SectorType > SYMBOLS;
    static {
        Map< Character, SectorType > tmpMap = new HashMap< Character, SectorType >();
        tmpMap.put( 'd', DANGEROUS );
        tmpMap.put( 's', SECURE );
        tmpMap.put( 'h', HUMAN );
        tmpMap.put( 'a', ALIEN );
        tmpMap.put( '1', ESCAPE_HATCH );
        tmpMap.put( '2', ESCAPE_HATCH );
        tmpMap.put( '3', ESCAPE_HATCH );
        tmpMap.put( '4', ESCAPE_HATCH );
        tmpMap.put( 'e', VOID );
        SYMBOLS = Collections.unmodifiableMap( tmpMap );
    }

    /**
     * 
     * @param symbol the character symbol relative to the desired SectorType.
     * @return the desired SectorType.
     */
    public static SectorType fromChar( char symbol ) {
        if ( ! SYMBOLS.containsKey( symbol ) )
            throw new IllegalArgumentException( "Invalid sector char" );
        return SYMBOLS.get( symbol );
    }
    
    /**
     * 
     * @return the character representation of the Sector.
     */
    public char toChar() {
        if ( this.equals( ESCAPE_HATCH ) || this.equals( ESCAPE_HATCH_CLOSED ) )
            return 'H';
        else if ( this.equals( VOID ) )
            return '-';
        for ( Character c : SYMBOLS.keySet() )
            if ( this.equals( SYMBOLS.get( c ) ) )
                return (char)c;
        return '-';
    }
}
