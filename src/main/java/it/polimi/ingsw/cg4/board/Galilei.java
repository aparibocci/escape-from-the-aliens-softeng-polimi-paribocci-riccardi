package it.polimi.ingsw.cg4.board;

class Galilei extends Board {

    /**
     * Creates an instance of the Galilei map.
     */
    public Galilei() {
        super( "Galilei", 14, 23 );
    }

}
