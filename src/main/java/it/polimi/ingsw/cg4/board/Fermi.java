package it.polimi.ingsw.cg4.board;

class Fermi extends Board {

    /**
     * Creates an instance of the Fermi map.
     */
    public Fermi() {
        super( "Fermi", 14, 23 );
    }

}
