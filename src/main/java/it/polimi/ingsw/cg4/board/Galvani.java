package it.polimi.ingsw.cg4.board;

class Galvani extends Board {

    /**
     * Creates an instance of the Galvani map.
     */
    public Galvani() {
        super( "Galvani", 14, 23 );
    }

}
