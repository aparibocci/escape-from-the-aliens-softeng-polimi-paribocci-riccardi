package it.polimi.ingsw.cg4.player.character;

/**
 * Class representing a captain (a specific human)
 *
 */
class Captain extends HumanCharacter {

    /**
     * Creates an instance of the class
     */
    public Captain() {
        super( "Captain" );
    }

}
