package it.polimi.ingsw.cg4.player.character;

/**
 * Class representing a pilot (a specific human)
 *
 */
class Pilot extends HumanCharacter {

    /**
     * Creates an instance of the class
     */
    public Pilot() {
        super( "Pilot" );
    }

}
