package it.polimi.ingsw.cg4.player.character;

/**
 * Class representing the first alien (a specific alien)
 *
 */
class FirstAlien extends AlienCharacter {

    /**
     * Creates an instance of the class
     */
    public FirstAlien() {
        super( "First Alien" );
    }

}
