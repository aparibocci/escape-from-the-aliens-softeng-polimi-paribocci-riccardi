package it.polimi.ingsw.cg4.player;
/**
 * Enumeration used to represent the player state in game context.
 *
 */
public enum PlayerState {
    NOT_CONNECTED, LOGGED_IN, CHAR_ASSIGNED, IN_GAME, WINNER, LOSER
}
