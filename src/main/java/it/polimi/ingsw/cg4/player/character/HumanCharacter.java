package it.polimi.ingsw.cg4.player.character;

import it.polimi.ingsw.cg4.rules.GameConfig;

class HumanCharacter extends Character {

    protected HumanCharacter( String name ) {
        super( name, GameConfig.HUMAN_RACE );
    }

}
