package it.polimi.ingsw.cg4.player.character;
/**
 * Enumeration used to represent character status.
 *
 */
public enum CharacterStatus {

    ATTACK, DEFENSE, ADRENALINE, SEDATIVES, VORACIOUS

}
