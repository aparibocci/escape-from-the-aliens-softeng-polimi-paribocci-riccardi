package it.polimi.ingsw.cg4.player.character;

/**
 * Class representing a soldier (a specific human)
 *
 */
class Soldier extends HumanCharacter {

    /**
     * Creates an instance of the class
     */
    public Soldier() {
        super( "Soldier" );
    }

}
