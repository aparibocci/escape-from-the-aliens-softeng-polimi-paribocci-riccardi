package it.polimi.ingsw.cg4.player.character;
/**
 * Enumeration use to represent the character race.
 *
 */
public enum CharacterRace {

    ALIEN, HUMAN

}
