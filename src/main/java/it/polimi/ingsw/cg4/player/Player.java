package it.polimi.ingsw.cg4.player;

import it.polimi.ingsw.cg4.board.Board;
import it.polimi.ingsw.cg4.board.Coordinate;
import it.polimi.ingsw.cg4.board.Sector;
import it.polimi.ingsw.cg4.card.ItemCard;
import it.polimi.ingsw.cg4.networking.server.User;
import it.polimi.ingsw.cg4.player.character.Character;
import it.polimi.ingsw.cg4.rules.GameConfig;
import it.polimi.ingsw.cg4.rules.MovementRules;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
/**
 * Class used to represent a game player.
 *
 */
public class Player {

    private final String name;
    private int score;
    private Board board;
    private Sector sector;
    private Character character;
    private final List< ItemCard > items;
    private final List< Coordinate > coordHistory;
    private PlayerState state;
    private Set< Sector > reachableSectors;
    private int movedSectors;
    private User user;
    private boolean hasAttacked;

    /**
     * Creates a new player.  
     * @param user the user being associated to the player.
     */
    public Player( User user ) {
        this.name = user.getName();
        this.user = user;
        this.setState( GameConfig.LOGGED_IN_STATE );
        this.score = 0;
        this.items = new ArrayList< ItemCard >();
        this.coordHistory = new ArrayList< Coordinate >();
        this.state = GameConfig.NOT_CONNECTED_STATE;
        this.resetMovedSectors();
        this.hasAttacked = false;
    }
    
    /**
     * Creates a new player.
     * @param name the player's name.
     */
    public Player( String name ) {
        this.name = name;
        this.user = null;
        this.setState( GameConfig.LOGGED_IN_STATE );
        this.score = 0;
        this.items = new ArrayList< ItemCard >();
        this.coordHistory = new ArrayList< Coordinate >();
        this.state = GameConfig.NOT_CONNECTED_STATE;
        this.resetMovedSectors();
        this.hasAttacked = false;
    }
    
    /**
     * 
     * @return the user.
     */
    public User getUser() {
        return user;
    }
    
    /**
     * Resets the number of moved sectors to zero.
     */
    public void resetMovedSectors() {
        movedSectors = 0;
    }
    
    /**
     * 
     * @return the number of moved sectors.
     */
    public int getMovedSectors() {
        return movedSectors;
    }
    
    /**
     * 
     * @return the list of the player's moved sectors.
     */
    public List< Coordinate > getCoordHistory() {
        return coordHistory;
    }
    
    /**
     * Adds a new coordinate to cordHistory.
     *
     * @param coordinate the sector coordinate.
     * @see coordHistory
     */
    public void addCoord( Coordinate coordinate ) {
        coordHistory.add( coordinate );
    }
    
    /**
     * Increases the number of moved sectors.
     * @param amount the number of moved sectors.
     */
    public void incMovedSectors( int amount ) {
        movedSectors += amount;
    }
    
    /**
     * 
     * @return the player's name.
     */
    public String getName() {
        return name;
    }
    
    /**
     * 
     * @return the player's score.
     */
    public int getScore() {
        return score;
    }
    
    /**
     * 
     * @return the game board.
     */
    public Board getBoard() {
        return board;
    }
    
    /**
     * 
     * @return the player sector.
     */
    public Sector getSector() {
        return sector;
    }
    
    /**
     * 
     * @return the player character.
     */
    public Character getCharacter() {
        return character;
    }
    
    /**
     * 
     * @param item item relative to the desired index.
     * @return the index of item.
     */
    public int getItemIndex( ItemCard item ) {
        return items.indexOf( item );
    }
    
    /**
     * Adds a new item to the player hand.
     * @param item the game item.
     */
    public void addItem( ItemCard item ) {
        if ( this.items.size() == GameConfig.MAX_HAND_SIZE )
            throw new IllegalStateException( "Chiamata addItem su mano piena" );
        this.items.add( item );
    }
    
    /**
     * 
     * @param index the index relative to the desidered item.
     * @return the desidered item.
     */
    public ItemCard getItem( int index ) {
        if ( index < 0 || index >= this.items.size() )
            throw new IllegalArgumentException( "Invalid item index. Please insert a valid index." );
        return items.get( index );
    }
    
    /**
     * Removes an item to the player hand.
     * @param index the index relative to the desidered item.
     * @return the removed item.
     */
    public ItemCard removeItem( int index ) {
        ItemCard removedItem = this.items.get( index );
        this.items.remove( index );
        return removedItem;
    }
    
    /**
     * 
     * @return the size of player item.
     */
    public int getHandSize() {
        return this.items.size();
    }
    
    /**
     * Assigns a specific board to the player.
     * @param board the game board.
     */
    public void setBoard( Board board ) {
        this.board = board;
    }
    
    /**
     * Assigns a specific sector to the player.
     * @param sector the board sector.
     */
    public void setSector( Sector sector ) {
        this.sector = sector;
    }
    
    /**
     * 
     * @return the player state.
     */
    public PlayerState getState() {
        return state;
    }
    
    /**
     * 
     * Assigns a specific character to the player.
     * @param character the game character.
     */
    public void setCharacter( Character character ) {
        this.character = character;
    }
    
    /**
     * Assigns a specific state to the player.
     * @param state the player state.
     */
    public void setState( PlayerState state ) {
        this.state = state;
    }
    
    /** 
     * Returns boolean value that specify if the player can move to the destination sector.
     * @param sector the player destination.
     * @return true if the player can move to destination, false otherwise.
     */
    public boolean canMoveTo( Sector sector ) {
        return this.reachableSectors.contains( sector );
    }
    
    /**
     * 
     * @return the player reachable sector.
     */
    public Set< Sector > getReachableSectors() {
        return Collections.unmodifiableSet( this.reachableSectors );
    }
    
    /**
     * Refreshes the player reachable sector using a breadth first search algorithm.
     */
    public void refreshReachableSectors() {
        this.reachableSectors = Sector.reachableSectors( this.board, this.sector, this.getCharacter().getRace(),
                MovementRules.getMovementRange( this ) - this.getMovedSectors() );
    }
    
    /**
     * Increases or decreases the player's score.
     * @param amount number of points to increase or decrease.
     */
    public void raiseScore( int amount ) {
        if ( amount < 0 )
            lowerScore( -amount );
        else
            this.score += amount;  
     }

    private void lowerScore( int amount ) {
        if( amount <= 0)
            throw new IllegalArgumentException( "Valore da decrementare non positivo ");
        this.score = Math.max( 0, this.score - amount );
    }
    
    /**
     * 
     * @param hasAttacked the value to set when the player attacks.
     */
    public void setHasAttacked( boolean hasAttacked ) {
        this.hasAttacked = hasAttacked;
    }

    /**
     * 
     * @return true if the player has attacked in his turn, false otherwise.
     */
    public boolean hasAttacked() {
        return this.hasAttacked;
    }

}
