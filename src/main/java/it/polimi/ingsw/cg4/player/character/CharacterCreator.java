package it.polimi.ingsw.cg4.player.character;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
/**
 * Class used to create a new character in game context.
 *
 */
public class CharacterCreator {

    private static final String CLASS_PATH = "it.polimi.ingsw.cg4.player.character.";

    private static final List< String > VALID_NAMES = Arrays.asList(
            "Captain",
            "Pilot",
            "Psychologist",
            "Soldier",
            "FirstAlien",
            "SecondAlien",
            "ThirdAlien",
            "FourthAlien"
    );
    
    private static final String NOT_FOUND_ERROR = "Invalid character name";

    /**
     * Given a name creates a new instance of the character relative to the name.
     * @param name the name of the character being created.
     * @return the specific character having the given name.
     */
    public Character create( String name ) {
        try {
            if ( VALID_NAMES.contains( name ) )
                return (Character) Class.forName( CLASS_PATH + name ).getConstructor().newInstance();
            throw new IllegalArgumentException( NOT_FOUND_ERROR );
        } catch ( Exception e ) {
            throw new RuntimeException( e );
        }
    }
    /**
     * 
     * @return a list containing all the game's aliens.
     */
    public List< Character > getAliensList() {
        List< Character > aliens = new ArrayList< Character >();
        aliens.add( this.create( "FirstAlien" ) );
        aliens.add( this.create( "SecondAlien" ) );
        aliens.add( this.create( "ThirdAlien" ) );
        aliens.add( this.create( "FourthAlien" ) );
        return aliens;
    }
    /**
     * 
     * @return a list containing all the game's aliens.
     */
    public List< Character > getHumansList() {
        List< Character > humans = new ArrayList< Character >();
        humans.add( this.create( "Captain" ) );
        humans.add( this.create( "Pilot" ) );
        humans.add( this.create( "Psychologist" ) );
        humans.add( this.create( "Soldier" ) );
        return humans;
    }

}
