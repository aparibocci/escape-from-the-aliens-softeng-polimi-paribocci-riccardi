package it.polimi.ingsw.cg4.player.character;

/**
 * Class representing the fourth alien (a specific alien)
 *
 */
class FourthAlien extends AlienCharacter {

    /**
     * Creates an instance of the class
     */
    public FourthAlien() {
        super( "Fourth Alien" );
    }

}
