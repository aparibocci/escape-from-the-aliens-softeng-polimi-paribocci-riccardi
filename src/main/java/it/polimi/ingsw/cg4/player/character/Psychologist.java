package it.polimi.ingsw.cg4.player.character;

/**
 * Class representing a psychologist (a specific human)
 *
 */
class Psychologist extends HumanCharacter {

    /**
     * Creates an instance of the class
     */
    public Psychologist() {
        super( "Psychologist" );
    }

}
