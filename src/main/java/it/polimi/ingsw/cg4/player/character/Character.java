package it.polimi.ingsw.cg4.player.character;

import java.util.HashSet;
import java.util.Set;

/**
 * Class used to represent a character in game context.
 *
 */
public abstract class Character {

    private final String name;
    private final CharacterRace race;
    private final Set< CharacterStatus > statuses;

    protected Character( String name, CharacterRace race ) {
        this.name = name;
        this.race = race;
        this.statuses = new HashSet< CharacterStatus >();
    }
    
    /**
     * 
     * @return the character name.
     */
    public String getName() {
        return name;
    }
    
    /**
     * 
     * @return the character race.
     */
    public CharacterRace getRace() {
        return race;
    }
    
    /**
     * 
     * @param status the status to check.
     * @return true, if the player is affected by at least a status, false otherwise. 
     */
    public boolean isAffectedBy( CharacterStatus status ) {
        return statuses.contains( status );
    }
    
    /**
     * Activates a specific status.
     * @param status the status to activate.
     */
    public void activateStatus( CharacterStatus status ) {
        statuses.add( status );
    }
    
    /**
     * Deactivate a specific status.
     * @param status the status to activate.
     */
    public void deactivateStatus( CharacterStatus status ) {
        statuses.remove( status );
    }

    @Override
    public String toString() {
        return this.name + "[" + this.race.toString() + "]";
    }

}
