package it.polimi.ingsw.cg4.player.character;

/**
 * Class representing the second alien (a specific alien)
 *
 */
class SecondAlien extends AlienCharacter {

    /**
     * Creates an instance of the class
     */
    public SecondAlien() {
        super( "Second Alien" );
    }

}
