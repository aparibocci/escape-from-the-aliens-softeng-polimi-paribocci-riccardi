package it.polimi.ingsw.cg4.player.character;

import it.polimi.ingsw.cg4.rules.GameConfig;

class AlienCharacter extends Character {

    protected AlienCharacter( String name ) {
        super( name, GameConfig.ALIEN_RACE );
    }

}
