package it.polimi.ingsw.cg4.player.character;

/**
 * Class representing the third alien (a specific alien)
 *
 */
class ThirdAlien extends AlienCharacter {

    /**
     * Creates an instance of the class
     */
    public ThirdAlien() {
        super( "Third Alien" );
    }

}
