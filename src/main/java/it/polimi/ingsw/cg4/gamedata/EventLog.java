package it.polimi.ingsw.cg4.gamedata;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represent a list of game events.
 *
 */
public class EventLog {
   
    private final List< EventLogEntry > entries;
    
    /**
     * Create a new list of events.
     */
    public EventLog() {
        this.entries = new ArrayList< EventLogEntry >();
    }
    
    /**
     * Create a new entry and adds it to the event list.
     * @param text the text of the entry being added.
     */
    public void add( String text ) {
        this.entries.add( new EventLogEntry( text ) );
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for ( EventLogEntry entry : entries )
            builder.append( entry.toString() + "\n" );
        return builder.toString();
    }

}
