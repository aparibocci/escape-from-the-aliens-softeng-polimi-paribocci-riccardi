package it.polimi.ingsw.cg4.gamedata;

import it.polimi.ingsw.cg4.board.Board;
import it.polimi.ingsw.cg4.board.Coordinate;
import it.polimi.ingsw.cg4.card.deck.CardDeck;
import it.polimi.ingsw.cg4.networking.common.ContextConfig;
import it.polimi.ingsw.cg4.networking.server.User;
import it.polimi.ingsw.cg4.player.Player;
import it.polimi.ingsw.cg4.player.character.CharacterStatus;
import it.polimi.ingsw.cg4.rules.EndTurnRules;
import it.polimi.ingsw.cg4.rules.GameConfig;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;

/**
 * Class used to represent a current game.
 *
 */
public class Game {

    private final GameState state;
    private int turn;
    private int curPlayerIndex;
    private final EventLog eventLog;
    private final EventLog chat;
    private final List< String > notifications;
    private Timer timer;
    private EndTurnTimerTask turnTimerTask;
    private final int lastPlayerIndex;
    
    /**
     * Creates a new game ready to start.
     * 
     * @param board the board of the game.
     * @param players the player list.
     * @param decks the list of card decks.
     */
    public Game( Board board, List< Player > players, List< CardDeck > decks ) {
        this.state = new GameState( board, players, decks );
        this.turn = 0;
        this.curPlayerIndex = new Random().nextInt( players.size() );
        this.lastPlayerIndex = ( curPlayerIndex - 1 ) % players.size();
        this.eventLog = new EventLog();
        this.chat = new EventLog();
        this.notifications = new ArrayList< String >();
        this.timer = new Timer();
    }
    
    /**
     * 
     * @return the current turn number.
     */
    public int getTurn() {
        return turn;
    }
    
    /**
     * 
     * @return  the game state.
     */
    public GameState getState() {
        return state;
    }
    
    /**
     * 
     * @return return the event log.
     */
    public EventLog getEventLog() {
        return eventLog;
    }
    
    /**
     * 
     * @return the game chat.
     */
    public EventLog getChat() {
        return chat;
    }
    
    /**
     * Increases the turn number.
     */
    public void nextTurn() {
        turn++;
        this.addToEventLog( "Turn #" + turn + " has started." );
    }
    
    /**
     * Adds a new entry  to the event log.
     * @param text the string being added.
     */
    public void addToEventLog( String text ) {
        this.eventLog.add( text );
        this.getCurrentPlayer().getUser().getRoom().sendToEveryone( ContextConfig.EVENT_CONTEXT, text );
    }
    
    /**
     * 
     * @return the player who is playing the turn.
     */
    public Player getCurrentPlayer() {
        return state.getPlayers().get( curPlayerIndex );
    }
    
    /**
     * Starts the next player's turn.
     */
    public void nextPlayer() {
        turnTimerTask.stop();
        this.getCurrentPlayer().getUser().getRoom().sendToUser( this.getCurrentPlayer().getUser(), ContextConfig.TIMER_RESET_CONTEXT, "Timer stop" );
        this.addToEventLog( this.getCurrentPlayer().getName() + " ends his turn." );
        do {
            if ( curPlayerIndex == lastPlayerIndex )
                this.nextTurn();
            if ( curPlayerIndex >= state.getPlayers().size() - 1 )
                curPlayerIndex = 0;
            else
                curPlayerIndex++;
        } while ( this.getCurrentPlayer().getState() != GameConfig.IN_GAME_STATE );
        this.addToEventLog( this.getCurrentPlayer().getName() + "'s turn starts." );
        
        this.clearNotifications();
        startTurnTimer(); 
        this.getCurrentPlayer().getUser().getRoom().sendToUser( this.getCurrentPlayer().getUser(), ContextConfig.MSGBOX_CONTEXT, "Your turn has started." );
        this.getCurrentPlayer().getUser().getRoom().sendToUser( this.getCurrentPlayer().getUser(), ContextConfig.CLI_CONTEXT, this.toString() );
    }
    
    /**
     * Starts the EndTurnTimerTask for the current player.
     */
    public void startTurnTimer() {
        this.turnTimerTask = new EndTurnTimerTask( this.getCurrentPlayer().getUser(), EndTurnRules.MAX_TURN_SECONDS );
        timer.schedule( turnTimerTask, 0 );
    }
    
    /**
     * Clears all notification.
     */
    public void clearNotifications() {
        this.notifications.clear();
    }
    
    /**
     * 
     * @return true if the game is over, false otherwise.
     */
    public boolean hasEnded() {
        if ( this.getTurn() > GameConfig.MAX_TURNS ) {
            for ( Player p : this.getState().getPlayers() )
                if ( p.getState().equals( GameConfig.IN_GAME_STATE ) ) {
                    if ( p.getCharacter().getRace().equals( GameConfig.ALIEN_RACE ) )
                        p.setState( GameConfig.WINNER_STATE );
                    else
                        p.setState( GameConfig.LOSER_STATE );
                }
            return true;
        }
        for ( Player p : this.getState().getPlayers() )
            if ( p.getState().equals( GameConfig.IN_GAME_STATE ) )
                return false;
        return true;
    }
    
    /**
     * Notifies the end of the game.
     */
    public void endGameReport() {
        turnTimerTask.stop();
        this.addToEventLog( "The game has ended!" );
        for ( Player p : this.getState().getPlayers() ) {
            this.addToEventLog( p.getName() + " (" + p.getScore() + " points): " + p.getState() );
            p.getUser().getRoom().sendToEveryone( ContextConfig.USER_REPORT_CONTEXT, p.getState().toString() + " " + p.getName() + " " + p.getCharacter().getRace() + " " + p.getScore() );
        }
        User u = this.getState().getPlayers().get( 0 ).getUser();
        u.getRoom().sendToEveryone( ContextConfig.END_GAME_CONTEXT, "End game" );
        for ( Player p : this.getState().getPlayers() ) {
            if ( p.getUser().hasDisconnected() && p.getUser().getRoom().contains( p.getUser() ) )
                p.getUser().getRoom().removeUser( p.getUser() );
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append( "================================================\n" );
        for ( String s : notifications )
            builder.append( s + "\n" );
        builder.append( this.getState().getBoard() + "\n" );
        builder.append( coordHistory() );
        builder.append( "\n" );
        builder.append( this.playerCurrentCoord() + "\n" );
        builder.append( "\n" );
        for ( CharacterStatus s : CharacterStatus.values() ) {
            if ( this.getCurrentPlayer().getCharacter().isAffectedBy( s ) ) {
                builder.append( s.toString() + " " );
            }
        }
        builder.append( "\n" );
        for ( int i = 0; i < this.getCurrentPlayer().getHandSize(); i++ )
            builder.append( i + " " + this.getCurrentPlayer().getItem( i ).getName() + "\n" );
        return builder.toString();
    }
    
    /**
     * 
     * @return the coordinate of current player.
     */
    public String playerCurrentCoord() {
        return this.getCurrentPlayer().getName() + " [" + this.getCurrentPlayer().getCharacter().getRace() + "]: " + this.getCurrentPlayer().getSector().getCoordinate();
    }
    
    private String coordHistory() {
        StringBuilder builder = new StringBuilder();
        for ( Coordinate c : this.getCurrentPlayer().getCoordHistory() )
            builder.append( c + " > " );
        return builder.toString();
    }
  

}
