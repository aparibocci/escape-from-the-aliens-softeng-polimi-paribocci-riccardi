package it.polimi.ingsw.cg4.gamedata;

import it.polimi.ingsw.cg4.board.Board;
import it.polimi.ingsw.cg4.board.BoardCreator;
import it.polimi.ingsw.cg4.card.deck.CardDeck;
import it.polimi.ingsw.cg4.card.deck.CardDeckCreator;
import it.polimi.ingsw.cg4.player.Player;
import it.polimi.ingsw.cg4.player.character.Character;
import it.polimi.ingsw.cg4.player.character.CharacterCreator;
import it.polimi.ingsw.cg4.rules.GameConfig;

import java.util.Collections;
import java.util.List;

/**
 *Class used to initialize some of the game components.
 *
 */
public class GameUtilities {
    
    private GameUtilities() {
        
    }

    /**
     * Create a new game board.
     * @param boardName the name of a specific board. 
     * @return the game board.
     */
    public static Board createBoard( String boardName ) {
        return new BoardCreator().create( boardName );
        
    }

    /**
     * Assigns the players character race using a random algorithm.
     * @param players the list of game player.
     */
    public static void setPlayersRaces( List< Player > players ) {
        CharacterCreator charCreator = new CharacterCreator();
        List< Character > aliens = charCreator.getAliensList();
        List< Character > humans = charCreator.getHumansList();
        Collections.shuffle( players );
        for ( int i = 0; i < players.size(); i++ ) {
            if ( i < players.size() / 2 ) {
                Collections.shuffle( humans );
                players.get( i ).setCharacter( humans.remove( 0 ) );
            } else {
                Collections.shuffle( aliens );
                players.get( i ).setCharacter( aliens.remove( 0 ) );
            }
            players.get( i ).setState( GameConfig.CHAR_ASSIGNED_STATE );
        }
        Collections.shuffle( players );
    }

    /**
     * Creates the decks needed for the game.
     * @return the game deck list.
     */
    public static List< CardDeck > createDecks() {
        CardDeckCreator deckCreator = new CardDeckCreator();
        return deckCreator.getDeckList();
    }

    /**
     * Assigns the players to their base. 
     * @param board the game board.
     * @param players the game player list.
     */
    public static void setPlayersPosition( Board board, List< Player > players ) {
        for ( Player p : players ) {
            if ( p.getCharacter().getRace().equals( GameConfig.ALIEN_RACE ) ) {
                board.addPlayer( p, board.getAlienBase().getCoordinate() );
            } else {
                board.addPlayer( p, board.getHumanBase().getCoordinate() );
            }
            p.refreshReachableSectors();
            p.setState( GameConfig.IN_GAME_STATE );
        }
    }

}
