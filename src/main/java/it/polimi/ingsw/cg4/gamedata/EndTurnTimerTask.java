package it.polimi.ingsw.cg4.gamedata;

import it.polimi.ingsw.cg4.networking.common.ContextConfig;
import it.polimi.ingsw.cg4.networking.server.GameActionEndTurn;
import it.polimi.ingsw.cg4.networking.server.User;

import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Represents a timer used to control a user's turn.
 * Every second, it notifies every user in the room about the remaining seconds.
 * When the timer reaches zero, the user is forced to end his turn.
 *
 */
class EndTurnTimerTask extends TimerTask {

    private static final Logger LOG = Logger.getLogger( Logger.GLOBAL_LOGGER_NAME );
    private final User user;
    private int seconds;
    private boolean running;

    /**
     * 
     * @param user the user starting his turn.
     * @param seconds the number of seconds before the timeout.
     */
    public EndTurnTimerTask( User user, int seconds ) {
        this.user = user;
        this.seconds = seconds;
        this.running = true;
    }

    @Override
    public void run() {
        while ( seconds > 0 ) {
            if ( ! running )
                return;
            user.getRoom().sendToEveryone( ContextConfig.TIMER_CONTEXT, user.getName() + " " + Integer.toString( seconds ) );
            seconds--;
            try {
                Thread.sleep( 1000 );
            } catch ( InterruptedException e ) {
                LOG.log( Level.SEVERE, e.getMessage(), e );
                return;
            }
        }
        if ( running ) {
            user.getRoom().getGame().addToEventLog( user.getName() + " skips his turn due to inactivity." );
            user.getPlayer().incMovedSectors( 1 );
            new GameActionEndTurn( user ).execute();
        }
    }

    public void stop() {
        this.running = false;
    }

}
