package it.polimi.ingsw.cg4.gamedata;

import java.util.Calendar;

/**
 * Represent an entry to be added to an event list.
 *
 */
class EventLogEntry {

    private final Calendar time;
    private final String text;
    
    /**
     * Create a new entry associated to the current time.
     * @param text the text of the entry.
     */
    public EventLogEntry( String text ) {
        this.time = Calendar.getInstance();
        this.text = text;
    }
    
    @Override
    public String toString() {
        return String.format( "%02d", time.get( Calendar.HOUR ) ) + ":" +
                String.format( "%02d", time.get( Calendar.MINUTE ) ) + ":" +
                String.format( "%02d", time.get( Calendar.SECOND ) ) + " - " +
                text;
    }

}
