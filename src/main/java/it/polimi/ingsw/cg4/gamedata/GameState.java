package it.polimi.ingsw.cg4.gamedata;

import java.util.List;

import it.polimi.ingsw.cg4.board.Board;
import it.polimi.ingsw.cg4.card.deck.CardDeck;
import it.polimi.ingsw.cg4.card.deck.EscapeHatchCardDeck;
import it.polimi.ingsw.cg4.card.deck.ItemCardDeck;
import it.polimi.ingsw.cg4.card.deck.SectorCardDeck;
import it.polimi.ingsw.cg4.player.Player;
import it.polimi.ingsw.cg4.rules.GameConfig;

/**
 * Provides a representation of the Game state, allowing access to the Board,
 * the Players and the Decks.
 */
public class GameState {
    
    private final Board board;
    private final List< Player > players;
    private final List< CardDeck > decks;

    /**
     * Create an instance of the GameState class, throwing exceptions when the parameters
     * do not follow the Game rules.
     * 
     * @param board the Game board.
     * @param players the list of playing Players.
     * @param decks the Deck list.
     */
    public GameState( Board board, List< Player > players, List< CardDeck > decks ) {
        this.board = board;
        if ( players.size() < GameConfig.MIN_GAME_PLAYERS || players.size() > GameConfig.MAX_GAME_PLAYERS )
            throw new IllegalArgumentException( "Invalid players list in GameState constructor" );
        this.players = players;
        if ( decks.size() != 6 )
            throw new IllegalArgumentException( "Invalid decks list in GameState constructor" );
        this.decks = decks;
    }
    
    /**
     * 
     * @return the Game board.
     */
    public Board getBoard() {
        return board;
    }
    
    /**
     * 
     * @return the list of Players in the Game.
     */
    public List< Player > getPlayers() {
        return players;
    }
    
    /**
     * 
     * @return the SectorCard Deck.
     */
    public SectorCardDeck getSectorDeck() {
        return ( SectorCardDeck )decks.get( 0 );
    }
    
    /**
     * 
     * @return the SectorCard discard Deck.
     */
    public SectorCardDeck getSectorDiscard() {
        return ( SectorCardDeck )decks.get( 1 );
    }
    
    /**
     * 
     * @return the EscapeHatchCard Deck.
     */
    public EscapeHatchCardDeck getEscapeHatchDeck() {
        return ( EscapeHatchCardDeck )decks.get( 2 );
    }

    /**
     * 
     * @return the EscapeHatchCard discard Deck.
     */
    public EscapeHatchCardDeck getEscapeHatchDiscard() {
        return ( EscapeHatchCardDeck )decks.get( 3 );
    }

    /**
     * 
     * @return the ItemCard Deck.
     */
    public ItemCardDeck getItemDeck() {
        return ( ItemCardDeck )decks.get( 4 );
    }

    /**
     * 
     * @return the ItemCard discard Deck.
     */
    public ItemCardDeck getItemDiscard() {
        return ( ItemCardDeck )decks.get( 5 );
    }

}
