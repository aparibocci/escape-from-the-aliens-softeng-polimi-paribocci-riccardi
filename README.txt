#==============================================================================
# ** Escape from Aliens in Outer Space
#==============================================================================
# * Team cg_4 (Amos Paribocci, Vincenzo Riccardi)
# * Software Engineering (Prova Finale) 2014/15
#==============================================================================
# * How to start the game:
#------------------------------------------------------------------------------
# 1. Start the server by running the main method of the class
#    it.polimi.ingsw.cg4.networking.server.Server.java
# 2. Start any number of clients by running the main methods of
#    it.polimi.ingsw.cg4.gui.application.ClientGUI.java	  [GUI mode]
#    or
#    it.polimi.ingsw.cg4.networking.client.ClientCLI.java [CLI mode]
# 3. Enjoy the game!
#------------------------------------------------------------------------------
# * Notes:
#------------------------------------------------------------------------------
# a. From a client perspective, room access may be executed in three ways:
#    -creating a specific room
#      CLI: create <ROOM_NAME> | GUI: Create room
#    -joining a specific room
#      CLI: join <ROOM_NAME>   | GUI: Join room
#    -joining any free room which is preparing for a game
#      CLI: autojoin           | GUI: Find room
# b. Server's messages and errors will be put in a log and not
#    displayed by default. This behavior may be changed in the ServerConfig
#    class by changing the HIDE_LOG value;
# c. When playing in GUI mode, it is possible to enable or disable music
#    and sound effects by clicking on the buttons located at the bottom
#    of the screen.
# d. Since MIDI format has been chosen for background music (due to very
#    light size and no copyright issues), sound will be reproduced
#    differently, depending on the specific machine (OSX synthetizers work
#    usually differently than Windows ones).
#    If the background music feels particularly annoying, it is suggested
#    to disable it (see c.).
# e. Many GUI elements will show a help tooltip text when mouse focus
#    is given. This may give hints on advanced features (i.e. adding
#    race markers when right-clicking the map panel).
# f. The GUI has been tested using a resolution of 1366x768 and higher.
#    Support for higher (and lower) resolutions is provided by scaling some
#    of the images.
#==============================================================================
